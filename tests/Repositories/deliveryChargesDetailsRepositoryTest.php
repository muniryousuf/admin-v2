<?php namespace Tests\Repositories;

use App\Models\deliveryChargesDetails;
use App\Repositories\deliveryChargesDetailsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class deliveryChargesDetailsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var deliveryChargesDetailsRepository
     */
    protected $deliveryChargesDetailsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->deliveryChargesDetailsRepo = \App::make(deliveryChargesDetailsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_delivery_charges_details()
    {
        $deliveryChargesDetails = deliveryChargesDetails::factory()->make()->toArray();

        $createddeliveryChargesDetails = $this->deliveryChargesDetailsRepo->create($deliveryChargesDetails);

        $createddeliveryChargesDetails = $createddeliveryChargesDetails->toArray();
        $this->assertArrayHasKey('id', $createddeliveryChargesDetails);
        $this->assertNotNull($createddeliveryChargesDetails['id'], 'Created deliveryChargesDetails must have id specified');
        $this->assertNotNull(deliveryChargesDetails::find($createddeliveryChargesDetails['id']), 'deliveryChargesDetails with given id must be in DB');
        $this->assertModelData($deliveryChargesDetails, $createddeliveryChargesDetails);
    }

    /**
     * @test read
     */
    public function test_read_delivery_charges_details()
    {
        $deliveryChargesDetails = deliveryChargesDetails::factory()->create();

        $dbdeliveryChargesDetails = $this->deliveryChargesDetailsRepo->find($deliveryChargesDetails->id);

        $dbdeliveryChargesDetails = $dbdeliveryChargesDetails->toArray();
        $this->assertModelData($deliveryChargesDetails->toArray(), $dbdeliveryChargesDetails);
    }

    /**
     * @test update
     */
    public function test_update_delivery_charges_details()
    {
        $deliveryChargesDetails = deliveryChargesDetails::factory()->create();
        $fakedeliveryChargesDetails = deliveryChargesDetails::factory()->make()->toArray();

        $updateddeliveryChargesDetails = $this->deliveryChargesDetailsRepo->update($fakedeliveryChargesDetails, $deliveryChargesDetails->id);

        $this->assertModelData($fakedeliveryChargesDetails, $updateddeliveryChargesDetails->toArray());
        $dbdeliveryChargesDetails = $this->deliveryChargesDetailsRepo->find($deliveryChargesDetails->id);
        $this->assertModelData($fakedeliveryChargesDetails, $dbdeliveryChargesDetails->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_delivery_charges_details()
    {
        $deliveryChargesDetails = deliveryChargesDetails::factory()->create();

        $resp = $this->deliveryChargesDetailsRepo->delete($deliveryChargesDetails->id);

        $this->assertTrue($resp);
        $this->assertNull(deliveryChargesDetails::find($deliveryChargesDetails->id), 'deliveryChargesDetails should not exist in DB');
    }
}
