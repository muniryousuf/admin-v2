<?php namespace Tests\Repositories;

use App\Models\CaosGenralSettings;
use App\Repositories\CaosGenralSettingsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CaosGenralSettingsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CaosGenralSettingsRepository
     */
    protected $caosGenralSettingsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->caosGenralSettingsRepo = \App::make(CaosGenralSettingsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_caos_genral_settings()
    {
        $caosGenralSettings = CaosGenralSettings::factory()->make()->toArray();

        $createdCaosGenralSettings = $this->caosGenralSettingsRepo->create($caosGenralSettings);

        $createdCaosGenralSettings = $createdCaosGenralSettings->toArray();
        $this->assertArrayHasKey('id', $createdCaosGenralSettings);
        $this->assertNotNull($createdCaosGenralSettings['id'], 'Created CaosGenralSettings must have id specified');
        $this->assertNotNull(CaosGenralSettings::find($createdCaosGenralSettings['id']), 'CaosGenralSettings with given id must be in DB');
        $this->assertModelData($caosGenralSettings, $createdCaosGenralSettings);
    }

    /**
     * @test read
     */
    public function test_read_caos_genral_settings()
    {
        $caosGenralSettings = CaosGenralSettings::factory()->create();

        $dbCaosGenralSettings = $this->caosGenralSettingsRepo->find($caosGenralSettings->id);

        $dbCaosGenralSettings = $dbCaosGenralSettings->toArray();
        $this->assertModelData($caosGenralSettings->toArray(), $dbCaosGenralSettings);
    }

    /**
     * @test update
     */
    public function test_update_caos_genral_settings()
    {
        $caosGenralSettings = CaosGenralSettings::factory()->create();
        $fakeCaosGenralSettings = CaosGenralSettings::factory()->make()->toArray();

        $updatedCaosGenralSettings = $this->caosGenralSettingsRepo->update($fakeCaosGenralSettings, $caosGenralSettings->id);

        $this->assertModelData($fakeCaosGenralSettings, $updatedCaosGenralSettings->toArray());
        $dbCaosGenralSettings = $this->caosGenralSettingsRepo->find($caosGenralSettings->id);
        $this->assertModelData($fakeCaosGenralSettings, $dbCaosGenralSettings->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_caos_genral_settings()
    {
        $caosGenralSettings = CaosGenralSettings::factory()->create();

        $resp = $this->caosGenralSettingsRepo->delete($caosGenralSettings->id);

        $this->assertTrue($resp);
        $this->assertNull(CaosGenralSettings::find($caosGenralSettings->id), 'CaosGenralSettings should not exist in DB');
    }
}
