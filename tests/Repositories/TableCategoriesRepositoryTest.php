<?php namespace Tests\Repositories;

use App\Models\TableCategories;
use App\Repositories\TableCategoriesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TableCategoriesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TableCategoriesRepository
     */
    protected $tableCategoriesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->tableCategoriesRepo = \App::make(TableCategoriesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_table_categories()
    {
        $tableCategories = TableCategories::factory()->make()->toArray();

        $createdTableCategories = $this->tableCategoriesRepo->create($tableCategories);

        $createdTableCategories = $createdTableCategories->toArray();
        $this->assertArrayHasKey('id', $createdTableCategories);
        $this->assertNotNull($createdTableCategories['id'], 'Created TableCategories must have id specified');
        $this->assertNotNull(TableCategories::find($createdTableCategories['id']), 'TableCategories with given id must be in DB');
        $this->assertModelData($tableCategories, $createdTableCategories);
    }

    /**
     * @test read
     */
    public function test_read_table_categories()
    {
        $tableCategories = TableCategories::factory()->create();

        $dbTableCategories = $this->tableCategoriesRepo->find($tableCategories->id);

        $dbTableCategories = $dbTableCategories->toArray();
        $this->assertModelData($tableCategories->toArray(), $dbTableCategories);
    }

    /**
     * @test update
     */
    public function test_update_table_categories()
    {
        $tableCategories = TableCategories::factory()->create();
        $fakeTableCategories = TableCategories::factory()->make()->toArray();

        $updatedTableCategories = $this->tableCategoriesRepo->update($fakeTableCategories, $tableCategories->id);

        $this->assertModelData($fakeTableCategories, $updatedTableCategories->toArray());
        $dbTableCategories = $this->tableCategoriesRepo->find($tableCategories->id);
        $this->assertModelData($fakeTableCategories, $dbTableCategories->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_table_categories()
    {
        $tableCategories = TableCategories::factory()->create();

        $resp = $this->tableCategoriesRepo->delete($tableCategories->id);

        $this->assertTrue($resp);
        $this->assertNull(TableCategories::find($tableCategories->id), 'TableCategories should not exist in DB');
    }
}
