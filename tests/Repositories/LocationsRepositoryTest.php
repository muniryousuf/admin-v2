<?php namespace Tests\Repositories;

use App\Models\Locations;
use App\Repositories\LocationsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class LocationsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var LocationsRepository
     */
    protected $locationsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->locationsRepo = \App::make(LocationsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_locations()
    {
        $locations = Locations::factory()->make()->toArray();

        $createdLocations = $this->locationsRepo->create($locations);

        $createdLocations = $createdLocations->toArray();
        $this->assertArrayHasKey('id', $createdLocations);
        $this->assertNotNull($createdLocations['id'], 'Created Locations must have id specified');
        $this->assertNotNull(Locations::find($createdLocations['id']), 'Locations with given id must be in DB');
        $this->assertModelData($locations, $createdLocations);
    }

    /**
     * @test read
     */
    public function test_read_locations()
    {
        $locations = Locations::factory()->create();

        $dbLocations = $this->locationsRepo->find($locations->id);

        $dbLocations = $dbLocations->toArray();
        $this->assertModelData($locations->toArray(), $dbLocations);
    }

    /**
     * @test update
     */
    public function test_update_locations()
    {
        $locations = Locations::factory()->create();
        $fakeLocations = Locations::factory()->make()->toArray();

        $updatedLocations = $this->locationsRepo->update($fakeLocations, $locations->id);

        $this->assertModelData($fakeLocations, $updatedLocations->toArray());
        $dbLocations = $this->locationsRepo->find($locations->id);
        $this->assertModelData($fakeLocations, $dbLocations->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_locations()
    {
        $locations = Locations::factory()->create();

        $resp = $this->locationsRepo->delete($locations->id);

        $this->assertTrue($resp);
        $this->assertNull(Locations::find($locations->id), 'Locations should not exist in DB');
    }
}
