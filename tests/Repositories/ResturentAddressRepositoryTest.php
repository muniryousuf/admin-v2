<?php namespace Tests\Repositories;

use App\Models\ResturentAddress;
use App\Repositories\ResturentAddressRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ResturentAddressRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ResturentAddressRepository
     */
    protected $resturentAddressRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->resturentAddressRepo = \App::make(ResturentAddressRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_resturent_address()
    {
        $resturentAddress = ResturentAddress::factory()->make()->toArray();

        $createdResturentAddress = $this->resturentAddressRepo->create($resturentAddress);

        $createdResturentAddress = $createdResturentAddress->toArray();
        $this->assertArrayHasKey('id', $createdResturentAddress);
        $this->assertNotNull($createdResturentAddress['id'], 'Created ResturentAddress must have id specified');
        $this->assertNotNull(ResturentAddress::find($createdResturentAddress['id']), 'ResturentAddress with given id must be in DB');
        $this->assertModelData($resturentAddress, $createdResturentAddress);
    }

    /**
     * @test read
     */
    public function test_read_resturent_address()
    {
        $resturentAddress = ResturentAddress::factory()->create();

        $dbResturentAddress = $this->resturentAddressRepo->find($resturentAddress->id);

        $dbResturentAddress = $dbResturentAddress->toArray();
        $this->assertModelData($resturentAddress->toArray(), $dbResturentAddress);
    }

    /**
     * @test update
     */
    public function test_update_resturent_address()
    {
        $resturentAddress = ResturentAddress::factory()->create();
        $fakeResturentAddress = ResturentAddress::factory()->make()->toArray();

        $updatedResturentAddress = $this->resturentAddressRepo->update($fakeResturentAddress, $resturentAddress->id);

        $this->assertModelData($fakeResturentAddress, $updatedResturentAddress->toArray());
        $dbResturentAddress = $this->resturentAddressRepo->find($resturentAddress->id);
        $this->assertModelData($fakeResturentAddress, $dbResturentAddress->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_resturent_address()
    {
        $resturentAddress = ResturentAddress::factory()->create();

        $resp = $this->resturentAddressRepo->delete($resturentAddress->id);

        $this->assertTrue($resp);
        $this->assertNull(ResturentAddress::find($resturentAddress->id), 'ResturentAddress should not exist in DB');
    }
}
