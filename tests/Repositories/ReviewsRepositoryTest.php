<?php namespace Tests\Repositories;

use App\Models\Reviews;
use App\Repositories\ReviewsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ReviewsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ReviewsRepository
     */
    protected $reviewsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->reviewsRepo = \App::make(ReviewsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_reviews()
    {
        $reviews = Reviews::factory()->make()->toArray();

        $createdReviews = $this->reviewsRepo->create($reviews);

        $createdReviews = $createdReviews->toArray();
        $this->assertArrayHasKey('id', $createdReviews);
        $this->assertNotNull($createdReviews['id'], 'Created Reviews must have id specified');
        $this->assertNotNull(Reviews::find($createdReviews['id']), 'Reviews with given id must be in DB');
        $this->assertModelData($reviews, $createdReviews);
    }

    /**
     * @test read
     */
    public function test_read_reviews()
    {
        $reviews = Reviews::factory()->create();

        $dbReviews = $this->reviewsRepo->find($reviews->id);

        $dbReviews = $dbReviews->toArray();
        $this->assertModelData($reviews->toArray(), $dbReviews);
    }

    /**
     * @test update
     */
    public function test_update_reviews()
    {
        $reviews = Reviews::factory()->create();
        $fakeReviews = Reviews::factory()->make()->toArray();

        $updatedReviews = $this->reviewsRepo->update($fakeReviews, $reviews->id);

        $this->assertModelData($fakeReviews, $updatedReviews->toArray());
        $dbReviews = $this->reviewsRepo->find($reviews->id);
        $this->assertModelData($fakeReviews, $dbReviews->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_reviews()
    {
        $reviews = Reviews::factory()->create();

        $resp = $this->reviewsRepo->delete($reviews->id);

        $this->assertTrue($resp);
        $this->assertNull(Reviews::find($reviews->id), 'Reviews should not exist in DB');
    }
}
