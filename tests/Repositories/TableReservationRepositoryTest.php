<?php namespace Tests\Repositories;

use App\Models\TableReservation;
use App\Repositories\TableReservationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TableReservationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TableReservationRepository
     */
    protected $tableReservationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->tableReservationRepo = \App::make(TableReservationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_table_reservation()
    {
        $tableReservation = TableReservation::factory()->make()->toArray();

        $createdTableReservation = $this->tableReservationRepo->create($tableReservation);

        $createdTableReservation = $createdTableReservation->toArray();
        $this->assertArrayHasKey('id', $createdTableReservation);
        $this->assertNotNull($createdTableReservation['id'], 'Created TableReservation must have id specified');
        $this->assertNotNull(TableReservation::find($createdTableReservation['id']), 'TableReservation with given id must be in DB');
        $this->assertModelData($tableReservation, $createdTableReservation);
    }

    /**
     * @test read
     */
    public function test_read_table_reservation()
    {
        $tableReservation = TableReservation::factory()->create();

        $dbTableReservation = $this->tableReservationRepo->find($tableReservation->id);

        $dbTableReservation = $dbTableReservation->toArray();
        $this->assertModelData($tableReservation->toArray(), $dbTableReservation);
    }

    /**
     * @test update
     */
    public function test_update_table_reservation()
    {
        $tableReservation = TableReservation::factory()->create();
        $fakeTableReservation = TableReservation::factory()->make()->toArray();

        $updatedTableReservation = $this->tableReservationRepo->update($fakeTableReservation, $tableReservation->id);

        $this->assertModelData($fakeTableReservation, $updatedTableReservation->toArray());
        $dbTableReservation = $this->tableReservationRepo->find($tableReservation->id);
        $this->assertModelData($fakeTableReservation, $dbTableReservation->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_table_reservation()
    {
        $tableReservation = TableReservation::factory()->create();

        $resp = $this->tableReservationRepo->delete($tableReservation->id);

        $this->assertTrue($resp);
        $this->assertNull(TableReservation::find($tableReservation->id), 'TableReservation should not exist in DB');
    }
}
