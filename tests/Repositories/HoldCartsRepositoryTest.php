<?php namespace Tests\Repositories;

use App\Models\HoldCarts;
use App\Repositories\HoldCartsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class HoldCartsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var HoldCartsRepository
     */
    protected $holdCartsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->holdCartsRepo = \App::make(HoldCartsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_hold_carts()
    {
        $holdCarts = HoldCarts::factory()->make()->toArray();

        $createdHoldCarts = $this->holdCartsRepo->create($holdCarts);

        $createdHoldCarts = $createdHoldCarts->toArray();
        $this->assertArrayHasKey('id', $createdHoldCarts);
        $this->assertNotNull($createdHoldCarts['id'], 'Created HoldCarts must have id specified');
        $this->assertNotNull(HoldCarts::find($createdHoldCarts['id']), 'HoldCarts with given id must be in DB');
        $this->assertModelData($holdCarts, $createdHoldCarts);
    }

    /**
     * @test read
     */
    public function test_read_hold_carts()
    {
        $holdCarts = HoldCarts::factory()->create();

        $dbHoldCarts = $this->holdCartsRepo->find($holdCarts->id);

        $dbHoldCarts = $dbHoldCarts->toArray();
        $this->assertModelData($holdCarts->toArray(), $dbHoldCarts);
    }

    /**
     * @test update
     */
    public function test_update_hold_carts()
    {
        $holdCarts = HoldCarts::factory()->create();
        $fakeHoldCarts = HoldCarts::factory()->make()->toArray();

        $updatedHoldCarts = $this->holdCartsRepo->update($fakeHoldCarts, $holdCarts->id);

        $this->assertModelData($fakeHoldCarts, $updatedHoldCarts->toArray());
        $dbHoldCarts = $this->holdCartsRepo->find($holdCarts->id);
        $this->assertModelData($fakeHoldCarts, $dbHoldCarts->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_hold_carts()
    {
        $holdCarts = HoldCarts::factory()->create();

        $resp = $this->holdCartsRepo->delete($holdCarts->id);

        $this->assertTrue($resp);
        $this->assertNull(HoldCarts::find($holdCarts->id), 'HoldCarts should not exist in DB');
    }
}
