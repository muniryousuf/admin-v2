<?php namespace Tests\Repositories;

use App\Models\CustomerReservations;
use App\Repositories\CustomerReservationsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CustomerReservationsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CustomerReservationsRepository
     */
    protected $customerReservationsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->customerReservationsRepo = \App::make(CustomerReservationsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_customer_reservations()
    {
        $customerReservations = CustomerReservations::factory()->make()->toArray();

        $createdCustomerReservations = $this->customerReservationsRepo->create($customerReservations);

        $createdCustomerReservations = $createdCustomerReservations->toArray();
        $this->assertArrayHasKey('id', $createdCustomerReservations);
        $this->assertNotNull($createdCustomerReservations['id'], 'Created CustomerReservations must have id specified');
        $this->assertNotNull(CustomerReservations::find($createdCustomerReservations['id']), 'CustomerReservations with given id must be in DB');
        $this->assertModelData($customerReservations, $createdCustomerReservations);
    }

    /**
     * @test read
     */
    public function test_read_customer_reservations()
    {
        $customerReservations = CustomerReservations::factory()->create();

        $dbCustomerReservations = $this->customerReservationsRepo->find($customerReservations->id);

        $dbCustomerReservations = $dbCustomerReservations->toArray();
        $this->assertModelData($customerReservations->toArray(), $dbCustomerReservations);
    }

    /**
     * @test update
     */
    public function test_update_customer_reservations()
    {
        $customerReservations = CustomerReservations::factory()->create();
        $fakeCustomerReservations = CustomerReservations::factory()->make()->toArray();

        $updatedCustomerReservations = $this->customerReservationsRepo->update($fakeCustomerReservations, $customerReservations->id);

        $this->assertModelData($fakeCustomerReservations, $updatedCustomerReservations->toArray());
        $dbCustomerReservations = $this->customerReservationsRepo->find($customerReservations->id);
        $this->assertModelData($fakeCustomerReservations, $dbCustomerReservations->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_customer_reservations()
    {
        $customerReservations = CustomerReservations::factory()->create();

        $resp = $this->customerReservationsRepo->delete($customerReservations->id);

        $this->assertTrue($resp);
        $this->assertNull(CustomerReservations::find($customerReservations->id), 'CustomerReservations should not exist in DB');
    }
}
