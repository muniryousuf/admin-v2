<?php namespace Tests\Repositories;

use App\Models\MessageTemplating;
use App\Repositories\MessageTemplatingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MessageTemplatingRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MessageTemplatingRepository
     */
    protected $messageTemplatingRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->messageTemplatingRepo = \App::make(MessageTemplatingRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_message_templating()
    {
        $messageTemplating = MessageTemplating::factory()->make()->toArray();

        $createdMessageTemplating = $this->messageTemplatingRepo->create($messageTemplating);

        $createdMessageTemplating = $createdMessageTemplating->toArray();
        $this->assertArrayHasKey('id', $createdMessageTemplating);
        $this->assertNotNull($createdMessageTemplating['id'], 'Created MessageTemplating must have id specified');
        $this->assertNotNull(MessageTemplating::find($createdMessageTemplating['id']), 'MessageTemplating with given id must be in DB');
        $this->assertModelData($messageTemplating, $createdMessageTemplating);
    }

    /**
     * @test read
     */
    public function test_read_message_templating()
    {
        $messageTemplating = MessageTemplating::factory()->create();

        $dbMessageTemplating = $this->messageTemplatingRepo->find($messageTemplating->id);

        $dbMessageTemplating = $dbMessageTemplating->toArray();
        $this->assertModelData($messageTemplating->toArray(), $dbMessageTemplating);
    }

    /**
     * @test update
     */
    public function test_update_message_templating()
    {
        $messageTemplating = MessageTemplating::factory()->create();
        $fakeMessageTemplating = MessageTemplating::factory()->make()->toArray();

        $updatedMessageTemplating = $this->messageTemplatingRepo->update($fakeMessageTemplating, $messageTemplating->id);

        $this->assertModelData($fakeMessageTemplating, $updatedMessageTemplating->toArray());
        $dbMessageTemplating = $this->messageTemplatingRepo->find($messageTemplating->id);
        $this->assertModelData($fakeMessageTemplating, $dbMessageTemplating->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_message_templating()
    {
        $messageTemplating = MessageTemplating::factory()->create();

        $resp = $this->messageTemplatingRepo->delete($messageTemplating->id);

        $this->assertTrue($resp);
        $this->assertNull(MessageTemplating::find($messageTemplating->id), 'MessageTemplating should not exist in DB');
    }
}
