<?php namespace Tests\Repositories;

use App\Models\RestaurantTimingsSpecific;
use App\Repositories\RestaurantTimingsSpecificRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RestaurantTimingsSpecificRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RestaurantTimingsSpecificRepository
     */
    protected $restaurantTimingsSpecificRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->restaurantTimingsSpecificRepo = \App::make(RestaurantTimingsSpecificRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_restaurant_timings_specific()
    {
        $restaurantTimingsSpecific = RestaurantTimingsSpecific::factory()->make()->toArray();

        $createdRestaurantTimingsSpecific = $this->restaurantTimingsSpecificRepo->create($restaurantTimingsSpecific);

        $createdRestaurantTimingsSpecific = $createdRestaurantTimingsSpecific->toArray();
        $this->assertArrayHasKey('id', $createdRestaurantTimingsSpecific);
        $this->assertNotNull($createdRestaurantTimingsSpecific['id'], 'Created RestaurantTimingsSpecific must have id specified');
        $this->assertNotNull(RestaurantTimingsSpecific::find($createdRestaurantTimingsSpecific['id']), 'RestaurantTimingsSpecific with given id must be in DB');
        $this->assertModelData($restaurantTimingsSpecific, $createdRestaurantTimingsSpecific);
    }

    /**
     * @test read
     */
    public function test_read_restaurant_timings_specific()
    {
        $restaurantTimingsSpecific = RestaurantTimingsSpecific::factory()->create();

        $dbRestaurantTimingsSpecific = $this->restaurantTimingsSpecificRepo->find($restaurantTimingsSpecific->id);

        $dbRestaurantTimingsSpecific = $dbRestaurantTimingsSpecific->toArray();
        $this->assertModelData($restaurantTimingsSpecific->toArray(), $dbRestaurantTimingsSpecific);
    }

    /**
     * @test update
     */
    public function test_update_restaurant_timings_specific()
    {
        $restaurantTimingsSpecific = RestaurantTimingsSpecific::factory()->create();
        $fakeRestaurantTimingsSpecific = RestaurantTimingsSpecific::factory()->make()->toArray();

        $updatedRestaurantTimingsSpecific = $this->restaurantTimingsSpecificRepo->update($fakeRestaurantTimingsSpecific, $restaurantTimingsSpecific->id);

        $this->assertModelData($fakeRestaurantTimingsSpecific, $updatedRestaurantTimingsSpecific->toArray());
        $dbRestaurantTimingsSpecific = $this->restaurantTimingsSpecificRepo->find($restaurantTimingsSpecific->id);
        $this->assertModelData($fakeRestaurantTimingsSpecific, $dbRestaurantTimingsSpecific->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_restaurant_timings_specific()
    {
        $restaurantTimingsSpecific = RestaurantTimingsSpecific::factory()->create();

        $resp = $this->restaurantTimingsSpecificRepo->delete($restaurantTimingsSpecific->id);

        $this->assertTrue($resp);
        $this->assertNull(RestaurantTimingsSpecific::find($restaurantTimingsSpecific->id), 'RestaurantTimingsSpecific should not exist in DB');
    }
}
