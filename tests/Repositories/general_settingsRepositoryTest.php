<?php namespace Tests\Repositories;

use App\Models\general_settings;
use App\Repositories\generalSettingsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class generalSettingsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var generalSettingsRepository
     */
    protected $generalSettingsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->generalSettingsRepo = \App::make(generalSettingsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_general_settings()
    {
        $generalSettings = general_settings::factory()->make()->toArray();

        $createdgeneral_settings = $this->generalSettingsRepo->create($generalSettings);

        $createdgeneral_settings = $createdgeneral_settings->toArray();
        $this->assertArrayHasKey('id', $createdgeneral_settings);
        $this->assertNotNull($createdgeneral_settings['id'], 'Created general_settings must have id specified');
        $this->assertNotNull(general_settings::find($createdgeneral_settings['id']), 'general_settings with given id must be in DB');
        $this->assertModelData($generalSettings, $createdgeneral_settings);
    }

    /**
     * @test read
     */
    public function test_read_general_settings()
    {
        $generalSettings = general_settings::factory()->create();

        $dbgeneral_settings = $this->generalSettingsRepo->find($generalSettings->id);

        $dbgeneral_settings = $dbgeneral_settings->toArray();
        $this->assertModelData($generalSettings->toArray(), $dbgeneral_settings);
    }

    /**
     * @test update
     */
    public function test_update_general_settings()
    {
        $generalSettings = general_settings::factory()->create();
        $fakegeneral_settings = general_settings::factory()->make()->toArray();

        $updatedgeneral_settings = $this->generalSettingsRepo->update($fakegeneral_settings, $generalSettings->id);

        $this->assertModelData($fakegeneral_settings, $updatedgeneral_settings->toArray());
        $dbgeneral_settings = $this->generalSettingsRepo->find($generalSettings->id);
        $this->assertModelData($fakegeneral_settings, $dbgeneral_settings->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_general_settings()
    {
        $generalSettings = general_settings::factory()->create();

        $resp = $this->generalSettingsRepo->delete($generalSettings->id);

        $this->assertTrue($resp);
        $this->assertNull(general_settings::find($generalSettings->id), 'general_settings should not exist in DB');
    }
}
