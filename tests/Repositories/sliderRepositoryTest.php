<?php namespace Tests\Repositories;

use App\Models\slider;
use App\Repositories\sliderRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class sliderRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var sliderRepository
     */
    protected $sliderRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->sliderRepo = \App::make(sliderRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_slider()
    {
        $slider = slider::factory()->make()->toArray();

        $createdslider = $this->sliderRepo->create($slider);

        $createdslider = $createdslider->toArray();
        $this->assertArrayHasKey('id', $createdslider);
        $this->assertNotNull($createdslider['id'], 'Created slider must have id specified');
        $this->assertNotNull(slider::find($createdslider['id']), 'slider with given id must be in DB');
        $this->assertModelData($slider, $createdslider);
    }

    /**
     * @test read
     */
    public function test_read_slider()
    {
        $slider = slider::factory()->create();

        $dbslider = $this->sliderRepo->find($slider->id);

        $dbslider = $dbslider->toArray();
        $this->assertModelData($slider->toArray(), $dbslider);
    }

    /**
     * @test update
     */
    public function test_update_slider()
    {
        $slider = slider::factory()->create();
        $fakeslider = slider::factory()->make()->toArray();

        $updatedslider = $this->sliderRepo->update($fakeslider, $slider->id);

        $this->assertModelData($fakeslider, $updatedslider->toArray());
        $dbslider = $this->sliderRepo->find($slider->id);
        $this->assertModelData($fakeslider, $dbslider->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_slider()
    {
        $slider = slider::factory()->create();

        $resp = $this->sliderRepo->delete($slider->id);

        $this->assertTrue($resp);
        $this->assertNull(slider::find($slider->id), 'slider should not exist in DB');
    }
}
