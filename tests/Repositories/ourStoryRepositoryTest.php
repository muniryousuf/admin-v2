<?php namespace Tests\Repositories;

use App\Models\ourStory;
use App\Repositories\ourStoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ourStoryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ourStoryRepository
     */
    protected $ourStoryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->ourStoryRepo = \App::make(ourStoryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_our_story()
    {
        $ourStory = ourStory::factory()->make()->toArray();

        $createdourStory = $this->ourStoryRepo->create($ourStory);

        $createdourStory = $createdourStory->toArray();
        $this->assertArrayHasKey('id', $createdourStory);
        $this->assertNotNull($createdourStory['id'], 'Created ourStory must have id specified');
        $this->assertNotNull(ourStory::find($createdourStory['id']), 'ourStory with given id must be in DB');
        $this->assertModelData($ourStory, $createdourStory);
    }

    /**
     * @test read
     */
    public function test_read_our_story()
    {
        $ourStory = ourStory::factory()->create();

        $dbourStory = $this->ourStoryRepo->find($ourStory->id);

        $dbourStory = $dbourStory->toArray();
        $this->assertModelData($ourStory->toArray(), $dbourStory);
    }

    /**
     * @test update
     */
    public function test_update_our_story()
    {
        $ourStory = ourStory::factory()->create();
        $fakeourStory = ourStory::factory()->make()->toArray();

        $updatedourStory = $this->ourStoryRepo->update($fakeourStory, $ourStory->id);

        $this->assertModelData($fakeourStory, $updatedourStory->toArray());
        $dbourStory = $this->ourStoryRepo->find($ourStory->id);
        $this->assertModelData($fakeourStory, $dbourStory->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_our_story()
    {
        $ourStory = ourStory::factory()->create();

        $resp = $this->ourStoryRepo->delete($ourStory->id);

        $this->assertTrue($resp);
        $this->assertNull(ourStory::find($ourStory->id), 'ourStory should not exist in DB');
    }
}
