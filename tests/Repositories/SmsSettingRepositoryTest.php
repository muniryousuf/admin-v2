<?php namespace Tests\Repositories;

use App\Models\SmsSetting;
use App\Repositories\SmsSettingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SmsSettingRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SmsSettingRepository
     */
    protected $smsSettingRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->smsSettingRepo = \App::make(SmsSettingRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_sms_setting()
    {
        $smsSetting = SmsSetting::factory()->make()->toArray();

        $createdSmsSetting = $this->smsSettingRepo->create($smsSetting);

        $createdSmsSetting = $createdSmsSetting->toArray();
        $this->assertArrayHasKey('id', $createdSmsSetting);
        $this->assertNotNull($createdSmsSetting['id'], 'Created SmsSetting must have id specified');
        $this->assertNotNull(SmsSetting::find($createdSmsSetting['id']), 'SmsSetting with given id must be in DB');
        $this->assertModelData($smsSetting, $createdSmsSetting);
    }

    /**
     * @test read
     */
    public function test_read_sms_setting()
    {
        $smsSetting = SmsSetting::factory()->create();

        $dbSmsSetting = $this->smsSettingRepo->find($smsSetting->id);

        $dbSmsSetting = $dbSmsSetting->toArray();
        $this->assertModelData($smsSetting->toArray(), $dbSmsSetting);
    }

    /**
     * @test update
     */
    public function test_update_sms_setting()
    {
        $smsSetting = SmsSetting::factory()->create();
        $fakeSmsSetting = SmsSetting::factory()->make()->toArray();

        $updatedSmsSetting = $this->smsSettingRepo->update($fakeSmsSetting, $smsSetting->id);

        $this->assertModelData($fakeSmsSetting, $updatedSmsSetting->toArray());
        $dbSmsSetting = $this->smsSettingRepo->find($smsSetting->id);
        $this->assertModelData($fakeSmsSetting, $dbSmsSetting->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_sms_setting()
    {
        $smsSetting = SmsSetting::factory()->create();

        $resp = $this->smsSettingRepo->delete($smsSetting->id);

        $this->assertTrue($resp);
        $this->assertNull(SmsSetting::find($smsSetting->id), 'SmsSetting should not exist in DB');
    }
}
