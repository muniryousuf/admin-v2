<?php namespace Tests\Repositories;

use App\Models\Vouchers;
use App\Repositories\VouchersRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class VouchersRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var VouchersRepository
     */
    protected $vouchersRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->vouchersRepo = \App::make(VouchersRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_vouchers()
    {
        $vouchers = Vouchers::factory()->make()->toArray();

        $createdVouchers = $this->vouchersRepo->create($vouchers);

        $createdVouchers = $createdVouchers->toArray();
        $this->assertArrayHasKey('id', $createdVouchers);
        $this->assertNotNull($createdVouchers['id'], 'Created Vouchers must have id specified');
        $this->assertNotNull(Vouchers::find($createdVouchers['id']), 'Vouchers with given id must be in DB');
        $this->assertModelData($vouchers, $createdVouchers);
    }

    /**
     * @test read
     */
    public function test_read_vouchers()
    {
        $vouchers = Vouchers::factory()->create();

        $dbVouchers = $this->vouchersRepo->find($vouchers->id);

        $dbVouchers = $dbVouchers->toArray();
        $this->assertModelData($vouchers->toArray(), $dbVouchers);
    }

    /**
     * @test update
     */
    public function test_update_vouchers()
    {
        $vouchers = Vouchers::factory()->create();
        $fakeVouchers = Vouchers::factory()->make()->toArray();

        $updatedVouchers = $this->vouchersRepo->update($fakeVouchers, $vouchers->id);

        $this->assertModelData($fakeVouchers, $updatedVouchers->toArray());
        $dbVouchers = $this->vouchersRepo->find($vouchers->id);
        $this->assertModelData($fakeVouchers, $dbVouchers->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_vouchers()
    {
        $vouchers = Vouchers::factory()->create();

        $resp = $this->vouchersRepo->delete($vouchers->id);

        $this->assertTrue($resp);
        $this->assertNull(Vouchers::find($vouchers->id), 'Vouchers should not exist in DB');
    }
}
