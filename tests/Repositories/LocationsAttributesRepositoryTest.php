<?php namespace Tests\Repositories;

use App\Models\LocationsAttributes;
use App\Repositories\LocationsAttributesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class LocationsAttributesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var LocationsAttributesRepository
     */
    protected $locationsAttributesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->locationsAttributesRepo = \App::make(LocationsAttributesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_locations_attributes()
    {
        $locationsAttributes = LocationsAttributes::factory()->make()->toArray();

        $createdLocationsAttributes = $this->locationsAttributesRepo->create($locationsAttributes);

        $createdLocationsAttributes = $createdLocationsAttributes->toArray();
        $this->assertArrayHasKey('id', $createdLocationsAttributes);
        $this->assertNotNull($createdLocationsAttributes['id'], 'Created LocationsAttributes must have id specified');
        $this->assertNotNull(LocationsAttributes::find($createdLocationsAttributes['id']), 'LocationsAttributes with given id must be in DB');
        $this->assertModelData($locationsAttributes, $createdLocationsAttributes);
    }

    /**
     * @test read
     */
    public function test_read_locations_attributes()
    {
        $locationsAttributes = LocationsAttributes::factory()->create();

        $dbLocationsAttributes = $this->locationsAttributesRepo->find($locationsAttributes->id);

        $dbLocationsAttributes = $dbLocationsAttributes->toArray();
        $this->assertModelData($locationsAttributes->toArray(), $dbLocationsAttributes);
    }

    /**
     * @test update
     */
    public function test_update_locations_attributes()
    {
        $locationsAttributes = LocationsAttributes::factory()->create();
        $fakeLocationsAttributes = LocationsAttributes::factory()->make()->toArray();

        $updatedLocationsAttributes = $this->locationsAttributesRepo->update($fakeLocationsAttributes, $locationsAttributes->id);

        $this->assertModelData($fakeLocationsAttributes, $updatedLocationsAttributes->toArray());
        $dbLocationsAttributes = $this->locationsAttributesRepo->find($locationsAttributes->id);
        $this->assertModelData($fakeLocationsAttributes, $dbLocationsAttributes->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_locations_attributes()
    {
        $locationsAttributes = LocationsAttributes::factory()->create();

        $resp = $this->locationsAttributesRepo->delete($locationsAttributes->id);

        $this->assertTrue($resp);
        $this->assertNull(LocationsAttributes::find($locationsAttributes->id), 'LocationsAttributes should not exist in DB');
    }
}
