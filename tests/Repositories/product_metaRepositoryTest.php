<?php namespace Tests\Repositories;

use App\Models\product_meta;
use App\Repositories\product_metaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class product_metaRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var product_metaRepository
     */
    protected $productMetaRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->productMetaRepo = \App::make(product_metaRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_product_meta()
    {
        $productMeta = product_meta::factory()->make()->toArray();

        $createdproduct_meta = $this->productMetaRepo->create($productMeta);

        $createdproduct_meta = $createdproduct_meta->toArray();
        $this->assertArrayHasKey('id', $createdproduct_meta);
        $this->assertNotNull($createdproduct_meta['id'], 'Created product_meta must have id specified');
        $this->assertNotNull(product_meta::find($createdproduct_meta['id']), 'product_meta with given id must be in DB');
        $this->assertModelData($productMeta, $createdproduct_meta);
    }

    /**
     * @test read
     */
    public function test_read_product_meta()
    {
        $productMeta = product_meta::factory()->create();

        $dbproduct_meta = $this->productMetaRepo->find($productMeta->id);

        $dbproduct_meta = $dbproduct_meta->toArray();
        $this->assertModelData($productMeta->toArray(), $dbproduct_meta);
    }

    /**
     * @test update
     */
    public function test_update_product_meta()
    {
        $productMeta = product_meta::factory()->create();
        $fakeproduct_meta = product_meta::factory()->make()->toArray();

        $updatedproduct_meta = $this->productMetaRepo->update($fakeproduct_meta, $productMeta->id);

        $this->assertModelData($fakeproduct_meta, $updatedproduct_meta->toArray());
        $dbproduct_meta = $this->productMetaRepo->find($productMeta->id);
        $this->assertModelData($fakeproduct_meta, $dbproduct_meta->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_product_meta()
    {
        $productMeta = product_meta::factory()->create();

        $resp = $this->productMetaRepo->delete($productMeta->id);

        $this->assertTrue($resp);
        $this->assertNull(product_meta::find($productMeta->id), 'product_meta should not exist in DB');
    }
}
