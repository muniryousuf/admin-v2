<?php namespace Tests\Repositories;

use App\Models\ResturentTimings;
use App\Repositories\ResturentTimingsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ResturentTimingsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ResturentTimingsRepository
     */
    protected $resturentTimingsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->resturentTimingsRepo = \App::make(ResturentTimingsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_resturent_timings()
    {
        $resturentTimings = ResturentTimings::factory()->make()->toArray();

        $createdResturentTimings = $this->resturentTimingsRepo->create($resturentTimings);

        $createdResturentTimings = $createdResturentTimings->toArray();
        $this->assertArrayHasKey('id', $createdResturentTimings);
        $this->assertNotNull($createdResturentTimings['id'], 'Created ResturentTimings must have id specified');
        $this->assertNotNull(ResturentTimings::find($createdResturentTimings['id']), 'ResturentTimings with given id must be in DB');
        $this->assertModelData($resturentTimings, $createdResturentTimings);
    }

    /**
     * @test read
     */
    public function test_read_resturent_timings()
    {
        $resturentTimings = ResturentTimings::factory()->create();

        $dbResturentTimings = $this->resturentTimingsRepo->find($resturentTimings->id);

        $dbResturentTimings = $dbResturentTimings->toArray();
        $this->assertModelData($resturentTimings->toArray(), $dbResturentTimings);
    }

    /**
     * @test update
     */
    public function test_update_resturent_timings()
    {
        $resturentTimings = ResturentTimings::factory()->create();
        $fakeResturentTimings = ResturentTimings::factory()->make()->toArray();

        $updatedResturentTimings = $this->resturentTimingsRepo->update($fakeResturentTimings, $resturentTimings->id);

        $this->assertModelData($fakeResturentTimings, $updatedResturentTimings->toArray());
        $dbResturentTimings = $this->resturentTimingsRepo->find($resturentTimings->id);
        $this->assertModelData($fakeResturentTimings, $dbResturentTimings->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_resturent_timings()
    {
        $resturentTimings = ResturentTimings::factory()->create();

        $resp = $this->resturentTimingsRepo->delete($resturentTimings->id);

        $this->assertTrue($resp);
        $this->assertNull(ResturentTimings::find($resturentTimings->id), 'ResturentTimings should not exist in DB');
    }
}
