<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\RestaurantTimingsSpecific;

class RestaurantTimingsSpecificApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_restaurant_timings_specific()
    {
        $restaurantTimingsSpecific = RestaurantTimingsSpecific::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/restaurant_timings_specifics', $restaurantTimingsSpecific
        );

        $this->assertApiResponse($restaurantTimingsSpecific);
    }

    /**
     * @test
     */
    public function test_read_restaurant_timings_specific()
    {
        $restaurantTimingsSpecific = RestaurantTimingsSpecific::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/restaurant_timings_specifics/'.$restaurantTimingsSpecific->id
        );

        $this->assertApiResponse($restaurantTimingsSpecific->toArray());
    }

    /**
     * @test
     */
    public function test_update_restaurant_timings_specific()
    {
        $restaurantTimingsSpecific = RestaurantTimingsSpecific::factory()->create();
        $editedRestaurantTimingsSpecific = RestaurantTimingsSpecific::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/restaurant_timings_specifics/'.$restaurantTimingsSpecific->id,
            $editedRestaurantTimingsSpecific
        );

        $this->assertApiResponse($editedRestaurantTimingsSpecific);
    }

    /**
     * @test
     */
    public function test_delete_restaurant_timings_specific()
    {
        $restaurantTimingsSpecific = RestaurantTimingsSpecific::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/restaurant_timings_specifics/'.$restaurantTimingsSpecific->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/restaurant_timings_specifics/'.$restaurantTimingsSpecific->id
        );

        $this->response->assertStatus(404);
    }
}
