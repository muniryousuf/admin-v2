<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ResturentTimings;

class ResturentTimingsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_resturent_timings()
    {
        $resturentTimings = ResturentTimings::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/resturent_timings', $resturentTimings
        );

        $this->assertApiResponse($resturentTimings);
    }

    /**
     * @test
     */
    public function test_read_resturent_timings()
    {
        $resturentTimings = ResturentTimings::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/resturent_timings/'.$resturentTimings->id
        );

        $this->assertApiResponse($resturentTimings->toArray());
    }

    /**
     * @test
     */
    public function test_update_resturent_timings()
    {
        $resturentTimings = ResturentTimings::factory()->create();
        $editedResturentTimings = ResturentTimings::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/resturent_timings/'.$resturentTimings->id,
            $editedResturentTimings
        );

        $this->assertApiResponse($editedResturentTimings);
    }

    /**
     * @test
     */
    public function test_delete_resturent_timings()
    {
        $resturentTimings = ResturentTimings::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/resturent_timings/'.$resturentTimings->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/resturent_timings/'.$resturentTimings->id
        );

        $this->response->assertStatus(404);
    }
}
