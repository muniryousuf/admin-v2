<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\deliveryChargesDetails;

class deliveryChargesDetailsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_delivery_charges_details()
    {
        $deliveryChargesDetails = deliveryChargesDetails::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/delivery_charges_details', $deliveryChargesDetails
        );

        $this->assertApiResponse($deliveryChargesDetails);
    }

    /**
     * @test
     */
    public function test_read_delivery_charges_details()
    {
        $deliveryChargesDetails = deliveryChargesDetails::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/delivery_charges_details/'.$deliveryChargesDetails->id
        );

        $this->assertApiResponse($deliveryChargesDetails->toArray());
    }

    /**
     * @test
     */
    public function test_update_delivery_charges_details()
    {
        $deliveryChargesDetails = deliveryChargesDetails::factory()->create();
        $editeddeliveryChargesDetails = deliveryChargesDetails::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/delivery_charges_details/'.$deliveryChargesDetails->id,
            $editeddeliveryChargesDetails
        );

        $this->assertApiResponse($editeddeliveryChargesDetails);
    }

    /**
     * @test
     */
    public function test_delete_delivery_charges_details()
    {
        $deliveryChargesDetails = deliveryChargesDetails::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/delivery_charges_details/'.$deliveryChargesDetails->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/delivery_charges_details/'.$deliveryChargesDetails->id
        );

        $this->response->assertStatus(404);
    }
}
