<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TableReservation;

class TableReservationApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_table_reservation()
    {
        $tableReservation = TableReservation::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/table_reservations', $tableReservation
        );

        $this->assertApiResponse($tableReservation);
    }

    /**
     * @test
     */
    public function test_read_table_reservation()
    {
        $tableReservation = TableReservation::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/table_reservations/'.$tableReservation->id
        );

        $this->assertApiResponse($tableReservation->toArray());
    }

    /**
     * @test
     */
    public function test_update_table_reservation()
    {
        $tableReservation = TableReservation::factory()->create();
        $editedTableReservation = TableReservation::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/table_reservations/'.$tableReservation->id,
            $editedTableReservation
        );

        $this->assertApiResponse($editedTableReservation);
    }

    /**
     * @test
     */
    public function test_delete_table_reservation()
    {
        $tableReservation = TableReservation::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/table_reservations/'.$tableReservation->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/table_reservations/'.$tableReservation->id
        );

        $this->response->assertStatus(404);
    }
}
