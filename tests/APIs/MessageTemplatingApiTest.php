<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MessageTemplating;

class MessageTemplatingApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_message_templating()
    {
        $messageTemplating = MessageTemplating::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/message_templatings', $messageTemplating
        );

        $this->assertApiResponse($messageTemplating);
    }

    /**
     * @test
     */
    public function test_read_message_templating()
    {
        $messageTemplating = MessageTemplating::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/message_templatings/'.$messageTemplating->id
        );

        $this->assertApiResponse($messageTemplating->toArray());
    }

    /**
     * @test
     */
    public function test_update_message_templating()
    {
        $messageTemplating = MessageTemplating::factory()->create();
        $editedMessageTemplating = MessageTemplating::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/message_templatings/'.$messageTemplating->id,
            $editedMessageTemplating
        );

        $this->assertApiResponse($editedMessageTemplating);
    }

    /**
     * @test
     */
    public function test_delete_message_templating()
    {
        $messageTemplating = MessageTemplating::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/message_templatings/'.$messageTemplating->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/message_templatings/'.$messageTemplating->id
        );

        $this->response->assertStatus(404);
    }
}
