<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CustomerReservations;

class CustomerReservationsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_customer_reservations()
    {
        $customerReservations = CustomerReservations::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/customer_reservations', $customerReservations
        );

        $this->assertApiResponse($customerReservations);
    }

    /**
     * @test
     */
    public function test_read_customer_reservations()
    {
        $customerReservations = CustomerReservations::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/customer_reservations/'.$customerReservations->id
        );

        $this->assertApiResponse($customerReservations->toArray());
    }

    /**
     * @test
     */
    public function test_update_customer_reservations()
    {
        $customerReservations = CustomerReservations::factory()->create();
        $editedCustomerReservations = CustomerReservations::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/customer_reservations/'.$customerReservations->id,
            $editedCustomerReservations
        );

        $this->assertApiResponse($editedCustomerReservations);
    }

    /**
     * @test
     */
    public function test_delete_customer_reservations()
    {
        $customerReservations = CustomerReservations::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/customer_reservations/'.$customerReservations->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/customer_reservations/'.$customerReservations->id
        );

        $this->response->assertStatus(404);
    }
}
