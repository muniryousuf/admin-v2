<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Reviews;

class ReviewsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_reviews()
    {
        $reviews = Reviews::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/reviews', $reviews
        );

        $this->assertApiResponse($reviews);
    }

    /**
     * @test
     */
    public function test_read_reviews()
    {
        $reviews = Reviews::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/reviews/'.$reviews->id
        );

        $this->assertApiResponse($reviews->toArray());
    }

    /**
     * @test
     */
    public function test_update_reviews()
    {
        $reviews = Reviews::factory()->create();
        $editedReviews = Reviews::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/reviews/'.$reviews->id,
            $editedReviews
        );

        $this->assertApiResponse($editedReviews);
    }

    /**
     * @test
     */
    public function test_delete_reviews()
    {
        $reviews = Reviews::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/reviews/'.$reviews->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/reviews/'.$reviews->id
        );

        $this->response->assertStatus(404);
    }
}
