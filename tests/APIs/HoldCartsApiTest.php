<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\HoldCarts;

class HoldCartsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_hold_carts()
    {
        $holdCarts = HoldCarts::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/hold_carts', $holdCarts
        );

        $this->assertApiResponse($holdCarts);
    }

    /**
     * @test
     */
    public function test_read_hold_carts()
    {
        $holdCarts = HoldCarts::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/hold_carts/'.$holdCarts->id
        );

        $this->assertApiResponse($holdCarts->toArray());
    }

    /**
     * @test
     */
    public function test_update_hold_carts()
    {
        $holdCarts = HoldCarts::factory()->create();
        $editedHoldCarts = HoldCarts::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/hold_carts/'.$holdCarts->id,
            $editedHoldCarts
        );

        $this->assertApiResponse($editedHoldCarts);
    }

    /**
     * @test
     */
    public function test_delete_hold_carts()
    {
        $holdCarts = HoldCarts::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/hold_carts/'.$holdCarts->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/hold_carts/'.$holdCarts->id
        );

        $this->response->assertStatus(404);
    }
}
