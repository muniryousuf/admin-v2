<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\product_meta;

class product_metaApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_product_meta()
    {
        $productMeta = product_meta::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/product_metas', $productMeta
        );

        $this->assertApiResponse($productMeta);
    }

    /**
     * @test
     */
    public function test_read_product_meta()
    {
        $productMeta = product_meta::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/product_metas/'.$productMeta->id
        );

        $this->assertApiResponse($productMeta->toArray());
    }

    /**
     * @test
     */
    public function test_update_product_meta()
    {
        $productMeta = product_meta::factory()->create();
        $editedproduct_meta = product_meta::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/product_metas/'.$productMeta->id,
            $editedproduct_meta
        );

        $this->assertApiResponse($editedproduct_meta);
    }

    /**
     * @test
     */
    public function test_delete_product_meta()
    {
        $productMeta = product_meta::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/product_metas/'.$productMeta->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/product_metas/'.$productMeta->id
        );

        $this->response->assertStatus(404);
    }
}
