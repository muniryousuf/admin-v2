<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\LocationsAttributes;

class LocationsAttributesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_locations_attributes()
    {
        $locationsAttributes = LocationsAttributes::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/locations_attributes', $locationsAttributes
        );

        $this->assertApiResponse($locationsAttributes);
    }

    /**
     * @test
     */
    public function test_read_locations_attributes()
    {
        $locationsAttributes = LocationsAttributes::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/locations_attributes/'.$locationsAttributes->id
        );

        $this->assertApiResponse($locationsAttributes->toArray());
    }

    /**
     * @test
     */
    public function test_update_locations_attributes()
    {
        $locationsAttributes = LocationsAttributes::factory()->create();
        $editedLocationsAttributes = LocationsAttributes::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/locations_attributes/'.$locationsAttributes->id,
            $editedLocationsAttributes
        );

        $this->assertApiResponse($editedLocationsAttributes);
    }

    /**
     * @test
     */
    public function test_delete_locations_attributes()
    {
        $locationsAttributes = LocationsAttributes::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/locations_attributes/'.$locationsAttributes->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/locations_attributes/'.$locationsAttributes->id
        );

        $this->response->assertStatus(404);
    }
}
