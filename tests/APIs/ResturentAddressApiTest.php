<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ResturentAddress;

class ResturentAddressApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_resturent_address()
    {
        $resturentAddress = ResturentAddress::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/resturent_addresses', $resturentAddress
        );

        $this->assertApiResponse($resturentAddress);
    }

    /**
     * @test
     */
    public function test_read_resturent_address()
    {
        $resturentAddress = ResturentAddress::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/resturent_addresses/'.$resturentAddress->id
        );

        $this->assertApiResponse($resturentAddress->toArray());
    }

    /**
     * @test
     */
    public function test_update_resturent_address()
    {
        $resturentAddress = ResturentAddress::factory()->create();
        $editedResturentAddress = ResturentAddress::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/resturent_addresses/'.$resturentAddress->id,
            $editedResturentAddress
        );

        $this->assertApiResponse($editedResturentAddress);
    }

    /**
     * @test
     */
    public function test_delete_resturent_address()
    {
        $resturentAddress = ResturentAddress::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/resturent_addresses/'.$resturentAddress->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/resturent_addresses/'.$resturentAddress->id
        );

        $this->response->assertStatus(404);
    }
}
