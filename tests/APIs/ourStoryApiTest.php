<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ourStory;

class ourStoryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_our_story()
    {
        $ourStory = ourStory::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/our_stories', $ourStory
        );

        $this->assertApiResponse($ourStory);
    }

    /**
     * @test
     */
    public function test_read_our_story()
    {
        $ourStory = ourStory::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/our_stories/'.$ourStory->id
        );

        $this->assertApiResponse($ourStory->toArray());
    }

    /**
     * @test
     */
    public function test_update_our_story()
    {
        $ourStory = ourStory::factory()->create();
        $editedourStory = ourStory::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/our_stories/'.$ourStory->id,
            $editedourStory
        );

        $this->assertApiResponse($editedourStory);
    }

    /**
     * @test
     */
    public function test_delete_our_story()
    {
        $ourStory = ourStory::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/our_stories/'.$ourStory->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/our_stories/'.$ourStory->id
        );

        $this->response->assertStatus(404);
    }
}
