<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Vouchers;

class VouchersApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_vouchers()
    {
        $vouchers = Vouchers::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/vouchers', $vouchers
        );

        $this->assertApiResponse($vouchers);
    }

    /**
     * @test
     */
    public function test_read_vouchers()
    {
        $vouchers = Vouchers::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/vouchers/'.$vouchers->id
        );

        $this->assertApiResponse($vouchers->toArray());
    }

    /**
     * @test
     */
    public function test_update_vouchers()
    {
        $vouchers = Vouchers::factory()->create();
        $editedVouchers = Vouchers::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/vouchers/'.$vouchers->id,
            $editedVouchers
        );

        $this->assertApiResponse($editedVouchers);
    }

    /**
     * @test
     */
    public function test_delete_vouchers()
    {
        $vouchers = Vouchers::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/vouchers/'.$vouchers->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/vouchers/'.$vouchers->id
        );

        $this->response->assertStatus(404);
    }
}
