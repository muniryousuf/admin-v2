<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SmsSetting;

class SmsSettingApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_sms_setting()
    {
        $smsSetting = SmsSetting::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/sms_settings', $smsSetting
        );

        $this->assertApiResponse($smsSetting);
    }

    /**
     * @test
     */
    public function test_read_sms_setting()
    {
        $smsSetting = SmsSetting::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/sms_settings/'.$smsSetting->id
        );

        $this->assertApiResponse($smsSetting->toArray());
    }

    /**
     * @test
     */
    public function test_update_sms_setting()
    {
        $smsSetting = SmsSetting::factory()->create();
        $editedSmsSetting = SmsSetting::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/sms_settings/'.$smsSetting->id,
            $editedSmsSetting
        );

        $this->assertApiResponse($editedSmsSetting);
    }

    /**
     * @test
     */
    public function test_delete_sms_setting()
    {
        $smsSetting = SmsSetting::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/sms_settings/'.$smsSetting->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/sms_settings/'.$smsSetting->id
        );

        $this->response->assertStatus(404);
    }
}
