<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TableCategories;

class TableCategoriesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_table_categories()
    {
        $tableCategories = TableCategories::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/table_categories', $tableCategories
        );

        $this->assertApiResponse($tableCategories);
    }

    /**
     * @test
     */
    public function test_read_table_categories()
    {
        $tableCategories = TableCategories::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/table_categories/'.$tableCategories->id
        );

        $this->assertApiResponse($tableCategories->toArray());
    }

    /**
     * @test
     */
    public function test_update_table_categories()
    {
        $tableCategories = TableCategories::factory()->create();
        $editedTableCategories = TableCategories::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/table_categories/'.$tableCategories->id,
            $editedTableCategories
        );

        $this->assertApiResponse($editedTableCategories);
    }

    /**
     * @test
     */
    public function test_delete_table_categories()
    {
        $tableCategories = TableCategories::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/table_categories/'.$tableCategories->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/table_categories/'.$tableCategories->id
        );

        $this->response->assertStatus(404);
    }
}
