<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\general_settings;

class general_settingsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_general_settings()
    {
        $generalSettings = general_settings::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/general_settings', $generalSettings
        );

        $this->assertApiResponse($generalSettings);
    }

    /**
     * @test
     */
    public function test_read_general_settings()
    {
        $generalSettings = general_settings::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/general_settings/'.$generalSettings->id
        );

        $this->assertApiResponse($generalSettings->toArray());
    }

    /**
     * @test
     */
    public function test_update_general_settings()
    {
        $generalSettings = general_settings::factory()->create();
        $editedgeneral_settings = general_settings::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/general_settings/'.$generalSettings->id,
            $editedgeneral_settings
        );

        $this->assertApiResponse($editedgeneral_settings);
    }

    /**
     * @test
     */
    public function test_delete_general_settings()
    {
        $generalSettings = general_settings::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/general_settings/'.$generalSettings->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/general_settings/'.$generalSettings->id
        );

        $this->response->assertStatus(404);
    }
}
