<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CaosGenralSettings;

class CaosGenralSettingsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_caos_genral_settings()
    {
        $caosGenralSettings = CaosGenralSettings::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/caos_genral_settings', $caosGenralSettings
        );

        $this->assertApiResponse($caosGenralSettings);
    }

    /**
     * @test
     */
    public function test_read_caos_genral_settings()
    {
        $caosGenralSettings = CaosGenralSettings::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/caos_genral_settings/'.$caosGenralSettings->id
        );

        $this->assertApiResponse($caosGenralSettings->toArray());
    }

    /**
     * @test
     */
    public function test_update_caos_genral_settings()
    {
        $caosGenralSettings = CaosGenralSettings::factory()->create();
        $editedCaosGenralSettings = CaosGenralSettings::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/caos_genral_settings/'.$caosGenralSettings->id,
            $editedCaosGenralSettings
        );

        $this->assertApiResponse($editedCaosGenralSettings);
    }

    /**
     * @test
     */
    public function test_delete_caos_genral_settings()
    {
        $caosGenralSettings = CaosGenralSettings::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/caos_genral_settings/'.$caosGenralSettings->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/caos_genral_settings/'.$caosGenralSettings->id
        );

        $this->response->assertStatus(404);
    }
}
