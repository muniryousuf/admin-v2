<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Locations;

class LocationsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_locations()
    {
        $locations = Locations::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/locations', $locations
        );

        $this->assertApiResponse($locations);
    }

    /**
     * @test
     */
    public function test_read_locations()
    {
        $locations = Locations::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/locations/'.$locations->id
        );

        $this->assertApiResponse($locations->toArray());
    }

    /**
     * @test
     */
    public function test_update_locations()
    {
        $locations = Locations::factory()->create();
        $editedLocations = Locations::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/locations/'.$locations->id,
            $editedLocations
        );

        $this->assertApiResponse($editedLocations);
    }

    /**
     * @test
     */
    public function test_delete_locations()
    {
        $locations = Locations::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/locations/'.$locations->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/locations/'.$locations->id
        );

        $this->response->assertStatus(404);
    }
}
