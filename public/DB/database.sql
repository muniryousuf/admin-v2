
--
-- Dumping data for table `cms`
--

INSERT INTO `cms` (`id`, `title`, `content`, `slug`, `meta_title`, `meta_desc`, `index_follow`, `created_at`, `updated_at`, `deleted_at`, `image_1`, `image_2`) VALUES
(1, 'About Us', 'Order food online in NORTHFIELD,BIRMINGHAM! It\'s so easy to use, fast and convenient. Try our new, online website which contains our entire takeaway menu. The Original Cottage is located in NORTHFIELD,BIRMINGHAM. You can now order online, all your favourite dishes and many more delicious options, and have them delivered straight to your door in no time at all.', 'about-us', 'about us', 'about us', 'about us', '2022-03-05 09:05:04', '2022-03-05 09:35:56', NULL, '1646490956.webp', 'img-phpKCfKoe.png'),
(2, 'Quia consequatur qui', 'Ea ea molestiae temp', 'Rerum ipsam eveniet', 'Ut quaerat et qui id', 'Eligendi pariatur V', 'Eius ducimus hic au', '2022-03-05 09:37:40', '2022-03-05 09:38:09', '2022-03-05 09:38:09', '1646491078.png', '1646491060.webp');

-- --------------------------------------------------------


--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `image`, `gallery_id`, `created_at`, `updated_at`, `deleted_at`, `title`) VALUES
(1, '1646491201.webp', 0, '2022-03-05 09:40:01', '2022-03-05 09:51:09', NULL, 'test'),
(2, '1646491246.webp', 0, '2022-03-05 09:40:46', '2022-03-05 09:51:22', NULL, 'galleries'),
(3, '1646491282.webp', 0, '2022-03-05 09:41:22', '2022-03-05 09:41:37', '2022-03-05 09:41:37', NULL),
(4, '1646491304.webp', 0, '2022-03-05 09:41:44', '2022-03-05 09:51:28', NULL, 'galleriesgalleries'),
(5, '1646491953.webp', 0, '2022-03-05 09:52:33', '2022-03-05 09:52:38', '2022-03-05 09:52:38', '123123123');

-- --------------------------------------------------------


INSERT INTO `general_settings` (`id`, `site_name`, `site_title`, `tag_line`, `copyright_text`, `header_logo`, `footer_logo`, `service_charges`, `vat`, `currencySign`, `shop_status`, `printer_ip_1`, `printer_ip_2`, `printer_ip_3`, `printer_ip_4`, `printer_ip_5`, `printer_ip`, `stripe_publishable_key`, `stripe_secret_key`, `facebook`, `twitter`, `instagram`, `pinterest`, `youtube`, `min_collection_time`, `min_delivery_time`, `total_allowed_person`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test site', 'test site', 'test site', 'test site', '1646485147.png', '1646485147.webp', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'Stripe Publishable Key:', 'Stripe Secret Key:', 'Facebook', 'twitter', 'instagram', 'pinterest', 'youtube', NULL, NULL, NULL, '2022-03-05 07:59:07', '2022-03-05 07:59:07', NULL);



-- --------------------------------------------------------

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `image`, `sort`, `url`, `gallery_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1646488068.webp', 1, 'test', 0, '2022-03-05 08:47:48', '2022-03-05 08:47:48', NULL),
(2, '1646488096.webp', 2, 'test', 0, '2022-03-05 08:48:16', '2022-03-05 08:48:16', NULL),
(3, '1646488143.webp', 3, 'test', 0, '2022-03-05 08:49:03', '2022-03-05 08:49:03', NULL);

-- --------------------------------------------------------

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Prescott', 'admin@server.com', NULL, '$2y$10$wNQsZcmkHLUJVdJJRfZwyeXWvIg.yQYlkWE/fhEYutKn7q2ptHdkm', NULL, '2022-03-05 07:50:33', '2022-03-05 07:50:33');

