$(document).ready(function () {
    new WOW().init();

    var getHeaderHeight = $('#header').height();
    $('.root-content').css('margin-top', getHeaderHeight);

    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('#header').outerHeight();

    $(window).scroll(function (event) {
        didScroll = true;

        if($(this).scrollTop() >  200) {
            $('.ant-back-top').addClass('active');
        } else {
            $('.ant-back-top').removeClass('active');
        }
    });

    setInterval(function () {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);
    function hasScrolled() {
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta)
            return;

        if (st > lastScrollTop && st > navbarHeight) {
            $('#header').removeClass('nav-down').addClass('nav-up');
        } else {
            if (st + $(window).height() < $(document).height()) {
                $('#header').removeClass('nav-up').addClass('nav-down');
            }
        }
        lastScrollTop = st;
    }

    $('.ant-back-top').on('click',function(){
        $("html, body").animate({ scrollTop: 0 }, "fast");
        return false;
    });

    $('.navbar-toggler').on('click',function(){
        $('.ant-drawer').addClass('ant-drawer-open');
        $('.ant-drawer').find('.ant-drawer-content-wrapper').css('width', '80vw')
    });

    $('body').on('click','.ant-drawer-mask',function(){
        $('.ant-drawer').removeClass('ant-drawer-open');
    });




    var addonstep = $('.modal').attr('data-addonstep');
    var dataselectiontype = $('.addon-group').attr('data-selectiontype');
    var validateParent = $(this).parents('')

    // $('body').on('click','.addons-item',function(){
    //     var mainthis = $(this);
    //     if(mainthis.parents('.addon-group').attr('data-selectiontype') == 'single') {
    //         mainthis.parents('.addon-group').next().removeClass('hide').addClass('focus');
    //         mainthis.parents('.addon-group').removeClass('focus').addClass('hide');
    //     }
    //     if(mainthis.parents('.addon-group').attr('data-selectiontype') == 'checkboxes' && mainthis.parents('.addon-group').find('.addons-select').length > 0 ) {
    //         var checkUserSelection = (mainthis.parents('.addon-group').find('.addons-select').find('.focused').text()).toLowerCase();
    //         if(checkUserSelection) {
    //             mainthis.find('.title-menu-addons').removeClass('less-item').removeClass('no-item');
    //             mainthis.find('.title-menu-addons').addClass(checkUserSelection + '-item');
    //         } else {

    //         }
    //     }
    // });
    // $('body').on('click','.back-button',function(){
    //     if($('.next-div').hasClass('add-to-cart')){
    //         $('.next-div').removeClass('add-to-cart');
    //         $('.next-div').addClass('next');
    //         $('.next-div').text('Next');
    //     }
    //     let last_iteration = parseInt(localStorage.getItem('last_meta_iteration'))-1;
    //     last_iteration = last_iteration < 0 ? 0 : last_iteration
    //     localStorage.setItem('last_meta_iteration',last_iteration)
    //     var mainthis = $(this);
    //     mainthis.parents('.addon-group').prev().removeClass('hide').addClass('focus');
    //     mainthis.parents('.addon-group').removeClass('focus').addClass('hide');
    // });

    // $('body').on('click','.next',function(){
    //     var mainthis = $(this);
    //     if(mainthis.parents('.addon-group').next().hasClass('hide')){
    //     // if(mainthis.parents('.addon-group').find('input:checked').length >  0) {
    //         let last_iteration = parseInt(localStorage.getItem('last_meta_iteration'))+1;
    //         localStorage.setItem('last_meta_iteration',last_iteration)
    //         mainthis.parents('.addon-group').next().removeClass('hide').addClass('focus');
    //         mainthis.parents('.addon-group').removeClass('focus').addClass('hide');
    //     }
    // });
    $('body').on('click','.specific-addons-btn button',function(){
        var mainthis = $(this);
        if(mainthis.hasClass('focused')){
            mainthis.removeClass('focused');
            return false;
        }
        mainthis.parent().find('button').removeClass('focused');
        mainthis.addClass('focused');
    });
    $('.basket-wrapper h3, .nav-basket').on('click',function(){
        $("body").find('.basket-wrapper').toggleClass('open');
    })


    $("#carousel").owlCarousel({
        autoplay: true,
        rewind: true, /* use rewind if you don't want loop */
        margin: 20,
        /*
       animateOut: 'fadeOut',
       animateIn: 'fadeIn',
       */
        responsiveClass: true,
        autoHeight: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        nav: true,
        responsive: {
            0: {
                items: 1
            },

            600: {
                items: 3
            },

            1024: {
                items: 4
            },

            1366: {
                items: 4
            }
        }
    });

});
