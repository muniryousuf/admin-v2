<?php
/**
 * @see https://github.com/Edujugon/PushNotification
 */

return [
    'gcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'My_ApiKey',
    ],
    'fcm' => [
        'priority' => 10,
        'dry_run' => false,
        'apiKey' => 'AAAAm6eTXEs:APA91bGHLSbBs73e8-QJoSSn73bJXRn-VMCb1jO_39AFrRrtpGGEav3NRKPpladUgOu_97eUVFAL19A74sQV_JtDTC6ppZhqvMu3EM2glXh90E5HofovRsYI97uY38cVEI5pCLk3ceoe',
    ],
    'apn' => [
        'certificate' => __DIR__ . '/iosCertificates/apns-dev-cert.pem',
        'passPhrase' => 'secret', //Optional
        'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
        'dry_run' => true,
    ],
];
