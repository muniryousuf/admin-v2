<!-- Start Time Field -->
<div class="form-group">
    {!! Form::label('start_time', __('models/restaurantTimingsSpecifics.fields.start_time').':') !!}
    <p>{{ $restaurantTimingsSpecific->start_time }}</p>
</div>

<!-- End Time Field -->
<div class="form-group">
    {!! Form::label('end_time', __('models/restaurantTimingsSpecifics.fields.end_time').':') !!}
    <p>{{ $restaurantTimingsSpecific->end_time }}</p>
</div>

<!-- Shop Closed Field -->
<div class="form-group">
    {!! Form::label('shop_closed', __('models/restaurantTimingsSpecifics.fields.shop_closed').':') !!}
    <p>{{ $restaurantTimingsSpecific->shop_closed }}</p>
</div>

<!-- Specific Date Field -->
<div class="form-group">
    {!! Form::label('specific_date', __('models/restaurantTimingsSpecifics.fields.specific_date').':') !!}
    <p>{{ $restaurantTimingsSpecific->specific_date }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/restaurantTimingsSpecifics.fields.created_at').':') !!}
    <p>{{ $restaurantTimingsSpecific->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/restaurantTimingsSpecifics.fields.updated_at').':') !!}
    <p>{{ $restaurantTimingsSpecific->updated_at }}</p>
</div>

