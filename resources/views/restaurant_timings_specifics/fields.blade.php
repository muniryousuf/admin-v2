<!-- Start Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_time', __('models/resturentTimings.fields.start_time').':') !!}
    {!! Form::time('start_time', null, ['class' => 'form-control','id'=>'start_time']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#start_time').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- End Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_time', __('models/resturentTimings.fields.end_time').':') !!}
    {!! Form::time('end_time', null, ['class' => 'form-control','id'=>'end_time']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#end_time').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush



<!-- Shop Closed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shop_closed', __('models/restaurantTimingsSpecifics.fields.shop_closed').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('shop_closed', 0) !!}
        {!! Form::checkbox('shop_closed', '1', null) !!} 
    </label>
</div>

<!-- Specific Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('specific_date', __('models/restaurantTimingsSpecifics.fields.specific_date').':') !!}
    {!! Form::date('specific_date', null, ['class' => 'form-control','id'=>'specific_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#specific_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('restaurantTimingsSpecifics.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
