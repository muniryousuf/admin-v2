<div class="table-responsive">
    <table class="table" id="restaurantTimingsSpecifics-table">
        <thead>
            <tr>
                <th>@lang('models/restaurantTimingsSpecifics.fields.start_time')</th>
        <th>@lang('models/restaurantTimingsSpecifics.fields.end_time')</th>
        <th>@lang('models/restaurantTimingsSpecifics.fields.shop_closed')</th>
        <th>@lang('models/restaurantTimingsSpecifics.fields.specific_date')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($restaurantTimingsSpecifics as $restaurantTimingsSpecific)
            <tr>
                       <td>{{ $restaurantTimingsSpecific->start_time }}</td>
            <td>{{ $restaurantTimingsSpecific->end_time }}</td>
            <td>{{ $restaurantTimingsSpecific->shop_closed }}</td>
            <td>{{ $restaurantTimingsSpecific->specific_date }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['restaurantTimingsSpecifics.destroy', $restaurantTimingsSpecific->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('restaurantTimingsSpecifics.show', [$restaurantTimingsSpecific->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('restaurantTimingsSpecifics.edit', [$restaurantTimingsSpecific->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
