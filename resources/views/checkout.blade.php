@extends('layouts.front')
@section('content')
<div class="root-content" style="margin-top: 60px;">
  @include('top-header')
  @php $currency_default = getSettings()['currency'] ?? ''; @endphp
  <div class="container pb-4">
      <div class="row">
          <div class="col-lg-8 col-12">
              <section class="link-background order-status box-shadow p-lg-3 p-2 adjustmargintop" id="order-status">
                  <div class="row my-2 mb-5 mb-lg-5 pl-0">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                          <h3><span>Order Status</span></h3>
                      </div>
                      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-right d-none">
                          <p>
                              <span class="d-none d-sm-inline-block">
                                  <span>Your order will be ready for
                                      collection in
                                  </span>
                              </span>
                              <span class="font-weight-bold collection-delivery-time ml-1">14:34</span>
                          </p>
                      </div>
                  </div>
                  <div class="row my-2">
                      <div class="col-12">
                          <div class="row trackRow">
                              <div class="col-12 col-sm-12 col-md-12 col-lg-12 trackerDiv">
                                  <section>
                                      <div class="wizard">
                                          <div class="wizard-inner">
                                              <div class="ant-steps ant-steps-horizontal ant-steps-label-vertical ant-steps-dot">
                                                  <!--
                                                      #To make an step active use following structure#
                                                      <div class="ant-steps-item ant-steps-item-process nav-item active ant-steps-item-active"> -->
                                                   @php
                                                          //ant-steps-item-finish
                                                          $classfor_step_zero = 'ant-steps-item-wait pending';
                                                          $classfor_step_one = 'ant-steps-item-wait pending';
                                                          $classfor_step_two = 'ant-steps-item-wait pending';
                                                          $classfor_step_three = 'ant-steps-item-wait pending';
                                                          $classfor_step_four = 'ant-steps-item-wait pending';
                                                          if($current_status == 0){
                                                            $classfor_step_zero = 'completed';
                                                          }
                                                          if($current_status == 1){
                                                            $classfor_step_one = 'completed';
                                                          }
                                                          if($current_status == 2){
                                                            $classfor_step_one = 'ant-steps-item-finish completed';
                                                            $classfor_step_two = 'completed';
                                                          }
                                                          if($current_status == 3){
                                                            $classfor_step_one = 'ant-steps-item-finish completed';
                                                            $classfor_step_two = 'ant-steps-item-finish completed';
                                                            $classfor_step_three = 'completed';
                                                          }
                                                          if($current_status == 4){
                                                            $classfor_step_one = 'ant-steps-item-finish completed';
                                                            $classfor_step_two = 'ant-steps-item-finish completed';
                                                            $classfor_step_three = 'ant-steps-item-finish completed';
                                                            $classfor_step_four = 'completed';
                                                          }

                                                      @endphp
                                                  @if($order->order_type == 'collection')
                                                    <div class="ant-steps-item nav-item {{$classfor_step_zero}}">
                                                        <div class="ant-steps-item-container">
                                                            <div class="ant-steps-item-tail"></div>
                                                            <div class="ant-steps-item-icon"><span class="ant-steps-icon"><span class="ant-steps-icon-dot"></span></span>
                                                            </div>
                                                            <div class="ant-steps-item-content">
                                                                <div class="ant-steps-item-title"><span class="icon"><svg width="50" height="50" viewBox="0 0 1024 1024" class="icon-Double-Tick icon-tracking" id="Double-Tick" fill="">
                                                                            <path d="M678.4 364.526l-38.4-38.4-179.2 172.8 38.4 38.4 179.2-172.8zM793.6 326.126l-294.4 294.4-115.2-115.2-38.4 38.4 153.6 153.6 332.8-332.8-38.4-38.4zM192 543.726l153.6 153.6 38.4-38.4-153.6-153.6-38.4 38.4z">
                                                                            </path>
                                                                        </svg></span></div>
                                                                <div class="ant-steps-item-description">
                                                                    <h2 class="text-center word-break">
                                                                        <span>Order Placed</span></h2>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ant-steps-item nav-item {{$classfor_step_one}}">
                                                        <div class="ant-steps-item-container">
                                                            <div class="ant-steps-item-tail"></div>
                                                            <div class="ant-steps-item-icon"><span class="ant-steps-icon"><span class="ant-steps-icon-dot"></span></span>
                                                            </div>
                                                            <div class="ant-steps-item-content">
                                                                <div class="ant-steps-item-title"><span class="icon"><svg width="50" height="50" viewBox="0 0 1024 1024" class="icon-Double-Tick icon-tracking" id="Double-Tick" fill="">
                                                                            <path d="M678.4 364.526l-38.4-38.4-179.2 172.8 38.4 38.4 179.2-172.8zM793.6 326.126l-294.4 294.4-115.2-115.2-38.4 38.4 153.6 153.6 332.8-332.8-38.4-38.4zM192 543.726l153.6 153.6 38.4-38.4-153.6-153.6-38.4 38.4z">
                                                                            </path>
                                                                        </svg></span></div>
                                                                <div class="ant-steps-item-description">
                                                                    <h2 class="text-center word-break">
                                                                        <span>Order Confirmed</span></h2>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ant-steps-item nav-item  {{$classfor_step_two}}">
                                                        <div class="ant-steps-item-container">
                                                            <div class="ant-steps-item-tail"></div>
                                                            <div class="ant-steps-item-icon"><span class="ant-steps-icon"><span class="ant-steps-icon-dot"></span></span>
                                                            </div>
                                                            <div class="ant-steps-item-content">
                                                                <div class="ant-steps-item-title"><svg width="50" height="50" viewBox="0 0 1024 1024" class="icon-Chef icon-tracking" id="Chef" fill="">
                                                                        <path d="M358.4 499.2c0-6.4-6.4-12.8-12.8-12.8s-19.2 0-19.2 12.8c0 38.4 44.8 96 51.2 102.4 0 6.4 6.4 6.4 12.8 6.4s6.4 0 12.8-6.4 6.4-12.8 0-19.2c-19.2-25.6-44.8-64-44.8-83.2z M678.4 480c-6.4 0-12.8 6.4-12.8 12.8 0 19.2-25.6 64-44.8 83.2-6.4 6.4-6.4 19.2 0 19.2 0 0 6.4 6.4 12.8 6.4s6.4 0 12.8-6.4 51.2-64 51.2-102.4c0-6.4-6.4-12.8-19.2-12.8z M512 480c-6.4 0-12.8 6.4-12.8 12.8v89.6c0 6.4 6.4 12.8 12.8 12.8s12.8-6.4 12.8-12.8v-83.2c0-12.8-6.4-19.2-12.8-19.2z M678.4 236.8c-6.4 0-19.2 0-32 0-32-38.4-83.2-64-140.8-64s-108.8 25.6-140.8 64c-12.8 0-19.2 0-32 0-96 0-166.4 76.8-166.4 166.4 0 64 38.4 121.6 89.6 153.6v230.4c0 19.2 6.4 32 19.2 44.8s25.6 19.2 44.8 19.2h364.8c32 0 64-25.6 64-64v-230.4c57.6-25.6 89.6-89.6 89.6-147.2 12.8-96-64-172.8-160-172.8zM729.6 787.2c0 19.2-12.8 32-32 32h-371.2c-6.4 0-19.2-6.4-25.6-6.4-6.4-6.4-6.4-12.8-6.4-25.6v-57.6h428.8v57.6zM736 531.2c0 0 0 0 0 0-6.4 0-6.4 6.4-6.4 6.4s0 6.4 0 6.4c0 0 0 0 0 0v153.6h-435.2v-153.6c0 0 0 0 0 0s0-6.4 0-6.4c0 0 0-6.4-6.4-6.4 0 0 0 0 0 0-51.2-19.2-83.2-70.4-83.2-128 0-76.8 64-140.8 140.8-140.8 0 0 6.4 0 6.4 0-12.8 32-25.6 64-25.6 96 0 6.4 6.4 12.8 12.8 12.8s12.8-6.4 12.8-12.8c0-83.2 70.4-153.6 153.6-153.6s153.6 70.4 153.6 153.6c0 6.4 6.4 12.8 12.8 12.8s12.8-6.4 12.8-12.8c0-32-6.4-64-25.6-89.6 0 0 6.4 0 6.4 0 76.8 0 140.8 64 140.8 140.8 12.8 51.2-19.2 96-70.4 121.6z">
                                                                        </path>
                                                                    </svg></div>
                                                                <div class="ant-steps-item-description">
                                                                    <h2 class="text-center">
                                                                        <span>Preparing</span></h2>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ant-steps-item  nav-item {{$classfor_step_three}}">
                                                        <div class="ant-steps-item-container">
                                                            <div class="ant-steps-item-tail"></div>
                                                            <div class="ant-steps-item-icon"><span class="ant-steps-icon"><span class="ant-steps-icon-dot"></span></span>
                                                            </div>
                                                            <div class="ant-steps-item-content">
                                                                <div class="ant-steps-item-title"><svg width="50" height="50" viewBox="0 0 1024 1024" class="icon-Delivery icon-tracking" id="Shopping-Cart" fill="">
                                                                        <path d="M192 192h102.4l32 64h473.6c6.4 0 19.2 0 19.2 6.4 6.4 6.4 6.4 12.8 6.4 25.6 0 6.4 0 12.8-6.4 12.8l-115.2 204.8c-6.4 12.8-12.8 19.2-25.6 25.6s-19.2 6.4-32 6.4h-236.8l-25.6 51.2v6.4c0 0 0 6.4 0 6.4s6.4 0 6.4 0h377.6v64h-384c-19.2 0-32-6.4-44.8-19.2s-19.2-25.6-19.2-44.8c0-12.8 0-19.2 6.4-32l44.8-76.8-115.2-236.8h-64v-64zM384 704c19.2 0 32 6.4 44.8 19.2s19.2 25.6 19.2 44.8c0 19.2-6.4 32-19.2 44.8s-25.6 19.2-44.8 19.2c-19.2 0-32-6.4-44.8-19.2s-19.2-25.6-19.2-44.8c0-19.2 6.4-32 19.2-44.8s25.6-19.2 44.8-19.2zM704 704c19.2 0 32 6.4 44.8 19.2s19.2 25.6 19.2 44.8c0 19.2-6.4 32-19.2 44.8s-25.6 19.2-44.8 19.2c-19.2 0-32-6.4-44.8-19.2s-19.2-25.6-19.2-44.8c0-19.2 6.4-32 19.2-44.8s25.6-19.2 44.8-19.2z">
                                                                        </path>
                                                                    </svg></div>
                                                                <div class="ant-steps-item-description">
                                                                    <h2 class="text-center"><span>Ready for collection</span></h2>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ant-steps-item nav-item {{$classfor_step_four}}">
                                                        <div class="ant-steps-item-container">
                                                            <div class="ant-steps-item-tail"></div>
                                                            <div class="ant-steps-item-icon"><span class="ant-steps-icon"><span class="ant-steps-icon-dot"></span></span>
                                                            </div>
                                                            <div class="ant-steps-item-content">
                                                                <div class="ant-steps-item-title"><svg width="50" height="50" viewBox="0 0 1024 1024" class="icon-Home icon-tracking" id="Home" fill="">
                                                                        <path d="M800 512c6.4 6.4 6.4 6.4 6.4 12.8s0 6.4-6.4 12.8c0 0-6.4 0-6.4 6.4 0 0-6.4 0-6.4 0s-6.4 0-6.4 0c0 0-6.4 0-6.4-6.4l-25.6-25.6v275.2c0 6.4 0 6.4-6.4 12.8s-6.4 6.4-12.8 6.4h-128c-6.4 0-6.4 0-12.8-6.4s-6.4-6.4-6.4-12.8v-179.2h-147.2v179.2c0 6.4 0 6.4-6.4 12.8s-6.4 6.4-12.8 6.4h-128c-6.4 0-12.8 0-12.8-6.4-6.4-6.4-6.4-6.4-6.4-12.8v-275.2l-25.6 25.6c-6.4 6.4-6.4 6.4-12.8 6.4s-6.4 0-12.8-6.4-6.4-6.4-6.4-12.8 0-6.4 6.4-12.8l275.2-288c0 0 6.4-6.4 6.4-6.4s6.4 0 6.4 0c0 0 6.4 0 6.4 0s6.4 0 6.4 6.4l281.6 288zM710.4 480c0 0 0 0 0 0 0-6.4 0-6.4 0 0l-198.4-211.2-198.4 211.2c0 0 0 0 0 0s0 0 0 0v288h89.6v-179.2c0-6.4 0-6.4 6.4-12.8s6.4-6.4 12.8-6.4h179.2c6.4 0 6.4 0 12.8 6.4s6.4 6.4 6.4 12.8v179.2h89.6v-288z">
                                                                        </path>
                                                                    </svg></div>
                                                                <div class="ant-steps-item-description">
                                                                    <h2 class="text-center"><span>Order
                                                                            Completed</span></h2>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  @else
                                                    <div class="ant-steps-item nav-item {{$classfor_step_one}}">
                                                        <div class="ant-steps-item-container">
                                                            <div class="ant-steps-item-tail"></div>
                                                            <div class="ant-steps-item-icon"><span class="ant-steps-icon"><span class="ant-steps-icon-dot"></span></span>
                                                            </div>
                                                            <div class="ant-steps-item-content">
                                                                <div class="ant-steps-item-title"><span class="icon"><svg width="50" height="50" viewBox="0 0 1024 1024" class="icon-Double-Tick icon-tracking" id="Double-Tick" fill="">
                                                                            <path d="M678.4 364.526l-38.4-38.4-179.2 172.8 38.4 38.4 179.2-172.8zM793.6 326.126l-294.4 294.4-115.2-115.2-38.4 38.4 153.6 153.6 332.8-332.8-38.4-38.4zM192 543.726l153.6 153.6 38.4-38.4-153.6-153.6-38.4 38.4z">
                                                                            </path>
                                                                        </svg></span></div>
                                                                <div class="ant-steps-item-description">
                                                                    <h2 class="text-center word-break">
                                                                        <span>Order Confirmed</span></h2>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ant-steps-item nav-item  {{$classfor_step_two}}">
                                                        <div class="ant-steps-item-container">
                                                            <div class="ant-steps-item-tail"></div>
                                                            <div class="ant-steps-item-icon"><span class="ant-steps-icon"><span class="ant-steps-icon-dot"></span></span>
                                                            </div>
                                                            <div class="ant-steps-item-content">
                                                                <div class="ant-steps-item-title"><svg width="50" height="50" viewBox="0 0 1024 1024" class="icon-Chef icon-tracking" id="Chef" fill="">
                                                                        <path d="M358.4 499.2c0-6.4-6.4-12.8-12.8-12.8s-19.2 0-19.2 12.8c0 38.4 44.8 96 51.2 102.4 0 6.4 6.4 6.4 12.8 6.4s6.4 0 12.8-6.4 6.4-12.8 0-19.2c-19.2-25.6-44.8-64-44.8-83.2z M678.4 480c-6.4 0-12.8 6.4-12.8 12.8 0 19.2-25.6 64-44.8 83.2-6.4 6.4-6.4 19.2 0 19.2 0 0 6.4 6.4 12.8 6.4s6.4 0 12.8-6.4 51.2-64 51.2-102.4c0-6.4-6.4-12.8-19.2-12.8z M512 480c-6.4 0-12.8 6.4-12.8 12.8v89.6c0 6.4 6.4 12.8 12.8 12.8s12.8-6.4 12.8-12.8v-83.2c0-12.8-6.4-19.2-12.8-19.2z M678.4 236.8c-6.4 0-19.2 0-32 0-32-38.4-83.2-64-140.8-64s-108.8 25.6-140.8 64c-12.8 0-19.2 0-32 0-96 0-166.4 76.8-166.4 166.4 0 64 38.4 121.6 89.6 153.6v230.4c0 19.2 6.4 32 19.2 44.8s25.6 19.2 44.8 19.2h364.8c32 0 64-25.6 64-64v-230.4c57.6-25.6 89.6-89.6 89.6-147.2 12.8-96-64-172.8-160-172.8zM729.6 787.2c0 19.2-12.8 32-32 32h-371.2c-6.4 0-19.2-6.4-25.6-6.4-6.4-6.4-6.4-12.8-6.4-25.6v-57.6h428.8v57.6zM736 531.2c0 0 0 0 0 0-6.4 0-6.4 6.4-6.4 6.4s0 6.4 0 6.4c0 0 0 0 0 0v153.6h-435.2v-153.6c0 0 0 0 0 0s0-6.4 0-6.4c0 0 0-6.4-6.4-6.4 0 0 0 0 0 0-51.2-19.2-83.2-70.4-83.2-128 0-76.8 64-140.8 140.8-140.8 0 0 6.4 0 6.4 0-12.8 32-25.6 64-25.6 96 0 6.4 6.4 12.8 12.8 12.8s12.8-6.4 12.8-12.8c0-83.2 70.4-153.6 153.6-153.6s153.6 70.4 153.6 153.6c0 6.4 6.4 12.8 12.8 12.8s12.8-6.4 12.8-12.8c0-32-6.4-64-25.6-89.6 0 0 6.4 0 6.4 0 76.8 0 140.8 64 140.8 140.8 12.8 51.2-19.2 96-70.4 121.6z">
                                                                        </path>
                                                                    </svg></div>
                                                                <div class="ant-steps-item-description">
                                                                    <h2 class="text-center">
                                                                        <span>Preparing</span></h2>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @if($order->table_id == 0)
                                                    <div class="ant-steps-item  nav-item {{$classfor_step_three}}">
                                                        <div class="ant-steps-item-container">
                                                            <div class="ant-steps-item-tail"></div>
                                                            <div class="ant-steps-item-icon"><span class="ant-steps-icon"><span class="ant-steps-icon-dot"></span></span>
                                                            </div>
                                                            <div class="ant-steps-item-content">
                                                                <div class="ant-steps-item-title"><svg width="50" height="50" viewBox="0 0 1024 1024" class="icon-Delivery icon-tracking" id="Shopping-Cart" fill="">
                                                                        <path d="M192 192h102.4l32 64h473.6c6.4 0 19.2 0 19.2 6.4 6.4 6.4 6.4 12.8 6.4 25.6 0 6.4 0 12.8-6.4 12.8l-115.2 204.8c-6.4 12.8-12.8 19.2-25.6 25.6s-19.2 6.4-32 6.4h-236.8l-25.6 51.2v6.4c0 0 0 6.4 0 6.4s6.4 0 6.4 0h377.6v64h-384c-19.2 0-32-6.4-44.8-19.2s-19.2-25.6-19.2-44.8c0-12.8 0-19.2 6.4-32l44.8-76.8-115.2-236.8h-64v-64zM384 704c19.2 0 32 6.4 44.8 19.2s19.2 25.6 19.2 44.8c0 19.2-6.4 32-19.2 44.8s-25.6 19.2-44.8 19.2c-19.2 0-32-6.4-44.8-19.2s-19.2-25.6-19.2-44.8c0-19.2 6.4-32 19.2-44.8s25.6-19.2 44.8-19.2zM704 704c19.2 0 32 6.4 44.8 19.2s19.2 25.6 19.2 44.8c0 19.2-6.4 32-19.2 44.8s-25.6 19.2-44.8 19.2c-19.2 0-32-6.4-44.8-19.2s-19.2-25.6-19.2-44.8c0-19.2 6.4-32 19.2-44.8s25.6-19.2 44.8-19.2z">
                                                                        </path>
                                                                    </svg></div>
                                                                <div class="ant-steps-item-description">

                                                                    
                                                                        <h2 class="text-center"><span>Driver Assigned</span></h2>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <div class="ant-steps-item nav-item {{$classfor_step_four}}">
                                                        <div class="ant-steps-item-container">
                                                            <div class="ant-steps-item-tail"></div>
                                                            <div class="ant-steps-item-icon"><span class="ant-steps-icon"><span class="ant-steps-icon-dot"></span></span>
                                                            </div>
                                                            <div class="ant-steps-item-content">
                                                                <div class="ant-steps-item-title"><svg width="50" height="50" viewBox="0 0 1024 1024" class="icon-Home icon-tracking" id="Home" fill="">
                                                                        <path d="M800 512c6.4 6.4 6.4 6.4 6.4 12.8s0 6.4-6.4 12.8c0 0-6.4 0-6.4 6.4 0 0-6.4 0-6.4 0s-6.4 0-6.4 0c0 0-6.4 0-6.4-6.4l-25.6-25.6v275.2c0 6.4 0 6.4-6.4 12.8s-6.4 6.4-12.8 6.4h-128c-6.4 0-6.4 0-12.8-6.4s-6.4-6.4-6.4-12.8v-179.2h-147.2v179.2c0 6.4 0 6.4-6.4 12.8s-6.4 6.4-12.8 6.4h-128c-6.4 0-12.8 0-12.8-6.4-6.4-6.4-6.4-6.4-6.4-12.8v-275.2l-25.6 25.6c-6.4 6.4-6.4 6.4-12.8 6.4s-6.4 0-12.8-6.4-6.4-6.4-6.4-12.8 0-6.4 6.4-12.8l275.2-288c0 0 6.4-6.4 6.4-6.4s6.4 0 6.4 0c0 0 6.4 0 6.4 0s6.4 0 6.4 6.4l281.6 288zM710.4 480c0 0 0 0 0 0 0-6.4 0-6.4 0 0l-198.4-211.2-198.4 211.2c0 0 0 0 0 0s0 0 0 0v288h89.6v-179.2c0-6.4 0-6.4 6.4-12.8s6.4-6.4 12.8-6.4h179.2c6.4 0 6.4 0 12.8 6.4s6.4 6.4 6.4 12.8v179.2h89.6v-288z">
                                                                        </path>
                                                                    </svg></div>
                                                                <div class="ant-steps-item-description">
                                                                    <h2 class="text-center"><span>Order
                                                                            Completed</span></h2>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                  @endif
                                              </div>
                                          </div>
                                      </div>
                                  </section>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row my-3">
                      <div class="col order-concerns">
                          <p><span>If you have any questions, one of our live agents will be happy to
                                  help.</span> <a class="live-chat"><span>Help</span></a></p>
                      </div>
                  </div>
              </section>
          </div>
          <div class="col col-lg-4 col-12 px-3 px-lg-0 adjustmargintop">
              <div class="link-background box-shadow order-status order-summary" id="order-reciept">
                  <div class="row ">
                      <div class="col-8">
                          <h3 class="mt-3 mb-2"><span>Order Summary</span></h3>
                      </div>
                      <div class="col-auto ml-auto align-self-center help"><span>Help</span></div>
                  </div>
                  <div class="row">
                      <div class="col order-summary-background">
                          <p><svg width="23" height="23" viewBox="0 0 1024 1024" class="icon-Delivery icon-delivery-acordian icon-description" id="Collection" fill="">
                                  <path d="M761.6 729.6l-38.4-326.4c0-6.4-6.4-12.8-19.2-12.8h-70.4c0-64-57.6-179.2-121.6-179.2-70.4 0-121.6 115.2-121.6 179.2h-76.8c-6.4 0-19.2 6.4-19.2 12.8l-38.4 326.4c0 0 0 0 0 0 0 44.8 44.8 76.8 96 76.8h313.6c51.2 6.4 96-32 96-76.8 0 0 0 0 0 0zM512 249.6c44.8 0 76.8 96 83.2 134.4h-166.4c6.4-38.4 38.4-134.4 83.2-134.4zM652.8 755.2h-281.6c-32 0-51.2-19.2-51.2-38.4l32-275.2h38.4v44.8c0 6.4 12.8 12.8 19.2 12.8s19.2-6.4 19.2-12.8v-44.8h160v44.8c0 6.4 12.8 12.8 19.2 12.8s19.2-6.4 19.2-12.8v-44.8h38.4l32 275.2c6.4 25.6-12.8 38.4-44.8 38.4z">
                                  </path>
                              </svg>
                              <span> <span>Order Id</span>: <span>{{$order->id ?? ''}}</span></span><br><span class="address-order-reciept" style="margin-left: 26px;"><span>Placed
                                      On</span>: <time datetime="1647871860000">
                                      {{$order->created_at->format('h:i d M Y')}}
                                    </time></span></p>

                                    @if($order->table_detail)
                                      <p><h3>Table Id: {{$order->table_detail->id ?? 'N/A'}}</h3></p>
                                    @endif
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-12">
                          <section class="basket-order-view pb-lg-2 pt-lg-2 ">
                              <div class=" basket-order-items-order-receipt" id="reciept-items" style="max-height: 323px;">
                                  <div class="items-in-basket my-2">
                                      @foreach($order->order_details as $detail)
                                      <div class="row">
                                          <div class="col-9 pl-lg-0 pl-0">
                                              <p class="title-menu">{{$detail->quantity}} <span class="quantity_x">x</span>{{$detail->product_name}} </p>
                                          </div>
                                          <div class="col col-3">
                                              <p class="menu-price pr-1"><span>{{$currency_default.$detail->price}}</span></p>
                                          </div>
                                      </div>
                                          @php
                                            $addons = json_decode($detail->extras);
                                          @endphp
                                          @if(is_array($addons))
                                            @foreach($addons as $addon)
                                              <div class="row">
                                                  <div class="col-9 pl-lg-0 pl-0">
                                                      <p class="title-menu">{{$addon->choice}} </p>
                                                  </div>
                                                  <div class="col col-3"></div>
                                              </div>
                                            @endforeach
                                          @endif
                                      @endforeach
                                  </div>
                              </div>
                              <div class="total_value">
                                  <div class="row">
                                      <div class="col mt-1 col-9">
                                          <p class="sub-total-menu-basket "><span>Sub Total</span></p>
                                      </div>
                                      <div class="col mt-1 ">

                                          <p class="menu-price-total text-right pr-1"><span>{{$currency_default.$order->total_amount_with_fee - $order->delivery_fees - $order->discounted_amount}}</span></p>
                                      </div>
                                  </div>
                                  @if($order->order_type == 'delivery')
                                  <div class="row">
                                      <div class="col col-9">
                                          <p class="other-charges ">Delivery Fees</p>
                                      </div>
                                      <div class="col">
                                          <p class=" other-charges text-right pr-1"> <span>{{$currency_default.$order->delivery_fees}}</span></p>
                                      </div>
                                  </div>
                                  @endif
                                  <div class="row">
                                      <div class="col col-9">
                                          <p class="other-charges ">Total Discount</p>
                                      </div>
                                      <div class="col">

                                          <p class=" other-charges text-right pr-1"> <span>{{$currency_default.$order->discounted_amount}}</span></p>
                                      </div>
                                  </div>
                                  <hr class="mx-0 my-2">
                                  <div class="row mb-1">
                                      <div class="col col-9">
                                          <p class="grand-total "><span>Total</span></p>
                                      </div>
                                      <div class="col">
                                          <p class="grand-total-value text-right pr-1">{{$currency_default.$order->total_amount_with_fee}}</p>
                                      </div>
                                  </div>
                                  <hr class="mx-0 my-2">
                                  <div class="row">
                                      <div class="col">
                                          <p class="pay-by "><span>Payment</span></p>
                                      </div>
                                      <div class="col">
                                          <p class="pay-by-method text-right pr-1">{{$order->payment}}</p>
                                      </div>
                                  </div>
                              </div>
                          </section>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection
