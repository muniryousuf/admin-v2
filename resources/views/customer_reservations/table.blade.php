<div class="table-responsive">
    <table class="table" id="customerReservations-table">
        <thead>
            <tr>
                <th>@lang('models/customerReservations.fields.firstname')</th>
        <th>@lang('models/customerReservations.fields.lastname')</th>
        <th>@lang('models/customerReservations.fields.phone')</th>
        <th>@lang('models/customerReservations.fields.email')</th>
        <th>@lang('models/customerReservations.fields.booking_date')</th>
        <th>@lang('models/customerReservations.fields.booking_time')</th>
        <th>@lang('models/customerReservations.fields.persons')</th>
        <th>@lang('models/customerReservations.fields.status')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($customerReservations as $customerReservations)
            <tr>
                       <td>{{ $customerReservations->firstname }}</td>
            <td>{{ $customerReservations->lastname }}</td>
            <td>{{ $customerReservations->phone }}</td>
            <td>{{ $customerReservations->email }}</td>
            <td>{{ date('Y-m-d', strtotime($customerReservations->booking_date)) }}</td>
            <td>{{ $customerReservations->booking_time }}</td>
            <td>{{ $customerReservations->persons }}</td>
            <td>{{ $customerReservations->status }}</td>
                        <td class=" text-center">
                           {!! Form::open(['route' => ['customerReservations.destroy', $customerReservations->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('customerReservations.show', [$customerReservations->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('customerReservations.edit', [$customerReservations->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div> 
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
