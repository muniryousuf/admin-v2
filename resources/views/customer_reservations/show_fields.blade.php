<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstname', __('models/customerReservations.fields.firstname').':') !!}
    <p>{{ $customerReservations->firstname }}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastname', __('models/customerReservations.fields.lastname').':') !!}
    <p>{{ $customerReservations->lastname }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', __('models/customerReservations.fields.phone').':') !!}
    <p>{{ $customerReservations->phone }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', __('models/customerReservations.fields.email').':') !!}
    <p>{{ $customerReservations->email }}</p>
</div>

<!-- Booking Date Field -->
<div class="form-group">
    {!! Form::label('booking_date', __('models/customerReservations.fields.booking_date').':') !!}
    <p>{{ $customerReservations->booking_date }}</p>
</div>

<!-- Persons Field -->
<div class="form-group">
    {!! Form::label('persons', __('models/customerReservations.fields.persons').':') !!}
    <p>{{ $customerReservations->persons }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', __('models/customerReservations.fields.status').':') !!}
    <p>{{ $customerReservations->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/customerReservations.fields.created_at').':') !!}
    <p>{{ $customerReservations->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/customerReservations.fields.updated_at').':') !!}
    <p>{{ $customerReservations->updated_at }}</p>
</div>

