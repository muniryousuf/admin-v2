<!-- Firstname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('firstname', __('models/customerReservations.fields.firstname').':') !!}
    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lastname', __('models/customerReservations.fields.lastname').':') !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', __('models/customerReservations.fields.phone').':') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', __('models/customerReservations.fields.email').':') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Booking Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('booking_date', __('models/customerReservations.fields.booking_date').':') !!}
    {!! Form::text('booking_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Persons Field -->
<div class="form-group col-sm-6">
    {!! Form::label('persons', __('models/customerReservations.fields.persons').':') !!}
    {!! Form::number('persons', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', __('models/customerReservations.fields.status').':') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('customerReservations.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
