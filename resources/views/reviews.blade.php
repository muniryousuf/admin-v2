@extends('layouts.front')
@section('content')  
<div class="root-content" style="margin-top: 60px;">
   @include('top-header')
   <div class="clearfix"></div>
   <div class="container" id="checkout-height">
      <section id="detail_view_menu">
         <div class="row feedback_view mobile_feedback_view">
            <div class="col col-lg-8 col-12 px-0 px-lg-2">
               <section class="bg-white px-4 pt-4 pb-4 mt-1 mt-lg-0 box-shadow">
                  <h3><span>Reviews</span></h3>
                  <div class="infinite-scroll-component__outerdiv">
                     <div class="infinite-scroll-component">
                        
                      @foreach($reviews as $review)
                        <div class="review-user ">
                           <div class="media mt-4">
                              <img src="images/usericon.svg" class="img-fluid mx-auto d-block mr-3" alt="User Default" title="User Default">
                              <div class="media-body ml-2">
                                 <h5 class=" col-12 mt-0 mb-0 pt-0 pt-lg-2 px-1">{{$review->name}}</h5>
                                 <div class="ratting-box">
                                    <div class="col-12 col-6 col-lg-9 px-0">
                                       <div class="star-filters">
                                          <ul>
                                            @for($i = 1; $i <= (int) $review->rating; $i++)
                                             <li><i class="fa fa-star"></i></li>
                                            @endfor
                                          </ul>
                                       </div>
                                    </div>
                                    <div class="col-12 col-6 col-lg-3 px-1 px-lg-0">
                                       <p class="date text-lg-right text-left"><time datetime="1644065371000">{{$review->created_at->format('d M Y')}}</time>
                                       </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <hr class="mx-1">
                      @endforeach
                        
                     </div>
                  </div>
               </section>
            </div>
            <div class="col col-lg-4 col-12 px-0 px-lg-2 ratings-feedback">
               <div style="position: sticky; top: 70px;">
                  <section class="bg-white px-3 pt-4 pb-4 box-shadow">
                     <div class="row">
                        <div class="col-12">
                           <h1 class="text-center mb-0">{{$ratings}}</h1>
                        </div>
                     </div>
                     <div class="row ">
                        <div class="col-12 d-flex justify-content-center ratings-feedback">
                           <div>
                             @for($i = 1; $i <= (int) $ratings; $i++)
                              <svg width="30" height="30" viewBox="0 0 1024 1024" class="icon-feedback" id="Star-Fill" fill="">
                                 <path d="M512 723.2l-198.4 108.8 38.4-230.4-160-166.4 224-32 96-211.2 96 211.2 224 32-160 166.4 38.4 230.4z">
                                 </path>
                              </svg>
                              @endfor
                              
                            
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-12">
                           <p class="text-center review-comments mb-1"><span>Highly Recommended</span></p>
                           <p class="text-center review-total">({{$reviews->count() ?? '0'}} Reviews)</p>
                        </div>
                     </div>
                     <hr class="mr-3">
                     <div class="row">
                        <div class="col">
                           <h3><span>Filter by</span></h3>
                        </div>
                     </div>
                     <!-- <div class="row my-1">
                        <div class="col onclick-cursor star-filters">
                           <span><i class="fa fa-circle"></i></span>
                           <ul>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                           </ul>
                           <span>All</span>
                        </div>
                     </div> -->
                     @foreach($reviews_count_array as $rating => $count)
                     <div class="row my-1">
                        <div class="col onclick-cursor star-filters">
                          @if($ratings == $rating)
                           <span><i class="fa fa-circle"></i></span>
                           @else
                           <span class="change-ratings" ref="{{$rating}}"><i class="fa-regular fa-circle"></i></span>
                           @endif
                           <ul>
                             @for($i = 1; $i <= $rating; $i++)
                              <li><i class="fa fa-star"></i></li>
                              @endfor
                           </ul>
                           <span>({{$count}})</span>
                        </div>
                     </div>
                     @endforeach
                  </section>
               </div>
            </div>
         </div>
      </section>
   </div>
</div>
@endsection

@section('scripts') 
<script type="text/javascript">
  $('.change-ratings').click(function(){
    let rating = $(this).attr('ref');
    window.location.href="{{route('reviews')}}/"+rating;

  });
</script>
@endsection