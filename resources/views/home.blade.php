@extends('layouts.app')
@section('title')
     @lang('models/dashboard.plural')
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@lang('models/dashboard.singular')</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('reset-cache')}}" class="btn btn-danger form-btn">Reset Cache</a>
            </div>
        </div>
               <div class="col-md-6">
            <div class="card ccard radius-t-0 h-100">
                <div class="position-tl w-102 border-t-3 brc-primary-tp3 ml-n1px mt-n1px"></div>
                <div class="card-header pb-3 brc-secondary-l3"><h5 class="card-title mb-2 mb-md-0 text-dark-m3">Top Products</h5></div>

                <div class="products-pills">
                    @php $count= 0; @endphp
                    @foreach($top_products as $month)

                        @foreach($month as $product)
                            @if($count < 11)
                            @php $count++; @endphp
                                <span class="btn btn-light badge-light">
                                    {{$product['name']}} <span class="badge badge-secondary">{{$product['count']}}</span>
                                </span> 
                            @endif
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card ccard radius-t-0 h-100">
                <div class="position-tl w-102 border-t-3 brc-primary-tp3 ml-n1px mt-n1px"></div>
                <div class="card-header pb-3 brc-secondary-l3"><h5 class="card-title mb-2 mb-md-0 text-dark-m3">Top Categories</h5></div>

                <div class="products-pills">
                @foreach($top_categories as $month)
                    @foreach($month as $category)
                        <span class="btn btn-light badge-light">
                            {{$category}}
                        </span> 
                    @endforeach
                @endforeach
                </div>
            </div>
        </div>
        
        <div class="col-md-12">
            <div class="main-chart-holder">
                <canvas id="order-chart" height="280" width="600"></canvas>
            </div>
        </div>
  
    </section>
@endsection
@section('scripts')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>
     //#########   orders ##############\\
    var months = <?php echo $months; ?>;
    var orders = <?php echo $orders; ?>;
    var barColors = ["red", "green","blue","orange","brown"];
    var barChartData = {
        labels: months,
        datasets: [{
            label: 'orders',
            backgroundColor: barColors,
            data: orders
        }]
    };

    //#########   orders ##############\\



    window.onload = function() {
         //#########   orders ##############\\
        var octx = document.getElementById("order-chart").getContext("2d");
        new Chart(octx, {
            type: 'bar',
            data: barChartData,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        // borderColor: '#c1c1c1',
                        // borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Orders of {{date("Y")}}'
                }
            }
        });
         //#########   orders ##############\\
       
    };

</script>
@endsection


