<div class="table-responsive">
    <table class="table" id="resturentTimings-table">
        <thead>
            <tr>
                <th>@lang('models/resturentTimings.fields.day')</th>
        <th>@lang('models/resturentTimings.fields.start_time')</th>
        <th>@lang('models/resturentTimings.fields.end_time')</th>
        <th>@lang('models/resturentTimings.fields.shop_close')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($resturentTimings as $resturentTimings)
            <tr>
                       <td>{{ $resturentTimings->day }}</td>
            <td>{{ $resturentTimings->start_time }}</td>
            <td>{{ $resturentTimings->end_time }}</td>
            <td>{{ $resturentTimings->shop_close }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['resturentTimings.destroy', $resturentTimings->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('resturentTimings.show', [$resturentTimings->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('resturentTimings.edit', [$resturentTimings->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
