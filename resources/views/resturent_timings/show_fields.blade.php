<!-- Day Field -->
<div class="form-group">
    {!! Form::label('day', __('models/resturentTimings.fields.day').':') !!}
    <p>{{ $resturentTimings->day }}</p>
</div>

<!-- Start Time Field -->
<div class="form-group">
    {!! Form::label('start_time', __('models/resturentTimings.fields.start_time').':') !!}
    <p>{{ $resturentTimings->start_time }}</p>
</div>

<!-- End Time Field -->
<div class="form-group">
    {!! Form::label('end_time', __('models/resturentTimings.fields.end_time').':') !!}
    <p>{{ $resturentTimings->end_time }}</p>
</div>

<!-- Shop Close Field -->
<div class="form-group">
    {!! Form::label('shop_close', __('models/resturentTimings.fields.shop_close').':') !!}
    <p>{{ $resturentTimings->shop_close }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/resturentTimings.fields.created_at').':') !!}
    <p>{{ $resturentTimings->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/resturentTimings.fields.updated_at').':') !!}
    <p>{{ $resturentTimings->updated_at }}</p>
</div>

