<!-- Day Field -->
<div class="form-group col-sm-6">
    @php $selected_day = $resturentTimings->day ?? ''; @endphp
    {!! Form::label('day', __('models/resturentTimings.fields.day').':') !!}
    <select name="day" class="form-control">
        @foreach(App\Models\ResturentTimings::DAYS  as $day)
            <option value="{{$day}}" {{$selected_day == $day ? 'selected' : ''}}>{{$day}}</option>
        @endforeach
    </select>
</div>

<!-- Start Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_time', __('models/resturentTimings.fields.start_time').':') !!}
    {!! Form::time('start_time', null, ['class' => 'form-control','id'=>'start_time']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#start_time').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- End Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_time', __('models/resturentTimings.fields.end_time').':') !!}
    {!! Form::time('end_time', null, ['class' => 'form-control','id'=>'end_time']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#end_time').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Shop Close Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shop_close', __('models/resturentTimings.fields.shop_close').':') !!}
    <label class="checkbox-inline">

        <input  name="shop_close" type="checkbox" value="1" id="shop_close" {{isset($resturentTimings->shop_close)  && $resturentTimings->shop_close == 0 ? 'checked="checked"' : ''}}>
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('resturentTimings.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
