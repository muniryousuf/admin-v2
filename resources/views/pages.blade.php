@extends('layouts.front')
@section('content')
	@if($page->page_type == 'informative')

	<div class="root-content {{$page->name}}-page-class" style="margin-top: 60px;">
		@include('top-header')
		<div class="container px-0 px-lg-2">
			<div class="row">
				<div class="col-lg-12 col-xl-12 col-12 mt-3">
					<section id="email_us" class="link-background info-status box-shadow p-lg-3 p-2 mt-2 mt-lg-0 mb-3">
						@php echo $page->html ?? '';  @endphp
					</section>
				</div>
			</div>
		</div>
	</div>
	@else

			@php echo $page->html ?? '';  @endphp

	@endif
@endsection


@if($page->page_type == 'informative')

	@section('scripts')
	 <script type="text/javascript">
	 	$('body').attr('style','')
	 		@if($page->background_type == 'no_background')
	 		 $("body").attr('style','background:none');
	 		@endif	

	 		@if($page->background_type == 'color')
	 		 $("body").attr('style','background:{{$page->background_type_value}}');
	 		@endif	
	 		@if($page->background_type == 'image')
	 		
	 		 $("body").attr('style','background: url("{{asset('images/'.$page->background_type_value)}}")');
	 		@endif	


	 </script>
	@endsection

@endif