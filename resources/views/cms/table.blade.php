<div class="table-responsive">
    <table class="table" id="cms-table">
        <thead>
            <tr>
                <th>@lang('models/cms.fields.title')</th>
        <th>@lang('models/cms.fields.content')</th>
        <th>@lang('models/cms.fields.slug')</th>
        <th>@lang('models/cms.fields.meta_title')</th>
        <th>@lang('models/cms.fields.meta_desc')</th>
        <th>@lang('models/cms.fields.index_follow')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($cms as $cms)
            <tr>
                       <td>{{ $cms->title }}</td>
            <td>{{ $cms->content }}</td>
            <td>{{ $cms->slug }}</td>
            <td>{{ $cms->meta_title }}</td>
            <td>{{ $cms->meta_desc }}</td>
            <td>{{ $cms->index_follow }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['cms.destroy', $cms->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('cms.show', [$cms->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('cms.edit', [$cms->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
