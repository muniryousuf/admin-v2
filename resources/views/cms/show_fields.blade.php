<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', __('models/cms.fields.title').':') !!}
    <p>{{ $cms->title }}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', __('models/cms.fields.content').':') !!}
    <p>{{ $cms->content }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', __('models/cms.fields.slug').':') !!}
    <p>{{ $cms->slug }}</p>
</div>

<!-- Meta Title Field -->
<div class="form-group">
    {!! Form::label('meta_title', __('models/cms.fields.meta_title').':') !!}
    <p>{{ $cms->meta_title }}</p>
</div>

<!-- Meta Desc Field -->
<div class="form-group">
    {!! Form::label('meta_desc', __('models/cms.fields.meta_desc').':') !!}
    <p>{{ $cms->meta_desc }}</p>
</div>

<!-- Index Follow Field -->
<div class="form-group">
    {!! Form::label('index_follow', __('models/cms.fields.index_follow').':') !!}
    <p>{{ $cms->index_follow }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/cms.fields.created_at').':') !!}
    <p>{{ $cms->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/cms.fields.updated_at').':') !!}
    <p>{{ $cms->updated_at }}</p>
</div>

