<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', __('models/cms.fields.title').':') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', __('models/cms.fields.content').':') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control' ,'id'=>"wysiwg-textarea"]) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', __('models/cms.fields.slug').':') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Meta Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meta_title', __('models/cms.fields.meta_title').':') !!}
    {!! Form::text('meta_title', null, ['class' => 'form-control']) !!}
</div>

<!-- Meta Desc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meta_desc', __('models/cms.fields.meta_desc').':') !!}
    {!! Form::text('meta_desc', null, ['class' => 'form-control']) !!}
</div>

<!-- Index Follow Field -->
<div class="form-group col-sm-6">
    {!! Form::label('index_follow', __('models/cms.fields.index_follow').':') !!}
    {!! Form::text('index_follow', null, ['class' => 'form-control']) !!}
</div>
<!-- Index Follow Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image_1', __('models/galleries.fields.image').':') !!}
    {!! Form::file('image_1') !!}
     @if(isset($cms->image_1))
    <img class="image-preview" src="{{asset('images/'.$cms->image_1)}}" />
    @endif
</div>
<div class="form-group col-sm-6">
     {!! Form::label('image_2', __('models/galleries.fields.image').':') !!}
    {!! Form::file('image_2') !!}

     @if(isset($cms->image_2))
    <img class="image-preview" src="{{asset('images/'.$cms->image_2)}}" />
    @endif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('cms.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
