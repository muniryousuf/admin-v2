@extends('layouts.front')
@section('content')
    <div class="root-content" style="margin-top: 60px;">
       @include('top-header')
        <div class="container order-online-page">
            <div class="row gutter-sm">
                <div class="col-lg-8 payment-type-box mb-0">
                    <div class="item-main-heading">
                        Payment
                    </div>
                    <div class="payment-type-tab">
                        <div class="payment-tab active">
                            <label>Cash</label>
                            <input type="radio" name="paymenttype" checked="checked" value="cash" />
                        </div>

                        @foreach($paymentMethods as $method)
                            <div class="payment-tab">
                                <label><i class="fa fa-credit-card"></i>{{$method->name}}</label>
                                <input type="radio" name="paymenttype" value="{{$method->name}}"  />
                            </div>
                        @endforeach

                    </div>
                    <div class="payment-type-detail">
                        <div id="payment-stripe" style="display: none" class="eachpaymenttype mt-5">

                            <div class="card-body">

                                <div id="card-element"></div>
                                <form id="stripe" method="post">
                                    <div class="col-md-12 px-0">
                                        <div class="row">
                                            <input id="card-holder-name" type="text">

                                            <input name="pmethod" type="hidden" id="pmethod" value="" />
                                            <div class="col-md-12 px-0 ">
                                                <div class="offset-md-3 col-md-6 col-12 text-center mt-lg-3 my_profile_pop_up px-0 px-lg-3">
                                                    <button id="card-button" class="btn-block btn-lg save-btn"><span>Pay Now</span></button></div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                        <div id="payment-cash" class="eachpaymenttype mt-5" style="display: none;">
                            <div class="row">
                                <div class="col-md-12 px-0 ">
                                    <div class="offset-md-3 col-md-6 col-12 text-center mt-lg-3 my_profile_pop_up px-0 px-lg-3">
                                        <button type="button" id="payment-bycash" class="btn-block btn-lg save-btn"><span>Place Order</span></button></div>
                                </div>
                            </div>
                        </div>
                        <div id="payment-Card" class="eachpaymenttype mt-5" style="display: none;">
                            <p>{{$settings['rider_dilver_message']}}</p>
                            <div class="row">
                                <div class="col-md-12 px-0 ">
                                    <div class="offset-md-3 col-md-6 col-12 text-center mt-lg-3 my_profile_pop_up px-0 px-lg-3">
                                        <button type="button" id="payment-bycard" class="btn-block btn-lg save-btn"><span>Place Order</span></button></div>
                                </div>
                            </div>
                        </div>
                        <div id="payment-cardstream" style="display: none" class="eachpaymenttype mt-5">

                            <div class="card-body">
                                 <form>
                                    <div class="col-md-12 px-0">
                                        <div class="row">
                                            <div class=" col-md-12 pl-lg-0 px-0 pr-lg-3 mb-3">
                                                <div class="field">
                                                    <input type="text" id="stream_card_number" required name="stream_card_number" autocomplete="off" class="to_capitalize" placeholder="Card Number" maxlength="19">
                                                        <label for="stream_card_number">
                                                        <span>Card Number</span>
                                                        <sup class="mandatory-field">*</sup></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 px-0 ">
                                        <div class="row">
                                            <div class=" col-md-4 pl-lg-0 px-0 pr-lg-3 mb-0">
                                                <div class="field">
                                                    <select name="stream_month_selection" id="stream_month_selection" required>
                                                        @foreach ($months as $month)
                                                            <option value="{{$month}}">{{$month}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="stream_month_selection" style="background: rgb(255, 255, 255);">
                                                    <span>Month</span>
                                                    <sup class="mandatory-field">*</sup></label>
                                                </div>
                                            </div>
                                            <div class=" col-md-4 pl-lg-0 px-0 pr-lg-3 mb-0">
                                                <div class="field">
                                                    <select id="stream_expiration_years" name="stream_expiration_years">
                                                        @foreach ($expiration_years as $year)
                                                            <option value="{{$year}}">{{$year}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="stream_expiration_years" style="background: rgb(255, 255, 255);">
                                                    <span>Year</span>
                                                    <sup class="mandatory-field">*</sup></label>
                                                </div>
                                            </div>
                                            <div class=" col-md-4 pl-lg-0 px-0 pr-lg-3 mb-3">
                                                <div class="field">
                                                    <input type="text" id="stream-cvv" name="stream-cvv" autocomplete="off" class="to_capitalize" placeholder="CVV" maxlength="4">
                                                        <label for="stream-cvv">
                                                        <span>CVV</span>
                                                        <sup class="mandatory-field">*</sup></label>
                                                </div>
                                            </div>


                                            <div class="col-md-12 px-0 ">
                                                <div class="offset-md-3 col-md-6 col-12 text-center mt-lg-3 my_profile_pop_up px-0 px-lg-3">
                                                    <button type="button" id="stream_pay_now" class="btn-block btn-lg save-btn"><span>Pay Now</span></button></div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4 basket-wrapper">
                    <div class="basket-wrapper-inner">
                        <section id="cart-Basket" class="basket-order-view bg-white">
                            <div class="d-flex cart_head">
                                <h3><span>Order Summary</span></h3>
                            </div>
                            <div id="basket-body">
                                <div class="row no-gutters" id="basketmoredetails">
                                    <div class="col-7 text-left px-0">
                                    <p>Items</p>
                                    </div>
                                    @if(request()->table_id == null)
                                    <div class="col-5 pr-0 text-right"><span id="total-items">0</span></div>
                                    <div class="col-7 text-left px-0">
                                    <p>Delivery Fees</p>
                                    </div>
                                    @endif

                                    <div class="col-5 pr-0 text-right"><span id="delivery-fees">
                                        <span class="loading"><span class="loading-bar"></span><span class="loading-bar"></span><span class="loading-bar"></span><span class="loading-bar"></span><span class="loading-bar"></span></span></span></div>
                                    <div class="col-7 text-left px-0">
                                        <p>Total Discount</p>
                                    </div>
                                    <div class="col-5 pr-0 text-right"><span id="total-discount-text">
                                        <span class="loading"><span class="loading-bar"></span><span class="loading-bar"></span><span class="loading-bar"></span><span class="loading-bar"></span><span class="loading-bar"></span></span></span></div>

                                    @if(request()->table_id == null)
                                    <div class="col-7 text-left px-0">
                                    <p>Address</p>
                                    </div>
                                    <div class="col-5 pr-0 text-right"><span id="address">
                                        <span class="loading"><span class="loading-bar"></span><span class="loading-bar"></span><span class="loading-bar"></span><span class="loading-bar"></span><span class="loading-bar"></span></span></span></div>
                                    @endif

                                    @if(request()->table_id > 0)

                                        <div class="col-7 text-left px-0">
                                            <p>Table Id</p>
                                        </div>
                                        <div class="col-5 pr-0 text-right">
                                            <span>{{request()->table_id}}</span>
                                        </div>
                                    @endif


                                    <div class="col-12 total-amount">
                                        <div class="row">
                                            <div class="col-7 text-left px-0">
                                                <p><strong>Total</strong></p>
                                            </div>
                                            <div class="col-5 pr-0 text-right">
                                                <span id="total-amount"><strong>0</strong></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </div>
            </div>
          </div>
    </div>
@endsection
@section('scripts')
<script src="https://js.stripe.com/v3/"></script>
<script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
<script>

    var total_amount = 0;
    var cart = {};
    var default_currecny = $('#default-currency').val();

    function placeOrder(cart){
        let _token = "{{csrf_token()}}";
        let table_id = {{request()->table_id ?? 0}}
        cart._token =_token;
        cart.table_id =table_id;
        cart.postal_code = localStorage.getItem('postal_code') ? localStorage.getItem('postal_code') : 0;
        cart.special_instructions = localStorage.getItem('allergy_instruction') ? localStorage.getItem('allergy_instruction') : null;
        let order = {};
        $.ajax({
            type:'POST',
            url:"{{route('web.place-order')}}",
            data:cart,
            dataType:'json',
            async:false,
            success:function(response) {
                order = response.data
            }
        });
        return order;
    }

    $(document).ready(function(){

        cart = fetchCartWithProducts(localStorage.getItem('id_cart'));

        $(document).on('click', '.payment-type-tab .payment-tab', function(e) {
            $('.payment-type-tab .payment-tab.active').removeClass('active');
            $(this).addClass('active');
            var paymentTypeName = $(this).find('input[name="paymenttype"]').val();
            console.log(paymentTypeName);
            $(".eachpaymenttype").hide();
            $("#payment-" + paymentTypeName).show();
        });

        $("#payment-cash").show()

        $(document).on('click', '#payment-bycash', function (e) {
            e.preventDefault();
            $(this).attr('id','');
            $(this).text('Please Wait..');

            let checkout_link = "{{route('checkout')}}";

            setTimeout(() => {
                let order = placeOrder(cart.data);

                if(order.reference){
                    localStorage.removeItem('id_cart');
                    window.location.href = checkout_link+'/'+order.reference
                }
            }, 100);

        })
        $(document).on('click', '#payment-bycard', function (e) {
            e.preventDefault();
            $(this).attr('id','');
            $(this).text('Please Wait..');
            let checkout_link = "{{route('checkout')}}";
            cart.data.payment = "creditcard";
            let order = placeOrder(cart.data);
            if(order.reference){
                localStorage.removeItem('id_cart');
                window.location.href = checkout_link+'/'+order.reference
            }
        })

        /* Card Stream Implementation */

        $(document).on('keyup', '#stream_card_number', function (e) {
            let text = this.value.split(" ").join("")
            //this.cardVdid is not formated in 4 spaces
            this.cardVadid = text
            if (text.length > 0) {
                //regExp 4 in 4 number add an space between
                text = text.match(new RegExp(/.{1,4}/, 'g')).join(" ")
                    //accept only numbers
                    .replace(new RegExp(/[^\d]/, 'ig'), " ");
            }
            //this.cardNumber is formated on 4 spaces
            this.value = text
        })

        $(document).on('click', '#stream_pay_now', function(e) {

           if($("#stream_card_number").val() == "") {
               return swal('',"Valid Card Number Is Required",'error');
           } else if($("#stream-cvv").val() == "") {
                return swal('',"Valid CVV Is Required",'error');
           } else {

               var data = {
                   card_number : $("#stream_card_number").val(),
                   card_cvv : $("#stream-cvv").val(),
                   card_month : $("#stream_month_selection").val(),
                   card_year : $("#stream_expiration_years").val(),
                   total_amount: total_amount
               }

               $.ajax({
                   type:'POST',
                   url:"{{route('api.stream_payment_process')}}",
                   data: data,
                   success:function(response) {
                       let checkout_link = "{{route('checkout')}}";
                       cart.data.payment = "creditcard";
                       let order = placeOrder(cart.data);

                       if(order.reference){
                            localStorage.removeItem('id_cart');
                            window.location.href = checkout_link+'/'+order.reference
                        }
                   },
                   error:function(jqXHR, textStatus, errorThrown) {

                       let errorResponse  = JSON.parse(jqXHR.responseText);

                       if(jqXHR.statusCode != 402) {
                           return swal('',errorResponse.error.message,'error');
                       } else {

                           console.log(errorResponse.error)

                       }
                   }

               });

           }

        })

    });

    function fetchCartWithProducts(id_cart){
        let return_data = [];
        let item_count = 0;
        $('#cart-items').html('');
        $.ajax({
            type:'GET',
            url:"{{route('api.get-cart')}}/"+id_cart,
            dataType:'json',
            async:false,
            success:function(response) {

                return_data = response

                let items = response.data.cart_products
                let total_price = 0;
                for(i in items){
                    total_price += items[i].price * items[i].quantity;
                    item_count += items[i].quantity;
                }

                if(response.data.delivery_fees == null){
                    response.data.delivery_fees = 0;
                }

                total_amount = parseFloat(parseFloat(total_price)+parseFloat(response.data.delivery_fees)).toFixed(2);

                let final_price = parseFloat(total_price)+parseFloat(response.data.delivery_fees)-parseFloat(response.data.total_discount)
                $('#total-amount').text(default_currecny+(final_price).toFixed(2) );
                $('#delivery-fees').text(default_currecny+response.data.delivery_fees);
                $('#total-discount-text').text(default_currecny+(response.data.total_discount).toFixed(2));


                // $('#total-amount').text(default_currecny+total_amount)
                // $('#delivery-fees').text(default_currecny+parseFloat(response.data.delivery_fees).toFixed(2))
                $('#total-items').text(item_count)
                $('#address').text(response.data.delivery_address)
             },
            error:function(response){
            }
        });

        return return_data;
    }

    const stripe = Stripe('{{ env("STRIPE_KEY") }}');
    const elements = stripe.elements();
    const cardElement = elements.create('card');
    cardElement.mount('#card-element');
    const cardHolderName = document.getElementById('card-holder-name');
    const form = document.getElementById('stripe');

    form.addEventListener('submit', async (e) => {
        e.preventDefault();
        const { paymentMethod, error } = await stripe.createPaymentMethod(
            'card', cardElement, {
                billing_details: { name: cardHolderName.value }
            }
        );
        if (error) {
            return swal('',error.message,'error');
        } else {
            console.log('Card verified successfully');
            document.getElementById('pmethod').setAttribute('value', paymentMethod.id);

            $.ajax({
                type:'POST',
                url:"{{route('api.payment_process')}}",
                data:{ pmethod:paymentMethod.id, total_amount: total_amount},
                success:function(response) {
                    let checkout_link = "{{route('checkout')}}";
                    cart.data.payment = "creditcard";
                    let order = placeOrder(cart.data);
                    if(order.reference){
                        localStorage.removeItem('id_cart');
                        window.location.href = checkout_link+'/'+order.reference
                    }
                },
                error:function(jqXHR, textStatus, errorThrown) {

                    let errorResponse  = JSON.parse(jqXHR.responseText);

                    stripe.confirmCardPayment(errorResponse.error.payment_data.client_secret, {
                        payment_method: errorResponse.error.payment_data.payment_method,
                    }).then(function(result) {

                        if(result.paymentIntent.status === "succeeded") {
                            let checkout_link = "{{route('checkout')}}";
                            let order = placeOrder(cart.data);
                            if(order.reference){
                                localStorage.removeItem('id_cart');
                                window.location.href = checkout_link+'/'+order.reference
                            }
                        }
                    });
                }

            });
        }
    });

</script>

@endsection
@section('page_css')
    <link href="{{ asset('assets/css/sweetalert.css') }}" rel="stylesheet" type="text/css"/>
@endsection


