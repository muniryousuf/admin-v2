@extends('layouts.front')
@section('content')
<section class="additional-header header-banner" style="margin-top: 60px;">
    <div class="container d-flex" id="my_profile">
        <div class="col-md-3 col-sm-3 adjustmargintop d-none d-lg-block">
            <div class="bg-white-tabs box-shadow">
                <p class="pt-0 mb-0"><span>My Profile</span></p>
                @include('userarea.sidebar')
            </div>
        </div>
        <div class="col col-lg-9 col-sm-12 px-0 px-sm-3 adjustmargintop">
            <div id="content" class="tab-content" role="tablist">
                <div class="bg-white-content box-shadow">
                    @include('userarea.orders')
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
@endsection
@section('scripts')
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
  <script type="text/javascript">
    $(document).ready( function () {
      $('#table_id').DataTable();
    });
    $(document).on('click','.re-order',function(){
        let ref  = $(this).attr('ref');
        let request_data = {
            ref: ref,
            _token: '{{ csrf_token() }}'
        };
        $.ajax({
            type:'POST',
            url:"{{route('get-cart-id')}}",
            data:request_data,
            dataType:'json',
            async:false,
            success:function(response) {
              localStorage.setItem('id_cart',response.id_cart)
              window.location.href="{{route('order-online')}}"
            },
            error:function(response) {
            },
        });  
    })
  </script>
@endsection