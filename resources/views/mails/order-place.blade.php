<html lang="en">
   <head>
      <meta charset="UTF-8">
      <title>Order Confirmation Email</title>
      <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <meta content="text/html charset=UTF-8" http-equiv="Content-Type">
      <link href="https://fonts.googleapis.com" rel="preconnect">
      <link crossorigin="" href="https://fonts.gstatic.com" rel="preconnect">
      <style>
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 400;
         font-display: swap;
         src: url('https://fonts.gstatic.com/s/poppins/v15/pxiEyp8kv8JHgFVrFJA.ttf') format('truetype');
         }
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 500;
         font-display: swap;
         src: url('https://fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLGT9V1s.ttf') format('truetype');
         }
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 600;
         font-display: swap;
         src: url('https://fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLEj6V1s.ttf') format('truetype');
         }
         html[lang="ar"] .main-wrapper{
         direction: rtl;
         }
         @media  screen and (max-device-width: 600px) {
         .order-content .logo img {
         width: 180px;
         }
         h3 {
         font-size: 40px !important;
         }
         p {
         font-size: 20px !important;
         line-height: 26px !important;
         }
         h2 {
         font-size: 24px !important;
         }
         thead.order-number-top td {
         padding-left: 15px !important;
         font-size: 16px;
         }
         td.prod-title-desc h5 {
         font-size: 16px !important;
         line-height: 22px !important;
         }
         p.estimated-delivery span {
         display: block;
         }
         td.prod-price {
         font-size: 20px !important;
         }
         td.prod-price del {
         font-size: 16px !important;
         }
         td.prod-qty span {
         font-size: 18px !important;
         line-height: 18px !important;
         }
         table.order-address h6 {
         font-size: 20px !important;
         line-height: 24px !important;
         }
         table.order-address p {
         max-width: 100% !important;
         }
         table.order-address td:nth-child(1) {
         width: 100% !important;
         }
         .order-address tr {
         display: flex;
         flex-flow: column;
         }
         }
         @media  screen and (max-width: 600px) {
         .order-content .logo img {
         width: 180px;
         }
         h3 {
         font-size: 40px !important;
         }
         p {
         font-size: 20px !important;
         line-height: 26px !important;
         }
         h2 {
         font-size: 24px !important;
         }
         thead.order-number-top td {
         padding-left: 15px !important;
         font-size: 16px;
         }
         td.prod-title-desc h5 {
         font-size: 16px !important;
         line-height: 22px !important;
         }
         p.estimated-delivery span {
         display: block;
         }
         td.prod-price {
         font-size: 20px !important;
         }
         td.prod-price del {
         font-size: 16px !important;
         }
         td.prod-qty span {
         font-size: 18px !important;
         line-height: 18px !important;
         }
         table.order-address h6 {
         font-size: 20px !important;
         line-height: 24px !important;
         }
         table.order-address p {
         max-width: 100% !important;
         }
         table.order-address td:nth-child(1) {
         width: 100% !important;
         }
         .order-address tr {
         display: flex;
         flex-flow: column;
         }
         }
      </style>
   </head>
   <body data-new-gr-c-s-check-loaded="14.1057.0" data-gr-ext-installed="">
      <div class="main-wrapper" style="width: 750px; max-width: 100%; background-color: #F2F6FA; font-family: 'Poppins', sans-serif; margin: 0 auto; padding: 68px 0px 70px;">
         <table class="wrapper" style="width: 600px; max-width: 100%; border-spacing: 0px; margin: 0 auto; ">
            <tbody>
               <tr>
                  <td>
                     <table bgcolor="#fff" class="order-content" style="width: 100%; border-spacing: 0; box-shadow: 0px 2px 6px #0000000D; border-radius: 10px;">
                        <thead class="logo" style="text-align: center;">
                           <tr>
                              <td align="center" style="border-bottom-width: 1px; border-bottom-color: #e5eaef; border-bottom-style: solid; padding: 20px;" valign="middle">
                                 <img style="max-width: 100px;" alt="Logo" src="{{asset('images/'.$settings['header_logo'] ?? '')}}">
                              </td>
                           </tr>
                        </thead>
                        <tbody class="text-content">
                           <tr>
                              <td style="padding: 30px 30px 0px;">
                                 <h3 style="font-size: 22px; font-weight: 500; line-height: 33px; margin-top: 0px; margin-bottom: 0px; position: relative;">
                                    Thank you for your order!
                                 </h3>
                              </td>
                           </tr>
                           <tr>
                              <td style="padding: 30px;">
                                 <p style="margin-top: 0px; margin-bottom: 20px; color: #1C3047; font-size: 16px; line-height: 24px; font-weight: 400;">
                                    Dear {{$order->user_detail->name ?? ''}} ,
                                 </p>
                                 <p style="margin-top: 0px; margin-bottom: 20px; color: #1C3047; font-size: 16px; line-height: 24px; font-weight: 400;">
                                    Thank you for placing your order # <span class="semibold-weight" style="font-weight: 600;">{{$order->reference ?? '' }}</span> with us at {{$settings['site_name'] ?? ''}}!
                                 </p>
                                 @if($order->status == 'pending')
                                 <p class="medium-weight" style="margin-top: 0px; margin-bottom: 20px; color: #1C3047; font-size: 16px; line-height: 24px; font-weight: 500;">We have received your order</p>
                                 <p style="margin-top: 0px; margin-bottom: 20px; color: #1C3047; font-size: 16px; line-height: 24px; font-weight: 400;">We will send you a delivery confirmation soon.</p>
                                 @endif
                                 @if($order->status == 'accepted')
                                 <p class="medium-weight" style="margin-top: 0px; margin-bottom: 20px; color: #1C3047; font-size: 16px; line-height: 24px; font-weight: 500;">Your order is accepted.</p>
                                 @endif
                                 @if($order->status == 'preparing')
                                 <p class="medium-weight" style="margin-top: 0px; margin-bottom: 20px; color: #1C3047; font-size: 16px; line-height: 24px; font-weight: 500;">We are preparing you order</p>
                                 @endif
                                 @if($order->status == 'prepared')
                                 <p class="medium-weight" style="margin-top: 0px; margin-bottom: 20px; color: #1C3047; font-size: 16px; line-height: 24px; font-weight: 500;">Your Order is prepared</p>
                                 @endif

                                 @if($order->status == 'refund')
                                 <p class="medium-weight" style="margin-top: 0px; margin-bottom: 20px; color: #1C3047; font-size: 16px; line-height: 24px; font-weight: 500;">Your Order is Refunded</p>
                                 @endif 
                                 @if($order->status == 'driver_assigned')
                                 <p class="medium-weight" style="margin-top: 0px; margin-bottom: 20px; color: #1C3047; font-size: 16px; line-height: 24px; font-weight: 500;">Your Order is Assigned to driver</p>
                                 @endif 
                                 @if($order->status == 'declined')
                                 <p class="medium-weight" style="margin-top: 0px; margin-bottom: 20px; color: #1C3047; font-size: 16px; line-height: 24px; font-weight: 500;">Your Order is Declined</p>
                                 @endif

                                 @if($order->status == 'completed' || $order->status == 'deliverd')
                                 <p class="medium-weight" style="margin-top: 0px; margin-bottom: 20px; color: #1C3047; font-size: 16px; line-height: 24px; font-weight: 500;">Your Order is {{ucfirst($order->status)}}</p>
                                 @endif
                                 @if($order->status == 'picked_up')
                                 <p class="medium-weight" style="margin-top: 0px; margin-bottom: 20px; color: #1C3047; font-size: 16px; line-height: 24px; font-weight: 500;">Your Order is Picked Up</p>
                                 @endif

                                 <p style="margin-bottom: 5px; margin-top: 0px; color: #1C3047; font-size: 16px; line-height: 24px; font-weight: 400;">Kind regards,</p>
                                 <p style="margin-bottom: 0px; margin-top: 0px; color: #1C3047; font-size: 16px; line-height: 24px; font-weight: 400;">Your {{$settings['site_name'] ?? ''}} Team</p>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </td>
               </tr>
               <tr>
                   <td>
                      <table bgcolor="#fff" cellpadding="30px" class="order-detail" style="width: 100%; margin-top: 25px; border-radius: 10px; border-spacing: 0px; box-shadow: 0px 2px 6px #0000000D; padding: 0px 30px 30px;">
                         <thead style="padding-bottom: 25px; padding-top: 23px;">
                            <tr class="order-top">
                               <td class="border-bottom" colspan="3" style="border-bottom-width: 1px; border-bottom-color: #e5eaef; border-bottom-style: solid; padding-left: 15px; padding-right: 15px;">
                                  <h2 align="center" style="color: #4C657A; font-weight: 500; font-size: 20px; line-height: 30px; margin-bottom: 0px; margin-top: 0px;">Order Detail</h2>
                               </td>
                            </tr>
                            <tr class="order-middle" style="border-spacing: 4px;">
                               <td style="padding: 13px 15px;">
                                  <p style="color: #8D9FB0; font-size: 12px; line-height: 18px; font-weight: 500; margin: 0;">Order #
                                     <span style="font-size: 14px; line-height: 21px; color: #304961; font-weight: 400; margin-left: 10px;">{{$order->reference}}</span>
                                  </p>
                               </td>
                               <td style="padding: 13px 15px;">
                                  <p style="color: #8D9FB0; font-size: 12px; line-height: 18px; font-weight: 500; margin: 0;">Date <span style="font-size: 14px; line-height: 21px; color: #304961; font-weight: 400; margin-left: 10px;">
                                      {{$order->craeted_at}}
                                  </span>
                                  </p>
                               </td>
                               <td style="padding: 13px 15px;">
                                  <p style="color: #8D9FB0; font-size: 12px; line-height: 18px; font-weight: 500; margin: 0;">Total <span style="font-size: 14px; line-height: 21px; color: #304961; font-weight: 400; margin-left: 10px;">{{$settings['currency']}} {{$order->total_amount_with_fee}}</span>
                                  </p>
                               </td>
                            </tr>
                         </thead>
                         <tbody>
                            <tr>
                               <td colspan="3" style="padding: 0px;">
                                  <table class="order-products" style="width: 100%; border-spacing: 0px; border-radius: 5px; margin-top: 0px;">
                                     <tbody>
                                        <tr>
                                            @foreach($order->order_details as $product)
                                           <td class="prod-title-desc" style="border-top-width: 1px; border-top-color: #e5eaef; border-top-style: solid; padding: 7px 5px;" valign="middle">
                                              <h5 style="line-height: 20px; font-size: 14px; font-weight: 500; color: #1C3047; margin: 0px 0;">
                                                 {{$product->product_name}}
                                              </h5>
                                           </td>
                                           <td class="prod-qty" style="border-top-width: 1px; border-top-color: #e5eaef; border-top-style: solid; padding: 7px 5px;" valign="middle">
                                              <span style="background-color: #F2F6FA; border-radius: 5px; min-width: 25px; text-align: center; color: #687E94; font-size: 14px; line-height: 16px; font-weight: 500; width: auto; display: inline-block; margin: 0 3px; padding: 5px 3px;">x {{$product->quantity}}</span>
                                           </td>
                                           <td align="right" class="prod-price" style="border-top-width: 1px; border-top-color: #e5eaef; border-top-style: solid; font-size: 16px; line-height: 16px; padding: 7px 5px;" valign="middle">
                                              <ins style="color: #36AAAB; font-weight: 600; display: block; text-decoration: none; margin-top: 5px; white-space: nowrap;">
                                              {{$settings['currency']}} {{$product->price}}
                                              </ins>
                                           </td>
                                           @endforeach
                                        </tr>

                                     </tbody>
                                  </table>
                               </td>
                            </tr>
                            <tr>
                               <td colspan="3" style="padding: 0px;">
                                  <table class="order-summary border-top" style="width: 100%; border-top-width: 1px; border-top-color: #e5eaef; border-top-style: solid; padding-top: 30px;">
                                     <thead style="padding-bottom: 25px; padding-top: 23px;">
                                        <tr>
                                           <td style="padding: 10px 0px;">
                                              <h6 style="font-size: 16px; line-height: 20px; color: #687E94; font-weight: 600; margin: 0;">
                                                 Summary
                                              </h6>
                                           </td>
                                           <td align="right" style="padding: 10px 0px;"></td>
                                        </tr>
                                     </thead>
                                     <tbody>
                                        <tr>
                                           <td style="padding: 10px 0px;">
                                              <h5 style="font-size: 16px; line-height: 8px; color: #1C3047; font-weight: 400; margin: 0;">
                                                 Items
                                              </h5>
                                           </td>
                                           <td align="right" style="padding: 10px 0px;">
                                              <h5 class="medium-weight" style="font-weight: 500; font-size: 16px; line-height: 8px; color: #1C3047; margin: 0;">
                                                 {{$settings['currency']}} {{$order->total_amount_with_fee - $order->delivery_fees + $order->discounted_amount}}
                                              </h5>
                                           </td>
                                        </tr>
                                        <tr>
                                           <td style="padding: 10px 0px;">
                                              <h5 style="font-size: 16px; line-height: 8px; color: #1C3047; font-weight: 400; margin: 0;">
                                                 Delivery fees
                                              </h5>
                                           </td>
                                           <td align="right" style="padding: 10px 0px;">
                                              <h5 class="medium-weight" style="font-weight: 500; font-size: 16px; line-height: 8px; color: #1C3047; margin: 0;">
                                                 {{$settings['currency']}} {{$order->delivery_fees}}
                                              </h5>
                                           </td>
                                        </tr>
                                        <tr>
                                           <td style="padding: 10px 0px;">
                                              <h5 style="font-size: 16px; line-height: 8px; color: #1C3047; font-weight: 400; margin: 0;">
                                                 Coupon discount
                                              </h5>
                                           </td>
                                           <td align="right" style="padding: 10px 0px;">
                                              <h5 class="medium-weight" style="font-weight: 500; font-size: 16px; line-height: 8px; color: #1C3047; margin: 0;">
                                                 {{$settings['currency']}} {{$order->discounted_amount}}
                                              </h5>
                                           </td>
                                        </tr>
                                     </tbody>
                                     <tfoot style="border-spacing: 0px;">
                                        <tr>
                                           <td class="border-top border-bottom" style="border-bottom-width: 1px; border-bottom-color: #e5eaef; border-bottom-style: solid; border-top-width: 1px; border-top-color: #e5eaef; border-top-style: solid; padding: 10px 0px;">
                                              <h4 style="font-size: 18px; line-height: 18px; font-weight: 600; letter-spacing: 0.43px; color: #1C3047; margin: 0;">
                                                 Total
                                              </h4>
                                           </td>
                                           <td align="right" class="border-top border-bottom" style="border-bottom-width: 1px; border-bottom-color: #e5eaef; border-bottom-style: solid; border-top-width: 1px; border-top-color: #e5eaef; border-top-style: solid; padding: 10px 0px;">
                                              <h4 style="font-size: 18px; line-height: 18px; font-weight: 600; letter-spacing: 0.43px; color: #1C3047; margin: 0;">
                                                 {{$settings['currency']}} {{$order->total_amount_with_fee + $order->delivery_fees - $order->discounted_amount}}
                                              </h4>
                                           </td>
                                        </tr>
                                     </tfoot>
                                  </table>
                               </td>
                            </tr>
                         </tbody>
                      </table>
                   </td>
                </tr>
            </tbody>
         </table>
      </div>
   </body>
</html>