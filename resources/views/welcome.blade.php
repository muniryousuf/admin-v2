@extends('layouts.front')
@section('content')
   <div class="root-content" style="margin-top: 60px;">
            @php
                $notification_top_data = getNotification();
            @endphp
            @if(isset($notification_top_data['message']) && $notification_top_data['message'] != '')
            <div class="announcementholder">
                <marquee direction="left" scrollamount=6>{{$notification_top_data['message'] ?? ''}}</marquee>
            </div>
            @endif
            <section class="carousel p-0">
                @php
                    $auto_rotation = '';
                    if(getSettings()['auto_rotation'] == 0){
                        $auto_rotation = 'data-interval=false';
                    }
                @endphp
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" {{$auto_rotation}}>
                    <div class="carousel-inner">
                        @foreach($sliders as $slider)
                        <div class="carousel-item {{$loop->iteration == 1 ? 'active' : ''}}">
                            <img src="{{asset('images/'.$slider->image)}}" class="img-fluid d-block mx-auto w-100" alt="Los Angeles">
                        </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </section>
            <section class="home-intro">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="resturant-box row">
                                <div class="discover-resturant-content col-md-6">
                                    <h1>{{$about_us->title ?? ''}}</h1>
                                    <p>{!! strip_tags($about_us->content ?? '') !!}</p>
                                </div>
                                <div class="col-md-6 about-imgholder">
                                    <img src="{{asset('images/'.@$about_us->image_2 ?? '')}}" alt="">
                                </div>
                                <div class="resturant-inner-right d-none">
                                    <img class="img-fluid center-block" src="{{asset('images/'.@$about_us->image_1 ?? '')}}" alt="Resturant Images">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="food-menu-section food-caoursel-item">
                <div class="container">
                    <div class="section-heading text-center">
                        <h2 class="section-title">
                            <span class="color-white">Our Menu Dishes</span>
                        </h2>
                    </div>
                    <div class="row">
                        @foreach($gallery as $image)
                        <div class="col-md-4 col-sm-4">
                            <div class="food-item">
                                <div class="food-effects section-colume-top80">
                                    <img class="img-fluid center-block" src="{{asset('images/'.$image->image)}}" alt="Food Images">
                                    <div class="food-inner-overlay">
                                        <div class="food-inner-view">
                                            <div class="food-inner-contetent">
                                                <h2 class="colume-heading">{{$image->title}}</h2>
                                            </div>
                                            <div class="food-price-circule"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach





                       <!--  <div class="col-md-4 col-sm-4">
                            <div class="food-item">
                                <div class="food-effects section-colume-top80">
                                    <img class="img-fluid center-block" src="images/img-phpiAjBDi.jpg" alt="Food Images">
                                    <div class="food-inner-overlay">
                                        <div class="food-inner-view">
                                            <div class="food-inner-contetent">
                                                <h2 class="colume-heading">Kebab</h2>
                                            </div>
                                            <div class="food-price-circule"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="food-item">
                                <div class="food-effects section-colume-top80">
                                    <img class="img-fluid center-block" src="images/img-phpKWXYEc.jpg" alt="Food Images">
                                    <div class="food-inner-overlay">
                                        <div class="food-inner-view">
                                            <div class="food-inner-contetent">
                                                <h2 class="colume-heading">Wraps</h2>
                                            </div>
                                            <div class="food-price-circule"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </section>

{{--       <section class="home-location">--}}
{{--           <div class="owl-slider">--}}
{{--               <div id="carousel" class="owl-carousel">--}}
{{--                   @foreach($gallery as $image)--}}
{{--                       <div class="item">--}}
{{--                           <img src="{{asset('images/'.$image->image)}}" alt="1000X1000">--}}
{{--                       </div>--}}
{{--                   @endforeach--}}

{{--               </div>--}}
{{--           </div>--}}
{{--       </section>--}}



            <section class="home-location">
                <div class="container">
                    <h2 class="my-heading">Contact Information</h2>
                    <div class="row align-items-center">
                        <div class="col-md-6 col-xs-12 margTop30px">
                            <div class="map-holder">
                                <iframe src="{{getSettings()['map']}}" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 margTop30px">
                            <div class="contactorderonline">
                                <h3>Order Now</h3>

                                <p><i class="fa fa-map-marker"></i><span>{{getSettings()['address'] ?? ''}}</span></p>
                                <p><i class="fa fa-envelope"></i><a href="mailto:{{getSettings()['email'] ?? ''}}"> {{getSettings()['email'] ?? ''}}</a></p>
                                <p><i class="fa fa-phone"></i><a href="tel:{{getSettings()['phone_number'] ?? ''}}">{{getSettings()['phone_number'] ?? ''}}</a></p>
                                <a class="ordernowbutton" href="{{route('order-online')}}">
                                        Order Online Now
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



        </div>




@endsection



