<div class="table-responsive">
    <table class="table" id="messageTemplatings-table">
        <thead>
            <tr>
                <th>@lang('models/messageTemplatings.fields.template_slug')</th>
        <th>@lang('models/messageTemplatings.fields.description')</th>
        <th>@lang('models/messageTemplatings.fields.status')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($messageTemplatings as $messageTemplating)
            <tr>
                       <td>{{ $messageTemplating->template_slug }}</td>
            <td>{{ $messageTemplating->description }}</td>
            <td>{{ $messageTemplating->status }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['messageTemplatings.destroy', $messageTemplating->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('messageTemplatings.show', [$messageTemplating->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('messageTemplatings.edit', [$messageTemplating->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
