<!-- Template Slug Field -->
<div class="form-group">
    {!! Form::label('template_slug', __('models/messageTemplatings.fields.template_slug').':') !!}
    <p>{{ $messageTemplating->template_slug }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', __('models/messageTemplatings.fields.description').':') !!}
    <p>{{ $messageTemplating->description }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', __('models/messageTemplatings.fields.status').':') !!}
    <p>{{ $messageTemplating->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/messageTemplatings.fields.created_at').':') !!}
    <p>{{ $messageTemplating->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/messageTemplatings.fields.updated_at').':') !!}
    <p>{{ $messageTemplating->updated_at }}</p>
</div>

