<!-- Template Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('template_slug', __('models/messageTemplatings.fields.template_slug').':') !!}
    {!! Form::text('template_slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', __('models/messageTemplatings.fields.description').':') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', __('models/messageTemplatings.fields.status').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', 0) !!}
        {!! Form::checkbox('status', '1', null) !!}
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('messageTemplatings.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
