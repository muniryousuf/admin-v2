@extends('layouts.app')
@section('title')
     @lang('models/messageTemplatings.plural')
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@lang('models/messageTemplatings.plural')</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('messageTemplatings.create')}}" class="btn btn-primary form-btn">@lang('crud.add_new')<i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('message_templatings.table')
            </div>
       </div>
   </div>
    
        @include('stisla-templates::common.paginate', ['records' => $messageTemplatings])

    </section>
@endsection



