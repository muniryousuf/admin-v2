<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/tableReservations.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Reserve Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_reserve', __('models/tableReservations.fields.is_reserve').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_reserve', 0) !!}
        {!! Form::checkbox('is_reserve', '1', null) !!}
    </label>
</div>

<!-- Reservation End Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reservation_end_time', __('models/tableReservations.fields.reservation_end_time').':') !!}
    {!! Form::date('reservation_end_time', null, ['class' => 'form-control','id'=>'reservation_end_time']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#reservation_end_time').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Reservation Start Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reservation_start_time', __('models/tableReservations.fields.reservation_start_time').':') !!}
    {!! Form::date('reservation_start_time', null, ['class' => 'form-control','id'=>'reservation_start_time']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#reservation_start_time').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush


<!-- Table Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('table_type', __('models/tableReservations.fields.table_type').':') !!}
    <select name="table_type" class="form-control">
        @foreach(App\Models\TableReservation::TABLE_TYPES as $types)
            <option value="{{$types}}">
                {{str_replace("_", "", ucfirst($types))}}
            </option>
        @endforeach
    </select>
</div>



<!-- Table Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_category', __('models/tableReservations.fields.category').':') !!}
    @php 
        $selected = '';
        if(isset($tableReservation->id_category)){
            $selected = $tableReservation->id_category;
        }
    @endphp
    
    <select name="id_category" class="form-control">
        @foreach($tableCategories as $category)
            <option value="{{$category->id}}" {{$selected == $category->id ? 'selected' : ''}}>
                {{$category->name}}
            </option>
        @endforeach
    </select>
</div>

<!-- Number Of Person Field -->
<div class="form-group col-sm-6">
    {!! Form::label('number_of_person', __('models/tableReservations.fields.number_of_person').':') !!}
    {!! Form::text('number_of_person', null, ['class' => 'form-control']) !!}
</div>

<!-- Number Of Person Sitting Field -->
<div class="form-group col-sm-6">
    {!! Form::label('number_of_person_sitting', __('models/tableReservations.fields.number_of_person_sitting').':') !!}
    {!! Form::text('number_of_person_sitting', null, ['class' => 'form-control']) !!}
</div>
@if(isset($tableReservation->id))
<div class="form-group col-sm-14" id="html-content-holder">
    @php $link = route('order-online',['table_id'=> $tableReservation->id]); @endphp
    {!! QrCode::size(200)->generate($link) !!}
</div>
<div class="form-group col-sm-12">
    <a class="btn btn-light" onclick="downloadPng()" id="btn-Convert-Html2Image" href="javascript:void(0);"> Download QR code</a>
</div>
@endif
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('tableReservations.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
