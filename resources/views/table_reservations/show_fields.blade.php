<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/tableReservations.fields.name').':') !!}
    <p>{{ $tableReservation->name }}</p>
</div>

<!-- Is Reserve Field -->
<div class="form-group">
    {!! Form::label('is_reserve', __('models/tableReservations.fields.is_reserve').':') !!}
    <p>{{ $tableReservation->is_reserve }}</p>
</div>

<!-- Reservation End Time Field -->
<div class="form-group">
    {!! Form::label('reservation_end_time', __('models/tableReservations.fields.reservation_end_time').':') !!}
    <p>{{ $tableReservation->reservation_end_time }}</p>
</div>

<!-- Reservation Start Time Field -->
<div class="form-group">
    {!! Form::label('reservation_start_time', __('models/tableReservations.fields.reservation_start_time').':') !!}
    <p>{{ $tableReservation->reservation_start_time }}</p>
</div>

<!-- Id Category Field -->
<div class="form-group">
    {!! Form::label('id_category', __('models/tableReservations.fields.id_category').':') !!}
    <p>{{ $tableReservation->id_category }}</p>
</div>

<!-- Table Type Field -->
<div class="form-group">
    {!! Form::label('table_type', __('models/tableReservations.fields.table_type').':') !!}
    <p>{{ $tableReservation->table_type }}</p>
</div>

<!-- Number Of Person Field -->
<div class="form-group">
    {!! Form::label('number_of_person', __('models/tableReservations.fields.number_of_person').':') !!}
    <p>{{ $tableReservation->number_of_person }}</p>
</div>

<!-- Number Of Person Sitting Field -->
<div class="form-group">
    {!! Form::label('number_of_person_sitting', __('models/tableReservations.fields.number_of_person_sitting').':') !!}
    <p>{{ $tableReservation->number_of_person_sitting }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/tableReservations.fields.created_at').':') !!}
    <p>{{ $tableReservation->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/tableReservations.fields.updated_at').':') !!}
    <p>{{ $tableReservation->updated_at }}</p>
</div>

