<div class="table-responsive">
    <table class="table" id="tableReservations-table">
        <thead>
            <tr>
                <th>@lang('models/tableReservations.fields.name')</th>
        <th>@lang('models/tableReservations.fields.is_reserve')</th>
        <th>@lang('models/tableReservations.fields.reservation_end_time')</th>
        <th>@lang('models/tableReservations.fields.reservation_start_time')</th>
        <th>@lang('models/tableReservations.fields.id_category')</th>
        <th>@lang('models/tableReservations.fields.table_type')</th>
        <th>@lang('models/tableReservations.fields.number_of_person')</th>
        <th>@lang('models/tableReservations.fields.number_of_person_sitting')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($tableReservations as $tableReservation)
            <tr>
                       <td>{{ $tableReservation->name }}</td>
            <td>{{ $tableReservation->is_reserve }}</td>
            <td>{{ $tableReservation->reservation_end_time }}</td>
            <td>{{ $tableReservation->reservation_start_time }}</td>
            <td>{{ $tableReservation->id_category }}</td>
            <td>{{ $tableReservation->table_type }}</td>
            <td>{{ $tableReservation->number_of_person }}</td>
            <td>{{ $tableReservation->number_of_person_sitting }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['tableReservations.destroy', $tableReservation->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('tableReservations.show', [$tableReservation->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('tableReservations.edit', [$tableReservation->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
