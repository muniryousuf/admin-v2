@extends('layouts.app')
@section('title')
    @lang('crud.edit') @lang('models/tableReservations.singular')
@endsection
@section('content')
    <section class="section">
            <div class="section-header">
                <h3 class="page__heading m-0">@lang('crud.edit') @lang('models/tableReservations.singular')</h3>
                <div class="filter-container section-header-breadcrumb row justify-content-md-end">
                    <a href="{{ route('tableReservations.index') }}"  class="btn btn-primary">@lang('crud.back')</a>
                </div>
            </div>
    
  <div class="content">
              @include('stisla-templates::common.errors')
              <div class="section-body">
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-body ">
                                    {!! Form::model($tableReservation, ['route' => ['tableReservations.update', $tableReservation->id], 'method' => 'patch']) !!}
                                        <div class="row">
                                            @include('table_reservations.fields')
                                         
                                        </div>

                                    {!! Form::close() !!}
                            </div>
                         </div>
                    </div>
                 </div>
              </div>
   </div>
  </section>
@endsection
@section('scripts')
 <script>
    downloadPng=function(){
        var img = new Image();
        img.onload = function (){
            var canvas = document.createElement("canvas");
            canvas.width = img.naturalWidth;
            canvas.height = img.naturalHeight;
            var ctxt = canvas.getContext("2d");
            ctxt.fillStyle = "#fff";
            ctxt.fillRect(0, 0, canvas.width, canvas.height);
                ctxt.drawImage(img, 0, 0);
            var a = document.createElement("a");
            a.href = canvas.toDataURL("image/png");
            a.download = "image.png"
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
        };
       var innerSvg = document.querySelector("#html-content-holder svg");
       var svgText = (new XMLSerializer()).serializeToString(innerSvg);
       img.src = "data:image/svg+xml;utf8," + encodeURIComponent(svgText);  
    }   
</script>
@endsection