<section class="bg-white additional-header header-banner">
  @php 
    $settings = getSettings(); 
    $reviews_data_for_header = getTotalReviews();
  @endphp
      <div class="container">
          <div class="row row-mobile align-items-center">
              <div class="col col-lg-auto ta-details">
                  <div class="row align-items-center">
                      <div class="col-lg col-8">
                          <h4>{{$settings['site_name'] ?? ''}}</h4>
                      </div>
                      <div class="col-4 d-flex align-items-center justify-content-end d-lg-none MinOrderType">
                          <div class="hb-offer hb-info-wrapper d-flex flex-column mb-0">
                              <span class="col-auto text-center m-auto offer-percent badge mb-1"></span>
                              <span class="col-auto text-center m-auto p-0 " style="top: 0.49rem;"></span>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-6">
                  <ul class="list-inline d-flex align-items-center hb-list mb-0">
                  <li class="hb-info-wrapper list-inline-item d-flex flex-column justify-content-center open-review-page">
                      <div class="hb-info-inner ml-0 mx-lg-auto"><a>
                          <div class="hb-info-top d-flex align-items-center"><svg width="20" height="20" viewBox="0 0 1024 1024" class="updated-svg-icons" id="Star-Stoke-1" fill="">
                              <path d="M881.433 434.595c0-17.592-14.074-31.666-35.184-35.184l-207.586-28.147-91.479-186.476c-7.037-14.074-21.11-24.629-35.184-24.629s-28.147 10.555-35.184 24.629l-91.479 186.476-204.068 31.666c-21.11 3.518-35.184 17.592-35.184 35.184 0 14.074 7.037 24.629 14.074 31.666l147.773 144.255-35.184 207.586c0 3.518 0 7.037 0 10.555 0 21.11 14.074 35.184 31.666 35.184 10.555 0 17.592-3.518 24.629-7.037l182.957-94.997 182.957 94.997c7.037 3.518 17.592 7.037 24.629 7.037 17.592 0 31.666-14.074 31.666-35.184 0-3.518 0-7.037 0-10.555l-35.184-204.068 147.773-144.255c10.555-17.592 17.592-28.147 17.592-38.702zM842.73 445.15l-154.81 151.292c-3.518 3.518-3.518 7.037-3.518 14.074l35.184 211.104c0 3.518 0 3.518 0 7.037s0 7.037-3.518 7.037c-3.518 0-7.037 0-10.555-3.518l-189.994-98.515c-3.518 0-3.518 0-7.037 0s-3.518 0-7.037 0l-189.994 102.034c-3.518 3.518-7.037 3.518-10.555 3.518 0-3.518 0-7.037 0-10.555s0-3.518 0-7.037l35.184-211.104c0-3.518 0-10.555-3.518-14.074l-154.81-151.292c-7.037-7.037-7.037-10.555-7.037-10.555s3.518-3.518 10.555-3.518l214.623-31.666c3.518 0 10.555-3.518 10.555-7.037l94.997-193.512c7.037-7.037 10.555-10.555 10.555-10.555s3.518 3.518 7.037 10.555l94.997 193.512c3.518 3.518 7.037 7.037 10.555 7.037l214.623 31.666c7.037 0 10.555 3.518 10.555 3.518s0 0 0 0 0 3.518-7.037 10.555z">
                              </path>
                          </svg>
                          <div class="txt-block">
                              <h6 class="hb-info mb-0">{{$reviews_data_for_header['average_rating']}}</h6>
                          </div>
                          </div>
                          <p class="mb-0 hb-info-title d-none d-lg-block text-underline"> {{$reviews_data_for_header['total_reviews']}} <span>Reviews</span></p>
                      </a></div>
                  </li>
                  @if($settings['is_pickup'] == 1)
                    <li class="hb-info-wrapper list-inline-item d-flex flex-column justify-content-center">
                        <div class="hb-info-inner mx-auto">
                        <div class="hb-info-top d-flex align-items-center">
                            <div class="icon-block"><svg width="22" height="22" viewBox="0 0 1024 1024" class="updated-svg-icons" id="Collection-fhbanner" fill="">
                                <path d="M763.733 388.267c0-8.533-8.533-14.933-23.467-14.933h-78.933c0-74.667-70.4-209.067-145.067-209.067-83.2 0-147.2 134.4-147.2 209.067h-87.467c-8.533 0-23.467 8.533-23.467 14.933l-44.8 381.867c0 53.333 53.333 89.6 113.067 89.6h366.933c59.733 8.533 113.067-38.4 113.067-89.6l-42.667-381.867zM516.267 204.8c55.467 0 93.867 117.333 102.4 166.4h-204.8c6.4-49.067 46.933-166.4 102.4-166.4zM706.133 817.067h-379.733c-42.667 0-68.267-38.4-68.267-61.867l42.667-345.6h66.133v61.867c0 8.533 14.933 14.933 23.467 14.933s23.467-8.533 23.467-14.933v-61.867h198.4v61.867c0 8.533 14.933 14.933 23.467 14.933s23.467-8.533 23.467-14.933v-61.867h61.867l42.667 345.6c10.667 32-14.933 61.867-57.6 61.867z">
                                </path>
                            </svg></div>
                            <div class="txt-block">
                            <h6 class="hb-info mb-0">{{$settings['min_collection_time'] ?? ''}} mins</h6>
                            </div>
                        </div>
                        <p class="mb-0 hb-info-title d-none d-lg-block">Collection</p>
                        </div>
                    </li>
                  @endif
                  @if($settings['is_delivery'] == 1)
                    <li class="hb-info-wrapper list-inline-item d-flex flex-column justify-content-center">
                        <div class="hb-info-inner mx-auto">
                        <div class="hb-info-top d-flex align-items-center">
                            <div class="icon-block"><svg width="23" height="23" viewBox="0 0 1024 1024" class="updated-svg-icons" id="Bike-01" fill="">
                                <path d="M883.2 682.667c0 44.8-36.267 81.067-81.067 81.067-36.267 0-68.267-25.6-76.8-59.733l142.933-68.267c10.667 14.933 14.933 29.867 14.933 46.933v0zM691.2 678.4l-12.8 6.4c-2.133 0-4.267 0-4.267 0-2.133 0-2.133-2.133-4.267-4.267l-2.133-4.267c-2.133-4.267-6.4-14.933-19.2-14.933h-36.267c4.267-6.4 8.533-14.933 10.667-23.467l2.133-8.533c10.667-40.533 23.467-83.2 51.2-106.667l2.133-2.133c10.667-10.667 12.8-23.467 8.533-36.267l-40.533-155.733v-59.733c0 0 0-2.133 2.133-2.133h102.4c2.133 2.133 4.267 8.533 4.267 14.933 0 12.8-2.133 29.867-17.067 29.867s-21.333 6.4-25.6 10.667c-10.667 12.8-8.533 32-6.4 34.133l53.333 155.733c2.133 8.533 8.533 14.933 17.067 17.067 51.2 10.667 85.333 23.467 104.533 38.4 2.133 2.133 4.267 4.267 6.4 4.267h2.133c2.133 2.133 4.267 4.267 4.267 8.533 0 2.133 0 4.267-4.267 6.4l-14.933 6.4-183.467 85.333zM405.333 665.6h-258.133c-4.267 0-6.4-4.267-6.4-6.4v-4.267c6.4-89.6 83.2-160 174.933-160h157.867c0 4.267-2.133 6.4-4.267 8.533l-2.133 2.133c-38.4 36.267-40.533 108.8-21.333 153.6 2.133 2.133 2.133 4.267 4.267 6.4h-44.8zM296.533 763.733c-38.4 0-70.4-27.733-78.933-66.133h155.733c-4.267 38.4-36.267 66.133-76.8 66.133v0zM247.467 456.533v-25.6c0-61.867 51.2-113.067 113.067-113.067s113.067 51.2 113.067 113.067v25.6h-226.133zM925.867 588.8c2.133-12.8-2.133-25.6-12.8-34.133-2.133-2.133-2.133-2.133-4.267-4.267 0 0-2.133-2.133-2.133-2.133l-2.133-2.133c-32-23.467-93.867-38.4-117.333-44.8l-53.333-157.867c0-4.267 0-8.533 2.133-10.667 0 0 4.267-2.133 4.267-2.133 29.867 0 46.933-23.467 46.933-59.733 0-17.067-8.533-32-14.933-38.4-2.133-2.133-6.4-6.4-17.067-6.4h-108.8c-17.067 0-32 14.933-32 32v64l42.667 170.667v2.133c0 0 0 4.267-2.133 6.4-38.4 29.867-51.2 78.933-64 125.867l-2.133 8.533c-6.4 19.2-23.467 32-42.667 32h-34.133c-17.067 0-29.867-6.4-34.133-19.2-17.067-42.667-10.667-102.4 17.067-128l2.133-2.133c8.533-8.533 12.8-19.2 12.8-29.867v0-53.333c0-76.8-59.733-140.8-134.4-147.2 6.4-4.267 10.667-10.667 10.667-19.2 0-12.8-10.667-21.333-21.333-21.333-12.8 0-21.333 10.667-21.333 21.333 0 8.533 4.267 14.933 10.667 19.2-74.667 6.4-134.4 70.4-134.4 147.2v51.2c-61.867 34.133-104.533 96-110.933 168.533v6.4c0 19.2 17.067 36.267 36.267 36.267h46.933c8.533 53.333 55.467 98.133 110.933 98.133 57.6 0 104.533-42.667 110.933-98.133h228.267c0 0 0 0 0 0v0c2.133 8.533 10.667 17.067 19.2 21.333 4.267 2.133 10.667 4.267 14.933 4.267s10.667-2.133 14.933-4.267l6.4-4.267c14.933 46.933 57.6 78.933 108.8 78.933 61.867 0 113.067-51.2 113.067-113.067 0-21.333-6.4-42.667-17.067-59.733l6.4-4.267c8.533-4.267 14.933-14.933 17.067-27.733v0z">
                                </path>
                            </svg></div>
                            <div class="txt-block">
                            <h6 class="hb-info mb-0">{{$settings['min_delivery_time'] ?? ''}} mins</h6>
                            </div>
                        </div>
                        <p class="mb-0 hb-info-title d-none d-lg-block">Delivery</p>
                        </div>
                    </li>
                  @endif
                  <li class="hb-info-wrapper list-inline-item d-flex flex-column justify-content-center" id=minimum-order-area>
                        <div class="hb-info-inner mx-auto">
                        <div class="hb-info-top d-flex align-items-center">
                            <div class="icon-block">
                              {{$settings['currency'] ?? ''}}
                            </div>
                            <div class="txt-block">
                            <h6 class="hb-info mb-0">{{$settings['minimum_amount_to_order'] ?? ''}}</h6>
                            </div>
                        </div>
                        <p class="mb-0 hb-info-title d-none d-lg-block">Minimum Order Amount For Delivery</p>
                        </div>
                    </li>

                  </ul>
              </div>
              @if(Request::is('order-online*'))
                <div class="col-auto d-lg-flex d-none order-type review-info ml-auto">
                    <div class="mr-3 d-none d-lg-block mobile-orderNow deliveryselector">
                        <a class="lang-dropdown nav-link basket-icon dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" id="navaccount" href="#">
                        <span class="lang-active" id="current-serving-method"></span></a>
                        <ul class="resp-navbar-nav-right-basket dropdown-menu" role="menu">
                          @if($settings['is_pickup'] == 1)
                            <a class="dropdown-item serving-method" data="collection">Collection</a>
                          @endif
                          @if($settings['is_delivery'] == 1)
                            <a class="dropdown-item serving-method" data="delivery">Delivery</a>
                          @endif
                        </ul>
                      </div>
                </div>
              @else  
                <div class="col-auto d-lg-flex d-none order-type review-info ml-auto">
                    <div class="row float-lg-right">
                        <div class="col-auto align-items-center text-center"></div>
                        <div class="col-auto align-items-center text-center">
                            <div class="row d-none d-lg-block mobile-orderNow">
                            <div class=""><a href="{{route('order-online')}}"  class=" nav-link basket-icon detailview-order"><span>Order Now</span></a></div>
                            </div>
                        </div>
                    </div>
                </div>
              @endif
              
          </div>
      </div>
  </section>