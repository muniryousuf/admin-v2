@extends('layouts.front')
@section('content')
<section class="additional-header header-banner" style="margin-top: 60px;">
    <div class="container d-flex" id="my_profile">
        <div class="col-md-3 col-sm-3 adjustmargintop d-none d-lg-block">
            <div class="bg-white-tabs box-shadow">
                <p class="pt-0 mb-0"><span>My Profile</span></p>
                @include('userarea.sidebar')
            </div>
        </div>
        <div class="col col-lg-9 col-sm-12 px-0 px-sm-3 adjustmargintop">
            <div id="content" class="tab-content" role="tablist">
                <div class="bg-white-content box-shadow">
                    @include('userarea.account-details')
                </div>
            </div>
        </div>
    </div>
</section>
@endsection