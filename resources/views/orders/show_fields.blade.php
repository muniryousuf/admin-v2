<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', __('models/orders.fields.order_id').':') !!}
    <p>{{ $orders->order_id }}</p>
</div>

<!-- Reference Field -->
<div class="form-group">
    {!! Form::label('Reference', __('models/orders.fields.Reference').':') !!}
    <p>{{ $orders->Reference }}</p>
</div>

<!-- Total Amount Field -->
<div class="form-group">
    {!! Form::label('total_amount', __('models/orders.fields.total_amount').':') !!}
    <p>{{ $orders->total_amount }}</p>
</div>

<!-- Payment Method Field -->
<div class="form-group">
    {!! Form::label('payment_method', __('models/orders.fields.payment_method').':') !!}
    <p>{{ $orders->payment_method }}</p>
</div>

<!-- Delivery Address Field -->
<div class="form-group">
    {!! Form::label('delivery_address', __('models/orders.fields.delivery_address').':') !!}
    <p>{{ $orders->delivery_address }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', __('models/orders.fields.status').':') !!}
    <p>{{ $orders->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/orders.fields.created_at').':') !!}
    <p>{{ $orders->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/orders.fields.updated_at').':') !!}
    <p>{{ $orders->updated_at }}</p>
</div>

