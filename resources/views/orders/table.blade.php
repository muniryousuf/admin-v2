<div class="table-responsive">
    <table class="table" id="orders-table">
        <thead>
            <tr>
                <th>@lang('models/orders.fields.order_id')</th>
                <th>@lang('models/orders.fields.Reference')</th>
                <th>@lang('models/orders.fields.total_amount')</th>
                <th>@lang('models/orders.fields.payment_method')</th>
                <th>@lang('models/orders.fields.delivery_address')</th>
                <th>@lang('models/orders.fields.status')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($orders as $order)
            <tr>
                <td>{{ $order->id }}</td>
                <td>{{ $order->reference }}</td>
                <td>{{ $order->total_amount_with_fee }}</td>
                <td>{{ $order->payment }}</td>
                <td>{{ $order->delivery_address }}</td>
                <td>{{ $order->status }}</td>
                <td class=" text-center">
                   {!! Form::open(['route' => ['orders.destroy', $order->id], 'method' => 'delete']) !!}
                   <div class='btn-group'>
                       <!-- <a href="{!! route('orders.show', [$order->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a> -->
                       <a href="{!! route('orders.edit', [$order->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                       <a 
                            href="{!! route('downloadPdf', [$order->reference]) !!}" 
                            class='btn btn-warning action-btn edit-btn d-none'
                            target="_blank" 
                        >
                            <i class="fa fa-download"></i>
                        </a>
                         <a 
                            href="{!! route('viewPdf', [$order->reference]) !!}" 
                            class='btn btn-primary edit-btn'
                            target="_blank" 
                        >
                            view Pdf
                        </a>
                        <a 
                            href="javascript:void(0);" 
                            class='btn btn-success send-email-to-admin  edit-btn' 
                            reference="{{$order->reference}}"
                        >
                            Send to admin
                        </a>

                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                   </div>
                   {!! Form::close() !!}
                </td>
           </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
