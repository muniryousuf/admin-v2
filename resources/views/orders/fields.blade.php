
<div class="col-md-6">
    <h3>{{__('models/orders.fields.order_products_detail')}}</h3>
      @foreach($order->order_details as $order_detail)
      <hr>
        <!-- Order Id Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('product_name', __('models/orders.fields.product_name').':') !!}
            <div class="form-control">{{$order_detail->product_name}}</div>
        </div>

        <!-- Reference Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('product_price', __('models/orders.fields.product_price').':') !!}
            <div class="form-control">{{$order_detail->price}}</div>
        </div>

        <!-- Total Amount Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('product_quantity', __('models/orders.fields.product_quantity').':') !!}
            <div class="form-control">{{$order_detail->quantity}}</div>
        </div>


        <!-- extra Field -->
        @if($order->is_pos == 0)
            @php $extras_data = json_decode($order_detail->selection_meta,true); @endphp
            @if(is_array($extras_data))
                <div class="form-group col-sm-6">
                <label for="addons">Addons:</label>
                    @foreach(json_decode($order_detail->selection_meta,true) as $extras)
                       <div> Group: {{GetGroupName($extras['product_meta_id'])['name'] ?? 'N/A'}}</div>
                       <div> Choice name: {{$extras['key']}}</div>
                       <div> Price: {{$extras['price'] > 0 ? $extras['price'] : 'FREE'}}</div><br>
                    @endforeach
                </div>
            @endif
        @else
            <div class="form-group col-sm-6">
                <label for="addons">Addons:</label>
                @foreach(json_decode($order_detail->extras,true) as $extras)
                   <div> Group: {{$extras['group_name'] ?? 'N/A'}}</div>
                   <div > Choice name: {{$extras['choice']}}</div>
                   <div > Price: {{$extras['price'] > 0 ? $extras['price'] : 'FREE'}}</div><br>
                @endforeach

            </div>
        @endif
        @endforeach

</div>
<div class="col-md-6">
    @php
        $user_details = $order->user_detail;

    @endphp
    <h3>User Details</h3>

    <!-- Order Id Field -->
    <div class="form-group col-sm-6">
        <label for="user-email">User Email:</label>
        {{$user_details->email ?? ''}}
    </div>
    <hr>
    <!-- Order Id Field -->
    <div class="form-group col-sm-6">
        <label for="user-email">User Name:</label>
        {{$user_details->name ?? ''}} {{$user_details->lname ?? ''}}
    </div>
    <hr>
    <div class="form-group col-sm-6">
        <label for="user-email">User Phone:</label>
        {{$user_details->phone ?? ''}}
    </div>
    <hr>
    <div class="form-group col-sm-6">
        <label for="user-email">Special Instructions:</label>
        {{$order->special_instructions ?? ''}}
    </div>
    <hr>

</div>

<div class="col-md-12">
    <h3>{{__('models/orders.fields.order_detail')}}</h3>
    <!-- Order Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('change_order_status', __('models/orders.fields.change_order_status').':') !!}
        <select name="status" class="form-control" id="order-status-update">
            @foreach($order::ORDER_STATUS as $status)
            <option value="{{$status}}" {{$status == $order->status ? 'selected' : ''}}>{{ucfirst(str_replace("_"," ",$status))}}</option>
            @endforeach
        </select>
    </div>
    <hr>
    <div class="form-group col-sm-6">
        {!! Form::label('total_amount_with_delivery', __('models/orders.fields.total_amount_with_delivery').':') !!}
        <div class="form-control">{{$order->total_amount_with_fee}}</div>
    </div>

    <hr>
    @if($order->table_id == 0)
    <div class="form-group col-sm-6">
        {!! Form::label('delivery_fees', __('models/orders.fields.delivery_fees').':') !!}
        <div class="form-control">{{$order->delivery_fees}}</div>
    </div>
    <hr>
      <div class="form-group col-sm-6">
        {!! Form::label('delivery_address', __('models/orders.fields.delivery_address').':') !!}
        <div class="form-control">{{$order->delivery_address}}</div>
    </div>
    <hr>
    <div class="form-group col-sm-6">
        {!! Form::label('postal_code', __('models/orders.fields.postal_code').':') !!}
        <div class="form-control">{{$order->postal_code}}</div>
    </div>
    @else
        <div class="form-group col-sm-6">
        {!! Form::label('postal_code', __('models/orders.fields.table').':') !!}
        <div class="form-control">{{$order->table_detail->name ?? ''}}</div>
    </div>

    @endif

    <hr>
    <div class="form-group col-sm-6">
        {!! Form::label('payment_method', __('models/orders.fields.payment_method').':') !!}
        <div class="form-control">{{$order->payment}}</div>
    </div>
    <hr>
    <div class="form-group col-sm-6">
        {!! Form::label('order_type', __('models/orders.fields.order_type').':') !!}
        <div class="form-control">{{$order->order_type}}</div>
    </div>
    <hr>
        <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('orders.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
    </div>


</div>
