@extends('layouts.app')
@section('title')
     @lang('models/orders.singular')
@endsection
@section('content')
<section >
    @php 
        $searchArray = App\Models\orders::SEARCH_ARRAY;
    @endphp
   
      <div class="input-group">
         <div class="input-group-btn search-panel">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            <span id="search_concept" >{{$searchArray[request()->searchBy ?? ''] ?? ''}}</span> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu scrollable-dropdown" role="menu">
               @foreach($searchArray as $field => $name)
                    <li  onclick="setCuurentFilter('{{$field}}','{{$name}}')" ><a href="javascript:void(0)">{{$name}}</a></li>
               @endforeach
              
            </ul>
         </div>
         <input type="text" class="form-control" name="x" id="search" placeholder="Search" value="{{request()->keyword ?? ''}}">
         <span class="input-group-btn">
         <button class="btn btn-info" type="button" onclick="Search()">
                <i class="fa fa-search"></i>
         </button>
         </span>
      </div>
</section>
    <section class="section">
        <div class="section-header">
            <h1>@lang('models/orders.singular')</h1>
        </div>
        
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('orders.table')
            </div>
       </div>
   </div>
     @include('stisla-templates::common.paginate', ['records' => $orders])
    </section>
@endsection
@section('scripts') 
<script type="text/javascript">
    function setCuurentFilter(filter,name){
        const url = new URL(window.location.href);
        url.searchParams.delete('searchBy');
        url.searchParams.set('searchBy', filter);
        $('#search_concept').text(name)
        window.history.replaceState(null, null, url);
    }
    function Search(){
        let search = $('#search').val();
        const parser = new URL(window.location);
        parser.searchParams.set('keyword', search);
        window.location = parser.href;
    }
    $(document).on('keypress',function(e) {
        if(e.which == 13) {
            Search()
        }
    });
    $(document).on('click','.send-email-to-admin',function(){
        let reference = $(this).attr('reference')
        $.ajax({
            type:'POST',
            url:"{{route('api.send-email-to-admin')}}",
            dataType:'json',
            data:{reference:reference},
            async:false,
            success:function(response) {
                swal('','Email Send','success')
            }
        });

    });

</script>
@endsection 



