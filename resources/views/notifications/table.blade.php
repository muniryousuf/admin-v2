<div class="table-responsive">
    <table class="table" id="notifications-table">
        <thead>
            <tr>
                <th>@lang('models/notifications.fields.title')</th>
                <th>@lang('models/notifications.fields.message')</th>
                <th>@lang('models/notifications.fields.description')</th>
                <th>@lang('models/notifications.fields.amount')</th>
        <th>@lang('models/notifications.fields.color')</th>
        <th>@lang('models/notifications.fields.status')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($notifications as $notifications)
            <tr>
                       <td>{{ $notifications->title }}</td>
                       <td>{{ $notifications->message }}</td>
                       <td>{{ $notifications->description }}</td>
                       <td>{{ $notifications->amount }}</td>
            <td>{{ $notifications->color }}</td>
            <td>{{ $notifications->status }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['notifications.destroy', $notifications->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               
                               <a href="{!! route('notifications.edit', [$notifications->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
