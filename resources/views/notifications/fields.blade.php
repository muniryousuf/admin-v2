<!-- Message Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', __('models/notifications.fields.title').':') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('message', __('models/notifications.fields.message').':') !!}
    {!! Form::text('message', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/notifications.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('amount', __('models/notifications.fields.amount').':') !!}
    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('image', __('models/galleries.fields.image').':') !!}
    {!! Form::file('image') !!}
     @if(isset($notifications->image))
    <img class="image-preview" src="{{asset('images/'.$notifications->image)}}" />
    @endif
</div>


<!-- Color Field -->
<div class="form-group col-sm-6">
    {!! Form::label('color', __('models/notifications.fields.color').':') !!}
    {!! Form::text('color', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', __('models/notifications.fields.status').':') !!}
    <select name="status" class="form-control">
        <option value="1">Active</option>
        <option value="0">Disable</option>
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('notifications.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
