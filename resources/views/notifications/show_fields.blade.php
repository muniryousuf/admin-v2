<!-- Message Field -->
<div class="form-group">
    {!! Form::label('title', __('models/notifications.fields.title').':') !!}
    <p>{{ $notifications->title }}</p>
</div>
<div class="form-group">
    {!! Form::label('description', __('models/notifications.fields.description').':') !!}
    <p>{{ $notifications->description }}</p>
</div>

<!-- Color Field -->
<div class="form-group">
    {!! Form::label('color', __('models/notifications.fields.color').':') !!}
    <p>{{ $notifications->color }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', __('models/notifications.fields.status').':') !!}
    <p>{{ $notifications->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/notifications.fields.created_at').':') !!}
    <p>{{ $notifications->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/notifications.fields.updated_at').':') !!}
    <p>{{ $notifications->updated_at }}</p>
</div>

