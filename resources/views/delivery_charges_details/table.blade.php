<div class="table-responsive">
    <table class="table" id="deliveryChargesDetails-table">
        <thead>
            <tr>
                
                <th>@lang('models/deliveryChargesDetails.fields.delivery_id')</th>
                <th>@lang('models/deliveryChargesDetails.fields.delivery_types')</th>
                <th>@lang('models/deliveryChargesDetails.fields.miles')</th>
                <th>@lang('models/deliveryChargesDetails.fields.postal_code')</th>
                <th>@lang('models/deliveryChargesDetails.fields.amount')</th>
                
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($deliveryChargesDetails as $deliveryChargesDetails)

            <tr>
                <td>{{ $deliveryChargesDetails->delivery_id }}</td>
                @php  $delivery_type = $deliveryChargesDetails->delivery_type->delivery_types ?? 'N/A';  @endphp
                <td>{{ucfirst(str_replace('_',' ',$delivery_type))}}</td>
                <td>{{ $deliveryChargesDetails->miles }}</td>
                <td>{{ $deliveryChargesDetails->postal_code }}</td>
                <td>{{ $deliveryChargesDetails->amount }}</td>
                
                <td class=" text-center">
                   {!! Form::open(['route' => ['deliveryChargesDetails.destroy', $deliveryChargesDetails->id], 'method' => 'delete']) !!}
                   <div class='btn-group'>
                       <a href="{!! route('deliveryChargesDetails.show', [$deliveryChargesDetails->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                       <a href="{!! route('deliveryChargesDetails.edit', [$deliveryChargesDetails->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                   </div>
                   {!! Form::close() !!}
               </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
