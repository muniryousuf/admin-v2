<!-- Delivery Id Field -->
<div class="form-group">
    {!! Form::label('delivery_id', __('models/deliveryChargesDetails.fields.delivery_id').':') !!}
    <p>{{ $deliveryChargesDetails->delivery_id }}</p>
</div>

<!-- Delivery Id Field -->
<div class="form-group">
    {!! Form::label('delivery_id', __('models/deliveryChargesDetails.fields.delivery_id').':') !!}
    <p>{{ $deliveryChargesDetails->delivery_id }}</p>
</div>

<!-- Miles Field -->
<div class="form-group">
    {!! Form::label('miles', __('models/deliveryChargesDetails.fields.miles').':') !!}
    <p>{{ $deliveryChargesDetails->miles }}</p>
</div>

<!-- Postal Code Field -->
<div class="form-group">
    {!! Form::label('postal_code', __('models/deliveryChargesDetails.fields.postal_code').':') !!}
    <p>{{ $deliveryChargesDetails->postal_code }}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', __('models/deliveryChargesDetails.fields.amount').':') !!}
    <p>{{ $deliveryChargesDetails->amount }}</p>
</div>

<!-- Fix Delivery Charges Field -->
<div class="form-group">
    {!! Form::label('fix_delivery_charges', __('models/deliveryChargesDetails.fields.fix_delivery_charges').':') !!}
    <p>{{ $deliveryChargesDetails->fix_delivery_charges }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/deliveryChargesDetails.fields.created_at').':') !!}
    <p>{{ $deliveryChargesDetails->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/deliveryChargesDetails.fields.updated_at').':') !!}
    <p>{{ $deliveryChargesDetails->updated_at }}</p>
</div>

