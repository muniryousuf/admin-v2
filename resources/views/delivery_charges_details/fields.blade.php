<!-- Delivery Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_types', __('models/deliveryChargesDetails.fields.delivery_types').':') !!}
    <select class="form-control" name="delivery_types" >
        @foreach(App\Models\deliveryChargesDetails::DELIVERY_TYPES as $delivery_type)
        <option  value="{{$delivery_type}}">{{ucfirst(str_replace('_',' ',$delivery_type))}}</option>
        @endforeach
    </select>
</div>

<!-- Miles Field -->
<div class="form-group col-sm-6">
    {!! Form::label('miles', __('models/deliveryChargesDetails.fields.miles').':') !!}
    {!! Form::text('miles', null, ['class' => 'form-control']) !!}
</div>

<!-- Postal Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('postal_code', __('models/deliveryChargesDetails.fields.postal_code').':') !!}
    {!! Form::text('postal_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Postal Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', __('models/deliveryChargesDetails.fields.amount').':') !!}
    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Fix Delivery Charges Field -->
<div class="form-group col-sm-6 d-none">
    {!! Form::label('fix_delivery_charges', __('models/deliveryChargesDetails.fields.fix_delivery_charges').':') !!}
    {!! Form::text('fix_delivery_charges', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('deliveryChargesDetails.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
