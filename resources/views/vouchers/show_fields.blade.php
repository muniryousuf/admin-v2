<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/vouchers.fields.name').':') !!}
    <p>{{ $vouchers->name }}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', __('models/vouchers.fields.type').':') !!}
    <p>{{ $vouchers->type }}</p>
</div>

<!-- Expiry Date Field -->
<div class="form-group">
    {!! Form::label('expiry_date', __('models/vouchers.fields.expiry_date').':') !!}
    <p>{{ $vouchers->expiry_date }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', __('models/vouchers.fields.status').':') !!}
    <p>{{ $vouchers->status }}</p>
</div>

<!-- Coupon Limit Field -->
<div class="form-group">
    {!! Form::label('coupon_limit', __('models/vouchers.fields.coupon_limit').':') !!}
    <p>{{ $vouchers->coupon_limit }}</p>
</div>

<!-- User Limit Field -->
<div class="form-group">
    {!! Form::label('user_limit', __('models/vouchers.fields.user_limit').':') !!}
    <p>{{ $vouchers->user_limit }}</p>
</div>

<!-- Auto Coupon Field -->
<div class="form-group">
    {!! Form::label('auto_coupon', __('models/vouchers.fields.auto_coupon').':') !!}
    <p>{{ $vouchers->auto_coupon }}</p>
</div>

<!-- For First Order Field -->
<div class="form-group">
    {!! Form::label('for_first_order', __('models/vouchers.fields.for_first_order').':') !!}
    <p>{{ $vouchers->for_first_order }}</p>
</div>

<!-- For All Orders Field -->
<div class="form-group">
    {!! Form::label('for_all_orders', __('models/vouchers.fields.for_all_orders').':') !!}
    <p>{{ $vouchers->for_all_orders }}</p>
</div>

<!-- With Delivery Field -->
<div class="form-group">
    {!! Form::label('with_delivery', __('models/vouchers.fields.with_delivery').':') !!}
    <p>{{ $vouchers->with_delivery }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/vouchers.fields.created_at').':') !!}
    <p>{{ $vouchers->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/vouchers.fields.updated_at').':') !!}
    <p>{{ $vouchers->updated_at }}</p>
</div>

