<div class="table-responsive">
    <table class="table" id="vouchers-table">
        <thead>
            <tr>
                <th>@lang('models/vouchers.fields.name')</th>
                <th>@lang('models/vouchers.fields.discount')</th>
        <th>@lang('models/vouchers.fields.type')</th>
        <th>@lang('models/vouchers.fields.expiry_date')</th>
        <th>@lang('models/vouchers.fields.status')</th>
        <th>@lang('models/vouchers.fields.coupon_limit')</th>
        <th>@lang('models/vouchers.fields.user_limit')</th>
        <th>@lang('models/vouchers.fields.auto_coupon')</th>
        <th>@lang('models/vouchers.fields.for_first_order')</th>
        <th>@lang('models/vouchers.fields.for_all_orders')</th>
        <th>@lang('models/vouchers.fields.with_delivery')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($vouchers as $vouchers)
            <tr>
                       <td>{{ $vouchers->name }}</td>
                       <td>{{ $vouchers->discount }}</td>
            <td>{{ $vouchers->type }}</td>
            <td>{{ $vouchers->expiry_date }}</td>
            <td>{{ $vouchers->status }}</td>
            <td>{{ $vouchers->coupon_limit }}</td>
            <td>{{ $vouchers->user_limit }}</td>
            <td>{{ $vouchers->auto_coupon }}</td>
            <td>{{ $vouchers->for_first_order }}</td>
            <td>{{ $vouchers->for_all_orders }}</td>
            <td>{{ $vouchers->with_delivery }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['vouchers.destroy', $vouchers->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('vouchers.show', [$vouchers->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('vouchers.edit', [$vouchers->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
