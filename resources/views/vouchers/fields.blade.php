<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/vouchers.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', __('models/vouchers.fields.type').':') !!}
    @php 
        $selected = '';
        if(isset($vouchers->type)){
            $selected = $vouchers->type;
        }

    @endphp
    <select name="type" class="form-control">

        <option value="percent" {{$selected == 'percent' ? 'selected' : ''}}>Percent</option>
        <option value="amount" {{$selected == 'amount' ? 'selected' : ''}}>Amount</option>
    </select>
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discount', __('models/vouchers.fields.discount').':') !!}
    {!! Form::text('discount', null, ['class' => 'form-control']) !!}
</div>

<!-- Expiry Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('expiry_date', __('models/vouchers.fields.expiry_date').':') !!}
    {!! Form::date('expiry_date', null, ['class' => 'form-control','id'=>'expiry_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#expiry_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', __('models/vouchers.fields.status').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', 0) !!}
        {!! Form::checkbox('status', '1', null) !!} 
    </label>
</div>

<!-- Coupon Limit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coupon_limit', __('models/vouchers.fields.coupon_limit').':') !!}
    {!! Form::text('coupon_limit', null, ['class' => 'form-control']) !!}
</div>

<!-- User Limit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_limit', __('models/vouchers.fields.user_limit').':') !!}
    {!! Form::text('user_limit', null, ['class' => 'form-control']) !!}
</div>

<!-- Auto Coupon Field -->
<div class="form-group col-sm-6">
    {!! Form::label('auto_coupon', __('models/vouchers.fields.auto_coupon').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('auto_coupon', 0) !!}
        {!! Form::checkbox('auto_coupon', '1', null) !!}
    </label>
</div>

<!-- For First Order Field -->
<div class="form-group col-sm-6">
    {!! Form::label('for_first_order', __('models/vouchers.fields.for_first_order').':') !!}
    
    <label class="checkbox-inline">
        {!! Form::hidden('for_first_order', 0) !!}
        {!! Form::checkbox('for_first_order', '1', null) !!}
    </label>
</div>

<!-- For All Orders Field -->
<div class="form-group col-sm-6">
    {!! Form::label('for_all_orders', __('models/vouchers.fields.for_all_orders').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('for_all_orders', 0) !!}
        {!! Form::checkbox('for_all_orders', '1', null) !!} 
    </label>
</div>

<!-- With Delivery Field -->
<div class="form-group col-sm-6">
    {!! Form::label('with_delivery', __('models/vouchers.fields.with_delivery').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('with_delivery', 0) !!}
        {!! Form::checkbox('with_delivery', '1', null) !!}
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('vouchers.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
