<div class="table-responsive">
    <table class="table" id="caosGenralSettings-table">
        <thead>
            <tr>
                <th>@lang('models/caosGenralSettings.fields.main_printer')</th>
        <th>@lang('models/caosGenralSettings.fields.kitchen_printer')</th>
        <th>@lang('models/caosGenralSettings.fields.kicthen_copies')</th>
        <th>@lang('models/caosGenralSettings.fields.shop_name_and_address')</th>
        <th>@lang('models/caosGenralSettings.fields.reciept_end_text')</th>
        <th>@lang('models/caosGenralSettings.fields.menu_url')</th>
        <th>@lang('models/caosGenralSettings.fields.enable_multi_payment')</th>
        <th>@lang('models/caosGenralSettings.fields.extra_info')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($caosGenralSettings as $caosGenralSettings)
            <tr>
                       <td>{{ $caosGenralSettings->main_printer }}</td>
            <td>{{ $caosGenralSettings->kitchen_printer }}</td>
            <td>{{ $caosGenralSettings->kicthen_copies }}</td>
            <td>{{ $caosGenralSettings->shop_name_and_address }}</td>
            <td>{{ $caosGenralSettings->reciept_end_text }}</td>
            <td>{{ $caosGenralSettings->menu_url }}</td>
            <td>{{ $caosGenralSettings->enable_multi_payment }}</td>
            <td>{{ $caosGenralSettings->extra_info }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['caosGenralSettings.destroy', $caosGenralSettings->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('caosGenralSettings.show', [$caosGenralSettings->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('caosGenralSettings.edit', [$caosGenralSettings->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
