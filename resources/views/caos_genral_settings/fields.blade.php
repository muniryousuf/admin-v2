<!-- Main Printer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('main_printer', __('models/caosGenralSettings.fields.main_printer').':') !!}
    {!! Form::text('main_printer', null, ['class' => 'form-control']) !!}
</div>

<!-- Kitchen Printer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kitchen_printer', __('models/caosGenralSettings.fields.kitchen_printer').':') !!}
    {!! Form::text('kitchen_printer', null, ['class' => 'form-control']) !!}
</div>
<!-- Kitchen Printer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kicthen_copies', __('models/caosGenralSettings.fields.kicthen_copies').':') !!}
    {!! Form::text('kicthen_copies', null, ['class' => 'form-control']) !!}
</div>

<!-- Shop Name And Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shop_name_and_address', __('models/caosGenralSettings.fields.shop_name_and_address').':') !!}
    {!! Form::text('shop_name_and_address', null, ['class' => 'form-control']) !!}
</div>

<!-- Reciept End Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reciept_end_text', __('models/caosGenralSettings.fields.reciept_end_text').':') !!}
    {!! Form::text('reciept_end_text', null, ['class' => 'form-control']) !!}
</div>

<!-- Menu Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('menu_url', __('models/caosGenralSettings.fields.menu_url').':') !!}
    {!! Form::text('menu_url', null, ['class' => 'form-control']) !!}
</div>

<!-- Enable Multi Payment Field -->
<div class="form-group col-sm-6">
    {!! Form::label('enable_multi_payment', __('models/caosGenralSettings.fields.enable_multi_payment').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('enable_multi_payment', 0) !!}
        {!! Form::checkbox('enable_multi_payment', '1', null) !!} 1
    </label>
</div>

<!-- Extra Info Field -->
<div class="form-group col-sm-6">
    {!! Form::label('extra_info', __('models/caosGenralSettings.fields.extra_info').':') !!}
    {!! Form::text('extra_info', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('caosGenralSettings.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
