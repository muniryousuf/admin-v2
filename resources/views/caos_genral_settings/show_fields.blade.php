<!-- Main Printer Field -->
<div class="form-group">
    {!! Form::label('main_printer', __('models/caosGenralSettings.fields.main_printer').':') !!}
    <p>{{ $caosGenralSettings->main_printer }}</p>
</div>

<!-- Kitchen Printer Field -->
<div class="form-group">
    {!! Form::label('kitchen_printer', __('models/caosGenralSettings.fields.kitchen_printer').':') !!}
    <p>{{ $caosGenralSettings->kitchen_printer }}</p>
</div>

<!-- Kicthen Copies Field -->
<div class="form-group">
    {!! Form::label('kicthen_copies', __('models/caosGenralSettings.fields.kicthen_copies').':') !!}
    <p>{{ $caosGenralSettings->kicthen_copies }}</p>
</div>

<!-- Shop Name And Address Field -->
<div class="form-group">
    {!! Form::label('shop_name_and_address', __('models/caosGenralSettings.fields.shop_name_and_address').':') !!}
    <p>{{ $caosGenralSettings->shop_name_and_address }}</p>
</div>

<!-- Reciept End Text Field -->
<div class="form-group">
    {!! Form::label('reciept_end_text', __('models/caosGenralSettings.fields.reciept_end_text').':') !!}
    <p>{{ $caosGenralSettings->reciept_end_text }}</p>
</div>

<!-- Menu Url Field -->
<div class="form-group">
    {!! Form::label('menu_url', __('models/caosGenralSettings.fields.menu_url').':') !!}
    <p>{{ $caosGenralSettings->menu_url }}</p>
</div>

<!-- Enable Multi Payment Field -->
<div class="form-group">
    {!! Form::label('enable_multi_payment', __('models/caosGenralSettings.fields.enable_multi_payment').':') !!}
    <p>{{ $caosGenralSettings->enable_multi_payment }}</p>
</div>

<!-- Extra Info Field -->
<div class="form-group">
    {!! Form::label('extra_info', __('models/caosGenralSettings.fields.extra_info').':') !!}
    <p>{{ $caosGenralSettings->extra_info }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/caosGenralSettings.fields.created_at').':') !!}
    <p>{{ $caosGenralSettings->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/caosGenralSettings.fields.updated_at').':') !!}
    <p>{{ $caosGenralSettings->updated_at }}</p>
</div>

