<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/holdCarts.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Hold Data Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hold_data', __('models/holdCarts.fields.hold_data').':') !!}
    {!! Form::text('hold_data', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('holdCarts.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
