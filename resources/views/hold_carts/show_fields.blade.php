<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/holdCarts.fields.name').':') !!}
    <p>{{ $holdCarts->name }}</p>
</div>

<!-- Hold Data Field -->
<div class="form-group">
    {!! Form::label('hold_data', __('models/holdCarts.fields.hold_data').':') !!}
    <p>{{ $holdCarts->hold_data }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/holdCarts.fields.created_at').':') !!}
    <p>{{ $holdCarts->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/holdCarts.fields.updated_at').':') !!}
    <p>{{ $holdCarts->updated_at }}</p>
</div>

