<div class="table-responsive">
    <table class="table" id="holdCarts-table">
        <thead>
            <tr>
                <th>@lang('models/holdCarts.fields.name')</th>
        <th>@lang('models/holdCarts.fields.hold_data')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($holdCarts as $holdCarts)
            <tr>
                       <td>{{ $holdCarts->name }}</td>
            <td>{{ $holdCarts->hold_data }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['holdCarts.destroy', $holdCarts->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('holdCarts.show', [$holdCarts->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('holdCarts.edit', [$holdCarts->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
