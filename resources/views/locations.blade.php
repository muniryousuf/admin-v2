@extends('layouts.front')
@section('content')
    <div class="root-content" style="margin-top: 100px;">
        
            
            @foreach($locations as $location)
            <div class="col-md-12">
                <div class="jumbotron jumbotron-fluid">
                  <div class="container">
                    <h1 class="display-4">{{$location->name}}</h1>
                    @if(!isset($location->map))
                        <p class="lead"><a class="btn btn-primary btn-lg" href="{{route('locations.attributes',[$location->id])}}">view detail</a></p>
                    @else
                    <p class="lead">{{$location->details}}</p>
                        <iframe
                          width="600"
                          height="450"
                          style="border:0"
                          loading="lazy"
                          allowfullscreen
                          referrerpolicy="no-referrer-when-downgrade"
                          src="{{$location->map}}">
                        </iframe>

                        @php
                            $map = explode('q=',$location->map)[1] ?? ''

                         @endphp
                        <p class="lead"><a class="btn btn-primary btn-lg" href="https://www.google.com/maps/search/{{$map}}">View Map</a></p>
                    @endif
                  </div>
                </div>
            </div>
            @endforeach

        

    </div>
@endsection

