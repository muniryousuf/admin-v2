<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', __('models/galleries.fields.image').':') !!}
    <p><img class="image-preview" src="{{asset('images/'.$gallery->image)}}" /></p>
</div>

<!-- Gallery Id Field -->
<div class="form-group">
    {!! Form::label('gallery_id', __('models/galleries.fields.gallery_id').':') !!}
    <p>{{ $gallery->gallery_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/galleries.fields.created_at').':') !!}
    <p>{{ $gallery->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/galleries.fields.updated_at').':') !!}
    <p>{{ $gallery->updated_at }}</p>
</div>

