<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', __('models/galleries.fields.image').':') !!}
    {!! Form::file('image') !!}
     @if(isset($gallery->image))
    <img class="image-preview" src="{{asset('images/'.$gallery->image)}}" />
    @endif
</div>

<!-- Site Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', __('models/galleries.fields.title').':') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="clearfix"></div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('galleries.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
