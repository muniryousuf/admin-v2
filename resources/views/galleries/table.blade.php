<div class="table-responsive">
    <table class="table" id="galleries-table">
        <thead>
            <tr>
                <th>@lang('models/galleries.fields.image')</th>
                <th>@lang('models/galleries.fields.gallery_id')</th>
                <th>@lang('models/galleries.fields.title')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($galleries as $gallery)
            <tr>
                       <td><img class="image-preview" src="{{asset('images/'.$gallery->image)}}" /></td>
                       <td>{{$gallery->title}}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['galleries.destroy', $gallery->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('galleries.show', [$gallery->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('galleries.edit', [$gallery->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
