<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/productMetas.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('label', __('models/productMetas.fields.label').':') !!}
    {!! Form::text('label', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('free_limit', __('models/productMetas.fields.limit').':') !!}
    {!! Form::text('free_limit', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('limit', __('models/productMetas.fields.limit_').':') !!}
    {!! Form::text('limit', null, ['class' => 'form-control']) !!}
</div>
@if(isset($id) && $id > 0)
    <input type="hidden" name="id_parent" value="{{$id}}">
@endif


<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', __('models/productMetas.fields.price').':') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_required', __('models/productMetas.fields.is_required').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_required', 0) !!}
        {!! Form::checkbox('is_required', '1', null) !!} 
    </label>
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('multiple_quantity_allowed', __('models/productMetas.fields.multiple_quantity_allowed').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('multiple_quantity_allowed', 0) !!}
        {!! Form::checkbox('multiple_quantity_allowed', '1', null) !!} 
    </label>
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('table_type', __('models/productMetas.fields.table_type').':') !!}
    <label class="checkbox-inline">
        <select name="table_type" class="form-control">
            @foreach(App\Models\product_meta::TABLE_TYPES as $types)
                <option value="{{$types}}">{{$types}}</option>
            @endforeach
        </select>
       
    </label>
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    <label for="image">Image:</label>
    <input name="image" type="file" id="image">
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary'] ) !!}
    <a href="{{ route('productMetas.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
