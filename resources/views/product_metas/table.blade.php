<div class="table-responsive">
    <table class="table" id="productMetas-table">
        <thead>
            <tr>
                <th>@lang('models/productMetas.fields.name')</th>
                <th>@lang('models/productMetas.fields.label')</th>
                <th>@lang('models/productMetas.fields.id_attribute')</th>
                <th>@lang('models/productMetas.fields.price')</th>
                <th>@lang('models/productMetas.fields.image')</th>
                <th>@lang('models/productMetas.fields.table_type')</th>
                <th>@lang('models/productMetas.fields.is_required')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($productMetas as $productMeta)
            <tr>
                <td>{{ $productMeta->name }}</td>
                <td>{{ $productMeta->label }}</td>
                
                <td>{{ $productMeta->attribute->key  ?? 'N/A'}}</td>
                <td>{{ $productMeta->price }}</td>
                <td>{{ $productMeta->table_type }}</td>
                <td>{{ $productMeta->is_required == 1 ? 'Required' : 'Optional'}}</td>
                <td><img 
                        class="image-preview" 
                        src="{{asset('images/'.$productMeta->image)}}" 
                        onerror="this.src='https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg'" 
                    />
                </td>
                   <td class=" text-center">
                       {!! Form::open(['route' => ['productMetas.destroy', $productMeta->id], 'method' => 'delete']) !!}
                       <div class='btn-group'>
                          
                           <a href="{!! route('productMetas.edit', [$productMeta->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                           {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                       </div>
                       {!! Form::close() !!}
                   </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
