@extends('layouts.app')
@section('title')
@lang('crud.edit') @lang('models/productMetas.singular')
@endsection
@section('content')
<section class="section">
    <div class="section-header">
        <h3 class="page__heading m-0">@lang('crud.edit') @lang('models/productMetas.singular')</h3>
        <div class="filter-container section-header-breadcrumb row justify-content-md-end">
            {{-- <form method="GET" action="{{ route('add-child-product-meta', ['id_meta' => $productMeta->id] ) }}">
                <button class="btn btn-primary" type="submit" >Add Child</button>
            </form> --}}
            <button id="add-attributes" class="btn btn-primary" type="button" >Add Attribute</button>
            <a href="{{ route('productMetas.index') }}"  class="btn btn-primary">@lang('crud.back')</a>
        </div>
    </div>
    @if(!$productMeta->childs->isEmpty())
    <ul class="d-none">
        @foreach($productMeta->childs as $child)
        <li><a href="{{route('productMetas.edit', [$child->id])}}">{{$child->name}}</a></li>
        @endforeach
        <!-- <li class="breadcrumb-item active" aria-current="page">Data</li> -->
    </ul>
    {{-- @php echo generateGroupsTree($productMeta->childs); @endphp --}}
    @endif
    <div class="content">
        @include('stisla-templates::common.errors')
        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body ">
                            {!! Form::model($productMeta, ['route' => ['productMetas.update', $productMeta->id], 'method' => 'patch']) !!}
                            <div class="row">
                                <!-- Name Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('name', __('models/productMetas.fields.name').':') !!}
                                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                </div>
                                <!-- Value Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('label', __('models/productMetas.fields.label').':') !!}
                                    {!! Form::text('label', null, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group col-sm-6">
                                    {!! Form::label('limit', __('models/productMetas.fields.limit_').':') !!}
                                    {!! Form::text('limit', null, ['class' => 'form-control']) !!}
                                </div>
                                <!-- Status Field -->
                                <div class="form-group col-sm-6">
                                {!! Form::label('table_type', __('models/productMetas.fields.table_type').':') !!}



                                <label class="checkbox-inline">
                                    <select name="table_type" class="form-control">
                                        @foreach(App\Models\product_meta::TABLE_TYPES as $types)
                                            <option value="{{$types}}" {{$productMeta->table_type == $types ? 'selected' : ''}}>{{$types}}</option>
                                        @endforeach
                                    </select>
                                   
                                </label>
                                </div>



                                <div class="form-group col-sm-6">
                                    {!! Form::label('multiple_quantity_allowed', __('models/productMetas.fields.multiple_quantity_allowed').':') !!}
                                    <label class="checkbox-inline">
                                        {!! Form::hidden('multiple_quantity_allowed', 0) !!}
                                        {!! Form::checkbox('multiple_quantity_allowed', '1', null) !!} 
                                    </label>
                                </div>
                                <!-- Status Field -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('is_required', __('models/productMetas.fields.is_required').':') !!}
                                        <label class="checkbox-inline">
                                            
                                           {!! Form::hidden('is_required', 0) !!}
                                            {!! Form::checkbox('is_required', '1', null) !!} 
                                        </label>
                                    </div>
                                     <div class="form-group col-sm-6">
                                    {!! Form::label('free_limit', __('models/productMetas.fields.limit').':') !!}
                                    {!! Form::text('free_limit', null, ['class' => 'form-control']) !!}
                                </div>
                                <div class="col-lg-12">
                                    <!-- FOR EDIT-->
                                    <div class="card d-none" id="attributes">
                                        <h3 class="page__heading m-0">Add Attributes</h3>
                                        <div class="card-body ">
                                            <div class="row">
                                                <!-- Name Field -->
                                                <div class="form-group col-sm-6">
                                                    <label for="attr-key">Key:</label>
                                                    <input name="id_attribute" type="hidden"  id="id_attribute" value="{{$productMeta->id}}">
                                                    <input class="form-control" name="key" type="text"  id="attr-key">
                                                </div>
                                                <!-- Value Field -->
                                                <div class="form-group col-sm-6">
                                                    <label for="attr-value">Label:</label>
                                                    <input class="form-control" name="value" type="text" id="attr-value">
                                                </div>
                                                <!-- Value Field -->
                                                <!-- <div class="form-group col-sm-6">
                                                    <label for="attr-id-parent">Select Parent:</label>
                                                    <select name="id_parent" id="attr-id-parent" class="form-control">
                                                    </select>
                                                </div>
 -->                                                <!-- price Field -->
                                                <div class="form-group col-sm-6">
                                                    <label for="attr-price">Price:</label>
                                                    <input class="form-control" name="price" type="text" id="attr-price">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="attr-image">Image:</label>
                                                    <input class="form-control" name="image" type="file" id="attr-image">
                                                </div>
                                                <div class="form-group col-sm-12">
                                                    <input class="btn btn-primary" type="button" value="Save" id="save-atribute">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- FOR EDIT-->

                                <!-- for update of group-->
                                <div class="col-lg-12">
                                    <!-- FOR EDIT-->
                                    <div class="card d-none" id="update-attribute-area">
                                        <h3 class="page__heading m-0">Update Group</h3>
                                        <div class="card-body ">
                                            <div class="row">
                                                <!-- Name Field -->
                                                <div class="form-group col-sm-6">
                                                    <label for="attr-key">Groups:</label>
                                                    <select class="form-control" id="group-select">
                                                        <option disabled selected>Select</option>
                                                        <option value="0">remove selected group</option>
                                                        @foreach($metas as $meta)
                                                            <option value="{{$meta->id}}">{{$meta->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="attr-key">Key:</label>
                                                   <input class="form-control" type="text" id="updated-key-name">
                                                </div>

                                                <div class="form-group col-sm-12">
                                                    <input class="btn btn-primary" type="button" value="Save" id="update-selected-atribute">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- for update of group-->

                                <!-- FOR TABLE -->
                                <h3 class="page__heading m-0">Attributes</h3>
                                <div class="table-responsive">
                                    <table class="table" id="productMetas-attributes-table">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Value</th>
                                                <th>Price</th>
                                                <th>Image</th>
                                                @if($productMeta->table_type == 'multiselect')
                                                    <th>preselected</th>
                                                @endif
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="product-meta-table-values">
                                        </tbody>
                                    </table>
                                </div>
                                <!-- FOR TABLE -->
                                <!-- Submit Field -->
                                <div class="form-group col-sm-12">
                                    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary'] ) !!}
                                    <a href="{{ route('productMetas.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('css')
<link src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.13.2/jquery-ui.min.js" />
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.13.2/jquery-ui.min.js" integrity="sha512-57oZ/vW8ANMjR/KQ6Be9v/+/h6bq9/l3f0Oc7vn6qMqyhvPd1cvKBRWWpzu0QoneImqr2SkmO4MSqU+RpHom3Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">
    function updateDisplayOrder() {
        var new_data = new Array();
        $('#product-meta-table-values tr').each(function() {
            new_data.push($(this).attr("data"));
        })
        $.ajax({
        type: "POST",
        url: "{{route('api.recreate_groups_attributes')}}",
        data: {new_data:new_data},
        dataType: "json",
        success: function(data){
        }
        });
    }
    $(function() {
        $( "#product-meta-table-values" ).sortable({
            placeholder: "ui-state-highlight",
            update: function( event, ui ) {
            updateDisplayOrder();
        }
        });
    });
    var _token = '{{csrf_token()}}';
    var id_attribute = $('#id_attribute').val(); 
    var image_path = "{{asset('images')}}";
    function getFilePath(image){
      return image_path+'/'+image; 
    }
    function fetchAttributes(){
      let is_multi_select = {{isset($productMeta->table_type) && $productMeta->table_type == 'multiselect' ? 1 : 0}}

      let data = 
        {
            _token : _token,
            id_attribute : id_attribute
        };
        $.ajax({
           type:'POST',
           url:"{{route('fetch-attributes')}}",
           data:data,
           success:function(data) {
            $('#product-meta-table-values').html('')
            // $('#attr-id-parent').html('');
            // $('#attr-id-parent').append(`<option value="0" selected> Select Parent</option>`)
            for( i in data){
              // $('#attr-id-parent').append(`<option value="${data[i].id}"> ${data[i].key}</option>`)
                $('#product-meta-table-values').append(`
                    <tr data='${JSON.stringify(data[i])}'>
                        <td>${data[i].key}</td>
                        <td>${data[i].value}</td>
                        <td>${data[i].price}</td>
                        <td><img 
                            class="image-preview" 
                            src="${getFilePath(data[i].image)}" 
                            onerror="this.src='https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg'"
                            />
                        </td>
                        @if($productMeta->table_type == 'multiselect')
                        <td>
                            <div class="custom-control custom-switch">
                              <input id="customSwitches${data[i].id}" type="checkbox" class="custom-control-input toogle-preselected" ${data[i].preselected == 1 ? 'checked': ''}  id_attribute="${data[i].id}">
                              <label class="custom-control-label" for="customSwitches${data[i].id}"></label>
                            </div>

                        </td>
                        @endif
                        <td>
                            <div class="btn-group">
                              ${!is_multi_select ? `
                                ${ data[i].child_product_meta_id == 0 ? `
                                <a href="javascript:void(0);" id_attribute="${data[i].id}" class="btn btn-warning  edit-btn update-attribute">
                                    Add Group
                                </a>
                                `: `<a href="javascript:void(0);" id_attribute="${data[i].id}" class="btn btn-warning  edit-btn update-attribute">
                                    ${data[i].child_meta.label}
                                </a>`}
                                `:''}
                                <button type="button" class="btn btn-danger action-btn delete-btn delete-attributes" id_attribute="${data[i].id}">
                                    <i class="fa fa-trash"></i>
                                </button>

                           </div>
                        </td>
                        
                    </tr>
                `);
    
            }
          }
        });
    }
    $(document).ready(function(){
        fetchAttributes();
    });
    
    $(document).on('click','.delete-attributes',function(){
        id = $(this).attr('id_attribute');
      
         $.ajax({
           type:'POST',
           url:"{{route('delete-product-meta-attribute')}}/"+id,
           data:{id_attribute:id},
           success:function(data) {
            fetchAttributes();
           }
        });
    
    
    
    });
    $(document).on('click','#add-attributes',function(){
        $('#attributes').removeClass('d-none');
    });
    $(document).on('click','.update-attribute',function(){
        let this_attribute_id = $(this).attr('id_attribute');
        $('#update-selected-atribute').attr('id_attribute',this_attribute_id);
        $('#update-attribute-area').removeClass('d-none');
    });
    $(document).on('click','#update-selected-atribute',function(){
        let child_meta_id = $('#group-select').val();
        let key_value = $('#updated-key-name').val();
        let id_attribute = $(this).attr('id_attribute');
        // if(key_value == ''){
        //     return swal('','Please enter name','error');
        // }

        $.ajax({
            type:'POST',
            url:"{{route('update-attribute')}}",
            data:{id_attribute:id_attribute,child_meta_id:child_meta_id,key_value:key_value},
               success:function(data) {
                fetchAttributes();
                $('#update-attribute-area').addClass('d-none');
               }
        });



    });

    $(document).on('click','.toogle-preselected',function(){

        let selected_val = $(this).is(':checked');
        let id_attribute = $(this).attr('id_attribute');
        $.ajax({
            type:'POST',
            url:"{{route('update-attribute')}}",
            data:{id_attribute:id_attribute,selected_val:selected_val},
               success:function(data) {
                fetchAttributes();
               }
        });
        
    });
    $(document).on('click','#save-atribute',function(){
    
        let key = $('#attr-key').val(); 
        let value = $('#attr-value').val(); 
        let price = $('#attr-price').val(); 
        let fd = new FormData();
        let image = $('#attr-image')[0].files;
        fd.append('price',price);
        fd.append('value',value);
        fd.append('key',key);
        fd.append('_token',_token);
        fd.append('product_meta_id',id_attribute);
        if(image.length > 0 ){
          fd.append('image',image[0]);
        }
    
        if(key == '' || value == ''){
          return swal(
              '',
              'fill both key and value fields to save attribute',
              'error'
          );
        }
    
        $.ajax({
           type:'POST',
           url:"{{route('add-attribute')}}",
           data:fd,
            contentType: false,
            processData: false,
           success:function(data) {
            fetchAttributes();
            $('#attributes').addClass('d-none');
            $('#attr-key').val(''); 
            $('#attr-value').val(''); 
            $('#attr-price').val(''); 
            $('#attr-id-parent').val('');
           }
        });
    
    });
    var toggler = document.querySelectorAll(".rootTree");
        Array.from(toggler).forEach(item => {
         item.addEventListener("click", () => {
            item.parentElement .querySelector(".children") .classList.toggle("active");
            item.classList.toggle("rootTree-down");
         });
      });
</script>
@endsection