@extends('layouts.app')
@section('title')
    @lang('crud.add_new') @lang('models/productMetas.singular')
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading m-0">@lang('crud.add_new') @lang('models/productMetas.singular')</h3>
            <div class="filter-container section-header-breadcrumb row justify-content-md-end">
                <a href="javascript:;" class="btn btn-success replicate">Replicate</a>
                <a href="{{ route('productMetas.index') }}" class="btn btn-primary">@lang('crud.back')</a>
            </div>
        </div>
        <div class="content">
            @include('stisla-templates::common.errors')
            <div class="section-body">
               <div class="row">
                   <div class="col-lg-12">
                       <div class="card">
                           <div class="card-body ">
                                {!! Form::open(['route' => 'productMetas.store', 'files' => true]) !!}
                                    <div class="row">
                                        @include('product_metas.fields')
                                    </div>
                                {!! Form::close() !!}
                           </div>
                       </div>
                   </div>
               </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

<script>
    $(document).on('click','.replicate',function(){
        swal({
            title: "Search Group by id!",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: "Write something"
        },
        function(inputValue){
            if (inputValue === null) return false;

            if (inputValue === "") {
             swal.showInputError("You need to write something!");
            return false
            }

            $.ajax({
                type:'POST',
                url:"{{route('api.productmetas.search')}}",
                data:{'search':inputValue},
                success:function(link) {
                    window.location.href=link;
                }
                });

        // swal("Nice!", "You wrote: " + inputValue, "success");
        });
    });

</script>
@endsection
