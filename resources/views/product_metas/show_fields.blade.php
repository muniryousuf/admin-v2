<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/productMetas.fields.name').':') !!}
    <p>{{ $productMeta->name }}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', __('models/productMetas.fields.value').':') !!}
    <p>{{ $productMeta->value }}</p>
</div>

<!-- Id Parent Field -->
<div class="form-group">
    {!! Form::label('id_parent', __('models/productMetas.fields.id_parent').':') !!}
    <p>{{ $productMeta->id_parent }}</p>
</div>

<!-- Id Attribute Field -->
<div class="form-group">
    {!! Form::label('id_attribute', __('models/productMetas.fields.id_attribute').':') !!}
    <p>{{ $productMeta->id_attribute }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/productMetas.fields.created_at').':') !!}
    <p>{{ $productMeta->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/productMetas.fields.updated_at').':') !!}
    <p>{{ $productMeta->updated_at }}</p>
</div>

