@extends('layouts.front')
@section('content')
<section class="additional-header header-banner" style="margin-top: 60px;">
    <div class="container d-flex" id="my_profile">
        <div class="col-md-3 col-sm-3 adjustmargintop d-none d-lg-block">
            <div class="bg-white-tabs box-shadow">
                <p class="pt-0 mb-0"><span>My Profile</span></p>
                @include('userarea.sidebar')
            </div>
        </div>
        <div class="col col-lg-9 col-sm-12 px-0 px-sm-3 adjustmargintop">
            <div id="content" class="tab-content" role="tablist">
                <div class="bg-white-content box-shadow">
                    @include('userarea.addresses')
                </div>
            </div>
        </div>
    </div>
</section>
@include('modals.user-address')
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
@endsection
@section('scripts')
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
  <script type="text/javascript">
    // $(document).ready( function () {
    //   $('#table_id').DataTable();
    // });
    $(document).on('click','#add-new-address',function(){
        $('.address-modal').modal('show');
    })
    $(document).on('click','.edit',function(){

        let address = JSON.parse($(this).attr('address'));
        $('.address-address').val(address.address);
        $('.address-street').val(address.street);
        $('.address-town').val(address.town);
        $('.address-postal_code').val(address.postal_code);
        $('.address-modal').modal('show');
        $('#send-address-save-request').attr('id_address',address.id);

    });
    $(document).on('click', '#send-address-save-request', function(e) {
        let id_address = $(this).attr('id_address');
        let address = $('.address-address').val();
        let street = $('.address-street').val();
        let town = $('.address-town').val();
        let postal_code = $('.address-postal_code').val();
        if(address == ''){
            return swal('','Please enter address','error');
        }
        if(street == ''){
            return swal('','Please enter street','error');
        }
        if(town == ''){
            return swal('','Please enter town','error');
        }
        if(postal_code == ''){
            return swal('','Please enter postal code','error');
        }
        let request_data = {
            id_address : id_address,
            address:address,
            street:street,
            town:town,
            postal_code:postal_code,
            user_id:"{{Auth::id()}}",
        };

        $.ajax({
            type:'POST',
            url:"{{route('api.update-user-address')}}",
            data:request_data,
            dataType:'json',
            async:false,
            success:function(response) {
                location.reload()
            }
        });



    });
  </script>
@endsection
