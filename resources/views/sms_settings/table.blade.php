<div class="table-responsive">
    <table class="table" id="smsSettings-table">
        <thead>
            <tr>
                <th>@lang('models/smsSettings.fields.key')</th>
        <th>@lang('models/smsSettings.fields.secret')</th>
        <th>@lang('models/smsSettings.fields.gateway_key')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($smsSettings as $smsSetting)
            <tr>
                       <td>{{ $smsSetting->key }}</td>
            <td>{{ $smsSetting->secret }}</td>
            <td>{{ $smsSetting->gateway_key }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['smsSettings.destroy', $smsSetting->customer_id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('smsSettings.show', [$smsSetting->customer_id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('smsSettings.edit', [$smsSetting->customer_id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
