<!-- Key Field -->
<div class="form-group">
    {!! Form::label('key', __('models/smsSettings.fields.key').':') !!}
    <p>{{ $smsSetting->key }}</p>
</div>

<!-- Secret Field -->
<div class="form-group">
    {!! Form::label('secret', __('models/smsSettings.fields.secret').':') !!}
    <p>{{ $smsSetting->secret }}</p>
</div>

<!-- Gateway Key Field -->
<div class="form-group">
    {!! Form::label('gateway_key', __('models/smsSettings.fields.gateway_key').':') !!}
    <p>{{ $smsSetting->gateway_key }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/smsSettings.fields.created_at').':') !!}
    <p>{{ $smsSetting->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/smsSettings.fields.updated_at').':') !!}
    <p>{{ $smsSetting->updated_at }}</p>
</div>

