<!-- Key Field -->
<div class="form-group col-sm-6">
    {!! Form::label('key', __('models/smsSettings.fields.key').':') !!}
    {!! Form::text('key', null, ['class' => 'form-control']) !!}
</div>

<!-- Secret Field -->
<div class="form-group col-sm-6">
    {!! Form::label('secret', __('models/smsSettings.fields.secret').':') !!}
    {!! Form::text('secret', null, ['class' => 'form-control']) !!}
</div>

<!-- Gateway Key Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gateway_key', __('models/smsSettings.fields.gateway_key').':') !!}
    {!! Form::text('gateway_key', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('smsSettings.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
