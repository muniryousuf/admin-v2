

<!-- Favicon Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fav_icon', __('models/generalSettings.fields.fav_icon').':') !!}
    {!! Form::file('fav_icon') !!}
    @if(isset($generalSettings->fav_icon))
    <img class="image-preview" src="{{asset('images/'.$generalSettings->fav_icon)}}" />
    @endif
</div>
<div class="clearfix"></div>


<!-- Site Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('site_name', __('models/generalSettings.fields.site_name').':') !!}
    {!! Form::text('site_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Site Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('site_title', __('models/generalSettings.fields.site_title').':') !!}
    {!! Form::text('site_title', null, ['class' => 'form-control']) !!}
</div>
<!-- site currency Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency', __('models/generalSettings.fields.currency').':') !!}
    {!! Form::text('currency', null, ['class' => 'form-control']) !!}
</div>
<!-- site map Field -->
<div class="form-group col-sm-6">
    {!! Form::label('map', __('models/generalSettings.fields.map').':') !!}
    {!! Form::text('map', null, ['class' => 'form-control']) !!}
</div>
<!-- Tag Line Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('tag_line', __('models/generalSettings.fields.tag_line').':') !!}
    {!! Form::textarea('tag_line', null, ['class' => 'form-control']) !!}
</div>

<!-- Copyright Text Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('copyright_text', __('models/generalSettings.fields.copyright_text').':') !!}
    {!! Form::textarea('copyright_text', null, ['class' => 'form-control']) !!}
</div>

<!-- Header Logo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('header_logo', __('models/generalSettings.fields.header_logo').':') !!}
    {!! Form::file('header_logo') !!}
    @if(isset($generalSettings->header_logo))
    <img class="image-preview" src="{{asset('images/'.$generalSettings->header_logo)}}" />
    @endif
</div>
<div class="clearfix"></div>

<!-- Footer Logo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('footer_logo', __('models/generalSettings.fields.footer_logo').':') !!}
    {!! Form::file('footer_logo') !!}
    @if(isset($generalSettings->footer_logo))
    <img class="image-preview" src="{{asset('images/'.$generalSettings->footer_logo)}}" />
    @endif
</div>
<div class="clearfix"></div>


<!-- Shop Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shop_status', __('models/generalSettings.fields.shop_status').':') !!}
    @php
        $status = ( isset($generalSettings->shop_status) && $generalSettings->shop_status) == 1 ? true : false;
    @endphp
    {!! Form::checkbox('shop_status', null, $status ,['class' => 'form-control']) !!}
</div>
<!-- facebook Field -->
<div class="form-group col-sm-6">
    {!! Form::label('facebook', __('models/generalSettings.fields.facebook').':') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

<!-- instagram Field -->
<div class="form-group col-sm-6">
    {!! Form::label('instagram', __('models/generalSettings.fields.instagram').':') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

<!-- youtube Field -->
<div class="form-group col-sm-6">
    {!! Form::label('youtube', __('models/generalSettings.fields.youtube').':') !!}
    {!! Form::text('youtube', null, ['class' => 'form-control']) !!}
</div>

<!-- pinterest Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pinterest', __('models/generalSettings.fields.pinterest').':') !!}
    {!! Form::text('pinterest', null, ['class' => 'form-control']) !!}
</div>


<!-- twitter Field -->
<div class="form-group col-sm-6">
    {!! Form::label('twitter', __('models/generalSettings.fields.twitter').':') !!}
    {!! Form::text('twitter', null, ['class' => 'form-control']) !!}
</div>
<!-- ticktok Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ticktok', __('models/generalSettings.fields.ticktok').':') !!}
    {!! Form::text('ticktok', null, ['class' => 'form-control']) !!}
</div>
<!-- snapchat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('snapchat', __('models/generalSettings.fields.snapchat').':') !!}
    {!! Form::text('snapchat', null, ['class' => 'form-control']) !!}
</div>
<!-- linkedinn Field -->
<div class="form-group col-sm-6">
    {!! Form::label('linkedinn', __('models/generalSettings.fields.linkedinn').':') !!}
    {!! Form::text('linkedinn', null, ['class' => 'form-control']) !!}
</div>


<!-- stripe_publishable_key Field -->
<div class="form-group col-sm-6 d-none">
    {!! Form::label('stripe_publishable_key', __('models/generalSettings.fields.stripe_publishable_key').':') !!}
    {!! Form::text('stripe_publishable_key', null, ['class' => 'form-control']) !!}
</div>

<!-- stripe_secret_key Field -->
<div class="form-group col-sm-6 d-none">
    {!! Form::label('stripe_secret_key', __('models/generalSettings.fields.stripe_secret_key').':') !!}
    {!! Form::text('stripe_secret_key', null, ['class' => 'form-control']) !!}
</div>

<!-- Mobile Printer IP Field -->
<div class="form-group col-sm-6">
    {!! Form::label('printer_ip_1', __('models/generalSettings.fields.printer_ip_1').':') !!}
    {!! Form::text('printer_ip_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Mobile min_collection_time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('min_collection_time', __('models/generalSettings.fields.min_collection_time').':') !!}
    {!! Form::text('min_collection_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Mobile min_delivery_time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('min_delivery_time', __('models/generalSettings.fields.min_delivery_time').':') !!}
    {!! Form::text('min_delivery_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Mobile total_allowed_person Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_allowed_person', __('models/generalSettings.fields.total_allowed_person').':') !!}
    {!! Form::text('total_allowed_person', null, ['class' => 'form-control']) !!}
</div>


<!-- Mobile Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', __('models/generalSettings.fields.address').':') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>
<!-- Mobile Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('minimum_amount_to_order', __('models/generalSettings.fields.minimum_amount_to_order').':') !!}
    {!! Form::text('minimum_amount_to_order', null, ['class' => 'form-control']) !!}
</div>
<!-- autorotaion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('auto_rotation', __('models/generalSettings.fields.auto_rotation').':') !!}
        <select class="form-control" name="auto_rotation">
            <option value="1" {{isset($generalSettings->auto_rotation) && $generalSettings->auto_rotation == 1 ? 'selected' : ''}}>Yes</option>
            <option value="0" {{isset($generalSettings->auto_rotation) && $generalSettings->auto_rotation == 0 ? 'selected' : ''}}>No</option>
        </select>
</div>
<!-- background_image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('background_image', __('models/generalSettings.fields.background_image').':') !!}
    {!! Form::file('background_image') !!}
    @if(isset($generalSettings->background_image))
        <img class="image-preview" src="{{asset('images/'.$generalSettings->background_image)}}" />
    @endif

</div>
<div class="form-group col-sm-6">
    {!! Form::label('rider_will_deliver', __('models/generalSettings.fields.rider_will_deliver').':') !!}
    @php
        $status = ( isset($generalSettings->rider_will_deliver) && $generalSettings->rider_will_deliver) == 1 ? true : false;
    @endphp
    {!! Form::checkbox('rider_will_deliver', null, $status ,['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('rider_dilver_message', __('models/generalSettings.fields.rider_dilver_message').':') !!}
    {!! Form::text('rider_dilver_message', null, ['class' => 'form-control']) !!}
</div>




<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('generalSettings.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
