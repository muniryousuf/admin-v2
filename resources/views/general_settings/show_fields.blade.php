<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/generalSettings.fields.id').':') !!}
    <p>{{ $generalSettings->id }}</p>
</div>

<!-- Site Name Field -->
<div class="form-group">
    {!! Form::label('site_name', __('models/generalSettings.fields.site_name').':') !!}
    <p>{{ $generalSettings->site_name }}</p>
</div>

<!-- Site Title Field -->
<div class="form-group">
    {!! Form::label('site_title', __('models/generalSettings.fields.site_title').':') !!}
    <p>{{ $generalSettings->site_title }}</p>
</div>

<!-- Tag Line Field -->
<div class="form-group">
    {!! Form::label('tag_line', __('models/generalSettings.fields.tag_line').':') !!}
    <p>{{ $generalSettings->tag_line }}</p>
</div>

<!-- Copyright Text Field -->
<div class="form-group">
    {!! Form::label('copyright_text', __('models/generalSettings.fields.copyright_text').':') !!}
    <p>{{ $generalSettings->copyright_text }}</p>
</div>

<!-- Header Logo Field -->
<div class="form-group">
    {!! Form::label('header_logo', __('models/generalSettings.fields.header_logo').':') !!}
    <p><img class="image-preview" src="{{asset('images/'.$generalSettings->header_logo)}}" /></p>
</div>

<!-- Footer Logo Field -->
<div class="form-group">
    {!! Form::label('footer_logo', __('models/generalSettings.fields.footer_logo').':') !!}
    <p><img class="image-preview" src="{{asset('images/'.$generalSettings->footer_logo)}}" /></p>
</div>

<!-- Shop Status Field -->
<div class="form-group">
    {!! Form::label('shop_status', __('models/generalSettings.fields.shop_status').':') !!}
    <p>{{ $generalSettings->shop_status == 1 ? __('crud.active') : __('crud.disable') }}</p>
</div>

<!-- Shop facebook Field -->
<div class="form-group">
    {!! Form::label('facebook', __('models/generalSettings.fields.facebook').':') !!}
    <p>{{ $generalSettings->facebook }}</p>
</div>

<!-- Shop instagram Field -->
<div class="form-group">
    {!! Form::label('instagram', __('models/generalSettings.fields.instagram').':') !!}
    <p>{{ $generalSettings->instagram }}</p>
</div>

<!-- Shop youtube Field -->
<div class="form-group">
    {!! Form::label('youtube', __('models/generalSettings.fields.youtube').':') !!}
    <p>{{ $generalSettings->youtube }}</p>
</div>


<!-- Shop pinterest Field -->
<div class="form-group">
    {!! Form::label('pinterest', __('models/generalSettings.fields.pinterest').':') !!}
    <p>{{ $generalSettings->pinterest }}</p>
</div>

<!-- Shop twitter Field -->
<div class="form-group">
    {!! Form::label('twitter', __('models/generalSettings.fields.twitter').':') !!}
    <p>{{ $generalSettings->twitter }}</p>
</div>

<!-- Shop Stripe Publishable Key: Field -->
<div class="form-group">
    {!! Form::label('stripe_publishable_key:', __('models/generalSettings.fields.stripe_publishable_key:').':') !!}
    <p>{{ $generalSettings->stripe_publishable_key }}</p>
</div>
