<div class="table-responsive">
    <table class="table" id="generalSettings-table">
        <thead>
            <tr>
                <th>@lang('models/generalSettings.fields.id')</th>
                <th>@lang('models/generalSettings.fields.site_name')</th>
                <th>@lang('models/generalSettings.fields.site_title')</th>
                <th>@lang('models/generalSettings.fields.tag_line')</th>
                <th>@lang('models/generalSettings.fields.copyright_text')</th>
                <th>@lang('models/generalSettings.fields.shop_status')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($generalSettings as $generalSettings)
            <tr>
            <td>{{ $generalSettings->id }}</td>
            <td>{{ $generalSettings->site_name }}</td>
            <td>{{ $generalSettings->site_title }}</td>
            <td>{{ $generalSettings->tag_line }}</td>
            <td>{{ $generalSettings->copyright_text }}</td>
            <td>{{ $generalSettings->shop_status  == 1  ? __('crud.active') : __('crud.disable')}}</td>
           <td class=" text-center">
               {!! Form::open(['route' => ['generalSettings.destroy', $generalSettings->id], 'method' => 'delete']) !!}
               <div class='btn-group'>
                   <a href="{!! route('generalSettings.show', [$generalSettings->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                   <a href="{!! route('generalSettings.edit', [$generalSettings->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                   {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
               </div>
               {!! Form::close() !!}
           </td>
            </tr>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
