@extends('layouts.front')
@section('content')
<div class="root-content" style="margin-top: 60px;">
            @include('top-header')

            <div class="container px-0 px-lg-2">
                <div class="row">
                <div class="col-lg-8 col-xl-8 col-12 mt-3">
                    <section id="email_us" class="link-background info-status box-shadow p-lg-3 p-2 mt-2 mt-lg-0 mb-3">
                        {!! $contact_us->content ?? ''!!}
                    {{--<div class="row my-2">
                        <div class="col">
                        <h3><span>Info</span></h3>
                        </div>
                    </div>
                    <div class="row enquiries">
                        <div class="col ">
                        <h5><span>Order Enquiries</span></h5>
                        </div>
                    </div>
                    <div class="row enquiries">
                        <div class="col">
                        <p><span>If your order is late, incorrect , or if you're not happy with the food, please contact us
                            directly on</span> <a target="_blank" href="tel:{{$restaurant->phone_number ?? ''}}">{{$restaurant->phone_number ?? ''}} </a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col ">
                        <h5><span>For Payment issues/Enquiries</span></h5>
                        </div>
                    </div>
                    <div class="row enquiries">
                        <div class="col">
                        <p><span>If you paid online and require a refund , and you're unable to contact the takeaway
                                please contact Datman LTD on</span>&nbsp;<a target="_blank" href="tel:{{$restaurant->phone_number ?? ''}}">{{$restaurant->phone_number ?? ''}}</a>
                        </p>
                        <p><span>If you would like to go through frequently asked question, you can visit our online</span> <a target="_blank" href=""><span>Help
                                Center</span></a></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col">
                        <h5><span>Location</span></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                        <p>{{$settings['address'] ?? ''}}</p>
                        </div>
                    </div>--}}
                    <div class="row">
                        <div class="col pb-3">
                          <iframe

                              style="border:0;width: 100%; height: 450px"
                              loading="lazy"
                              allowfullscreen
                              referrerpolicy="no-referrer-when-downgrade"
                              src="{{$settings['map']}}">
                            </iframe>
                        </div>
                    </div>
                    <hr>
                    <section></section>
                    </section>
                </div>
                <div class="col-xl-4 col-lg-4 col-12 pl-lg-0 mt-3">
                    <section>
                    <section class="box-shadow link-background info-status pb-lg-0 pt-lg-3 py-3 mb-3 order-summary">
                        <div class="row mb-3">
                        <div class="col-12">
                            <h5><span>Opening Hours</span></h5>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col">
                            <table class="table table-bordered table-striped mt-0">
                            <thead>
                                <tr>
                                <th scope="col"><span>Day</span></th>
                                <th scope="col"><span>Collection</span></th>
                                <th scope="col"><span>Delivery</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($restaurant_timings as $timings)
                                <tr>
                                  <td><span>{{$timings->day}}</span></td>
                                  <td class="">
                                      <div>{{$timings->start_time}} - {{$timings->end_time}}<br></div>
                                  </td>
                                  <td>
                                      <div>{{$timings->start_time}} - {{$timings->end_time}}<br></div>
                                  </td>
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                        </div>
                        </div>
                    </section>
                    <section class="box-shadow link-background info-status py-lg-3 py-3 mb-3 order-summary">
                        <div class="row">
                        <div class="col-12">
                            <h5> <span>Cuisines</span></h5>
                        </div>
                        </div>
                        <div class="row">
                          @foreach($categories as $category)
                          <div class="col-6">
                              <div class="cuisine-item">{{$category->name}}</div>
                          </div>
                          @endforeach
                        </div>
                    </section>
                    <section class="box-shadow link-background info-status py-lg-3 py-3 mb-3 d-none">
                        <div class="row my-3">
                        <div class="col ">
                            <h5><span>Delivery price checker</span></h5>
                            <div>
                            <div class="row delivery_checker">
                                <div class="col-8 px-0"><input type="text" class="form-control text-uppercase" placeholder="Delivery price checker" name="price_checker" autocomplete="off"></div>
                                <div class="col"><button class="btn btn-outline-secondary" type="button"><span>Check</span></button></div>
                            </div>
                            <div></div>
                            </div>
                        </div>
                        </div>
                    </section>
                    </section>
                </div>
                </div>
            </div>
        </div>
@endsection
