<div class="table-responsive">
    <table class="table" id="locations-table">
        <thead>
            <tr>
                <th>@lang('models/locations.fields.name')</th>
        <th>@lang('models/locations.fields.details')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($locations as $locations)
            <tr>
                       <td>{{ $locations->name }}</td>
            <td>{{ $locations->details }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['locations.destroy', $locations->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('locations.show', [$locations->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('locations.edit', [$locations->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
