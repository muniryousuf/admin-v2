<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/locations.fields.name').':') !!}
    <p>{{ $locations->name }}</p>
</div>

<!-- Details Field -->
<div class="form-group">
    {!! Form::label('details', __('models/locations.fields.details').':') !!}
    <p>{{ $locations->details }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/locations.fields.created_at').':') !!}
    <p>{{ $locations->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/locations.fields.updated_at').':') !!}
    <p>{{ $locations->updated_at }}</p>
</div>

