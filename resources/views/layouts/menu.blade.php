<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="{{route('admin')}}">
        <i class=" fas fa-building"></i><span>Dashboard</span>
    </a>
    
</li>

<li class="{{ Request::is('generalSettings*') ? 'active' : '' }}">
    <a href="{{ route('generalSettings.index') }}"><i class="fa fa-edit"></i><span>@lang('models/generalSettings.plural')</span></a>
    <span class="submenutoggler"><i class="fa fa-plus"></i></span>
    <div class="submenulist">
        <ul>
            <li class="{{ Request::is('resturentAddresses*') ? 'active' : '' }}">
                <a href="{{ route('resturentAddresses.index') }}"><i class="fa fa-edit"></i><span>@lang('models/resturentAddresses.plural')</span></a>
            </li>
            <li class="{{ Request::is('deliveryChargesDetails*') ? 'active' : '' }}">
                <a href="{{ route('deliveryChargesDetails.index') }}"><i class="fa fa-edit"></i><span>@lang('models/deliveryChargesDetails.plural')</span></a>
            </li>
            <li class="{{ Request::is('sliders*') ? 'active' : '' }}">
                <a href="{{ route('sliders.index') }}"><i class="fa fa-edit"></i><span>@lang('models/sliders.plural')</span></a>
            </li>
            <li class="{{ Request::is('ourStories*') ? 'active' : '' }}">
                <a href="{{ route('ourStories.index') }}"><i class="fa fa-edit"></i><span>@lang('models/ourStories.plural')</span></a>
            </li>
            <li class="{{ Request::is('galleries*') ? 'active' : '' }}">
                <a href="{{ route('galleries.index') }}"><i class="fa fa-edit"></i><span>@lang('models/galleries.plural')</span></a>
            </li>

            <li class="{{ Request::is('cms*') ? 'active' : '' }}">
                <a href="{{ route('cms.index') }}"><i class="fa fa-edit"></i><span>@lang('models/cms.plural')</span></a>
            </li>
            <li class="{{ Request::is('smsSettings*') ? 'active' : '' }}">
                <a href="{{ route('smsSettings.index') }}"><i class="fa fa-edit"></i><span>@lang('models/smsSettings.plural')</span></a>
            </li>
            <li class="{{ Request::is('messageTemplatings*') ? 'active' : '' }}">
                <a href="{{ route('messageTemplatings.index') }}"><i class="fa fa-edit"></i><span>@lang('models/messageTemplatings.plural')</span></a>
            </li>

        </ul>
    </div>
</li>

<li class="{{ Request::is('orders*') ? 'active' : '' }}">
    <a href="{{ route('orders.index') }}"><i class="fa fa-edit"></i><span>@lang('models/orders.plural')</span></a>
</li>

<li class="{{ Request::is('tableReservations*') ? 'active' : '' }}">
    <a href="{{ route('tableReservations.index') }}"><i class="fa fa-edit"></i><span>@lang('models/tableReservations.plural')</span></a>
     <span class="submenutoggler"><i class="fa fa-plus"></i></span>
    <div class="submenulist"></div>
</li>

<li class="{{ Request::is('pages*') ? 'active' : '' }}">
    <a href="{{ route('pages.index') }}"><i class="fa fa-edit"></i><span>@lang('models/pages.plural')</span></a>
</li>

<li class="{{ Request::is('products*') ? 'active' : '' }}">
    <a href="{{ route('products.index') }}"><i class="fa fa-edit"></i><span>@lang('models/products.plural')</span></a>
</li>

<li class="{{ Request::is('productMetas*') ? 'active' : '' }}">
    <a href="{{ route('productMetas.index') }}"><i class="fa fa-edit"></i><span>@lang('models/productMetas.plural')</span></a>
</li>

<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{{ route('categories.index') }}"><i class="fa fa-edit"></i><span>@lang('models/categories.plural')</span></a>
</li>

<li class="{{ Request::is('resturentTimings*') || Request::is('restaurantTimingsSpecifics*') ? 'active' : '' }}">
    <a href="{{ route('resturentTimings.index') }}"><i class="fa fa-edit"></i><span>@lang('models/resturentTimings.plural')</span></a>
    <span class="submenutoggler"><i class="fa fa-plus"></i></span>
    <div class="submenulist">
        <ul>
            <li class="{{ Request::is('resturentTimings*') ? 'active' : '' }}">
                <a href="{{ route('resturentTimings.index') }}"><i class="fa fa-edit"></i><span>@lang('models/resturentTimings.plural')</span></a>
            </li>
            <li class="{{ Request::is('restaurantTimingsSpecifics*') ? 'active' : '' }}">
                <a href="{{ route('restaurantTimingsSpecifics.index') }}"><i class="fa fa-edit"></i><span>@lang('models/restaurantTimingsSpecifics.plural')</span></a>
            </li>
        </ul>
    </div>
</li>



<li class="{{ Request::is('paymentMethods*') ? 'active' : '' }}">
    <a href="{{ route('paymentMethods.index') }}"><i class="fa fa-edit"></i><span>@lang('models/paymentMethods.plural')</span></a>
</li>

<li class="{{ Request::is('reviews*') ? 'active' : '' }}">
    <a href="{{ route('reviews.index') }}"><i class="fa fa-edit"></i><span>@lang('models/reviews.plural')</span></a>
</li>

<li class="{{ Request::is('notifications*') ? 'active' : '' }}">
    <a href="{{ route('notifications.index') }}"><i class="fa fa-edit"></i><span>@lang('models/notifications.plural')</span></a>
</li>

<li class="{{ Request::is('vouchers*') ? 'active' : '' }}">
    <a href="{{ route('vouchers.index') }}"><i class="fa fa-edit"></i><span>@lang('models/vouchers.plural')</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{{ route('users.index') }}"><i class="fa fa-edit"></i><span>@lang('models/users.plural')</span></a>
</li>

<li class="{{ Request::is('tableCategories*') ? 'active' : '' }}">
    <a href="{{ route('tableCategories.index') }}"><i class="fa fa-edit"></i><span>@lang('models/tableCategories.plural')</span></a>
</li>

<li class="{{ Request::is('customerReservations*') ? 'active' : '' }}">
    <a href="{{ route('customerReservations.index') }}"><i class="fa fa-edit"></i><span>@lang('models/customerReservations.plural')</span></a>
</li>

<li class="{{ Request::is('locations*') ? 'active' : '' }}">
    <a href="{{ route('locations.index') }}"><i class="fa fa-edit"></i><span>@lang('models/locations.plural')</span></a>
</li>

<li class="{{ Request::is('locationsAttributes*') ? 'active' : '' }}">
    <a href="{{ route('locationsAttributes.index') }}"><i class="fa fa-edit"></i><span>@lang('models/locationsAttributes.plural')</span></a>
</li>

<li class="{{ Request::is('holdCarts*') ? 'active' : '' }}">
    <a href="{{ route('holdCarts.index') }}"><i class="fa fa-edit"></i><span>@lang('models/holdCarts.plural')</span></a>
</li>

<li class="{{ Request::is('caosGenralSettings*') ? 'active' : '' }}">
    <a href="{{ route('caosGenralSettings.index') }}"><i class="fa fa-edit"></i><span>@lang('models/caosGenralSettings.plural')</span></a>
</li>

