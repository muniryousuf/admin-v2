<div id="header" class="transform-header nav-down">
    <nav id="primary-navbar" class="resp-primary-navbar navbar navbar-light navbar-inverse navbar-fixed-top navbar-expand-lg p-sm-0 p-0">
        <div class="resp-navbar-container container container-bootstrap4 ">
            <div class="resp-navbar-header navbar-header navbar-bootstrap4">
                <button class="navbar-toggler navbar-toggle collapsed float-left mt-0" type="button">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <a class="navbar-brand mx-auto d-lg-none" href="{{route('home-page')}}">
                    <img src="{{asset('images/'.$settings['header_logo'] ?? '')}}" alt="{{ $settings['header_logo'] ?? ''}}">
                </a>
                <span class="nav-basket d-lg-none">
                    <a class="nav-link basket-icon" href="javascript:void(0);" >
                        <svg width="20" height="20" viewBox="0 0 1024 1024" class="icon-cart icon-Shopping-Cart" id="Shopping-Cart" fill="">
                            <path d="M192 192h102.4l32 64h473.6c6.4 0 19.2 0 19.2 6.4 6.4 6.4 6.4 12.8 6.4 25.6 0 6.4 0 12.8-6.4 12.8l-115.2 204.8c-6.4 12.8-12.8 19.2-25.6 25.6s-19.2 6.4-32 6.4h-236.8l-25.6 51.2v6.4c0 0 0 6.4 0 6.4s6.4 0 6.4 0h377.6v64h-384c-19.2 0-32-6.4-44.8-19.2s-19.2-25.6-19.2-44.8c0-12.8 0-19.2 6.4-32l44.8-76.8-115.2-236.8h-64v-64zM384 704c19.2 0 32 6.4 44.8 19.2s19.2 25.6 19.2 44.8c0 19.2-6.4 32-19.2 44.8s-25.6 19.2-44.8 19.2c-19.2 0-32-6.4-44.8-19.2s-19.2-25.6-19.2-44.8c0-19.2 6.4-32 19.2-44.8s25.6-19.2 44.8-19.2zM704 704c19.2 0 32 6.4 44.8 19.2s19.2 25.6 19.2 44.8c0 19.2-6.4 32-19.2 44.8s-25.6 19.2-44.8 19.2c-19.2 0-32-6.4-44.8-19.2s-19.2-25.6-19.2-44.8c0-19.2 6.4-32 19.2-44.8s25.6-19.2 44.8-19.2z">
                            </path>
                            </svg>
                        <span class="badge badge-danger "></span>
                    </a>
            </span>
                <a class="btn btn-fdhb d-lg-none" href="{{route('order-online')}}">
                    <span>Order Now</span>
                </a>
            </div>
            <div id="navbar" class="resp-navbar navbar-collapse collapse">
                <a class="navbar-brand d-none d-lg-block" href="{{route('home-page')}}">
                    <img src="{{asset('images/'.$settings['header_logo'] ?? '')}}" alt="{{ $settings['header_logo'] ?? ''}}">
                </a>

                <div id="menu-header" class="menu-header">
                    <ul id="menu-ul" class="resp-navbar-nav nav navbar-nav">
                        <li class="home set-redircets">
                            <a class="hyper_link set-redircets" href="{{route('home-page')}}">
                                <span>Home</span>
                            </a>
                        </li>
                        <li class="ordernow">
                            <a class="hyper_link" href="{{route('order-online')}}">
                                <span>Order Now</span>
                            </a>
                        </li>
                        <li class="ordernow d-none">
                            <a class="hyper_link" href="{{route('reservations')}}">
                                <span>Reservations</span>
                            </a>
                        </li>
                        <li class="ordernow">
                            <a class="hyper_link" href="{{route('contact-us')}}">
                                <span>Contact Us</span>
                            </a>
                        </li>
                        @foreach($headers as $header)
                            <li class="home set-redircets">


                                @if($header['page_type'] == 'static')
                                    <a class="hyper_link set-redircets" href="{{route('pages',['name'=>$header['name']])}}">
                                        <span>{{ucfirst(str_replace('-',' ',$header['name']))}}</span>
                                    </a>
                                @endif
                                @if($header['page_type'] == 'informative')
                                    <a class="hyper_link set-redircets" href="{{route('pages',['name'=>$header['name']])}}">
                                        <span>{{ucfirst(str_replace('-',' ',$header['name']))}}</span>
                                    </a>
                                @endif

                                @if($header['page_type'] == 'dynamic')
                                    <a class="hyper_link set-redircets" href="{{$header['name']}}">
                                        <span>{{ucfirst(str_replace('-',' ',$header['name']))}}</span>
                                    </a>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>

                <div class="header-social-holder ml-auto">
                    @if($settings['facebook'] != '')
                        <a class="header-social">
                            <i class="fa-brands fa-facebook social-link-opener" link="{{$settings['facebook'] ?? ''}}"></i>
                        </a>
                    @endif
                    @if($settings['instagram'] != '')
                        <a class="header-social">
                            <i class="fa-brands fa-instagram social-link-opener" link="{{$settings['instagram'] ?? ''}}"></i>
                        </a>
                    @endif
                    @if($settings['twitter'] != '')
                        <a class="header-social">
                            <i class="fa-brands fa-twitter social-link-opener" link="{{$settings['twitter'] ?? ''}}"></i>
                        </a>
                    @endif
                    @if($settings['ticktok'] != '')
                        <a class="header-social">
                            <i class="fa-brands fa-tiktok social-link-opener" link="{{$settings['ticktok'] ?? ''}}"></i>
                        </a>
                    @endif
                    @if($settings['linkedinn'] != '')
                        <a class="header-social">
                            <i class="fa-brands fa-linkedin social-link-opener" link="{{$settings['linkedinn'] ?? ''}}"></i>
                        </a>
                    @endif
                    @if($settings['snapchat'] != '')
                        <a class="header-social">
                            <i class="fa-brands fa-snapchat social-link-opener" link="{{$settings['snapchat'] ?? ''}}"></i>
                        </a>
                    @endif
                    @if($settings['youtube'] != '')
                        <a class="header-social">
                            <i class="fa-brands fa-youtube social-link-opener" link="{{$settings['youtube'] ?? ''}}"></i>
                        </a>
                    @endif
                    <button type="button" class="social-icons-collapse"><i class="fa-solid fa-ellipsis-vertical"></i></button>
                </div>
                <ul id="lang" class="resp-navbar-nav-right nav navbar-nav navbar-right flex-nowrap">
                    <li class="nav-basket d-none d-lg-block">
                        <a class="nav-link basket-icon" href="{{route('order-online')}}">
                            <svg width="20" height="20" viewBox="0 0 1024 1024" class="icon-cart icon-Shopping-Cart" id="Shopping-Cart" fill="">
                                <path d="M192 192h102.4l32 64h473.6c6.4 0 19.2 0 19.2 6.4 6.4 6.4 6.4 12.8 6.4 25.6 0 6.4 0 12.8-6.4 12.8l-115.2 204.8c-6.4 12.8-12.8 19.2-25.6 25.6s-19.2 6.4-32 6.4h-236.8l-25.6 51.2v6.4c0 0 0 6.4 0 6.4s6.4 0 6.4 0h377.6v64h-384c-19.2 0-32-6.4-44.8-19.2s-19.2-25.6-19.2-44.8c0-12.8 0-19.2 6.4-32l44.8-76.8-115.2-236.8h-64v-64zM384 704c19.2 0 32 6.4 44.8 19.2s19.2 25.6 19.2 44.8c0 19.2-6.4 32-19.2 44.8s-25.6 19.2-44.8 19.2c-19.2 0-32-6.4-44.8-19.2s-19.2-25.6-19.2-44.8c0-19.2 6.4-32 19.2-44.8s25.6-19.2 44.8-19.2zM704 704c19.2 0 32 6.4 44.8 19.2s19.2 25.6 19.2 44.8c0 19.2-6.4 32-19.2 44.8s-25.6 19.2-44.8 19.2c-19.2 0-32-6.4-44.8-19.2s-19.2-25.6-19.2-44.8c0-19.2 6.4-32 19.2-44.8s25.6-19.2 44.8-19.2z">
                                </path>
                            </svg>
                            <span class="badge badge-danger "></span>
                        </a>
                    </li>
                    @if(!Auth::check())
                        <li class="login">
                            <a class="nav-login login-modal-show" id="login" href="javascript:void(0);"><span>Log In</span>
                            </a>
                        </li>
                    @else
                        <li>
                            <a class="nav-login" href="{{route('user.my-account')}}">My Account</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</div>
@include('modals.login-modal')
@include('modals.signup-modal')
@include('modals.guest-modal')
