<div id="mobile-accordion">
    <div class="ant-back-top active"><div class="ant-back-top-content"><div class="ant-back-top-icon"></div></div></div>
        <footer class="page-footer">
            <div class="container">
                <div class="row">
                    @foreach(App\Models\Pages::TYPE_FOOTER as $type)
                    <div class="col-12 col-md-2 col-lg-2 col-sm-12">
                        @if(isset($footers[$type]))
                            <ul class="list-unstyled list-block social">
                                @foreach($footers[$type] as $footer)
                                    <li class="list-block-item">
                                    @if($footer['page_type'] == 'static')
                                    <a class="hyper_link set-redircets" href="{{route('pages',['name'=>$footer['name']])}}">
                                        <span>{{ucfirst(str_replace('-',' ',$footer['name']))}}</span>
                                    </a>
                                @endif
                                @if($footer['page_type'] == 'informative')
                                    <a class="hyper_link set-redircets" href="{{route('pages',['name'=>$footer['name']])}}">
                                        <span>{{ucfirst(str_replace('-',' ',$footer['name']))}}</span>
                                    </a>
                                @endif

                                @if($footer['page_type'] == 'dynamic')
                                    <a class="hyper_link set-redircets" href="{{$footer['name']}}">
                                        <span>{{ucfirst(str_replace('-',' ',$footer['name']))}}</span>
                                    </a>
                                @endif
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    @endforeach


                  <!--   <hr class="d-block d-lg-none mt-1 mb-3 w-100">
                    <div class="col-4 col-lg-2 col-md-6 app-images ml-lg-auto">
                        <div class="store row m-0"></div>
                    </div> -->
                    <div class="col-12 col-lg-3 col-md-6 app-images my-3 mb-lg-0">
                        <div class="logo row m-0">
                            <img src="{{asset('images/compressed_images-verified-by.svg')}}" class="img-fluid mb-3 lazyload" alt="Verify" title="Verify" loading="lazy" height="100%" width="100%">
                            <a href="{{route('home-page')}}" target="_blank">
                             <img class="footer-logo img-fluid d-block mx-auto" src="{{asset('images/'.$settings['footer_logo'] ?? '')}}" alt="{{ $settings['footer_logo'] ?? ''}}">
                            </a>
                    </div>
                </div>
            </div>
        </footer>
        <div class="footer-bottom container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-6 copyright">

                            <p>{{$settings['copyright_text']}}</p>
                        </div>
                        <div class="col-md-6 social-icon">
                            <p>
                                @if($settings['facebook'] != '')
                                    <a><i class="fa-brands fa-facebook social-link-opener"
                                        link="{{$settings['facebook'] ?? ''}}"></i>
                                    </a>
                                @endif
                                @if($settings['instagram'] != '')
                                <a><i class="fa-brands fa-instagram social-link-opener"
                                    link="{{$settings['instagram'] ?? ''}}"></i>
                                </a>
                                 @endif
                                @if($settings['twitter'] != '')
                                <a><i class="fa-brands fa-twitter social-link-opener"
                                    link="{{$settings['twitter'] ?? ''}}"></i>
                                </a>
                                 @endif
                                @if($settings['ticktok'] != '')
                                <a><i class="fa-brands fa-tiktok social-link-opener"
                                    link="{{$settings['ticktok'] ?? ''}}"></i>
                                </a>
                                 @endif
                                @if($settings['linkedinn'] != '')
                                <a><i class="fa-brands fa-linkedin social-link-opener"
                                    link="{{$settings['linkedinn'] ?? ''}}"></i>
                                </a>
                                 @endif
                                @if($settings['snapchat'] != '')
                                <a><i class="fa-brands fa-snapchat social-link-opener"
                                    link="{{$settings['snapchat'] ?? ''}}"></i>
                                </a>
                                 @endif
                                @if($settings['youtube'] != '')
                                <a><i class="fa-brands fa-youtube social-link-opener"
                                    link="{{$settings['youtube'] ?? ''}}"></i>
                                </a>
                                 @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
