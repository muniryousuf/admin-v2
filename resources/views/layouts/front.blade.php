<!DOCTYPE html>
<html>
    <head>
        @php
           $settings =  getSettings();
           $headers =  getHeader();
           $footers =  getFooter();
           $currency_default = $settings['currency'] ?? '';
        @endphp
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        <title id="home_title">@yield('title') {{ $settings['site_title'] ?? ''}}</title>
        <link rel="icon" type="image/x-icon" href="{{asset('images/'.$settings['fav_icon']) ?? ''}}">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
{{--        <link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.1/owl.carousel.css" rel="stylesheet">--}}



        <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet" type="text/css"/>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.1/owl.theme.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
        <link href="{{ asset('css/libraries.css') }}" rel="stylesheet">
        <link href="{{ asset('css/maincss.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/sweetalert.css') }}" rel="stylesheet" type="text/css"/>
        @yield('page_css')
    </head>

    @php
        $custom_style = '';
       // if(Request::is('order-online*') || Request::is('/*')){
            $image_path = asset('images/'.$settings['background_image']);
            $custom_style = "style=background:url(".$image_path.")";
        //}
    @endphp
    <body class="resp-body pt-0" {{$custom_style}}>
        <input type="hidden" id="default-currency" value="{{$currency_default}}">
        <div class="wrapper">
           <!-- header -->
           @include('layouts.front-header', ['settings'=>$settings,'headers'=>$headers])
            <!-- header -->

            <!-- Main Content -->
                @yield('content')
            <!-- Main Content -->

             <!-- footer -->
            @include('layouts.front-footer', ['settings'=>$settings , 'footers' => $footers])
            <!-- footer -->

            <div class="ant-drawer ant-drawer-left">
                <div class="ant-drawer-mask"></div>
                    <div class="ant-drawer-content-wrapper">
                        <div class="ant-drawer-content">
                            <div class="ant-drawer-wrapper-body">
                                <div class="ant-drawer-body">
                                    <a class="navbar-brand" href="/">
                                        <img src="{{asset('images/'.$settings['header_logo'] ?? '')}}" class="img-fluid d-block" alt="logo" title="logo">
                                    </a>
                                    <ul id="menu-ul" class="resp-navbar-nav nav navbar-nav">
                                        <li class="home set-redircets">
                                            <a class="hyper_link set-redircets" href="{{route('home-page')}}">
                                                <span>Home</span>
                                            </a>
                                        </li>
                                        <li class="ordernow">
                                            <a class="hyper_link" href="{{route('order-online')}}">
                                                <span>Order Now</span>
                                            </a>
                                        </li>
                                        <li class="ordernow">
                                            <a class="hyper_link" href="{{route('contact-us')}}">
                                                <span>Contact Us</span>
                                            </a>
                                        </li>

                                        @foreach($headers as $header)
                                            <li class="home set-redircets">

                                                @if($header['page_type'] == 'static')
                                                    <a class="hyper_link set-redircets" href="{{route('pages',['name'=>$header['name']])}}">
                                                        <span>{{ucfirst(str_replace('-',' ',$header['name']))}}</span>
                                                    </a>
                                                @endif

                                                @if($header['page_type'] == 'dynamic')
                                                    <a class="hyper_link set-redircets" href="{{$header['name']}}">
                                                        <span>{{ucfirst(str_replace('-',' ',$header['name']))}}</span>
                                                    </a>
                                                @endif

                                                @if($header['page_type'] == 'informative')
                                                    <a class="hyper_link set-redircets" href="{{route('pages',['name'=>$header['name']])}}">
                                                        <span>{{ucfirst(str_replace('-',' ',$header['name']))}}</span>
                                                    </a>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </body>


    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/wow.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="{{ asset('js/mainscripts.js') }}"></script>
    <script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>



    @yield('scripts')
    <script type="text/javascript">
        var website_settings = @json($settings);
        $('.open-signup-modal').click(function(){
            $('.login-modal').modal('hide');
            $('.signup-modal').modal('show');
        });
        $('.open-guest-modal').click(function(){
            $('.login-modal').modal('hide');
            $('.signup-modal').modal('hide');
            $('.guest-modal').modal('show');
        });
        $('#send-signup-request').click(function(){
            let name = $('.signup-name').val();
            let lname = $('.signup-lname').val();
            let email = $('.signup-email').val();
            let phone = $('.signup-phone').val();
            let password = $('.signup-password').val();
            let confirm_password = $('.signup-confirm-password').val();

            if(name == ''){
               return swal('','Please enter first name','error');
            }
            if(lname == ''){
               return swal('','Please enter last name','error');
            }
            if(email == ''){
               return swal('','Please enter email','error');
            }
            if(phone == ''){
               return swal('','Please enter phone number','error');
            }
            if(password == ''){
               return swal('','Please enter password','error');
            }
            if(confirm_password == ''){
               return swal('','Please enter confirm password','error');
            }
             if(password != confirm_password){
               return swal('',"Password doesn't match",'error');
            }

            let request_data = {
                name:name,
                lname:lname,
                email:email,
                phone:phone,
                password:password,
                password_confirmation:confirm_password,
                _token: '{{ csrf_token() }}'
            };
            $.ajax({
                type:'POST',
                url:"{{route('user-registration')}}",
                data:request_data,
                dataType:'json',
                async:false,
                success:function(response) {
                  location.reload()
                },
                error:function(response) {
                    let errors = $.parseJSON(response.responseText);
                    swal('',errors.errors.email[0],'error');
                },
            });

        });
        $('#send-guest-guest-request').click(function(){
            let name = $('.guest-name').val();
            let email = $('.guest-email').val();
            let phone = $('.guest-phone').val();

            if(name == ''){
               return swal('','Please enter first name','error');
            }
            if(email == ''){
               return swal('','Please enter email','error');
            }
            if(phone == ''){
               return swal('','Please enter phone number','error');
            }


            let request_data = {
                name:name,
                email:email,
                phone:phone,
                guest:true,
                _token: '{{ csrf_token() }}'
            };
            $.ajax({
                type:'POST',
                url:"{{route('user-registration')}}",
                data:request_data,
                dataType:'json',
                async:false,
                success:function(response) {
                  location.reload()
                },
                error:function(response) {
                    let errors = $.parseJSON(response.responseText);
                    swal('',errors.errors.email[0],'error');
                },
            });

        });



        $('.login-modal-show').click(function(){
            if($(window).width() < 768) {
                $('.ant-drawer').removeClass('ant-drawer-open');
            }
            $('.login-modal').modal('show');
        });
        $('.logout-user').click(function(){
            let request_data = {
                _token: '{{ csrf_token() }}'
            };

            $.ajax({
                type:'POST',
                url:"{{route('logout')}}",
                data:request_data,
                dataType:'json',
                async:false,
                success:function(response) {
                  window.location.href='{{route("home-page")}}';
                },
                error:function(response) {
                    let errors = $.parseJSON(response.responseText);
                    swal('',errors.errors.email[0],'error');
                },
            });

        })
        $('#send-login-request').click(function(){
            let email = $('#login-email').val();
            let password = $('#login-password').val();
            if(email == ''){
                swal('','Please enter email address','error');
            }
            if(password == ''){
                swal('','Please enter password','error');
            }
            let request_data = {
                email: email,
                password: password,
                _token: '{{ csrf_token() }}'
            };
            $.ajax({
                type:'POST',
                url:"{{route('login')}}",
                data:request_data,
                dataType:'json',
                async:false,
                success:function(response) {
                  location.reload()
                },
                error:function(response) {
                    let errors = $.parseJSON(response.responseText);
                    swal('',errors.errors.email[0],'error');
                },
            });
        });
        $('.go-to-page').click(function(){
            let page_link = $(this).attr('page-link');
            window.location.href=page_link;
        });
        $('.social-link-opener').click(function(){
            let link = $(this).attr('link');
            window.open(link, '_blank');
        });
        $('.open-review-page').click(function(){
          window.location.href = "{{route('reviews')}}";
        });
        $(document).ready(function(){
        var totalSocialIcons = $('.header-social-holder').find('.header-social').length;
        var sliceItem = totalSocialIcons - 2;
        $('.header-social').slice(2,totalSocialIcons).wrapAll('<div class="extra-social-icons">');
        $('.social-icons-collapse').on('click',function(e){
            e.preventDefault();
            $('.extra-social-icons').stop(true,true).toggleClass('show');
        });
        $(document).on("click", function(event){
            var $trigger = $(".social-icons-collapse");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $('.extra-social-icons').removeClass('show');
            }
        });

         function removeStorage(name) {
            try {
                localStorage.removeItem(name);
                localStorage.removeItem(name + '_expiresIn');
            } catch(e) {
                console.log('removeStorage: Error removing key ['+ key + '] from localStorage: ' + JSON.stringify(e) );
                return false;
            }
            return true;
        }

        function getStorage(key) {

            var now = Date.now();  //epoch time, lets deal only with integer
            // set expiration for storage
            var expiresIn = localStorage.getItem(key+'_expiresIn');
            if (expiresIn===undefined || expiresIn===null) { expiresIn = 0; }

            if (expiresIn < now) {// Expired
                removeStorage(key);
                return null;
            } else {
                try {
                    var value = localStorage.getItem(key);
                    return value;
                } catch(e) {
                    console.log('getStorage: Error reading key ['+ key + '] from localStorage: ' + JSON.stringify(e) );
                    return null;
                }
            }
        }

        function setStorage(key, value, expires) {

            if (expires===undefined || expires===null) {
                expires = (24*60*60);  // default: seconds for 1 day
            } else {
                expires = Math.abs(expires); //make sure it's positive
            }

            var now = Date.now();  //millisecs since epoch time, lets deal only with integer
            var schedule = now + expires*1000;
            try {
                localStorage.setItem(key, value);
                localStorage.setItem(key + '_expiresIn', schedule);
            } catch(e) {
                console.log('setStorage: Error setting key ['+ key + '] in localStorage: ' + JSON.stringify(e) );
                return false;
            }
            return true;
        }
        $(window).on('load',function(){
            @if(Route::currentRouteName() == 'home-page')
                // $('.promotion-modal-close-area').append(`
                //     <button type="button" class="never-show-again btn btn-small btn-danger" data-dismiss="modal" aria-label="Close">
                //     <span aria-hidden="true">Never Show Again</span>
                //     </button>
                //     `)
                if(!localStorage.getItem('promoion-show-never')){
                    $('.promotion-modal').modal('show')
                }
            @else
               let promotion = getStorage('promotion');
               if(!promotion){
                $('.promotion-modal').modal('show')
                setStorage('promotion',1)
               }
            @endif
        });
        $(document).on('click', '.never-show-again', function(e) {
            localStorage.setItem('promoion-show-never',1);
            $('.promotion-modal').modal('hide');
        });
    });






    </script>
</html>
