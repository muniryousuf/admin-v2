@extends('layouts.app')
@section('title')
     @lang('models/ourStories.plural')
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@lang('models/ourStories.plural')</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('ourStories.create')}}" class="btn btn-primary form-btn">@lang('crud.add_new')<i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('our_stories.table')
            </div>
       </div>
   </div>
    
    </section>
@endsection



