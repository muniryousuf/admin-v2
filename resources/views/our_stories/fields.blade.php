<!-- Main Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('main_title', __('models/ourStories.fields.main_title').':') !!}
    {!! Form::text('main_title', null, ['class' => 'form-control']) !!}
</div>

<!-- All Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('all_title', __('models/ourStories.fields.all_title').':') !!}
    {!! Form::text('all_title', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/ourStories.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', __('models/ourStories.fields.image').':') !!}
    {!! Form::file('image') !!}
     @if(isset($ourStory->image))
    <img class="image-preview" src="{{asset('images/'.$ourStory->image)}}" />
    @endif
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('ourStories.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
