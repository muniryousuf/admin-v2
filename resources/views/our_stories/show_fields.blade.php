<!-- Main Title Field -->
<div class="form-group">
    {!! Form::label('main_title', __('models/ourStories.fields.main_title').':') !!}
    <p>{{ $ourStory->main_title }}</p>
</div>

<!-- All Title Field -->
<div class="form-group">
    {!! Form::label('all_title', __('models/ourStories.fields.all_title').':') !!}
    <p>{{ $ourStory->all_title }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', __('models/ourStories.fields.description').':') !!}
    <p>{{ $ourStory->description }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', __('models/ourStories.fields.image').':') !!}
    <p><img class="image-preview" src="{{asset('images/'.$ourStory->image)}}" /></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/ourStories.fields.created_at').':') !!}
    <p>{{ $ourStory->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/ourStories.fields.updated_at').':') !!}
    <p>{{ $ourStory->updated_at }}</p>
</div>

