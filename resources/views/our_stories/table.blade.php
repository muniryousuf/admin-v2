<div class="table-responsive">
    <table class="table" id="ourStories-table">
        <thead>
            <tr>
                <th>@lang('models/ourStories.fields.main_title')</th>
        <th>@lang('models/ourStories.fields.all_title')</th>
        <th>@lang('models/ourStories.fields.description')</th>
        <th>@lang('models/ourStories.fields.image')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($ourStories as $ourStory)
            <tr>
                       <td>{{ $ourStory->main_title }}</td>
            <td>{{ $ourStory->all_title }}</td>
            <td>{{ $ourStory->description }}</td>
            <td><img class="image-preview" src="{{asset('images/'.$ourStory->image)}}" /></td>
               <td class=" text-center">
                   {!! Form::open(['route' => ['ourStories.destroy', $ourStory->id], 'method' => 'delete']) !!}
                   <div class='btn-group'>
                       <a href="{!! route('ourStories.show', [$ourStory->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                       <a href="{!! route('ourStories.edit', [$ourStory->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                   </div>
                   {!! Form::close() !!}
               </td>
           </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
