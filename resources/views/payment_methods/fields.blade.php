<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/paymentMethods.fields.name').':') !!}
    @php $selected_method = $paymentMethods->name ?? ''; @endphp
    <select name="name" class="form-control">
        @foreach(App\Models\PaymentMethods::PAYMENT_METHODS as $payment_method)
            <option value="{{$payment_method}}" {{$selected_method == $payment_method ? 'selected' : ''}}>{{ucfirst(str_replace('_',' ',$payment_method))}}</option>
        @endforeach
    </select>
</div>

<!-- Private Key Field -->
<div class="form-group col-sm-6">
    {!! Form::label('private_key', __('models/paymentMethods.fields.private_key').':') !!}
    {!! Form::text('private_key', null, ['class' => 'form-control']) !!}
</div>

<!-- Public Key Field -->
<div class="form-group col-sm-6">
    {!! Form::label('public_key', __('models/paymentMethods.fields.public_key').':') !!}
    {!! Form::text('public_key', null, ['class' => 'form-control']) !!}
</div>

<!-- Another Key Field -->
<div class="form-group col-sm-6">
    {!! Form::label('another_key', __('models/paymentMethods.fields.another_key').':') !!}
    {!! Form::text('another_key', null, ['class' => 'form-control']) !!}
</div>

<!-- Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('active', __('models/paymentMethods.fields.active').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('active', 0) !!}
        {!! Form::checkbox('active', '1', null) !!} 
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('paymentMethods.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
