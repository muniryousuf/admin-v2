<div class="table-responsive">
    <table class="table" id="paymentMethods-table">
        <thead>
            <tr>
                <th>@lang('models/paymentMethods.fields.name')</th>
        <th>@lang('models/paymentMethods.fields.private_key')</th>
        <th>@lang('models/paymentMethods.fields.public_key')</th>
        <th>@lang('models/paymentMethods.fields.another_key')</th>
        <th>@lang('models/paymentMethods.fields.active')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($paymentMethods as $paymentMethods)
            <tr>
                       <td>{{ $paymentMethods->name }}</td>
            <td>{{ $paymentMethods->private_key }}</td>
            <td>{{ $paymentMethods->public_key }}</td>
            <td>{{ $paymentMethods->another_key }}</td>
            <td>{{ $paymentMethods->active }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['paymentMethods.destroy', $paymentMethods->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('paymentMethods.show', [$paymentMethods->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('paymentMethods.edit', [$paymentMethods->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
