<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/paymentMethods.fields.name').':') !!}
    <p>{{ $paymentMethods->name }}</p>
</div>

<!-- Private Key Field -->
<div class="form-group">
    {!! Form::label('private_key', __('models/paymentMethods.fields.private_key').':') !!}
    <p>{{ $paymentMethods->private_key }}</p>
</div>

<!-- Public Key Field -->
<div class="form-group">
    {!! Form::label('public_key', __('models/paymentMethods.fields.public_key').':') !!}
    <p>{{ $paymentMethods->public_key }}</p>
</div>

<!-- Another Key Field -->
<div class="form-group">
    {!! Form::label('another_key', __('models/paymentMethods.fields.another_key').':') !!}
    <p>{{ $paymentMethods->another_key }}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', __('models/paymentMethods.fields.active').':') !!}
    <p>{{ $paymentMethods->active }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/paymentMethods.fields.created_at').':') !!}
    <p>{{ $paymentMethods->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/paymentMethods.fields.updated_at').':') !!}
    <p>{{ $paymentMethods->updated_at }}</p>
</div>

