<div class="table-responsive">
    <table class="table" id="tableCategories-table">
        <thead>
            <tr>
                <th>@lang('models/tableCategories.fields.name')</th>
        <th>@lang('models/tableCategories.fields.status')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($tableCategories as $tableCategories)
            <tr>
                       <td>{{ $tableCategories->name }}</td>
            <td>{{ $tableCategories->status }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['tableCategories.destroy', $tableCategories->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('tableCategories.show', [$tableCategories->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('tableCategories.edit', [$tableCategories->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
