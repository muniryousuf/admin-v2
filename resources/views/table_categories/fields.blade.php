<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/tableCategories.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', __('models/tableCategories.fields.status').':') !!}
    @php 
        $selected = '';
        if(isset($tableCategories->status)){
            $selected = $tableCategories->status;
        }


    @endphp
    <select name="status" class="form-control">
        <option {{$selected == 1}} value="1">Active</option>
        <option {{$selected == 0}} value="0">Disabled</option>
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('tableCategories.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
