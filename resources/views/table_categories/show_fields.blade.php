<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/tableCategories.fields.name').':') !!}
    <p>{{ $tableCategories->name }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', __('models/tableCategories.fields.status').':') !!}
    <p>{{ $tableCategories->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/tableCategories.fields.created_at').':') !!}
    <p>{{ $tableCategories->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/tableCategories.fields.updated_at').':') !!}
    <p>{{ $tableCategories->updated_at }}</p>
</div>

