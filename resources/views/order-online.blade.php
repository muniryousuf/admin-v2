@extends('layouts.front')
@section('content')
    <div class="root-content" style="margin-top: 60px;">
       @include('top-header')
        <div class="container order-online-page">
            <div class="row gutter-sm">
              <div class="col-lg category-list-wrapper d-none d-lg-block">
                <div class="inner">
                  <section class="on-mobile-view category-card">
                    <h3>Categories</h3>
                    <ul class="nav flex-column resp-side-nav" id="ul-scroll-category">
                    </ul>
                  </section>
                </div>
              </div>
              <div class="col-lg menu-list-wrapper mb-0">
                <div class="sticky-menu-list d-none" id="shop-closed-message">
                    <span class="text-center">Shop is closed</span>
                </div>
                <div class="sticky-menu-list">
                    <div class="search-background">
                        <div class="search takeaway-search-input">
                            <div class="input-group ">
                                <div class="input-group-append">
                                    <button class="btn border-0 pl-0" type="button">
                                       <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                <input type="text" class="form-control search-here" autocomplete="off" placeholder="Search Items">
                            </div>
                        </div>
                    </div>
                </div>
                @if(request()->table_id > 0)
                    <p class="btn btn-proceed btn-lg btn-block" >Table id:{{request()->table_id}}</p>
                @endif
                <div class="main-items-list">
                    <div id="accordion" class="products-area"></div>
                </div>
              </div>
              <div class="col-lg basket-wrapper">
                <div class="basket-wrapper-inner">
                    <section id="cart-Basket" class="basket-order-view bg-white">
                        <div class="d-flex cart_head">
                            <h3><span>Your Basket</span></h3>
                        </div>
                        <div id="basket-body">
                            <p class="title-menu">Time </p>
                            <select class="form-control" id="time-selection-for-delivery">
                            </select>
                            <div class="basket-order-items basket-quick-checkout" id="cart-items">
                            </div>
                            <section class="basket-order-view border-0 link-background quick_checkout_flow" id="basket-view-order-reciept">
                                <hr class="my-2 hr-text hr-more-less" data-content="Options" data-toggle="collapse" href="#basketmoredetails">
                                <div class="collapse" id="basketmoredetails">
                                    <div class="row no-gutters">
                                        @if(request()->table_id > 0)
                                            <div class="col-7 text-left px-0">
                                            <p>Table id</p>
                                            </div>
                                            <div class="col-5 pr-0 text-right"><span >{{request()->table_id}}</span></div>
                                        @endif
                                        <div class="col-7 text-left px-0">
                                        <p>Delivery Fees</p>
                                        </div>
                                        <div class="col-5 pr-0 text-right"><span id="delivery-fees-text">0</span></div>
                                        <div class="col-7 text-left px-0">
                                         <p>Total Discount</p>
                                        </div>
                                        <div class="col-5 pr-0 text-right"><span id="total-discount-text">0</span></div>
                                        <div class="col-7 text-left px-0">
                                        <p>Sub Total</p>
                                        </div>
                                        <div class="col-5 pr-0 text-right"><span id="total-price">
                                            <span class="loading"><span class="loading-bar"></span><span class="loading-bar"></span><span class="loading-bar"></span><span class="loading-bar"></span><span class="loading-bar"></span></span></span></div>
                                        <div class="col-7 text-left px-0">
                                        <p>Address</p>
                                        </div>
                                        <div class="col-5 pr-0 text-right"><span id="selected-address">Not Selected</span></div>
                                        <div class="col-7 text-left px-0">
                                        <p>Postal Code</p>
                                        </div>
                                        <div class="col-5 pr-0 text-right"><span id="selected-postal-code">Not Selected</span></div>
                                    </div>
                                    <div class="col col-12 px-0  p-lg-0">
                                        <div class="total_value">
                                        <div class="row no-gutters d-none">
                                            <div class="col col-9">
                                            <p class="other-charges">Service charge</p>
                                            </div>
                                            <div class="col text-right">
                                            <p class=" other-charges text-right" >£0.50</p>
                                            </div>
                                        </div>
                                        <div class="row"></div>
                                        <div class="col col-12  px-0  p-lg-0">
                                            <div class="total_value">
                                            <div class="coupon_code_apply ">
                                                <div class=" pb-0 link-background border-0">
                                                <div class="input-group border-0">
                                                    <div class="col-12 px-0  border-bottom-instruction"><input type="text" id="allergy_instruction" name="allergy_instruction" class="form-control noborder pl-0" autocomplete="off" placeholder="e.g. instructions for your order" value=""></div>
                                                </div>
                                                <div class="col px-0 mb-1 allergy-instruction my-auto"><a><span>Any food allergy?</span></a></div>

                                                </div>
                                                <hr>
                                                <div>
                                                    <div class="col px-0 mb-1 allergy-instruction my-auto"><a><span>Coupon</span></a></div>
                                                    <div class="input-group border-0">
                                                    <div class="col-8 px-0  border-bottom-instruction">
                                                        <input type="text" id="coupon-code" name="coupon-code" class="form-control noborder pl-0" autocomplete="off" placeholder="enter coupon code">
                                                    </div>
                                                    <div class="col-4">   <span class="border-add-item apply-coupon-code">Apply</span></div>

                                                </div>
                                                </div>
                                                <div class="col col-12  px-0  p-lg-0 mt-2">
                                                <div class="quick_checkout total_value"></div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <hr class="my-2 hr-text hr-more-less " data-content="Options" data-toggle="collapse" href="#basketmoredetails">
                                </div>
                                <div class="arrow-icon text-center mt-n3" data-toggle="collapse" href="#basketmoredetails">
                                    <a class="accordian-open">
                                        <i class="down-arrow icon-accordian">
                                            <svg width="20" height="20" viewBox="0 0 1024 1024" class="" id="Down-Arrow" fill="">
                                            <path d="M294.4 332.8l217.6 224 217.6-224 70.4 70.4-288 288-288-288 70.4-70.4z"></path>
                                            </svg>
                                        </i>
                                    </a>
                                </div>
                            </section>
                        </div>
                        <section id="checkout_sec">
                            <div class="total_value">
                                <div class="row">
                                <div class="col" id="checkout-btn"><button class="btn btn-proceed btn-lg btn-block" id="quick_checkout_button"><span>Place Order</span></button></div>
                                </div>
                            </div>
                        </section>
                    </section>

                </div>
              </div>
            </div>
          </div>
    </div>

@include('modals.product-attributes-modal')
@include('modals.user-address')
@include('modals.user-addresses')
@include('modals.order-type-selection')
@include('modals.postal-code-modal')
@endsection
@section('scripts')
<script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
<script>
    var default_currecny = $('#default-currency').val();
    var allowed_timings = {};
    var selected_order_type_time;
    var is_order_allowed = false;
    var is_dine_in = {{isset(request()->table_id) ? 1 : 0}};

    function fetchTimings(){
        $.ajax({
            type:'GET',
            url:"{{route('api.get-today-timing')}}",
            dataType:'json',
            async:false,
            success:function(response) {
                if(!response.data.shop_closed){
                    is_order_allowed = true;
                }
            }
        });

         if(!is_order_allowed){
            $('#shop-closed-message').removeClass('d-none')
            $('#quick_checkout_button').addClass('disabled');
         }

    }
    function placeOrder(cart){
        let _token = "{{csrf_token()}}";
        let table_id = {{request()->table_id ?? 0}}
        cart._token =_token;
        cart.table_id =table_id;
        cart.special_instructions = $('#allergy_instruction').val();
        let order = {};
        $.ajax({
            type:'POST',
            url:"{{route('web.place-order')}}",
            data:cart,
            dataType:'json',
            async:false,
            success:function(response) {
                order = response.data
            }
        });
        return order;
    }

    function getCartItemsHtml(item){
        let selection = '';
        if(item.selection){
            selection = JSON.parse(item.selection).join(" , ");
        }
        // console.log(selection)
        let product = item.product;
        if(!product){
            return;
        }
        return ` <div class="items-in-basket">
                    <div class="row">
                        <div class="col"></div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col">
                            <p class="title-menu">${product.name} </p>
                        </div>
                        <div class="col-auto"><span class="border-add-item">
                            <a id="delete" ref_id="${item.id}">
                                <svg width="16" height="17" viewBox="0 0 1024 1024" class="icon-Delete"  fill="">
                                <path d="M584.533 422.4c-6.4 0-10.667 2.133-14.933 6.4s-6.4 10.667-6.4 17.067v243.2c0 6.4 2.133 12.8 6.4 17.067s8.533 6.4 14.933 6.4c6.4 0 10.667-2.133 14.933-6.4s6.4-10.667 6.4-17.067v-243.2c0-6.4-2.133-12.8-6.4-17.067s-8.533-6.4-14.933-6.4z M437.333 422.4c-6.4 0-10.667 2.133-14.933 6.4s-6.4 10.667-6.4 17.067v243.2c0 6.4 2.133 12.8 6.4 17.067s8.533 6.4 14.933 6.4c6.4 0 10.667-2.133 14.933-6.4s6.4-10.667 6.4-17.067v-243.2c0-6.4-2.133-12.8-6.4-17.067s-8.533-6.4-14.933-6.4z M793.6 288c-4.267-4.267-8.533-6.4-14.933-6.4h-121.6v-44.8c0-12.8-4.267-25.6-12.8-34.133-8.533-10.667-19.2-14.933-29.867-14.933h-202.667c-12.8 0-23.467 4.267-32 14.933-8.533 8.533-12.8 21.333-12.8 34.133v44.8h-121.6c-6.4 0-10.667 2.133-14.933 6.4s-6.4 10.667-6.4 17.067 2.133 12.8 6.4 17.067c4.267 4.267 8.533 6.4 14.933 6.4h21.333v456.533c0 12.8 4.267 25.6 12.8 34.133 8.533 10.667 19.2 14.933 29.867 14.933h407.467c12.8 0 21.333-4.267 29.867-14.933 8.533-8.533 12.8-21.333 12.8-34.133v-456.533h14.933c6.4 0 10.667-2.133 14.933-6.4s6.4-10.667 6.4-17.067c4.267-6.4 2.133-10.667-2.133-17.067zM407.467 236.8h206.933v46.933h-206.933v-46.933zM721.067 789.333h-411.733v-460.8h411.733v460.8z">
                                </path>
                            </svg></a>
                            <span class="add-menu-item">${item.quantity}</span>
                                <a id="add-quantity" ref_id="${item.id}">
                                    <svg width="16" height="17" viewBox="0 0 1024 1024" class="icon-Add add-icon" id="Add" fill="">
                                <path d="M800 550.4h-249.6v249.6h-76.8v-249.6h-249.6v-76.8h249.6v-249.6h76.8v249.6h249.6v76.8z">
                                </path>
                                    </svg>
                                </a>
                            </span>
                        </div>
                        <div class="col-auto">
                        <p class="menu-price">${default_currecny}${parseFloat(item.price * item.quantity).toFixed(2) }</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-7 addons-added"><span class="title-menu-addons">${selection}</span></div>
                    </div>

                </div>`

    }
    function createCart(selection,delivery_fees = 0){

        $.ajax({
            type:'GET',
            url:"{{route('api.create-cart')}}",
            dataType:'json',
            data:{selection:selection,delivery_fees:delivery_fees},
            async:false,
            success:function(response) {
                localStorage.setItem('id_cart',response.data.id_cart);
                id_cart = response.data.id_cart;
                fetchCartWithProducts(id_cart);
                setTimeout(function(){
                    $('.order-type-modal').modal('hide');
                }, 800);

            }
        });

    }
    function fetchTimingsForDelivery(type){
              //time-selection-for-delivery
        duration = '';
        if(type == 'collection' || type == 'Collection'){
            duration = website_settings.min_collection_time

        }else{
            duration = website_settings.min_delivery_time
        }
        selected_order_type_time = duration;
        let route = "{{route('api.get-time-slots')}}/"+duration;

        $.ajax({
            type:'GET',
            url:route,
            dataType:'json',
            async:false,
            success:function(response) {
                for(i in response){
                    $('#time-selection-for-delivery').append(`<option value="${response[i]}">${response[i]}</option>`)
                }
            }
        })
    }
    function fetchCartWithProducts(id_cart){
        let return_data = [];
        $('#cart-items').html('');
        let user_id = "{{Auth::id()}}" > 0 ? "{{Auth::id()}}" : 0;
        $.ajax({
            type:'GET',
            url:"{{route('api.get-cart')}}/"+id_cart+"/"+user_id,
            dataType:'json',
            async:false,
            success:function(response) {

                return_data = response
                // for dine in

                // console.log(response.data);

                if(is_dine_in == 1 && response.data.order_type != 'eatin'){

                    localStorage.removeItem('id_cart')
                }

                if(response.data.order_type){
                    if(response.data.order_type != 'delivery' ){
                         $('#minimum-order-area').children().addClass('d-none');
                     }else{
                         $('#minimum-order-area').children().removeClass('d-none');
                     }
                    if(response.data.order_type == 'collection'){
                         $('#time-selection-for-delivery').addClass('d-none');
                         $('#time-selection-for-delivery').prev().addClass('d-none');

                         $('#selected-address').parent().addClass('d-none');
                         $('#selected-address').parent().prev().addClass('d-none');

                         $('#selected-postal-code').parent().addClass('d-none');
                         $('#selected-postal-code').parent().prev().addClass('d-none');


                     }else{
                         $('#time-selection-for-delivery').removeClass('d-none');
                         $('#time-selection-for-delivery').prev().removeClass('d-none');


                         $('#selected-address').parent().removeClass('d-none');
                         $('#selected-address').parent().prev().removeClass('d-none');

                         $('#selected-postal-code').parent().removeClass('d-none');
                         $('#selected-postal-code').parent().prev().removeClass('d-none');
                     }
                    $('#current-serving-method').text(response.data.order_type.charAt(0).toUpperCase() + response.data.order_type.slice(1));
                    fetchTimingsForDelivery(response.data.order_type)
                }
                if(response.data.delivery_address){
                    $('#selected-address').text(response.data.delivery_address)
                }
                let items = response.data.cart_products
                let total_items = 0;
                let total_price = 0;
                for(i in items){
                    total_price += items[i].price * items[i].quantity;
                    total_items += items[i].quantity;
                    $('#cart-items').append(getCartItemsHtml(items[i]))
                }
                if(response.data.delivery_fees == null){
                    response.data.delivery_fees = 0;
                }
                let final_price = parseFloat(total_price)+parseFloat(response.data.delivery_fees)-parseFloat(response.data.total_discount)
                $('#total-price').text(default_currecny+(final_price).toFixed(2) );
                $('#delivery-fees-text').text(default_currecny+response.data.delivery_fees);
                $('#total-discount-text').text(default_currecny+(response.data.total_discount).toFixed(2));
                $('.cart-count').remove();
                $('#Shopping-Cart').next().append(`<p class="cart-count">${total_items}</p>`)
                $('#selected-postal-code').text(localStorage.getItem('postal_code') ? localStorage.getItem('postal_code') : 'N/A')
                // fetchTimings(response.data.order_type);
            },
            error:function(response){
               localStorage.removeItem('id_cart')
               localStorage.removeItem('selection_hierarchy')
               localStorage.removeItem('postal_code')
               localStorage.removeItem('product_data')
               localStorage.removeItem('price_hierarchy')
            }
        });
        return return_data;

    }
    function fetchAllCategoriesAndProducts(){
        $.ajax({
            type:'GET',
            url:"{{route('api.get-all-categories-for-web')}}",
            dataType:'json',
            success:function(response) {
                if(response.success){
                   let categories = response.data.categories;
                   let category_wise_products = response.data.category_wise_products;

                   // products
                    for( i in category_wise_products){
                        $('.products-area').append(`
                            <div
                                class="card menucategorycard"
                                id="menucategory-for-${category_wise_products[i][0].id_category}"
                                data-target="#menuproduct-${category_wise_products[i][0].id_category}"
                            >
                                <div class="card-header">
                                    <h6 class="card-title" data-toggle="collapse" data-target="#menuproduct-${category_wise_products[i][0].id_category}" aria-expanded="true">
                                            ${category_wise_products[i][0].category.name}
                                    </h6>
                                </div>
                               ${getProductsHtml(category_wise_products[i])}
                            </div>
                        `);
                    }


                   //categories
                   for( i in categories){
                    $('#ul-scroll-category').append(`
                        <li class="nav-item d-block">
                            <a
                                href="#menucategory-for-${categories[i].id}"
                                class="nav-link fetch-products"
                                cat_name = "${categories[i].name} "
                            >
                                ${categories[i].name}
                            </a>
                        </li>
                    `);
                   }
                }
            }
        });
    }


    function getProductsHtml(products){
        let products_html = '';
        let local_count = 1;

        for(i in products){

            products_html += ` <div id="menuproduct-${products[i].id_category}" class="collapse" style="">
                        <div class="card-body">
                            <h5 class="card-sub-title">${products[i].name}</h5>
                            ${products[i].price > 0 ?
                                `
                            <p class="card-sub-title"> Price :${default_currecny+products[i].price}</p>
                            `:''
                            }
                            <p class="title-menu-desc"> ${products[i].description}</p>
                            ${products[i].food_allergy ? `
                            <p class="title-menu-desc"> Allergy: ${products[i].food_allergy}</p>
                            `:''}
                            <div class="single-menu-item d-flex flex-wrap">
                                <div class="row col p-0 no-gutters m-0 align-items-center">
                                    <div class="col">
                                        <div class="menu-name-wrapper d-flex" style="display:none!important">
                                            <p class="title-menu ">Peri Chicken Fillet Wrap</p>
                                            <p class="menu-price ml-auto "> 5.00</p>
                                        </div>
                                    </div>
                                    <div class="col-auto ml-2">
                                        <span class="border-add-item" id="add-item" id_product="${products[i].id}"">Add</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    local_count++
        }
        return products_html
    }

    function recursiveCallingMetas(product_meta){
        let attributes = product_meta.attributes;
        for( j in attributes){
            if(attributes[j].product_meta){
                recursiveCallingMetas(attributes[j].product_meta)
            }
        }
    }
    function preSelect(node_data,modal_type='single',free_limit=0){
        let hierarchy = localStorage.getItem('selection_hierarchy');
        let hierarchy_with_price = localStorage.getItem('selection_hierarchy_with_price');
        let price_hierarchy = localStorage.getItem('price_hierarchy') ? parseFloat(localStorage.getItem('price_hierarchy')) : 0;

        if(hierarchy_with_price){
            if(free_limit_count[node_data.product_meta_id]){
                    free_limit_count[node_data.product_meta_id] = free_limit_count[node_data.product_meta_id] + 1;

            }else{

                free_limit_count[node_data.product_meta_id] = 1;

            }
            let price = node_data.price;
            if(free_limit_count[node_data.product_meta_id] <= free_limit){
                if(modal_type != 'single'){
                    price = 0

                }
            }


            if(node_data.price == null){
                node_data.price = 0;
            }
            if(!node_data.quantity){
                node_data.quantity = 1;
            }
            price_hierarchy = parseFloat(price_hierarchy) + parseFloat(price);
            hierarchy = JSON.parse(hierarchy);
            hierarchy.push(node_data.key);

            localStorage.setItem('selection_hierarchy',JSON.stringify(hierarchy));
            localStorage.setItem('price_hierarchy',price_hierarchy);
            selection_holder = hierarchy;



            hierarchy_with_price = JSON.parse(hierarchy_with_price);
            hierarchy_with_price.push(node_data)
            localStorage.setItem('selection_hierarchy_with_price',JSON.stringify(hierarchy_with_price));
            node_holder = hierarchy_with_price;








        }else{


            let this_arr = [];
            this_arr.push(node_data)
            localStorage.setItem('selection_hierarchy_with_price',JSON.stringify(this_arr));
            node_holder = this_arr;


            let arr = [];
            arr.push(node_data.key)
            localStorage.setItem('selection_hierarchy',JSON.stringify(arr))
            selection_holder = arr;
            if(node_data.price == null){
                node_data.price = 0;
            }
            localStorage.setItem('price_hierarchy', parseFloat(localStorage.getItem('price_hierarchy')) + parseFloat(node_data.price))

        }
    }
    function generateAttributes(attributes,parent_node = false,type='single',free_limit=0,multiple_quantity_allowed=0){
        let attributes_html =  '';
            for(i in attributes){
                if(type == 'single'){
                    attributes_html += `

                        <div class="row pt-2 addons-item   onclick-cursor" data-values='${JSON.stringify(attributes[i])}'  ${parent_node ? 'is_parent="1"' : ''}>
                            <div class="col col-8 col-lg-9">
                                <span class="title-menu-addons">${attributes[i].key} </span>
                            </div>
                            <div class="col col-lg-3 col-4 input-holder">
                                <p class="menu-price-addons">
                                    <span class="mr-2">
                                        ${   parseFloat(attributes[i].price) > 0  ?  default_currecny+attributes[i].price : 'Free'}
                                    </span>
                                    <span>
                                        <input
                                            class="open-child"
                                            type="radio"
                                            name="step${attributes[i].product_meta_id}"
                                            value='${JSON.stringify(attributes[i])}' ${parent_node ? 'is_parent="1"' : ''}
                                            data-values='${JSON.stringify(attributes[i])}'
                                        >
                                        <span class="checkmark"></span>
                                    </span>
                                </p>
                            </div>

                        </div>




                    <div class="col-auto ${multiple_quantity_allowed == 1 ? '' : 'd-none'}"><span class="border-add-item">
                            <a class="delete-addon-quantity"
                                data-values='${JSON.stringify(attributes[i])}'
                                 free-limit='${free_limit}'

                            >
                                <svg width="16" height="17" viewBox="0 0 1024 1024" class="icon-Delete" fill="">
                                <path d="M584.533 422.4c-6.4 0-10.667 2.133-14.933 6.4s-6.4 10.667-6.4 17.067v243.2c0 6.4 2.133 12.8 6.4 17.067s8.533 6.4 14.933 6.4c6.4 0 10.667-2.133 14.933-6.4s6.4-10.667 6.4-17.067v-243.2c0-6.4-2.133-12.8-6.4-17.067s-8.533-6.4-14.933-6.4z M437.333 422.4c-6.4 0-10.667 2.133-14.933 6.4s-6.4 10.667-6.4 17.067v243.2c0 6.4 2.133 12.8 6.4 17.067s8.533 6.4 14.933 6.4c6.4 0 10.667-2.133 14.933-6.4s6.4-10.667 6.4-17.067v-243.2c0-6.4-2.133-12.8-6.4-17.067s-8.533-6.4-14.933-6.4z M793.6 288c-4.267-4.267-8.533-6.4-14.933-6.4h-121.6v-44.8c0-12.8-4.267-25.6-12.8-34.133-8.533-10.667-19.2-14.933-29.867-14.933h-202.667c-12.8 0-23.467 4.267-32 14.933-8.533 8.533-12.8 21.333-12.8 34.133v44.8h-121.6c-6.4 0-10.667 2.133-14.933 6.4s-6.4 10.667-6.4 17.067 2.133 12.8 6.4 17.067c4.267 4.267 8.533 6.4 14.933 6.4h21.333v456.533c0 12.8 4.267 25.6 12.8 34.133 8.533 10.667 19.2 14.933 29.867 14.933h407.467c12.8 0 21.333-4.267 29.867-14.933 8.533-8.533 12.8-21.333 12.8-34.133v-456.533h14.933c6.4 0 10.667-2.133 14.933-6.4s6.4-10.667 6.4-17.067c4.267-6.4 2.133-10.667-2.133-17.067zM407.467 236.8h206.933v46.933h-206.933v-46.933zM721.067 789.333h-411.733v-460.8h411.733v460.8z">
                                </path>
                            </svg></a>
                            <span class="add-menu-item addon-${attributes[i].product_meta_id+'-'+attributes[i].id}">1</span>
                                <a
                                    class="add-addon-quantity"
                                    data-values='${JSON.stringify(attributes[i])}'
                                    free-limit='${free_limit}'
                                >
                                    <svg width="16" height="17" viewBox="0 0 1024 1024" class="icon-Add add-icon" id="Add" fill="">
                                <path d="M800 550.4h-249.6v249.6h-76.8v-249.6h-249.6v-76.8h249.6v-249.6h76.8v249.6h249.6v76.8z">
                                </path>
                                    </svg>
                                </a>
                            </span>
                        </div>`


                    }else{
                        if(parseInt(attributes[i].preselected) == 1){
                            preSelect(attributes[i],type,free_limit);
                        }
                        attributes_html += `
                        <div class="row pt-2 addons-item open-child  onclick-cursor" data-values='${JSON.stringify(attributes[i])}'  ${parent_node ? 'is_parent="1"' : ''}>
                            <div class="col col-8 col-lg-9">
                                <span class="title-menu-addons">${attributes[i].key} </span>
                            </div>
                            <div class="col col-lg-3 col-4 input-holder">
                                <p class="menu-price-addons">
                                    <span class="mr-2"> ${default_currecny+attributes[i].price}</span>
                                    <span>
                                            <input type="checkbox" name="step4" value='${JSON.stringify(attributes[i])}' ${parent_node ? 'is_parent="1"' : ''} ${parseInt(attributes[i].preselected) == 1 ? 'checked':''}>
                                            <span class="checkmark checkbox"></span>
                                        </span>
                                </p>
                            </div>
                        </div>
                        <div class="col-auto ${multiple_quantity_allowed == 1 ? '' : 'd-none'}"><span class="border-add-item">
                            <a class="delete-addon-quantity"
                            data-values='${JSON.stringify(attributes[i])}'
                            free-limit='${free_limit}'
                            >
                                <svg width="16" height="17" viewBox="0 0 1024 1024" class="icon-Delete" fill="">
                                <path d="M584.533 422.4c-6.4 0-10.667 2.133-14.933 6.4s-6.4 10.667-6.4 17.067v243.2c0 6.4 2.133 12.8 6.4 17.067s8.533 6.4 14.933 6.4c6.4 0 10.667-2.133 14.933-6.4s6.4-10.667 6.4-17.067v-243.2c0-6.4-2.133-12.8-6.4-17.067s-8.533-6.4-14.933-6.4z M437.333 422.4c-6.4 0-10.667 2.133-14.933 6.4s-6.4 10.667-6.4 17.067v243.2c0 6.4 2.133 12.8 6.4 17.067s8.533 6.4 14.933 6.4c6.4 0 10.667-2.133 14.933-6.4s6.4-10.667 6.4-17.067v-243.2c0-6.4-2.133-12.8-6.4-17.067s-8.533-6.4-14.933-6.4z M793.6 288c-4.267-4.267-8.533-6.4-14.933-6.4h-121.6v-44.8c0-12.8-4.267-25.6-12.8-34.133-8.533-10.667-19.2-14.933-29.867-14.933h-202.667c-12.8 0-23.467 4.267-32 14.933-8.533 8.533-12.8 21.333-12.8 34.133v44.8h-121.6c-6.4 0-10.667 2.133-14.933 6.4s-6.4 10.667-6.4 17.067 2.133 12.8 6.4 17.067c4.267 4.267 8.533 6.4 14.933 6.4h21.333v456.533c0 12.8 4.267 25.6 12.8 34.133 8.533 10.667 19.2 14.933 29.867 14.933h407.467c12.8 0 21.333-4.267 29.867-14.933 8.533-8.533 12.8-21.333 12.8-34.133v-456.533h14.933c6.4 0 10.667-2.133 14.933-6.4s6.4-10.667 6.4-17.067c4.267-6.4 2.133-10.667-2.133-17.067zM407.467 236.8h206.933v46.933h-206.933v-46.933zM721.067 789.333h-411.733v-460.8h411.733v460.8z">
                                </path>
                            </svg></a>
                            <span class="add-menu-item addon-${attributes[i].product_meta_id+'-'+attributes[i].id}">1</span>
                                <a
                                    class="add-addon-quantity"
                                    data-values='${JSON.stringify(attributes[i])}'
                                    free-limit='${free_limit}'
                                >
                                    <svg width="16" height="17" viewBox="0 0 1024 1024" class="icon-Add add-icon" id="Add" fill="">
                                <path d="M800 550.4h-249.6v249.6h-76.8v-249.6h-249.6v-76.8h249.6v-249.6h76.8v249.6h249.6v76.8z">
                                </path>
                                    </svg>
                                </a>
                            </span>
                        </div>



                        `;

                    }


            }

    return attributes_html


    }

    function GetMetaHtml(meta_data,parent_node=false,show_next=true){
        if(meta_data){



            let has_children = false;
            for(i in meta_data.attributes){
                if(meta_data.attributes[i].child_product_meta_id > 0){
                    has_children = true;
                }
            }
            let type = 'single'
            if(meta_data.table_type == "multiselect"){
                type = 'checkboxes'
            }

                $('#modal-body-area').append(`
                <div class="addon-group ${show_next ? 'focus' : 'hide'} ${parent_node ? 'is_parent' : 'is_child'}" data-selectiontype="${type}" is_required="${meta_data.is_required}" free_limit="${meta_data.free_limit}" limit="${meta_data.limit}">
                    <div class="addons-box-shadow">
                        <div class="modal-header border-0 p-0"><button type="button" class="close modal-close-button"
                                data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button></div>

                        <div class="addon-group">
                            <div class="row addons-select align-items-center">
                                <p class="col mb-0 mt-4"><span>Please select ${meta_data.label}</span> </p>
                                <div class="col-auto"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body px-0 pt-0">
                        <div class="addons-section" id="attr-area">
                            ${generateAttributes(meta_data.attributes,parent_node,type,meta_data.free_limit,meta_data.multiple_quantity_allowed)}
                        </div>
                        <div class="addon-group mt-2">
                            <div class="row addons-select align-items-center">
                                <p class="col mb-0  mt-2"><span>Selected Addons</span> </p>
                                <div class="col-auto"></div>
                            </div>
                        </div>
                        <div class="row addons">
                            <div class="col">
                                <p class="selected-addons selected-atrributes">${JSON.parse(localStorage.getItem('selection_hierarchy')) ? JSON.parse(localStorage.getItem('selection_hierarchy'))?.join(" , ") : ''}</p>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer border-0">
                        <div class="w-100">
                            <div class="add-checkout-button">
                                <div class="row align-items-center">
                                    <div class="col-5">
                                        <p class="mb-0"><span class="title-button " >Item Total</span><span class="item-value item-total-value"> ${default_currecny}${meta_data.price ? parseFloat(meta_data.price)+parseFloat(localStorage.getItem('price_hierarchy')) : localStorage.getItem('price_hierarchy')}</span></p>
                                    </div>
                                    <div class="col text-right pr-0">
                                       ${!parent_node ? `
                                        <a class="back-button">
                                            <p class="mb-0"> <span>BACK</span></p>
                                        </a>` : '' }
                                        <a class="next-div next">
                                            <p class="mb-0"><span>Next</span></p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`);

            // if(meta_data.is_required == 0){
            //     let last_iteration = parseInt(localStorage.getItem('last_meta_iteration'));
            //     let product_data = JSON.parse(localStorage.getItem('product_data'));
            //     if(product_data.product_metas[last_iteration+1]){
            //         localStorage.setItem('last_meta_iteration',parseInt(last_iteration)+1);
            //         GetMetaHtml(product_data.product_metas[last_iteration+1],false,false);
            //     }
            // }

        }
    }
    function setModalData(product_data){
        let type = 'single'
        if(product_data.product_metas[0].table_type == "multiselect"){
            type = 'checkboxes'
        }
       $('#modal-body-area').append(`
        <div class="addon-group focus" data-selectiontype="${type}" is_required="${product_data.product_metas[0].is_required}" free_limit="${product_data.product_metas[0].free_limit}" limit="${product_data.product_metas[0].limit}">
            <div class="addons-box-shadow">
                <div class="modal-header border-0 p-0"><button type="button" class="close modal-close-button"
                        data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button></div>
                <div class="addon-group">
                    <div class="row addons-select align-items-center">
                        <p class="col mb-0 mt-4"><span>Please select ${product_data.product_metas[0].label}</span> </p>
                        <div class="col-auto"></div>
                    </div>
                </div>
            </div>
            <div class="modal-body px-0 pt-0">
                <div class="addons-section" id="attr-area">
                    ${generateAttributes(product_data.product_metas[0].attributes,true,type,product_data.product_metas[0].free_limit,product_data.product_metas[0].multiple_quantity_allowed)}
                </div>
                <div class="addon-group">
                    <div class="row addons-select align-items-center">
                        <p class="col mb-0 mt-2"><span>Selected Addons</span> </p>
                        <div class="col-auto"></div>
                    </div>
                </div>
                <div class="row addons">
                    <div class="col">
                        <p class="selected-addons selected-atrributes">${JSON.parse(localStorage.getItem('selection_hierarchy')) ? JSON.parse(localStorage.getItem('selection_hierarchy'))?.join(" , ") : ''}</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer border-0">
                <div class="w-100">
                    <div class="add-checkout-button">
                        <div class="row align-items-center">
                            <div class="col-5">
                                <p class="mb-0"><span class="title-button">Item Total</span><span class="item-value"> ${default_currecny+product_data.price}</span></p>
                            </div>
                            <div class="col text-right pr-0">
                                <a class="next next-div" >
                                    <p class="mb-0"><span>NEXT</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`);

    }
    var node_holder = [];
    var selection_holder = [];
    function removeNull(array) {
        return array.filter(x => x !== null)
    };




    $(document).on('click','.add-addon-quantity', function(){
        let mainthissss = $(this);
        let is_selected = mainthissss.parent().parent().prev().children().find('input').prop("checked");
        if(!is_selected){
            return ;
        }
        let node_data = $(this).attr('data-values') ? JSON.parse($(this).attr('data-values')) : {};
        let free_limit = $(this).attr('free-limit') ? parseInt($(this).attr('free-limit')) : 0;

        if(node_data.price == null){
            node_data.price = 0;
        }
        let hierarchy = localStorage.getItem('selection_hierarchy');
        let hierarchy_with_price = localStorage.getItem('selection_hierarchy_with_price');
        let price_hierarchy = localStorage.getItem('price_hierarchy') ? parseFloat(localStorage.getItem('price_hierarchy')) : 0;
        let exist = false;



        for(index in node_holder){
            if(node_data.product_meta_id == node_holder[index]?.product_meta_id){
                if(node_data.id == node_holder[index]?.id){
                    node_data.quantity = parseInt(node_holder[index].quantity)+1
                    selection_holder[index] = node_data.key;
                    localStorage.setItem('selection_hierarchy',JSON.stringify(selection_holder));
                // if(parseFloat(price_hierarchy) > 0 && free_limit_count[node_data.product_meta_id] >= free_limit  ){
                if(parseFloat(price_hierarchy) > 0 && free_limit_count[node_data.product_meta_id] >= free_limit  ){
                    if((parseFloat(price_hierarchy) - parseFloat(node_data.price)) >= parseFloat(node_data.price) ){



                        // price_hierarchy = parseFloat(price_hierarchy) - parseFloat(node_holder[index].price * node_holder[index].quantity);

                        let used_quantity =0;
                        let to_subtract_quantity_foraddon =0;
                        let tosubtract_here =0;
                        for(yyyyyyyyyy in node_holder){
                            if(node_holder[yyyyyyyyyy].product_meta_id == node_data.product_meta_id){
                                used_quantity += parseInt(node_holder[yyyyyyyyyy].quantity);
                            }
                        }

                        if(free_limit>used_quantity){
                            to_subtract_quantity_foraddon = free_limit-used_quantity;
                        }
                        if(to_subtract_quantity_foraddon > 0){
                            tosubtract_here = node_data.quantity - to_subtract_quantity_foraddon;
                        }else{
                            tosubtract_here = node_data.quantity;
                        }
                        price_hierarchy = parseFloat(price_hierarchy) + parseFloat(node_data.price);

                        // price_hierarchy = parseFloat(price_hierarchy) + parseFloat(node_data.price*node_data.quantity);


                        localStorage.setItem('price_hierarchy',price_hierarchy);
                    }
                }
                if(free_limit_count[node_holder[index]?.product_meta_id] ){
                    if(free_limit_count[node_holder[index]?.product_meta_id] > 0){
                        free_limit_count[node_holder[index]?.product_meta_id] = free_limit_count[node_holder[index]?.product_meta_id]+1;
                    }else{
                        free_limit_count[node_holder[index]?.product_meta_id] = 1;
                    }
                }





                    node_holder[index] = node_data;
                    localStorage.setItem('selection_hierarchy_with_price',JSON.stringify(node_holder));


                }

            }
        }
        $('.addon-'+node_data.product_meta_id+'-'+node_data.id).text(node_data.quantity)
        $('.item-total-value').text(default_currecny+parseFloat(price_hierarchy).toFixed(2))
        $('.item-value').text(default_currecny+parseFloat(price_hierarchy).toFixed(2));



    });

    $(document).on('click','.delete-addon-quantity', function(){
        let node_data = $(this).attr('data-values') ? JSON.parse($(this).attr('data-values')) : {};
        let free_limit = $(this).attr('free-limit') ? parseInt($(this).attr('free-limit')) : 0;
        if(node_data.price == null){
            node_data.price = 0;
        }
        let hierarchy = localStorage.getItem('selection_hierarchy');
        let hierarchy_with_price = localStorage.getItem('selection_hierarchy_with_price');
        let price_hierarchy = localStorage.getItem('price_hierarchy') ? parseFloat(localStorage.getItem('price_hierarchy')) : 0;
        let exist = false;



        for(index in node_holder){
            if(node_data.product_meta_id == node_holder[index]?.product_meta_id){
                if(node_data.id == node_holder[index]?.id){
                    if(parseInt(node_holder[index].quantity) != 1){
                        node_data.quantity = parseInt(node_holder[index].quantity)-1

                        selection_holder[index] = node_data.key;
                        localStorage.setItem('selection_hierarchy',JSON.stringify(selection_holder));
                        console.log('delete=>'+free_limit_count[node_data.product_meta_id])
                        if(parseFloat(price_hierarchy) > 0 && free_limit_count[node_data.product_meta_id] > free_limit  ){
                            if((parseFloat(price_hierarchy) - parseFloat(node_data.price)) >= parseFloat(node_data.price) ){
                                price_hierarchy = parseFloat(price_hierarchy) - parseFloat(node_data.price);
                                localStorage.setItem('price_hierarchy',price_hierarchy);
                            }

                        }
                        if(free_limit_count[node_holder[index]?.product_meta_id] ){
                            if(free_limit_count[node_holder[index]?.product_meta_id] > 0){
                                free_limit_count[node_holder[index]?.product_meta_id] = free_limit_count[node_holder[index]?.product_meta_id]-1;

                            }
                        }

                        node_holder[index] = node_data;
                        localStorage.setItem('selection_hierarchy_with_price',JSON.stringify(node_holder));

                    }


                }

            }
        }
        $('.addon-'+node_data.product_meta_id+'-'+node_data.id).text(node_data.quantity)
        $('.item-total-value').text(default_currecny+parseFloat(price_hierarchy).toFixed(2))
        $('.item-value').text(default_currecny+parseFloat(price_hierarchy).toFixed(2));



    });
    var free_limit_count = {}
    var limit_count = {}


    function checkUsedFreeLimit(node_data){
        let total_quantity =0;
        for(index in node_holder){
            if(node_holder[index].product_meta_id == node_data.product_meta_id){
                if(node_data.id != node_holder[index]?.id){
                    total_quantity += parseInt(node_holder[index].quantity);
                }
            }
        }
        return total_quantity;
    }

    $(document).on('click','.open-child', function(){


        var mainthis = $(this);
        let modal_type = mainthis.parents('.addon-group').attr('data-selectiontype');
        let free_limit = parseInt(mainthis.parents('.addon-group').attr('free_limit'));
        let limit = parseInt(mainthis.parents('.addon-group').attr('limit'));
        let node_data = $(this).attr('data-values') ? JSON.parse($(this).attr('data-values')) : {};
        let hierarchy = localStorage.getItem('selection_hierarchy');
        let hierarchy_with_price = localStorage.getItem('selection_hierarchy_with_price');
        let price_hierarchy = localStorage.getItem('price_hierarchy') ? parseFloat(localStorage.getItem('price_hierarchy')) : 0;
        let product_data = localStorage.getItem('product_data') ? JSON.parse(localStorage.getItem('product_data')) : null;
        let exist = false;
        let quantity_found = false;
        if(node_data.price == null){
            node_data.price = 0;
        }
        // console.log(mainthis.parents('.addon-group input'))
        // mainthis.parents('.addon-group .addons-item').each(function (e) {
        //     console.log(e)
        // })
        // mainthis.parent().parent().parent().parent().children('.open-child').attr('disabled','disabled')
        // mainthis.parents('.addon-group .open-child').each(function(){
        //     var input = $(this);
        //     console.log(input)
        // });

        // if(limit_count[node_data.product_meta_id] > limit ){
        //     console.log(mainthis.parents('.addon-group').children('input'))
        //     node_data.price = 0
        // }
        //########################## quantity work########################\\
        for(index in node_holder){
            if(node_data.product_meta_id == node_holder[index]?.product_meta_id){
                if(node_data.id == node_holder[index]?.id){
                    // console.log(node_holder[index])
                    quantity_found = true;
                    node_data.quantity = node_holder[index].quantity
                }

            }
        }
        if(!quantity_found){
            node_data.quantity = 1;
        }
        //########################## quantity work########################\\


        // keys inserting work
        for(index in node_holder){
            if(node_data.product_meta_id == node_holder[index]?.product_meta_id){
                if(modal_type != 'single'){
                    // console.log(node_data,node_holder)
                    if(node_data.id == node_holder[index].id){
                        if(limit_count[node_data.product_meta_id] && limit_count[node_data.product_meta_id] > 0){

                            
                            limit_count[node_data.product_meta_id] = limit_count[node_data.product_meta_id] - 1
                            if(limit_count[node_data.product_meta_id] < limit ){
                                $('.addon-group:visible input:checkbox:not(:checked)').attr('disabled',false)
                            }
                            
                        }
                        if(parseFloat(price_hierarchy) > 0 && free_limit_count[node_data.product_meta_id] >= free_limit  ){
                            if((parseFloat(price_hierarchy) - parseFloat(node_data.price)) >= parseFloat(node_data.price) ){

                                let used_free_limit = checkUsedFreeLimit(node_data);
                                let to_subtract_quantity = 0;
                                if(used_free_limit < free_limit){
                                    to_subtract_quantity = free_limit-used_free_limit;
                                }
                                if(to_subtract_quantity > 0){
                                    subtract_here = node_data.quantity - to_subtract_quantity
                                }else{
                                    subtract_here = node_data.quantity
                                }

                                price_hierarchy = parseFloat(price_hierarchy) - (parseFloat(node_data.price) * parseInt(subtract_here));


                            }else{
                                price_hierarchy = product_data.price;
                            }
                        }

                        // if equal or less then free limit this condition will make price equal to product price
                        // if(parseFloat(price_hierarchy) > 0 && free_limit_count[node_data.product_meta_id]-1 <= free_limit  ){
                        //     price_hierarchy = product_data.price;
                        // }


                        localStorage.setItem('price_hierarchy',price_hierarchy);
                        exist = true;

                        let is_selected = mainthis.children().find('input').prop("checked");
                        if(free_limit_count[node_holder[index]?.product_meta_id] && !is_selected){
                            if(free_limit_count[node_holder[index]?.product_meta_id] > 0){
                                let remainig_quantity = checkUsedFreeLimit(node_data);
                                free_limit_count[node_holder[index]?.product_meta_id] = remainig_quantity;
                                node_data.quantity = 1;

                            }
                        }



                        delete selection_holder[index];
                        selection_holder = removeNull(selection_holder)
                        localStorage.setItem('selection_hierarchy',JSON.stringify(selection_holder));

                        for ( indexxxx  in node_holder){
                            if(node_holder[indexxxx].id === node_data.id){
                                node_holder.splice(indexxxx, 1);
                            }
                        }
                        node_holder = removeNull(node_holder)
                        localStorage.setItem('selection_hierarchy_with_price',JSON.stringify(node_holder));
                    }
                    

                }else{

                    exist = true;
                    selection_holder[index] = node_data.key;
                    localStorage.setItem('selection_hierarchy',JSON.stringify(selection_holder));

                    if(node_holder[index].id != node_data.id && parseFloat(price_hierarchy) > 0){
                        price_hierarchy = parseFloat(price_hierarchy) - parseFloat(node_holder[index].price * node_holder[index].quantity);
                        price_hierarchy = parseFloat(price_hierarchy) + parseFloat(node_data.price);

                    }
                    localStorage.setItem('price_hierarchy',price_hierarchy);
                    node_holder[index] = node_data;
                    localStorage.setItem('selection_hierarchy_with_price',JSON.stringify(node_holder));

                }
            }
        }
    
        
        if(hierarchy_with_price){

            if(!exist){


                if(free_limit_count[node_data.product_meta_id]){
                    free_limit_count[node_data.product_meta_id] = parseInt(free_limit_count[node_data.product_meta_id]) + 1;
                }else{
                    free_limit_count[node_data.product_meta_id] = 1;
                }

                if(limit_count[node_data.product_meta_id]){
                    limit_count[node_data.product_meta_id] = parseInt(limit_count[node_data.product_meta_id]) + 1;
                }else{
                    limit_count[node_data.product_meta_id] = 1;
                }

                if(free_limit_count[node_data.product_meta_id] <= free_limit){
                    if(modal_type != 'single'){
                        node_data.price = 0

                    }
                }
                console.log('count-here '+limit_count[node_data.product_meta_id])
                if(limit_count[node_data.product_meta_id] == limit ){
                    // $(this).prop('disabled',true)
                    $('.addon-group:visible input:checkbox:not(:checked)').attr('disabled','disabled')
                    // return;
                }
                price_hierarchy = parseFloat(price_hierarchy) + parseFloat(node_data.price);
                hierarchy = JSON.parse(hierarchy);
                hierarchy.push(node_data.key);
                localStorage.setItem('selection_hierarchy',JSON.stringify(hierarchy));
                localStorage.setItem('price_hierarchy',price_hierarchy);
                selection_holder = hierarchy;
                hierarchy_with_price = JSON.parse(hierarchy_with_price);
                hierarchy_with_price.push(node_data)
                localStorage.setItem('selection_hierarchy_with_price',JSON.stringify(hierarchy_with_price));
                node_holder = hierarchy_with_price;


            }

        }else{

            free_limit_count[node_data.product_meta_id] = 1;
            limit_count[node_data.product_meta_id] = 1;

            if(free_limit_count[node_data.product_meta_id] <= free_limit){
                if(modal_type != 'single'){
                    node_data.price = 0

                }
            }


            let this_arr = [];



            this_arr.push(node_data)
            localStorage.setItem('selection_hierarchy_with_price',JSON.stringify(this_arr));
            node_holder = this_arr;


            let arr = [];
            arr.push(node_data.key)
            localStorage.setItem('selection_hierarchy',JSON.stringify(arr))
            selection_holder = arr;



            localStorage.setItem('price_hierarchy', parseFloat(localStorage.getItem('price_hierarchy')) + parseFloat(node_data.price))

        }

        
        let selected_attributes = selection_holder;
        if(selected_attributes){
            $('.selected-atrributes').text(' '+selected_attributes.join(" , "));

        }

        $('.item-total-value').text(default_currecny+localStorage.getItem('price_hierarchy'))



        if(node_data.child_product_meta_id != 0){
            // $('.main-addon-group').addClass('hide');
            // // $('.main-addon-group').remove();
            // if(!$(this).parents('.addon-group').prev().hasClass('addon-group')) {
            //     $(this).parents('.addon-group').next().remove()
            //     // $('.main-addon-group').remove();
            // }
            // if(parseInt(localStorage.getItem('last_meta_iteration')) == 0){
            //     $('.main-addon-group').addClass('');
            // }
            // $('.main-addon-group').each(function(e,val){
            //     console.log(e,val)
            // })
            //  if(!$(this).parents('.addon-group').next().hasClass('addon-group')) {
                $.ajax({
                    type:'GET',
                    url:"{{route('api.meta-with-attribute')}}/"+node_data.child_product_meta_id,
                    dataType:'json',
                    async:false,
                    success:function(response) {
                        GetMetaHtml(response.data)
                        proceedToStep(mainthis);
                        $('.main-addon-group:not(:last)').addClass('hide')
                    }
                })
            //  }

        }else{

            let product_data =  JSON.parse(localStorage.getItem('product_data'))
            let last_iteration = parseInt(localStorage.getItem('last_meta_iteration'));
            let has_previous_div = $(this).parents('.addon-group').prev().hasClass('addon-group');

            last_iteration = last_iteration+1;
            if(product_data.product_metas[last_iteration]){


                if(!$(this).parents('.addon-group').next().hasClass('addon-group')) {

                    GetMetaHtml(product_data.product_metas[last_iteration])
                    localStorage.setItem('last_meta_iteration',last_iteration);

                 }else{
                        // console.log('popup already appended')
                        proceedToStep(mainthis);

                 }
                if(modal_type == 'single'){
                    $(this).parents('.addon-group').addClass('hide').removeClass('focus');
                }else{
                    $(this).parents('.addon-group').next().addClass('hide').removeClass('focus');
                }

            }else{
                if(!$(this).parents('.addon-group').next().hasClass('addon-group')) {
                    $('.next-div').addClass('add-to-cart');
                    $('.next-div').removeClass('next');
                    $('.next-div').text('Add to cart');
                }

            }

        }

        let price = parseFloat(localStorage.getItem('price_hierarchy'));
        $('.item-value').text(default_currecny+parseFloat(price).toFixed(2));
        $('.addon-'+node_data.product_meta_id+'-'+node_data.id).text(node_data.quantity)

    });
    function proceedToStep(mainthis){
        if(mainthis.parents('.addon-group').attr('data-selectiontype') == 'single') {
            mainthis.parents('.addon-group').next().removeClass('hide').addClass('focus');
            mainthis.parents('.addon-group').removeClass('focus').addClass('hide');
        }
        if( mainthis.parents('.addon-group').attr('data-selectiontype') == 'checkboxes' && mainthis.parents('.addon-group').find('.addons-select').length > 0
            ) {
            var checkUserSelection = (mainthis.parents('.addon-group').find('.addons-select').find('.focused').text()).toLowerCase();
            if(checkUserSelection) {
                mainthis.find('.title-menu-addons').removeClass('less-item').removeClass('no-item');
                mainthis.find('.title-menu-addons').addClass(checkUserSelection + '-item');
            } else {

            }
        }
    }
    $(document).on('click','#add-item', function(){

        localStorage.removeItem('selection_hierarchy');
        localStorage.removeItem('price_hierarchy');
        localStorage.removeItem('selection_hierarchy_with_price');
        let id_product = $(this).attr('id_product');
        node_holder = [];
        free_limit_count = {};
        let item_url = "{{route('api.get-product-detailv2')}}"+"/"+id_product;
        $.ajax({
            type:'GET',
            url:item_url,
            dataType:'json',
            success:function(response) {
                // console.log(response.data)
                localStorage.setItem('product_data',JSON.stringify(response.data));
               if(response.data.product_metas.length>0){
                $('#modal-body-area').html('');
                    setModalData(response.data)
                     $('.ordernowmodal').modal('show')
                    localStorage.setItem('last_meta_iteration',0);
                    if(response.data.product_metas[0].is_required == 0){

                         let last_iteration = parseInt(localStorage.getItem('last_meta_iteration'));
                         // localStorage.setItem('last_meta_iteration',last_iteration+1);
                         if(!response.data.product_metas[last_iteration+1]){
                                $('.next-div').addClass('add-to-cart');
                                $('.next-div').removeClass('next');
                                $('.next-div').text('Add to cart');
                         }


                    }
                    localStorage.setItem('price_hierarchy',parseFloat(response.data.price));

                }else{
                    localStorage.removeItem('selection_hierarchy');
                    localStorage.removeItem('price_hierarchy');
                    let id_cart = localStorage.getItem('id_cart') ? localStorage.getItem('id_cart') : 0;
                    let request_data = {
                        selection : localStorage.getItem('selection_hierarchy'),
                        price : response.data.price,
                        id_cart : id_cart,
                        product_data : localStorage.getItem('product_data'),
                        order_type: is_dine_in == 1 ? 'eatin' : 'collection'
                    }
                    $.ajax({
                        type:'POST',
                        url:"{{route('api.add-to-cart')}}",
                        data:request_data,
                        dataType:'json',
                        async:false,
                        success:function(response) {
                            if(id_cart == 0){
                                localStorage.setItem('id_cart',response.data.id_cart);
                                id_cart = response.data.id_cart;
                            }
                            showMessage();
                            fetchCartWithProducts(id_cart);
                             $('.modal').modal('hide')
                             localStorage.removeItem('selection_hierarchy');
                             localStorage.removeItem('price_hierarchy');
                             localStorage.removeItem('product_data');
                        }
                    });




                }
            }
        });
    })
    function handleDineIn(){
        if(is_dine_in == 1){
            $('#current-serving-method').parent().parent().parent().parent().addClass('d-none');
            $('#time-selection-for-delivery').hide();
            $('#time-selection-for-delivery').prev().hide();
            $('#delivery-fees-text').parent().hide();
            $('#delivery-fees-text').parent().prev().hide();
            $('#selected-address').parent().hide();
            $('#selected-address').parent().prev().hide();
            $('#selected-postal-code').parent().hide();
            $('#selected-postal-code').parent().prev().hide();

        }
    }
    $(document).ready(function(){
        $('#allergy_instruction').val(localStorage.getItem('allergy_instruction'))
        setPostalCodeInAddressForm();
        handleDineIn();
        fetchAllCategoriesAndProducts();
        if(localStorage.getItem('id_cart')){
            fetchCartWithProducts(localStorage.getItem('id_cart'))
        }else{
            if(is_dine_in == 0){
                $('.order-type-modal').modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                });

            }
        }
        fetchTimings('collection')
    });

    $(document).on('click','.order-type-selection', function(){
        let selection = $(this).val()
        if(selection == 'delivery'){
            setTimeout(function(){
                $('.order-type-modal').modal('hide');
                $('.postal-code-modal').modal('show');
            }, 800);

        }else{
            createCart(selection);
        }

    })
    $(document).on('click','.close-postal-code-modal', function(){
        if(!localStorage.getItem('id_cart')){
            createCart('collection')

        }
    })
    $(document).on('click','#send-postal-code-request', function(){
        let postal_code = $('#postal-code').val();
        $.ajax({
            type:'POST',
            url:"{{route('api.check-postal-code')}}",
            data:{order_type:'Delivery',postal_code:postal_code},
            dataType:'json',
            success:function(response) {
                if(response.data){
                    if(!localStorage.getItem('id_cart')){
                        createCart('delivery',response.data.amount)

                    }else{

                        let id_cart = localStorage.getItem('id_cart');
                        let request_data = {
                            method:"delivery",
                            id_cart:id_cart,
                            amount:response.data.amount,
                        };
                        $.ajax({
                            type:'POST',
                            url:"{{route('api.change-serving-method')}}",
                            data:request_data,
                            dataType:'json',
                            async:false,
                            success:function(response) {
                                fetchCartWithProducts(response.data.id_cart);
                                localStorage.setItem('postal_code',postal_code)
                            }
                        });
                    }
                    $('.postal-code-modal').modal('hide');
                    $('#time-selection-for-delivery').removeClass('d-none');
                }
                localStorage.setItem('postal_code',postal_code)
                setPostalCodeInAddressForm()
                $('#selected-postal-code').text(localStorage.getItem('postal_code') ? localStorage.getItem('postal_code') : 'N/A')
                if(response.error){
                    swal('','Sorry We don’t deliver Here','error');
                }
            }
        });

    })
    function setPostalCodeInAddressForm(){
        if(localStorage.getItem('postal_code')){
            $('.address-postal_code').val(localStorage.getItem('postal_code'))
            $('.address-postal_code').attr('disabled','true')
        }
    }
    function showMessage(){

        $('body').append(`<div class="alert alert-success alert-dismissible fade show custom-alert" role="alert">
            <strong>Product Added Successfully</strong> Add more items from our cousines.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>`);
        setTimeout(function(){
                    $('.alert-dismissible').remove();
        }, 1500);
    }
    $(document).on('click','.add-to-cart', function(){
        // let is_allowed = false;
        // let current_date = new Date();
        // current_date.setMinutes( current_date.getMinutes() + selected_order_type_time );
        //  for (i in allowed_timings){
        //     let provided_time = new Date(allowed_timings[i]);
        //      if (provided_time.getTime() > current_date.getTime()){
        //         is_allowed = true;
        //      }
        //  }

        // if(!is_order_allowed){
        //     return swal('','Shop is closed', 'error');
        // }


        let id_cart = localStorage.getItem('id_cart') ? localStorage.getItem('id_cart') : 0;
        let request_data = {
            selection : localStorage.getItem('selection_hierarchy'),
            selection_with_price : localStorage.getItem('selection_hierarchy_with_price'),
            price : parseFloat(localStorage.getItem('price_hierarchy')),
            id_cart : id_cart,
            product_data : localStorage.getItem('product_data'),
            order_type: is_dine_in == 1 ? 'eatin' : 'collection'
        }
        $.ajax({
            type:'POST',
            url:"{{route('api.add-to-cart')}}",
            data:request_data,
            dataType:'json',
            async:false,
            success:function(response) {
                if(id_cart == 0){
                    localStorage.setItem('id_cart',response.data.id_cart);
                    id_cart = response.data.id_cart;
                }
                fetchCartWithProducts(id_cart);
                showMessage();
                 $('.modal').modal('hide')
                 localStorage.removeItem('selection_hierarchy');
                 localStorage.removeItem('selection_hierarchy_with_price');
                 localStorage.removeItem('price_hierarchy');
                 localStorage.removeItem('product_data');
            }
        });
    });
    $(document).on('click','#add-quantity', function(){
        let id_cart = localStorage.getItem('id_cart');
        let ref_id = $(this).attr('ref_id');
        let request_data = {
            id_cart:id_cart,
            ref_id:ref_id,
            type:'add',
        }
        $.ajax({
            type:'POST',
            url:"{{route('api.change-quantity')}}",
            data:request_data,
            dataType:'json',
            async:false,
            success:function(response) {
                fetchCartWithProducts(id_cart);
            }
        });
    });
    $(document).on('click','#delete', function(){
        let id_cart = localStorage.getItem('id_cart');
        let ref_id = $(this).attr('ref_id');
        let request_data = {
            id_cart:id_cart,
            ref_id:ref_id,
            type:'delete',
        }
        $.ajax({
            type:'POST',
            url:"{{route('api.change-quantity')}}",
            data:request_data,
            dataType:'json',
            async:false,
            success:function(response) {
                fetchCartWithProducts(id_cart);
            }
        });
    });
     $(document).on('keyup', '.search-here', function(e) {
        var filter = $(this).val();
        $(".menucategorycard").each(function(){
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
            } else {
                $(this).show();
            }
        });
    });
    $(document).on('click', '#quick_checkout_button', function(e) {
        let user_id = "{{Auth::id()}}";
        localStorage.setItem('allergy_instruction',$('#allergy_instruction').val())
        if(!is_order_allowed){
            return swal('','Shop is closed','error')
        }

        let is_auth = "{{Auth::check()}}";

        // for dine in

        if(is_dine_in == 1){
            let cart = fetchCartWithProducts(localStorage.getItem('id_cart'));
            let checkout_link = "{{route('payment-confirm')}}"+'?table_id={{request()->table_id}}';
            cart.data.selected_time = '';
            window.location.href = checkout_link;
            return;
        }


        if(is_auth == 1){
            let cart = fetchCartWithProducts(localStorage.getItem('id_cart'));
            let minimum_amount_to_order = parseFloat('{{getSettings()['minimum_amount_to_order'] ?? 0}}');

            let items = cart.data.cart_products
            let final_check_price = 0;
            for(i in items){
                final_check_price += items[i].price * items[i].quantity;
            }

            if(cart.data.order_type == 'delivery') {
                if(final_check_price < minimum_amount_to_order){
                    return swal("", "The minimum amount to place an order is "+default_currecny+""+minimum_amount_to_order+".", "error");
                }
            }

            if(cart.data.cart_products.length == 0){
                return swal("Empty Cart!", "Your cart is empty!", "error");
            }else if(cart.data.order_type == 'delivery' && !cart.data.delivery_address){

                $.ajax({
                    type:'GET',
                    url:"{{route('api.get-customer-addresses')}}",
                    data:{user_id:user_id},
                    dataType:'json',
                    async:false,
                    success:function(response) {
                        if(response?.data?.length > 0){
                            let address_html = '';
                            for(i in response.data){
                                address_html += `
                                        <div class="row no-gutters pt-2 addons-item onclick-cursor">
                                            <div class="col col-8 col-lg-9">
                                                <span class="title-menu-addons" for="address${response.data[i].id}">${response.data[i].address}</span>
                                            </div>

                                            <div class="col col-lg-3 col-4 input-holder">
                                                <p class="menu-price-addons">
                                                    <span>
                                                        <input class="form-check-input address-selection"
                                                        type="radio"
                                                        name="exampleRadios"
                                                        id="address${response.data[i].id}"
                                                        value="${response.data[i].id}"
                                                        name_ref="${response.data[i].address}">
                                                        <span class="checkmark"></span>
                                                    </span>
                                                </p>
                                            </div>
                                        </div>`
                            }
                            $('#addresses-body').html('')
                            $('#addresses-body').append(`
                                <div class="container">
                                    <strong id="no-address-message">Which address do you want to choose?</strong>
                                    ${address_html}
                                </div>

                            `);
                            $('.addresses-modal').modal('show');
                        }else{
                            $('.address-modal').modal('show');
                        }
                    }
                });

            }else{
                let checkout_link = "{{route('payment-confirm')}}"
                let selected_time = $('#time-selection-for-delivery').val();
                cart.data.selected_time = selected_time;
                // let order = placeOrder(cart.data);
                // localStorage.removeItem('id_cart');

                window.location.href = checkout_link
            }
        }else{
            $('.login-modal').modal('show');
        }
    });
    $(document).on('click', '.serving-method', function(e) {
        let method = $(this).attr('data');
        let id_cart = localStorage.getItem('id_cart');
        let request_data = {
            method:method,
            id_cart:id_cart,
        };
        if(method == 'collection'){
            $.ajax({
                type:'POST',
                url:"{{route('api.change-serving-method')}}",
                data:request_data,
                dataType:'json',
                async:false,
                success:function(response) {
                    $('#current-serving-method').text(response.data.order_type);
                     $('#time-selection-for-delivery').addClass('d-none');
                     fetchCartWithProducts(response.data.id_cart)
                }
            });

        }else{
            $('.postal-code-modal').modal('show');
        }


    });
    var selected_address_id = 0;
    $(document).on('click', '.address-selection', function(e) {
        selected_address_id = $(this).val();
        let text = $(this).attr('name_ref');
        $('#no-address-message').text(text);

    });

    $(document).on('click', '#send-id-address-save-request', function(e) {
        if(selected_address_id == 0){
            return swal('','please select address ','error');
        }
        let postal_code = localStorage.getItem('postal_code');
        let id_cart = localStorage.getItem('id_cart')
         $.ajax({
            type:'GET',
            url:"{{route('api.set-address-id')}}",
            data:{id_address:selected_address_id,id_cart:id_cart,postal_code:postal_code},
            dataType:'json',
            async:false,
            success:function(response) {
                $('#selected-address').text(response.data.address)
                if(response.data?.place_order){

                    let cart = fetchCartWithProducts(localStorage.getItem('id_cart'));
                    let checkout_link = "{{route('payment-confirm')}}";
                    cart.data.selected_time = '';
                    window.location.href = checkout_link;

                    /*let cart = fetchCartWithProducts(localStorage.getItem('id_cart'));
                    let checkout_link = "{{route('checkout')}}";
                    let order = placeOrder(cart.data);
                    localStorage.removeItem('id_cart');
                    window.location.href = checkout_link+'/'+order.reference*/
                }
            }
        });

    })
    $(document).on('click', '.apply-coupon-code', function(e) {
        let coupon_code = $('#coupon-code').val();
        if(coupon_code == ''){
           return swal('','Enter coupon code','error');
        }
        let request_data = {
            id_cart: localStorage.getItem('id_cart'),
            code: coupon_code,
        }
        $.ajax({
            type:'POST',
            url:"{{route('api.apply-coupon-code')}}",
            data:request_data,
            dataType:'json',
            async:false,
            success:function(response) {
                if(response.data.error){
                    swal('',response.data.error,'error')
                }
                if(response.data.success){
                    swal('',response.data.success,'success')
                    fetchCartWithProducts(localStorage.getItem('id_cart'));
                }
            }
        });



    });
    $(document).on('click', '.fetch-products', function(e) {
        let text = $(this).attr('cat_name');
        $('.item-main-heading').text(text)
    });
    $(document).on('click', '#send-address-save-request', function(e) {
        let address = $('.address-address').val();
        let street = $('.address-street').val();
        let town = $('.address-town').val();
        let postal_code = $('.address-postal_code').val();
        if(address == ''){
            return swal('','Please enter address','error');
        }
        if(street == ''){
            return swal('','Please enter street','error');
        }
        if(town == ''){
            return swal('','Please enter town','error');
        }
        if(postal_code == ''){
            return swal('','Please enter postal code','error');
        }
        let request_data = {
            address:address,
            street:street,
            town:town,
            postal_code:postal_code,
            user_id:"{{Auth::id()}}",
            id_cart:localStorage.getItem('id_cart')
        };

        $.ajax({
            type:'POST',
            url:"{{route('api.add-user-address')}}",
            data:request_data,
            dataType:'json',
            async:false,
            success:function(response) {
                $('#selected-address').text(address)
                if(response.data?.place_order){

                    let cart = fetchCartWithProducts(localStorage.getItem('id_cart'));
                    let checkout_link = "{{route('payment-confirm')}}";
                    cart.data.selected_time = '';
                    window.location.href = checkout_link;

                   /* let cart = fetchCartWithProducts(localStorage.getItem('id_cart'));
                    let checkout_link = "{{route('checkout')}}";
                    let order = placeOrder(cart.data);
                    localStorage.removeItem('id_cart');
                    window.location.href = checkout_link+'/'+order.reference*/
                }
            }
        });



    });
    function showLoader(){
        $('body').append(`
        <div class="cart-loader" id="main-loader">
            <div class="lds-facebook"><div></div><div></div><div></div></div>
        </div>`);
    }
    function closeLoader(){
        $('#main-loader').remove();
    }
    $(document).bind('ajaxStart', function(){
        showLoader()
    }).bind('ajaxStop', function(){
        closeLoader()
    });

    $('body').on('click','.back-button',function(){
        if($('.next-div').hasClass('add-to-cart')){
            $('.next-div').removeClass('add-to-cart');
            $('.next-div').addClass('next');
            $('.next-div').text('Next');
        }
        let last_iteration = parseInt(localStorage.getItem('last_meta_iteration'))-1;
        last_iteration = last_iteration < 0 ? 0 : last_iteration
        localStorage.setItem('last_meta_iteration',last_iteration)
        var mainthis = $(this);
        mainthis.parents('.addon-group').prev().removeClass('hide').addClass('focus');
        mainthis.parents('.addon-group').removeClass('focus').addClass('hide');
    });

    $('body').on('click','.next',function(){
        var mainthis = $(this);
        let allow_iterate = true;
        let product_data = JSON.parse(localStorage.getItem('product_data'));
        let last_iteration = parseInt(localStorage.getItem('last_meta_iteration'));
        if(product_data.product_metas[last_iteration]?.is_required == 0){
            if(product_data.product_metas[last_iteration+1]){
                localStorage.setItem('last_meta_iteration',parseInt(last_iteration)+1);
                allow_iterate = false;
                GetMetaHtml(product_data.product_metas[last_iteration+1],false,false);

            }
        }


        if(mainthis.parents('.addon-group').next().hasClass('hide')){
        // if(mainthis.parents('.addon-group').find('input:checked').length >  0) {
            if(allow_iterate){
                let last_iteration = parseInt(localStorage.getItem('last_meta_iteration'))+1;
                localStorage.setItem('last_meta_iteration',last_iteration)

            }
            mainthis.parents('.addon-group').next().removeClass('hide').addClass('focus');
            mainthis.parents('.addon-group').removeClass('focus').addClass('hide');
        }else{
            $('.next-div').addClass('add-to-cart');
            $('.next-div').removeClass('next');
            $('.next-div').text('Add to cart');
        }
    });
</script>
@endsection

