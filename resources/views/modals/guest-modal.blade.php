<div role="dialog" aria-modal="true" class="fade modal guest-modal" tabindex="-1" style="padding-right: 16px;">
        <div id="addonsModal" class="modal-dialog modal-dialog-centered" data-addonstep="1">
            <div class="modal-content">
                <div class="addon-group focus" data-selectiontype="single">
                    <div class="addons-box-shadow">
                        <div class="modal-header border-0 p-0"><button type="button" class="close modal-close-button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                        <div class="addon-group">
                            <div class="row addons-select align-items-center">
                                <p class="col mb-0"><span>Guest Login</span> </p>
                                <div class="col-auto"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body px-0 pt-0">
                        <form method="POST" >
                        <div class="addons-section">
                            <div class="row pt-2">
                                <div class="col-md-3">First Name:</div>
                                <div class="col-md-9"><input type="text" name="name" class="form-control guest-name"></div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-md-3">Phone:</div>
                                <div class="col-md-9"><input type="text" name="phone" class="form-control guest-phone"></div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-md-3">Email:</div>
                                <div class="col-md-9"><input type="text" name="email" class="form-control guest-email"></div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="modal-footer border-0">
                        <div class="w-100">
                            <div class="add-checkout-button">
                                <div class="row align-items-center">
                                    <div class="col text-right pr-0">
                                        <button class="btn  btn-block" id="send-guest-guest-request" >
                                            <span>Login</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
