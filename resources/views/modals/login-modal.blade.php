<div role="dialog" aria-modal="true" class="fade modal login-modal" tabindex="-1" style="padding-right: 16px;">
        <div id="addonsModal" class="modal-dialog modal-dialog-centered" data-addonstep="1">
            <div class="modal-content">
                <div class="addon-group focus" data-selectiontype="single">
                    <div class="addons-box-shadow">
                        <div class="modal-header border-0 p-0"><button type="button" class="close modal-close-button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                        <div class="addon-group">
                            <div class="row addons-select align-items-center">
                                <p class="col mb-0"><span>Login</span> </p>
                                <div class="col-auto"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body px-0 pt-0">
                        <div class="addons-section">
                            <div class="row pt-2">
                                <div class="col-md-3">Email:</div>
                                <div class="col-md-9"><input type="text" id="login-email" name="email" class="form-control"></div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-md-3">Password:</div>
                                <div class="col-md-9"><input type="password" id="login-password" name="password" class="form-control"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer no-gutters border-0 row">
                        <div class="col-12">
                            <button class="btn btn-block btn-login" id="send-login-request" >
                                <span>Login</span>
                            </button>
                        </div>
                        <div class="col-12 my-2 text-center">Or</div>
                        <div class="col-12">
                            <button type="button" class="btn btn-success btn-site-color btn-block open-signup-modal">Signup</button>
                        </div>
                        <div class="col-12 my-2 text-center">Or</div>
                        <div class="col-12">
                            <button type="button" class="btn btn-success btn-site-color btn-block open-guest-modal">Guest Login</button>
                        </div>
                        <div class="col-12 my-2 text-center">Or</div>
                        <div class="col-12">
                            <a href="{{route('password.request')}}" class="btn btn-success btn-site-color btn-block">Forget Password</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@php 
    $notification_data = getNotification();

@endphp
@if($notification_data && request()->table_id == null)
    <div role="dialog" aria-modal="true" class="fade modal promotion-modal" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered" data-addonstep="1">
            <div class="modal-content">
                <div class="addon-group focus" data-selectiontype="single">
                    <div class="addons-box-shadow">
                        <div class="modal-header border-0 p-0 promotion-modal-close-area">
                            <button type="button" class="close modal-close-button " data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                    </div>
                    <div class="modal-body px-0 pt-0">
                        <span class="promotionicon">
                        </span>
                        <div class="w-100 text text-center">
                           @if(isset($notification_data['image']) && $notification_data['image'] != '')
                            <span class="subheading">
                                <img src="{{asset('images/'.$notification_data['image'] ?? '')}}">
                            </span>
                            @endif
                            <h3 class="sale">{{$notification_data['title'] ?? ''}}</h3>
                            <p class="upper">{{$notification_data['description'] ?? ''}}</p>
                            <h2><span>{{$notification_data['amount'] ?? ''}}</span><sup>%</sup><sub>off</sub></h2>
                        </div>
                         @if(Route::currentRouteName() == 'home-page')
                            <div class="text-center">
                                <button type="button" class="promotionbtn never-show-again">Never Again</button>
                            </div>
                         @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
