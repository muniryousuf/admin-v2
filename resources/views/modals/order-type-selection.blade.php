 <div role="dialog" aria-modal="true" class="fade modal show order-type-modal order-type-selector" tabindex="-1" data-keyboard="false" data-backdrop="static">
    @php 
        $settings_for_modal = getSettings();

    @endphp
        <div id="addonsModal" class="modal-dialog modal-dialog-centered" data-addonstep="1">
              <div class="modal-content">
                <div class="addon-group focus" data-selectiontype="single">
                    <div class="modal-body px-0 pt-0">
                        <div class="addons-section">
                            <div class="row addon-group mt-2">
                                <div class="addons-select align-items-center">
                                    <p class="col mb-0  mt-2"><span>Please select delivery type</span> </p>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-6">
                                @if($settings_for_modal['is_pickup'] == 1)
                                    <div class="row pt-2 addons-item  onclick-cursor">
                                        <div class="col-12">
                                            <span class="title-menu-addons"><img src="images/collectitem.png"></span>
                                        </div>
                                        <div class="col-12 input-holder">
                                            <p class="menu-price-addons">
                                                <span>
                                                    <input class="order-type-selection" type="radio" name="order-type" tabindex="3" id="Collection" value="collection">
                                                    <button type="button" class="inputcheckmark">Collection</button>
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                @endif
                                </div>
                                <div class="col-6">
                                @if($settings_for_modal['is_delivery'] == 1)
                                    <div class="row pt-2 addons-item  onclick-cursor">
                                        <div class="col-12">
                                            <span class="title-menu-addons"><img src="images/deliveritem.png"></span>
                                        </div>

                                        <div class="col-12 input-holder">
                                            <p class="menu-price-addons">
                                                <span>
                                                    <input class="order-type-selection" type="radio" name="order-type" tabindex="3" id="Delivery" value="delivery">
                                                    <button type="button" class="inputcheckmark">Delivery</button>
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                @endif
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
