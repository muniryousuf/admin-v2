<div role="dialog" aria-modal="true" class="fade modal postal-code-modal" tabindex="-1" style="padding-right: 16px;" data-keyboard="false" data-backdrop="static">
        <div id="addonsModal" class="modal-dialog modal-dialog-centered" data-addonstep="1">
            <div class="modal-content">
                <div class="addon-group focus" data-selectiontype="single">
                     <div class="addons-box-shadow">
                        <div class="modal-header border-0 p-0"><button type="button" class="close modal-close-button close-postal-code-modal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                        <div class="addon-group">
                            <div class="row addons-select align-items-center">
                                
                                <div class="col-auto"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body px-0 pt-0">
                        <div class="addons-section">
                            <div class="row pt-2">
                                <div class="col-md-3">Postal Code:</div>
                                <div class="col-md-9"><input type="text" id="postal-code" name="postal_code" class="form-control"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer border-0">
                        <div class="w-100">
                            <div class="add-checkout-button">
                                <div class="row align-items-center">
                                    <div class="col text-right pr-0">
                                        <button class="btn  btn-block" id="send-postal-code-request" >
                                            <span>Check</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
