<div role="dialog" aria-modal="true" class="fade modal addresses-modal" tabindex="-1" style="padding-right: 16px;">
        <div id="addonsModal" class="modal-dialog modal-dialog-centered" data-addonstep="1">
            <div class="modal-content">
                <div class="addon-group focus" data-selectiontype="single">
                    <div class="addons-box-shadow">
                        <div class="modal-header border-0 p-0"><button type="button" class="close modal-close-button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                        <div class="addon-group">
                            <div class="row addons-select align-items-center">
                                <p class="col mb-0"><span>Addresses</span> </p>
                                <div class="col-auto"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body px-0 pt-0" id="addresses-body">
                        
                    </div>
                    <div class="modal-footer border-0">
                        <div class="w-100">
                            <div class="add-checkout-button">
                                <div class="row align-items-center">
                                    <div class="col text-right pr-0">
                                        <button class="btn  btn-block" id="send-id-address-save-request" >
                                            <span>Save</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
