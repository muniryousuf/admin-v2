@extends('layouts.app')
@section('title')
    @lang('crud.edit') @lang('models/users.singular')
@endsection
@section('content')
    <section class="section">
            <div class="section-header">
                <h1>@lang('models/users.singular')</h1>
                <div class="filter-container section-header-breadcrumb row justify-content-md-end">
                    <a href="{{ route('users.index') }}"  class="btn btn-primary">@lang('crud.back')</a>
                </div>
            </div>
            <div class="content">
              @include('stisla-templates::common.errors')
              <div class="section-body">
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-body ">
                                    {!! Form::model($users, ['route' => ['users.update', $users->id], 'method' => 'patch']) !!}
                                        <div class="row">
                                            @include('users.fields')
                                        </div>
                                    {!! Form::close() !!}
                            </div>
                         </div>
                    </div>
                    @if(isset($orders))
                         <h1>Order's List</h1>
                        <div class="col-lg-12">
                             <div class="card">
                                 <div class="card-body ">
                                    <div class="row">
                                        <div class="table-responsive">
                                            <table class="table" id="table_id">
                                                <thead>
                                                    <tr>
                                                        <th>@lang('models/orders.fields.Reference')</th>
                                                        <th>@lang('models/orders.fields.total_amount')</th>
                                                        <th>@lang('models/orders.fields.payment_method')</th>
                                                        <th>@lang('models/orders.fields.delivery_address')</th>
                                                        <th >@lang('crud.action')</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($orders as $order)
                                                        <tr>
                                                        <td>{{ $order->reference }}</td>
                                                        <td>{{$settings['currency']}} {{ $order->total_amount_with_fee }}</td>
                                                        <td>{{ $order->payment }}</td>
                                                        <td>{{ $order->delivery_address }}</td>
                                                        <td class=" text-center">
                                                           <div class='btn-group'>
                                                               <a 
                                                                    class='btn btn-warning send-email-to-customer' 
                                                                    user_id="{{$order->user_id}}" 
                                                                    reference="{{$order->reference}}" 
                                                                >Send order mail</a>
                                                               <a 
                                                                    class='btn btn-warning'


                                                                >Send Voucher mail</a>
                                                               <a 
                                                                    class='btn btn-warning send-message-to-customer'
                                                                    user_id="{{$order->user_id}}"

                                                                >Send order text</a>
                                                               <a class='btn btn-warning'>Send Voucher text</a>
                                                           </div>
                                                        </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                             </div>
                        </div>
                    @endif


                    @if(isset($carts))
                    <h1>Pending Cart's</h1>
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-body ">
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table" id="cart_table_id">
                                            <thead>
                                                <tr>
                                                    <th>Id Cart</th>
                                                    <th>Total Amount</th>
                                                    <th>Payment</th>
                                                    <th>Order Type</th>
                                                    <th >@lang('crud.action')</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($carts as $cart)
                                                    <tr>
                                                    <td>{{ $cart->id_cart }}</td>
                                                    <td>{{$settings['currency']}} {{ $cart->total_amount }}</td>
                                                    <td>{{ $cart->payment }}</td>
                                                    <td>{{ $cart->order_type }}</td>
                                                     <td class=" text-center">
                                                        <div class='btn-group'>
                                                            <a class='btn btn-warning'>Send Abandon cart mail</a>
                                                        </div>
                                                    </td>
                                                   
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                    @endif
                 </div>
              </div>
            </div>
    </section>
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
@endsection
@section('scripts')
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
  <script type="text/javascript">
    $(document).ready( function () {
      $('#table_id').DataTable();
      $('#cart_table_id').DataTable();
    });
    $(document).on('click','.send-email-to-customer',function(){
        let reference = $(this).attr('reference')
        let user_id = $(this).attr('user_id')
        $.ajax({
            type:'POST',
            url:"{{route('api.send-email-to-customer')}}",
            dataType:'json',
            data:{reference:reference,user_id:user_id},
            async:false,
            success:function(response) {
                swal('','Email Send','success')
            }
        });

    });
    $(document).on('click','.send-message-to-customer',function(){
        let user_id = $(this).attr('user_id')
        $.ajax({
            type:'POST',
            url:"{{route('api.send-message-to-customer')}}",
            dataType:'json',
            data:{user_id:user_id},
            async:false,
            success:function(response) {
                swal('','Email Send','success')
            }
        });

    });
</script>
@endsection