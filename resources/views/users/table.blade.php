<div class="table-responsive">
    <table class="table" id="users-table">
        <thead>
            <tr>
                <th>@lang('models/users.fields.name')</th>
                <th>@lang('models/users.fields.lname')</th>
                <th>@lang('models/users.fields.email')</th>
                <th>@lang('models/users.fields.phone')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $users)
            <tr>
            <td>{{ $users->name }}</td>
            <td>{{ $users->lname }}</td>
            <td>{{ $users->email }}</td>
            <td>{{ $users->phone }}</td>
            <td class=" text-center">
               {!! Form::open(['route' => ['users.destroy', $users->id], 'method' => 'delete']) !!}
               <div class='btn-group'>
                <a href="{!! route('users.view.orders', [$users->id]) !!}" class='btn btn-warning'>View Orders</i></a>
                <a href="{!! route('users.view.carts', [$users->id]) !!}" class='btn btn-warning'>View Carts</i></a>
               </div>
               {!! Form::close() !!}
            </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
