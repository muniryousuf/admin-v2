@extends('layouts.app')
@section('title')
     @lang('models/resturentAddresses.plural')
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@lang('models/resturentAddresses.plural')</h1>
            <div class="section-header-breadcrumb">
                @if($resturentAddresses->count() == 0)
                <a href="{{ route('resturentAddresses.create')}}" class="btn btn-primary form-btn">@lang('crud.add_new')<i class="fas fa-plus"></i></a>
                @endif
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('resturent_addresses.table')
            </div>
       </div>
   </div>
    
        @include('stisla-templates::common.paginate', ['records' => $resturentAddresses])

    </section>
@endsection



