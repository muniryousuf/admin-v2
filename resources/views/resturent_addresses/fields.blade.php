<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/resturentAddresses.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', __('models/resturentAddresses.fields.email').':') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Website Url Field -->
<div class="form-group col-sm-6">

    {!! Form::label('website_url', __('models/resturentAddresses.fields.website_url').':') !!}

    {!! Form::text('website_url', null, ['class' => 'form-control']) !!}
</div>

<!-- Logo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logo', __('models/resturentAddresses.fields.logo').':') !!}
    
     @if(isset($resturentAddress->logo))
        <img class="image-preview" src="{{asset('images/'.$resturentAddress->logo)}}" />
     @endif
     {!! Form::file('logo') !!}
    
</div>

<!-- Phone Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_number', __('models/resturentAddresses.fields.phone_number').':') !!}
    {!! Form::text('phone_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Admin Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('admin_email', __('models/resturentAddresses.fields.admin_email').':') !!}
    {!! Form::text('admin_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Pickup Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_pickup', __('models/resturentAddresses.fields.is_pickup').':') !!}
    {!! Form::checkbox('is_pickup', '1', null) !!} 
    
</div>

<!-- Is Delivery Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_delivery', __('models/resturentAddresses.fields.is_delivery').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_delivery', 0) !!}
        {!! Form::checkbox('is_delivery', '1', null) !!} 
    </label>
</div>

<!-- Is Dine Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_dine', __('models/resturentAddresses.fields.is_dine').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_dine', 0) !!}
        {!! Form::checkbox('is_dine', '1', null) !!} 
    </label>
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('resturentAddresses.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
