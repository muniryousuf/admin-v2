<div class="table-responsive">
    <table class="table" id="resturentAddresses-table">
        <thead>
            <tr>
                <th>@lang('models/resturentAddresses.fields.name')</th>
        <th>@lang('models/resturentAddresses.fields.email')</th>
        <th>@lang('models/resturentAddresses.fields.website_url')</th>
        <th>@lang('models/resturentAddresses.fields.logo')</th>
        <th>@lang('models/resturentAddresses.fields.phone_number')</th>
        <th>@lang('models/resturentAddresses.fields.id_user')</th>
        <th>@lang('models/resturentAddresses.fields.is_pickup')</th>
        <th>@lang('models/resturentAddresses.fields.is_delivery')</th>
        <th>@lang('models/resturentAddresses.fields.is_dine')</th>
        <th>@lang('models/resturentAddresses.fields.admin_email')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($resturentAddresses as $resturentAddress)
            <tr>
                       <td>{{ $resturentAddress->name }}</td>
            <td>{{ $resturentAddress->email }}</td>
            <td>{{ $resturentAddress->website_url }}</td>
            <td>{{ $resturentAddress->logo }}</td>
            <td>{{ $resturentAddress->phone_number }}</td>
            <td>{{ $resturentAddress->id_user }}</td>
            <td>{{ $resturentAddress->is_pickup }}</td>
            <td>{{ $resturentAddress->is_delivery }}</td>
            <td>{{ $resturentAddress->is_dine }}</td>
            <td>{{ $resturentAddress->admin_email }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['resturentAddresses.destroy', $resturentAddress->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('resturentAddresses.show', [$resturentAddress->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('resturentAddresses.edit', [$resturentAddress->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
