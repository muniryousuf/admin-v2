<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/resturentAddresses.fields.name').':') !!}
    <p>{{ $resturentAddress->name }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', __('models/resturentAddresses.fields.email').':') !!}
    <p>{{ $resturentAddress->email }}</p>
</div>

<!-- Website Url Field -->
<div class="form-group">
    {!! Form::label('website_url', __('models/resturentAddresses.fields.website_url').':') !!}
    <p>{{ $resturentAddress->website_url }}</p>
</div>

<!-- Logo Field -->
<div class="form-group">
    {!! Form::label('logo', __('models/resturentAddresses.fields.logo').':') !!}
    <p>{{ $resturentAddress->logo }}</p>
</div>

<!-- Phone Number Field -->
<div class="form-group">
    {!! Form::label('phone_number', __('models/resturentAddresses.fields.phone_number').':') !!}
    <p>{{ $resturentAddress->phone_number }}</p>
</div>

<!-- Id User Field -->
<div class="form-group">
    {!! Form::label('id_user', __('models/resturentAddresses.fields.id_user').':') !!}
    <p>{{ $resturentAddress->id_user }}</p>
</div>

<!-- Is Pickup Field -->
<div class="form-group">
    {!! Form::label('is_pickup', __('models/resturentAddresses.fields.is_pickup').':') !!}
    <p>{{ $resturentAddress->is_pickup }}</p>
</div>

<!-- Is Delivery Field -->
<div class="form-group">
    {!! Form::label('is_delivery', __('models/resturentAddresses.fields.is_delivery').':') !!}
    <p>{{ $resturentAddress->is_delivery }}</p>
</div>

<!-- Is Dine Field -->
<div class="form-group">
    {!! Form::label('is_dine', __('models/resturentAddresses.fields.is_dine').':') !!}
    <p>{{ $resturentAddress->is_dine }}</p>
</div>

<!-- Admin Email Field -->
<div class="form-group">
    {!! Form::label('admin_email', __('models/resturentAddresses.fields.admin_email').':') !!}
    <p>{{ $resturentAddress->admin_email }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/resturentAddresses.fields.created_at').':') !!}
    <p>{{ $resturentAddress->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/resturentAddresses.fields.updated_at').':') !!}
    <p>{{ $resturentAddress->updated_at }}</p>
</div>

