@extends('layouts.app')
@section('title')
    @lang('crud.edit') @lang('models/resturentAddresses.singular')
@endsection
@section('content')
    <section class="section">
            <div class="section-header">
                <h3 class="page__heading m-0">@lang('crud.edit') @lang('models/resturentAddresses.singular')</h3>
                <div class="filter-container section-header-breadcrumb row justify-content-md-end">

                    <a href="{{ route('resturentAddresses.index') }}"  class="btn btn-primary">@lang('crud.back')</a>
                </div>
            </div>
  <div class="content">
              @include('stisla-templates::common.errors')
              <div class="section-body">
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-body ">
                                    {!! Form::model($restaurant_address, ['route' => ['resturentAddresses.save-address', $restaurant_address->id ?? 0], 'method' => 'post' ,'files' => true]) !!}
                                        <div class="row">
                                          <!-- Name Field -->
                                                <div class="form-group col-sm-6">
                                                    {!! Form::label('name', __('models/resturentAddresses.fields.address').':') !!}
                                                    {!! Form::text('address', null, ['class' => 'form-control']) !!}
                                                </div>

                                                <!-- Email Field -->
                                                <div class="form-group col-sm-6">
                                                    {!! Form::label('zip_code', __('models/resturentAddresses.fields.zip_code').':') !!}
                                                    {!! Form::text('zip_code', null, ['class' => 'form-control']) !!}
                                                </div>

                                                <!-- Website Url Field -->
                                                <div class="form-group col-sm-6">

                                                    {!! Form::label('city', __('models/resturentAddresses.fields.city').':') !!}

                                                    {!! Form::text('city', null, ['class' => 'form-control']) !!}
                                                </div>

                                                <input name="id_restaurant" type="hidden" value="{{$id_restaurant}}">
                                                <input name="id" type="hidden" value="{{$restaurant_address->id ?? 0}}">

                                             
                                              


                                                <!-- Submit Field -->
                                                <div class="form-group col-sm-12">
                                                    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
                                                    <a href="{{ route('resturentAddresses.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
                                                </div>

                                        </div>

                                    {!! Form::close() !!}
                            </div>
                         </div>
                    </div>
                 </div>
              </div>
   </div>
  </section>
@endsection
