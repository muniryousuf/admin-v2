<div class="table-responsive">
    <table class="table" id="categories-table">
        <thead>
            <tr>
                <th>@lang('models/categories.fields.name')</th>
                <th>@lang('models/categories.fields.description')</th>
                <th>@lang('models/categories.fields.image')</th>
                <th>@lang('models/categories.fields.sort')</th>
                <th>@lang('models/categories.fields.status')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{{ $category->name }}</td>
                <td>{{ $category->description }}</td>
                <td><img class="image-preview" src="{{asset('images/'.$category->image)}}" onerror="this.src='https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg'"/></td>
                <td>{{ $category->sort }}</td>
                <td>{{ $category->status }}</td>
                <td class=" text-center">
                {!! Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('categories.show', [$category->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                    <a href="{!! route('categories.edit', [$category->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                </div>
                {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
