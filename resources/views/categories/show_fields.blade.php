<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/categories.fields.name').':') !!}
    <p>{{ $category->name }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', __('models/categories.fields.description').':') !!}
    <p>{{ $category->description }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', __('models/categories.fields.image').':') !!}
    <p><img class="image-preview" src="{{asset('images/'.$category->image)}}" /></p>
</div>

<!-- Sort Field -->
<div class="form-group">
    {!! Form::label('sort', __('models/categories.fields.sort').':') !!}
    <p>{{ $category->sort }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', __('models/categories.fields.status').':') !!}
    <p>{{ $category->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/categories.fields.created_at').':') !!}
    <p>{{ $category->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/categories.fields.updated_at').':') !!}
    <p>{{ $category->updated_at }}</p>
</div>

