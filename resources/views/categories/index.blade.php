@extends('layouts.app')
@section('title')
     @lang('models/categories.plural')
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@lang('models/categories.plural')</h1>
             <div class="col-md-6">
                <input placeholder="Search product here" class="form-control" id="search-product" value="{{$search}}"/>
                <button class="btn btn-success" onclick="search()">Search</button>
                <button class="btn btn-primary" onclick="clearSearch()">Clear search</button>
            </div>
            <div class="section-header-breadcrumb">
                <a href="{{ route('categories.create')}}" class="btn btn-primary form-btn">@lang('crud.add_new')<i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('categories.table')
            </div>
       </div>
   </div>
    @include('stisla-templates::common.paginate', ['records' => $categories])
    </section>
@endsection

@section('scripts')
<script type="text/javascript">
    function search(){
        link = window.location.href.split("?")[0];
        var url = new URL(link);
        let search_value = document.getElementById('search-product');
        if(search_value.value != ''){
          
            url.searchParams.delete('search');
            url.searchParams.set('search', search_value.value);
            window.history.replaceState(null, null, url);
            window.location.reload()
        }
    }
    function clearSearch(){
      window.location.href="{{route('categories.index')}}"
    }
     
  $(document).keyup(function (e) {
        if (e.keyCode === 13) {
         search();
        }
     })
</script>
@endsection

