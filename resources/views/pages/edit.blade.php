@extends('layouts.app')
@section('title')
    @lang('crud.edit') @lang('models/pages.singular')
@endsection
@section('content')
    <section class="section">
            <div class="section-header">
                <h3 class="page__heading m-0">@lang('crud.edit') @lang('models/pages.singular')</h3>
                <div class="filter-container section-header-breadcrumb row justify-content-md-end">
                    <a href="{{ route('pages.index') }}"  class="btn btn-primary">@lang('crud.back')</a>
                </div>
            </div>
  <div class="content">
              @include('stisla-templates::common.errors')
              <div class="section-body">
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-body ">
                                    {!! Form::model($pages, ['route' => ['pages.update', $pages->id], 'method' => 'patch', 'files' => true]) !!}
                                        <div class="row">
                                            @include('pages.fields')
                                        </div>

                                    {!! Form::close() !!}
                            </div>
                         </div>
                    </div>
                 </div>
              </div>
   </div>
  </section>
@endsection
@section('scripts')
<script type="text/javascript">
    var selection = "{{$pages->page_type}}";
    if(selection =='static'){
        $('#page_template_name_area').show();
        $('#html_area').hide();
    }
    if(selection == 'dynamic'){
        $('#page_template_name_area').hide();
        $('#html_area').hide();
    }else{
        $('#page_template_name_area').hide();
        $('#html_area').show();
    }
    $('#page_type').change(function(){
        let select = $(this).val();
        if(select =='static'){
            $('#page_template_name_area').show();
            $('#html_area').hide();
        }

        if(select == 'dynamic'){
            $('#page_template_name_area').hide();
            $('#html_area').hide();
        }
        if(select =='informative'){
            $('#page_template_name_area').hide();
            $('#html_area').show();
        }
    });
     var background_type = "{{$pages->background_type}}";
     var background_type_value = "{{$pages->background_type_value}}";
     if(background_type != 'no_background'){
        $('#background_area').html('')
        if(background_type == 'color'){
            $('#background_area').append(`
                
                <label for="background_type_value">Color:</label>
                <input class="form-control" name="background_type_value" type="text" id="background_type_value" value="${background_type_value}">

            `)    
        }
        if(background_type == 'image'){
            $('#background_area').append(`
                
                <label for="background_type_value">Image:</label>
                <input class="form-control" name="background_type_value" type="file" id="background_type_value">

            `)    
        }
        $('#background_area').removeClass('d-none');


    }else{
        $('#background_area').addClass('d-none');
    }



    $('#background_type').change(function(){
        let type = $(this).val();
        if(type != 'no_background'){
            $('#background_area').html('')
            if(type == 'color'){
                $('#background_area').append(`
                    
                    <label for="background_type_value">Color:</label>
                    <input class="form-control" name="background_type_value" type="text" id="background_type_value">

                `)    
            }
            if(type == 'image'){
                $('#background_area').append(`
                    
                    <label for="background_type_value">Image:</label>
                    <input class="form-control" name="background_type_value" type="file" id="background_type_value">

                `)    
            }
            $('#background_area').removeClass('d-none');


        }else{
            $('#background_area').addClass('d-none');
        }
    });


</script>
@endsection
