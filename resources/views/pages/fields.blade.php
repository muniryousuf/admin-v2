<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/pages.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', __('models/pages.fields.type').':') !!}
    <select name="type" class="form-control">
        @foreach(App\Models\Pages::TYPES as $type)
            <option value="{{$type}}">{{ucfirst($type)}}</option>
        @endforeach
    </select>
</div>

<!-- page_type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('page_type', __('models/pages.fields.page_type').':') !!}
    @php 
        $selected = '';

    @endphp
    <select name="page_type" class="form-control" id="page_type">
        @foreach(App\Models\Pages::PAGE_TYPES as $page_type)
            @php  
                if(isset($pages->page_type) && $pages->page_type == $page_type){
                    $selected = 'selected';
                } 

            @endphp
            <option value="{{$page_type}}" {{$selected}}>{{ucfirst($page_type)}}</option>
        @endforeach
    </select>
</div>

<!-- template Field -->
<div class="form-group col-sm-6" id="page_template_name_area">
    {!! Form::label('template', __('models/pages.fields.template').':') !!}
    <select name="page_template_name" class="form-control" id="page_template_name">
        @foreach(App\Models\Pages::PAGE_TEMPLATES as $template)
            <option value="{{$template}}">{{ucfirst($template)}}</option>
        @endforeach
    </select>
</div>


<!-- background_type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('background_type', __('models/pages.fields.background_type').':') !!}
    @php 
        $bg_selected = '';

    @endphp

    <select name="background_type" class="form-control" id="background_type">
        @foreach(App\Models\Pages::BACKGROUND_TYPE as $background_type)
            @php  
                if(isset($pages->background_type) && $pages->background_type == $background_type){
                    $bg_selected = 'selected';
                } 
            @endphp
            <option value="{{$background_type}}" {{$bg_selected}}>{{ucfirst(str_replace('_', ' ', $background_type))}}</option>
        @endforeach
    </select>
</div>
<!-- background_type Field -->
<div class="form-group col-sm-6 d-none" id="background_area">
</div>




<!-- template Field -->
<div class="form-group col-sm-6" id="html_area">
    {!! Form::label('template', __('models/pages.fields.template').':') !!}
    <textarea id="wysiwg-textarea" name="html">{{isset($pages->html) ? $pages->html : ''}}</textarea>
</div>




<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('pages.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
