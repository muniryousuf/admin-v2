<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/pages.fields.name').':') !!}
    <p>{{ $pages->name }}</p>
</div>

<!-- Html Field -->
<div class="form-group">
    {!! Form::label('html', __('models/pages.fields.html').':') !!}
    <p>{{ $pages->html }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/pages.fields.created_at').':') !!}
    <p>{{ $pages->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/pages.fields.updated_at').':') !!}
    <p>{{ $pages->updated_at }}</p>
</div>

