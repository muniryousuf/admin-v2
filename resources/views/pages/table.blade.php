<div class="table-responsive">
    <table class="table" id="pages-table">
        <thead>
            <tr>
                <th>@lang('models/pages.fields.name')</th>
        
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($pages as $page)
            <tr>
                       <td>{{ $page->name }}</td>
           
                       <td class=" text-center">
                           {!! Form::open(['route' => ['pages.destroy', $page->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('page-builder') !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               @if($page->page_type == 'static')
                                <a href="{!! route('page-builder') !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               @endif
                               @if($page->page_type == 'dynamic' || $page->page_type == 'informative')
                               
                                <a href="{!! route('pages.edit',[$page->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               @endif


                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
