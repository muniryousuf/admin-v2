<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/locationsAttributes.fields.name').':') !!}
    <p>{{ $locationsAttributes->name }}</p>
</div>

<!-- Details Field -->
<div class="form-group">
    {!! Form::label('details', __('models/locationsAttributes.fields.details').':') !!}
    <p>{{ $locationsAttributes->details }}</p>
</div>

<!-- Map Field -->
<div class="form-group">
    {!! Form::label('map', __('models/locationsAttributes.fields.map').':') !!}
    <p>{{ $locationsAttributes->map }}</p>
</div>

<!-- Id Location Field -->
<div class="form-group">
    {!! Form::label('id_location', __('models/locationsAttributes.fields.id_location').':') !!}
    <p>{{ $locationsAttributes->id_location }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/locationsAttributes.fields.created_at').':') !!}
    <p>{{ $locationsAttributes->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/locationsAttributes.fields.updated_at').':') !!}
    <p>{{ $locationsAttributes->updated_at }}</p>
</div>

