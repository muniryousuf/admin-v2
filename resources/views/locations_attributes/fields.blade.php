<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/locationsAttributes.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Details Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('details', __('models/locationsAttributes.fields.details').':') !!}
    {!! Form::textarea('details', null, ['class' => 'form-control']) !!}
</div>

<!-- Map Field -->
<div class="form-group col-sm-6">
    {!! Form::label('map', __('models/locationsAttributes.fields.map').':') !!}
    {!! Form::text('map', null, ['class' => 'form-control']) !!}
</div>
@php 
$selected = '';
if(isset($locationsAttributes)){
    $selected = $locationsAttributes->id_location;
}
@endphp
<div class="form-group col-sm-6">
    {!! Form::label('id_location', __('models/locationsAttributes.fields.id_location').':') !!}
    <select name="id_location" class="form-control">
        @foreach($locations as $location)
            <option value="{{$location->id}}" {{$selected == $location->id ? 'selected' : ''}}>{{$location->name}}</option>
        @endforeach

    </select>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('locationsAttributes.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
