<div class="table-responsive">
    <table class="table" id="locationsAttributes-table">
        <thead>
            <tr>
                <th>@lang('models/locationsAttributes.fields.name')</th>
        <th>@lang('models/locationsAttributes.fields.details')</th>
        <th>@lang('models/locationsAttributes.fields.map')</th>
        <th>@lang('models/locationsAttributes.fields.id_location')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($locationsAttributes as $locationsAttributes)
            <tr>
                       <td>{{ $locationsAttributes->name }}</td>
            <td>{{ $locationsAttributes->details }}</td>
            <td>{{ $locationsAttributes->map }}</td>
            <td>{{ $locationsAttributes->id_location }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['locationsAttributes.destroy', $locationsAttributes->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('locationsAttributes.show', [$locationsAttributes->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('locationsAttributes.edit', [$locationsAttributes->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
