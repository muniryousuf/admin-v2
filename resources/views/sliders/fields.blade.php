<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', __('models/sliders.fields.image').':') !!}
    {!! Form::file('image') !!}
     @if(isset($slider->image))
    <img class="image-preview" src="{{asset('images/'.$slider->image)}}" />
    @endif
</div>
<div class="clearfix"></div>

<!-- Sort Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sort', __('models/sliders.fields.sort').':') !!}
    {!! Form::text('sort', null, ['class' => 'form-control']) !!}
</div>

<!-- Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('url', __('models/sliders.fields.url').':') !!}
    {!! Form::text('url', null, ['class' => 'form-control']) !!}
</div>

<!-- Gallery Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gallery_id', __('models/sliders.fields.gallery_id').':') !!}
    {!! Form::text('gallery_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('sliders.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
