<div class="table-responsive">
    <table class="table" id="sliders-table">
        <thead>
            <tr>
                <th>@lang('models/sliders.fields.image')</th>
        <th>@lang('models/sliders.fields.sort')</th>
        <th>@lang('models/sliders.fields.url')</th>
        <th>@lang('models/sliders.fields.gallery_id')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($sliders as $slider)
            <tr>
            <td><img class="image-preview" src="{{asset('images/'.$slider->image)}}" /></td>
            <td>{{ $slider->sort }}</td>
            <td>{{ $slider->url }}</td>
            <td>{{ $slider->gallery_id }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['sliders.destroy', $slider->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('sliders.show', [$slider->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('sliders.edit', [$slider->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
