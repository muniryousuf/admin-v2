<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', __('models/sliders.fields.image').':') !!}
    <p><img class="image-preview" src="{{asset('images/'.$slider->image)}}" /></p>
</div>

<!-- Sort Field -->
<div class="form-group">
    {!! Form::label('sort', __('models/sliders.fields.sort').':') !!}
    <p>{{ $slider->sort }}</p>
</div>

<!-- Url Field -->
<div class="form-group">
    {!! Form::label('url', __('models/sliders.fields.url').':') !!}
    <p>{{ $slider->url }}</p>
</div>

<!-- Gallery Id Field -->
<div class="form-group">
    {!! Form::label('gallery_id', __('models/sliders.fields.gallery_id').':') !!}
    <p>{{ $slider->gallery_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/sliders.fields.created_at').':') !!}
    <p>{{ $slider->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/sliders.fields.updated_at').':') !!}
    <p>{{ $slider->updated_at }}</p>
</div>

