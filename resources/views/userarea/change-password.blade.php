<div id="pane-A" class="card tab-pane show active" role="tabpanel" aria-labelledby="tab-A">
    <div class="card-header" role="tab" id="heading-A">
        <h5 class="mb-0">
            <a href="#"><span>Change Password</span></a>
        </h5>
    </div>
    <div>
        <div class="card-body">
            <p class="d-none d-lg-block"><span>Change Password</span></p>
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{route('user.password.update')}}" method="POST">
              @csrf
                <div class="col-md-12 px-0">
                    <div class="row">
                        <div class=" col-md-6 pl-lg-0 px-0 pr-lg-3 mb-3">
                            <div class="field">
                                <input type="password" id="newpassword" name="password" autocomplete="off" class="to_capitalize" placeholder="New Password" maxlength="80" required>
                                    <label for="newpassword">
                                    <span>New Password</span>
                                    <sup class="mandatory-field">*</sup></label>
                            </div>
                        </div>
                        <div class=" col-md-6 pl-lg-0 px-0 pr-lg-3 mb-3">
                            <div class="field">
                                <input type="password" id="confirmpassword" name="password_confirmation" autocomplete="off" class="to_capitalize" placeholder="Confirm Password" maxlength="80" required>
                                    <label for="confirmpassword">
                                    <span>Confirm Password</span>
                                    <sup class="mandatory-field">*</sup></label>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="offset-md-3 col-md-6 col-12 text-center mt-lg-3 my_profile_pop_up px-0 px-lg-3">
                        <button type="submit" class="btn-block btn-lg save-btn"><span>Save</span></button></div>
            </form>
        </div>
    </div>
</div>