<div id="pane-A" class="card tab-pane show active" role="tabpanel" aria-labelledby="tab-A">
    <div class="card-header" role="tab" id="heading-A">
        <h5 class="mb-0">
            <a href="#"><span>Orders</span></a>
        </h5>
    </div>
    <div>
        <div class="card-body address-book">

            <div class="meal-items-history">
                @php $currency = getSettings()['currency'] ?? ''; @endphp
                @foreach($orders as $order)
                    <div class="meal-selection-item">
                        <span class="addresseditbtn"><i class="fa-solid fa-pencil"></i></span>
                        <div class="tile">
                            <div class="tile__texts">
                                <div class="tile__title">Order Id: {{$order->id}}</div>
                                <div class="tile__kcal">Reference: {{$order->reference}} </div>
                                <div class="tile__kcal">status: {{$order->status}} </div>
                                <div class="tile__kcal">payment: {{$order->payment}} </div>
                                <div class="tile__kcal">Total Amount: {{$currency.$order->total_amount_with_fee}} </div>
                                <a class="btn btn-sm btn-success" href="{{route('checkout',['reference'=>$order->reference])}}">View details</a>
                                <a class="btn btn-sm btn-success re-order" ref="{{$order->reference}}">Re Order</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        {{$orders->links()}}
    </div>
</div>
