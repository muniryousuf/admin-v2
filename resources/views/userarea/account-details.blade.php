<div id="pane-A" class="card tab-pane show active" role="tabpanel" aria-labelledby="tab-A">
    <div class="card-header" role="tab" id="heading-A">
        <h5 class="mb-0">
            <a href="#"><span>My Account</span></a>
        </h5>
    </div>
    <div>
        <div class="card-body">
            <p class="d-none d-lg-block"><span>My Account</span></p>
            <form action="{{route('user-details-update')}}" method="POST">
              @csrf
                <div class="col-md-12 px-0">
                    <div class="row">
                        <div class=" col-md-6 pl-lg-0 px-0 pr-lg-3 mb-3">
                            <div class="field">
                                <input type="text" id="m_firstname" name="first_name" autocomplete="off" class="to_capitalize" placeholder="First Name" maxlength="80" value="{{Auth::user()->name}}">
                                    <label for="m_firstname">
                                    <span>First Name</span>
                                    <sup class="mandatory-field">*</sup></label>
                            </div>
                        </div>
                        <div class=" col-md-6 pl-lg-0 px-0 pr-lg-3 mb-3">
                            <div class="field">
                                <input type="text" id="l_firstname" name="last_name" autocomplete="off" class="to_capitalize" placeholder="Last Name" maxlength="80" value="{{Auth::user()->lname}}">
                                    <label for="l_firstname">
                                    <span>Last Name</span>
                                    <sup class="mandatory-field">*</sup></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 px-0 ">
                    <div class="row">
                        <div class=" col-md-6 pl-lg-0 px-0 pr-lg-3 mb-0">
                            <div class="field">
                                <div id="countryExtentionNumber">+44</div>
                                <input type="tel" id="m_mobNo" name="phone" autocomplete="off" class="" placeholder="Phone" maxlength="14" value="{{Auth::user()->phone}}">
                                    <label for="m_mobNo" style="background: rgb(255, 255, 255);">
                                    <span>Phone</span>
                                    <sup class="mandatory-field">*</sup></label>
                            </div>
                        </div>
                        <div class=" col-md-6 pr-lg-0 px-0 px-lg-3 mb-2">
                            <div class="field ">
                                <input type="text" name="email" id="m_email" autocomplete="off" class="" placeholder="Enter email" maxlength="100" value="{{Auth::user()->email}}" disabled>
                                    <label for="m_email">
                                        <span>Email</span>
                                        <sup class="mandatory-field">*</sup></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 px-0 ">
                    <div class="row d-none">
                        <div class="col-md-10 pl-lg-0 px-0"><span class="recv-offers"><span>Receive offers from</span>
                                Original Cottage</span>
                        </div>
                        <div class="col-12">
                            <div class="row no-gutters">
                                <div class="col-4 pt-2 addons-item onclick-cursor">
                                    <div class="input-holder">
                                        <p class="menu-price-addons d-flex">
                                            <span>
                                                <input type="checkbox" name="step3" value="onion">
                                                <span class="checkmark checkbox"></span>
                                            </span>
                                            <span class="title-menu-addons pl-1">Email</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-4 pt-2 addons-item onclick-cursor">
                                    <div class="input-holder">
                                        <p class="menu-price-addons d-flex">
                                            <span>
                                                <input type="checkbox" name="step3" value="onion">
                                                <span class="checkmark checkbox"></span>
                                            </span>
                                            <span class="title-menu-addons pl-1">SMS</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="offset-md-3 col-md-6 col-12 text-center mt-lg-3 my_profile_pop_up px-0 px-lg-3">
                        <button type="submit" class="btn-block btn-lg save-btn"><span>Save</span></button></div>
                </div>
            </form>
        </div>
    </div>
</div>