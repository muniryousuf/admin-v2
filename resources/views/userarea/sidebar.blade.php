<ul id="tabs" class="nav nav-tabs" role="tablist">
   <li class="nav-item">
      <a id="tab-A" class="nav-link {{ Request::is('my-account*') ? 'active' : '' }} go-to-page" data-toggle="tab" role="tab" aria-selected="false" page-link="{{route('user.my-account')}}">
        <span>My Account</span>
      </a>
    </li>
   <li class="nav-item">
      <a id="tab-A" class="nav-link {{ Request::is('order-history*') ? 'active' : '' }} go-to-page" page-link="{{route('user.order.history')}}">
        <span>Order History</span>
      </a>
    </li>
   <li class="nav-item">
      <a id="tab-B" class="nav-link {{ Request::is('addresses*') ? 'active' : '' }} go-to-page" page-link="{{route('user.addresses')}}" data-toggle="tab" role="tab" aria-selected="false" >
        <span>Address Book</span>
      </a>
    </li>
   <li class="nav-item">
      <a id="tab-C" class="nav-link false go-to-page" page-link="{{route('user.change.password')}}" data-toggle="tab" role="tab">
        <span>Change Password</span>
      </a>
    </li>
   <li class="nav-item d-none">
      <a id="tab-D" class="nav-link false" data-toggle="tab" role="tab" aria-selected="true">
        <span>Advanced Options</span>
      </a>
    </li>
   <li class="nav-item">
      <a 
         id="tab-E" 
         class="nav-link false logout-user" 
         data-toggle="tab" 
         role="tab" 
         aria-selected="true"
         >
      <span>Logout</span>
      </a>
   </li>
</ul>