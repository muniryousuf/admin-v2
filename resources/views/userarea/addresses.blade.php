<div id="pane-A" class="card tab-pane show active" role="tabpanel" aria-labelledby="tab-A">
    <div class="card-header" role="tab" id="heading-A">
        <h5 class="mb-0">
            <a href="#"><span>Addresses</span></a>
        </h5>
    </div>
    <div class="card-body">
        <button id="add-new-address" class="btn btn-success">Add new address</button>
    </div>
    <div>
        <div class="card-body address-book">

            <div class="meal-items-history">

                @foreach($user_address as $address)
                    <div class="meal-selection-item">
                        <a
                            class="edit"
                            href="javascript:void(0);"
                            address='{!! json_encode($address) !!}'
                        >
                            <span class="addresseditbtn"><i class="fa-solid fa-pencil"></i></span>
                            <div class="tile">
                                <div class="tile__texts">
                                    <div class="tile__title">{{$address->address.' '.$address->street.' '.$address->town}}</div>
                                    <div class="tile__kcal"> {{$address->postal_code}} </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    {{$user_address->links()}}
</div>
