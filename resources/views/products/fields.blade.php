<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/products.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/products.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Food Allergy Field -->
<div class="form-group col-sm-6">
    {!! Form::label('food_allergy', __('models/products.fields.food_allergy').':') !!}
    {!! Form::text('food_allergy', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', __('models/products.fields.price').':') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', __('models/products.fields.image').':') !!}
    {!! Form::file('image') !!}
</div>
<div class="clearfix"></div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', __('models/products.fields.status').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', 0) !!}
        {!! Form::checkbox('status', '1', null) !!}
    </label>
</div>
<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('offer', __('models/products.fields.offer').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('offer', 0) !!}
        {!! Form::checkbox('offer', '1', null) !!}
    </label>
</div>
<!-- id_category Field-->
<div class="form-group col-sm-6">
    {!! Form::label('id_category', __('models/products.fields.id_category').':') !!}
        <select name="id_category" class="form-control">
            <option disabled selected>Select</option>

                @foreach($categories as $cat)
                    <option value="{{$cat->id}}" {{($products->id_category ?? '') == ($cat->id ?? '') ? 'selected' : ''}}>{{$cat->name}}</option>
                @endforeach

        </select>
</div>

<!-- id_category Field-->
<div class="form-group col-sm-6">
    {!! Form::label('id_meta', __('models/products.fields.id_meta').':') !!}
        <select name="id_metas[]" id="id_meta" class="form-control" multiple="multiple">
            <option disabled >Select</option>
            @foreach($meats as $meta)
            @php
                $selected = '';
                if(isset($products)){
                    foreach($products->id_of_metas as $ids){
                        if($ids->id_meta == $meta->id){
                            $selected = 'selected';
                        }
                    }
                }

            @endphp
                <option value="{{$meta->id}}" {{$selected}}>{{$meta->name}}</option>
            @endforeach
        </select>
</div>
<!-- related_product_ids Field-->
<div class="form-group col-sm-6">
    {!! Form::label('related_product_ids', __('models/products.fields.related_product_ids').':') !!}
        <select name="related_product_ids[]" id="related_product_ids" class="form-control" multiple="multiple">
            <option disabled >Select</option>
            @foreach($products_data as $product)
            @php
                $selected = '';
                if(isset($products) && $products->related_product_ids != null){
                    foreach($products->related_product_ids as $ids){
                        if($ids == $product->id){
                            $selected = 'selected';
                        }
                    }
                }

            @endphp
                <option value="{{$product->id}}" {{$selected}}>{{$product->name}}</option>
            @endforeach
        </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('products.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#id_meta').select2();
        $('#related_product_ids').select2();

        $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);

        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
        });
    });
</script>
@endsection
