<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/products.fields.name').':') !!}
    <p>{{ $products->name }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', __('models/products.fields.description').':') !!}
    <p>{{ $products->description }}</p>
</div>

<!-- Food Allergy Field -->
<div class="form-group">
    {!! Form::label('food_allergy', __('models/products.fields.food_allergy').':') !!}
    <p>{{ $products->food_allergy }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', __('models/products.fields.price').':') !!}
    <p>{{ $products->price }}</p>
</div>

<!-- Id Category Field -->
<div class="form-group">
    {!! Form::label('id_category', __('models/products.fields.id_category').':') !!}
    <p>{{ $products->id_category }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', __('models/products.fields.image').':') !!}
    <p><img class="image-preview" src="{{asset('images/'.$products->image)}}" /></p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', __('models/products.fields.status').':') !!}
    <p>{{ $products->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/products.fields.created_at').':') !!}
    <p>{{ $products->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/products.fields.updated_at').':') !!}
    <p>{{ $products->updated_at }}</p>
</div>

