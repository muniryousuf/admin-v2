<div class="table-responsive">
    <table class="table" id="products-table">
        <thead>
            <tr>
                <th>@lang('models/products.fields.name')</th>
                <th>@lang('models/products.fields.description')</th>
                <th>@lang('models/products.fields.food_allergy')</th>
                <th>@lang('models/products.fields.price')</th>
                <th>@lang('models/products.fields.id_category')</th>
                <th>@lang('models/products.fields.image')</th>
                <th>@lang('models/products.fields.status')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($products as $products)
            <tr>
                       <td>{{ $products->name }}</td>
            <td>{{ $products->description }}</td>
            <td>{{ $products->food_allergy }}</td>
            <td>{{ $products->price }}</td>
            <td>{{ $products->category->name ?? 'N/A' }}</td>
            <td><img   onerror="this.src='https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg'" class="image-preview" src="{{asset('images/'.$products->image)}}" /></td>
            <td>{{ $products->status == '1' ? 'ON' : 'OFF'}}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['products.destroy', $products->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('products.show', [$products->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('products.edit', [$products->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
