@extends('layouts.app')
@section('title')
     @lang('models/products.plural')
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@lang('models/products.plural')</h1>
            <div class="col-md-6">
                <input placeholder="Search product here" class="form-control" id="search-product" value="{{$search}}"/>
                <button class="btn btn-success" onclick="search()">Search</button>
                <button class="btn btn-primary" onclick="clearSearch()">Clear search</button>
            </div>
            <div class="form-group col-sm-6">
              
                    <select name="categories_ids[]" id="categories_ids" class="form-control" multiple="multiple">
                        <option disabled >Select</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
            </div>
            <div class="section-header-breadcrumb">
                <a href="{{ route('products.create')}}" class="btn btn-primary form-btn">@lang('crud.add_new')<i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('products.table')
            </div>
       </div>
   </div>
     @include('stisla-templates::common.paginate', ['records' => $products])
    </section>
@endsection
@section('scripts')
<script type="text/javascript">
    function search(){
        link = window.location.href
        var url = new URL(link);
        let search_value = document.getElementById('search-product');
        if(search_value.value != ''){
          
            url.searchParams.delete('search');
            url.searchParams.set('search', search_value.value);
            window.history.replaceState(null, null, url);
            window.location.reload()
        }
    }
    function clearSearch(){
      window.location.href="{{route('products.index')}}"
    }

    $(document).keyup(function (e) {
        if (e.keyCode === 13) {
         search();
        }
     })
</script>
<script type="text/javascript">
    var id_categories = {};
    $(document).ready(function() {
        $('#categories_ids').select2();

        $("select").on("select2:select", function (evt) {
            
            var element = evt.params.data.element;
            id_categories[element.value] = element.value
            
            let ids = '';
            for (const [key, value] of Object.entries(id_categories)) {
                    ids+= '-'+value
            }
            link = window.location.href;
            var url = new URL(link);
            let search_value = document.getElementById('search-product');
            if(ids != ''){

                url.searchParams.delete('ids');
                url.searchParams.set('ids', ids);
                window.history.replaceState(null, null, url);
            }
            
            var $element = $(element);
            $element.detach();
            $(this).append($element);
            $(this).trigger("change");
        });
    });
</script>
@endsection

