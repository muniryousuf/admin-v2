@extends('layouts.front')
@section('content')
  <div class="root-content" style="margin-top: 60px;">
       @include('top-header')
        <section class="section-fullwidth reservation-section">
            <div class="container">
                <div class="deal-content">
                    <div class="offers-head">
                         <h2 class="text-center">Book your Reservation</h2>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div id="messages">
                                <div class="alert alert-success d-none" role="alert" id="successMessage">

                                </div>
                                <div class="alert alert-danger d-none" role="alert" id="errorMessage">

                                </div>
                            </div>
                            <form >
                                <div class="form-details">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Enter Booking Date <span style="color:red">*</span></label>
                                                <input class="form-control" type="date" id="booking_date"/>
                                            </div>
                                        </div>

                                        <div class="col-md-4 select-person">
                                            <div class="form-group">
                                                <label for="">Enter No. of Persons <span style="color:red">*</span></label>
                                                <span class="f-icon"><i class="fas fa-user-friends"></i></span>
                                                <select  class="form-control select-box" id="no_of_persons" >
                                                    <option value="null" selected disabled>Select No Of Persons</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">&nbsp;</label>
                                            <a class="btn btn-rounded-danger  btn-block" onclick="checkDate()">
                                                Book Your Table
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="select-reservation-time boxed">
                                    <h6>Select a Time: <span style="color:red">*</span></h6>
                                    <div class="d-flex time-avaiable">

                                    </div>
                                </div>

                                <div class="diner-details d-none" id="dinerDetails">
                                    <h3>Diner Details</h3>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">First Name <span style="color:red">*</span></label>
                                                <input type="text" class="form-control" placeholder="Enter your First Name" id="formData-firstname">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Last Name <span style="color:red">*</span></label>
                                                <input type="text" class="form-control" placeholder="Enter your Last Name" id="formData-lastname">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Phone Number <span style="color:red">*</span></label>
                                                <input type="number" class="form-control" placeholder="07XXXXXXXXX" id="formData-phone" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Email Address <span style="color:red">*</span></label>
                                                <input type="email" class="form-control" placeholder="Enter your Email Address" id="formData-email">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Special occasion:</label>

                                                <select name="" id="" class="form-control" id="formData.special_occasion">
                                                    <option value="default">Select an occasion (optional)</option>
                                                    <option value="1">Birthday</option>
                                                    <option value="2">Anniversary</option>
                                                    <option value="3">Date night</option>
                                                    <option value="4">Business Meal</option>
                                                    <option value="5">Celebration</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label for="">Add a special request:</label>
                                                <textarea class="form-control" placeholder="Add a special request (optional)" rows="1" id="formData.special_request">
                                                </textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12 custom-checkbox-input">
                                            <div class="form-group">
                                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" id="formData.restaurant_newsletter">
                                                <label class="form-check-label" for="defaultCheck1">
                                                    Sign me up to receive dining offers and news from this restaurant by email.
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 custom-checkbox-input d-none">
                                            <div class="form-group">
                                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2" id="formData.opentable_newsletter">
                                                <label class="form-check-label" for="defaultCheck2">
                                                    Sign me up to receive dining offers and news from OpenTable by email.
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 custom-checkbox-input">
                                            <div class="form-group">
                                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck3" id="formData.reservations_reminder">
                                                <label class="form-check-label" for="defaultCheck3">
                                                    Yes, I want to get text updates and reminders about my reservations.
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="">&nbsp;</label>
                                            <button class="btn btn-rounded-danger btn-block" type="button" onclick="validateBeforeSubmit()">
                                                Complete Reservation
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment.min.js"></script>
    <script type="text/javascript">
        var booking_date = $('#booking_date');
        var booking_time = '';
        booking_date.attr('min',new Date().toISOString().split("T")[0]);
        booking_date.attr('value',new Date().toISOString().split("T")[0]);
        var restaurantTimes = {};

        function getRestaurantPersons() {

             $.ajax({
                type:'GET',
                url:"api/no_of_persons",
                dataType:'json',
                async:false,
                success:function(response) {
                    let no_of_persons = response.data
                    for (var i = 1; i <=response.data; i++) {
                        $('#no_of_persons').append(`<option>${i}</option>`)
                    }
                }
            });
        }
        function getRestaurantTime() {

             $.ajax({
                type:'GET',
                url:"api/restuarant_time",
                dataType:'json',
                async:false,
                success:function(response) {
                    restaurantTimes = response.data
                }
            });
        }

        function checkDate() {
            var self     = this;
            let day = moment(booking_date.val()).format('dddd');
            restaurantTimes.forEach(function(item, index) {
                if (day == item.day) {
                    self.selectedTime  = item
                }
            });

            if (Object.keys(self.selectedTime).length > 0) {
                if (self.selectedTime.shop_close == 1) {
                    calcTime(self.selectedTime.start_time, self.selectedTime.end_time );
                } else {
                    errorMessage = 'Sorry!!! Restaurant Is Close.';
                    $('#errorMessage').removeClass('d-none');
                    $('#errorMessage').text(errorMessage);
                    scrollToTop();
                }
            } else {
                 errorMessage = 'Please Select Proper Booking Date.';
                    $('#errorMessage').removeClass('d-none');
                    $('#errorMessage').text(errorMessage);
                    scrollToTop();

            }
            setTimeout(function () {
                self.errorMessage = '';
            }, 3000);

        }
            function calcTime(startTime = 0, endTime = 0) {
                this.timeSection = true;
                if (startTime == 0 || endTime == 0) {
                    return false;
                }
                this.timeSlot = this.returnTimesInBetween(startTime, endTime);

                $('.time-avaiable').html('');
                this.timeSlot.forEach(function(slot, index) {
                    $('.time-avaiable').append(

                            `
                                <div class="form-group">
                                    <input class="inputTime booking_time" type="radio" id="time${index}" name="time" value="${slot}">
                                    <label class="labelTime" for="time${index}"><i class="fas fa-clock mr-2"></i>${slot}</label>
                                </div>



                            `

                    )
                });

            }
            $(document).on('click','.booking_time',function(){
                booking_time = $(this).val();
                $('#dinerDetails').removeClass('d-none');
            })
            function returnTimesInBetween(start, end) {
                var timesInBetween = [];

                var startH  = parseInt(start.split(":")[0]);
                var startM  = parseInt(start.split(":")[1]);
                var endH    = parseInt(end.split(":")[0]);
                var endM    = parseInt(end.split(":")[1]);

                if (startM == 30)
                    startH++;

                for (var i = startH; i < endH; i++) {
                    timesInBetween.push(i < 10 ? "0" + i + ":00" : i + ":00");
                    timesInBetween.push(i < 10 ? "0" + i + ":30" : i + ":30");
                }

                timesInBetween.push(endH + ":00");
                if (endM == 30)
                    timesInBetween.push(endH + ":30")

                let time = timesInBetween.map((timeString) => {
                    let H = +timeString.substr(0, 2);
                    let h = (H % 12) || 12;
                    let ampm = H < 12 ? " AM" : " PM";
                    return timeString = h + timeString.substr(2, 3) + ampm;
                });

                return time;
            }



        $(document).ready(function(){
            getRestaurantPersons();
            getRestaurantTime();
        })
        function scrollToTop() {
            $('html, body').animate({
                scrollTop: $("div#messages").offset().top
            }, 1000);
        }
        function resetForm(){
            $('#dinerDetails').addClass('d-none');
            $('#no_of_persons').val('');
            $('#formData-firstname').val('');
            $('#formData-lastname').val('');
            $('#formData-phone').val('');
            $('#formData-email').val('');
            $('.time-avaiable').html('');

        }
        function validateBeforeSubmit() {
                var self     = this;
                let error    = false;
                let formData = {};

                formData.booking_date = booking_date.val();
                formData.booking_time = booking_time
                formData.persons = $('#no_of_persons').val();
                formData.firstname = $('#formData-firstname').val();
                formData.lastname = $('#formData-lastname').val();
                formData.phone = $('#formData-phone').val();
                formData.email = $('#formData-email').val();



                if (formData.firstname == null || formData.firstname == '') {
                    errorMessage = 'First Name is required';
                    $('#errorMessage').removeClass('d-none');
                    $('#errorMessage').text(errorMessage);
                    scrollToTop();
                    error = true;
                    return;
                }

                if (formData.booking_date == null) {
                    errorMessage = 'Booking Date is required';
                    $('#errorMessage').removeClass('d-none');
                    $('#errorMessage').text(errorMessage);
                    scrollToTop();
                    return;
                }
                if (formData.booking_time == null) {
                    errorMessage = 'Booking Time is required';
                    $('#errorMessage').removeClass('d-none');
                    $('#errorMessage').text(errorMessage);
                    scrollToTop();
                    error = true;
                    return;
                }


                if (formData.lastname == null || formData.lastname == '') {
                    errorMessage = 'Last Name is required';
                    $('#errorMessage').removeClass('d-none');
                    $('#errorMessage').text(errorMessage);
                    scrollToTop();
                    error = true;
                    return;
                }
                if (formData.phone == null || formData.phone == '') {
                    errorMessage = 'Phone is required';
                    $('#errorMessage').removeClass('d-none');
                    $('#errorMessage').text(errorMessage);
                    scrollToTop();
                    error = true;
                    return;
                }
                if (formData.email == null || formData.email == '') {
                    errorMessage = 'Email is required';
                    $('#errorMessage').removeClass('d-none');
                    $('#errorMessage').text(errorMessage);
                    scrollToTop();
                    error = true;
                    return;
                }
                if (!validateEmail(formData.email)) {
                    errorMessage = 'Email is must be a valid email';
                    $('#errorMessage').removeClass('d-none');
                    $('#errorMessage').text(errorMessage);
                    scrollToTop();
                    error = true;
                    return;
                }

                if (error == false) {

                    $.ajax({
                        type:'POST',
                        url:"api/reservation",
                        dataType:'json',
                        data:formData,
                        async:false,
                        success:function(response) {
                            successMessage = response.message;
                            $('#errorMessage').addClass('d-none');
                            $('#successMessage').removeClass('d-none');
                            $('#successMessage').text(successMessage);
                            scrollToTop();
                            setTimeout(function () {
                               resetForm();
                            }, 3000);
                        },
                        error:function(error) {
                            errorMessage = error.message;
                            $('#successMessage').addClass('d-none');
                            $('#errorMessage').removeClass('d-none');
                            $('#errorMessage').text(errorMessage);
                            scrollToTop();
                        }
                    });
                }
            }
            function validateEmail(email) {
                var re = /\S+@\S+\.\S+/;
                return re.test(email);
            }

    </script>
@endsection

@section('page_css')
<style type="text/css">

/*************** Reservation-section-start *******************/

.reservation-section {
  /*background-image: url('images/tasty-land/reservation-banner.png');
  background-repeat: no-repeat;
  background-size: cover;
  background-attachment: fixed;*/
  background: #000;
    padding-top: 50px;
    padding-bottom: 50px;
}

.reservation-section h2 {
  color: #fff;
  margin-bottom: 20px;
}

.reservation-section form {
  margin-top: 20px;
}

.reservation-section form .form-group label span {
  position: static;
  font-size: 13px;
}

.reservation-section form .form-group input {
  width: 100%;
  padding: 15px 20px;
  border-radius: 3px;
  border: none;
  line-height: 1;
  color: #000;
}

.reservation-section form .form-group .form-control {
  width: 100%;
  padding: 15px 20px;
  border-radius: 3px;
  border: none;
  line-height: 1;
  height: auto;
  color: #000;
  min-height: 51px;
}

.reservation-section form .form-group .form-control.select-box {
  padding: 15px 40px;
}

.reservation-section form .form-group .form-control option {
  color: #000;
}

.reservation-section form .form-group .f-icon {
  top: 42px;
}

.reservation-section form .btn {
  height: 52px;
  line-height: 39px;
  text-transform: uppercase;
  font-size: 20px;
  font-weight: 500;
}

.reservation-section form .time-avaiable {
  display: flex;
  align-items: center;
  flex-wrap: wrap;
}
@media only screen and (max-width: 600px) {
    .reservation-section form .time-avaiable {
       justify-content: center;
    }
}
.reservation-section form .time-avaiable .form-group {
  margin-right: 5px;
}

.form-details label {
  color: #fff;
  font-weight: bold;
}

.form-details .mx-datepicker {
  width: 100%;
}

.form-details .mx-datepicker .mx-input {
  height: calc(1.6em + 0.75rem + 2px);
}

.select-time .form-group,
.select-person .form-group {
  position: relative;
}

.select-time .form-group span,
.select-person .form-group span {
  position: absolute;
  top: 36px;
  left: 7px;
  font-size: 18px;
}

.select-time .form-group select,
.select-person .form-group select {
  padding: 10px 35px;
}

.select-person svg path {
  fill: rgba(0, 0, 0, 0.5);
}

.diner-details {
  margin-top: 20px;
}

.diner-details .count-down-timer {
  text-align: center;
}

.diner-details h3 {
  text-align: center;
  margin-bottom: 20px;
}

.diner-details label {
  color: #fff;
  text-align: left !important;
  font-weight: bold;
}

.diner-details p {
  background: #f3d6d4;
  border-radius: 4px;
  color: #000;
  font-size: 12px;
  padding: 5px 0;
  margin-bottom: 20px;
  display: inherit;
}

.diner-details h3 {
  color: #fff;
  font-weight: bold;
}

.vuejs-countdown {
  display: inline-block;
}

.vuejs-countdown .digit {
  font-size: 14px !important;
}

.vuejs-countdown li:after {
  top: 0px !important;
  font-size: 16px !important;
}

.custom-checkbox-input .form-group {
  display: block;
  margin-bottom: 15px;
}

.custom-checkbox-input .form-group input {
  padding: 0;
  height: initial;
  width: initial;
  margin-bottom: 0;
  display: none;
  cursor: pointer;
}

.custom-checkbox-input .form-group label {
  position: relative;
  cursor: pointer;
}

.custom-checkbox-input .form-group label:before {
  content: "";
  -webkit-appearance: none;
  background-color: transparent;
  border: 2px solid #0079bf;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
  padding: 10px;
  display: inline-block;
  position: relative;
  vertical-align: middle;
  cursor: pointer;
  margin-right: 5px;
}

.custom-checkbox-input .form-group input:checked + label:after {
  content: "";
  display: block;
  position: absolute;
  top: 4px;
  left: 9px;
  width: 6px;
  height: 14px;
  border: solid #0079bf;
  border-width: 0 2px 2px 0;
  transform: rotate(45deg);
}

.custom-checkbox-input label {
  font-weight: normal;
  font-size: 15px;
}

.reservation-agreement p {
  color: #fff;
  background: none;
  font-size: 13px;
  margin-top: 10px;
}

.boxed label {
  display: inline-block;
  width: 130px;
  padding: 10px;
  border: solid 2px #f48a32;
  transition: all 0.3s;
  color: #fff !important;
  background: linear-gradient(to right, #f48a32 0%, #d4972b 100%);
  cursor: pointer;
}

.boxed input[type=radio] {
  display: none;
}

.boxed input[type=radio]:checked + label {
  border: solid 3px #862c2f;
  cursor: pointer;
}

.select-reservation-time.boxed h6 {
  color: #fff;
  font-size: 18px;
}

.image-gallery {
  justify-content: center;
}

.section-gallery {
  text-align: center;
  padding: 30px 0;
}

.section-gallery h3 {
  margin-bottom: 20px;
  font-size: 35px;
}

.order-information .cell-1 {
  border-collapse: separate;
  border-spacing: 0 4em;
  background: #ffffff;
  border-bottom: 3px solid #efefef;
  background-clip: padding-box;
  cursor: pointer;
}

.order-information thead {
  background: #f48a32;
  color: #fff;
}

.order-information .table-elipse {
  cursor: pointer;
}

.order-information #demo {
  transition: all 0.3s ease-in-out;
}

.order-information .row-child {
  background-color: #ebf4fb;
  color: #0a5385;
}

.product-detail {
  padding: 30px;
}

.product-detail td {
  padding: 0;
}

.product-detail td:first-child {
  width: 40%;
  padding-bottom: 10px;
}

.product-detail td:last-child {
  padding-bottom: 0px;
}

.product-detail {
  padding-bottom: 0;
}

.order-information .nav-pills .nav-pills .show > .nav-link {
  background-color: #f48a32 !important;
}

.order-information .nav-pills .nav-link {
  color: #f48a32;
}

.nav-pills .nav-link.active {
  color: #fff;
  background-color: #f48a32 !important;
}

.order-information td,
.order-information th {
  font-size: 14px;
}

.order-information .btn.cstm-btn.btn-primary {
  background-color: #f48a32 !important;
  border-color: #f48a32 !important;
}

.card-text label {
  font-size: 14px;
}

.order-conmfirm thead {
  background: transparent;
  color: grey;
}

.item-detail .more-info {
  font-size: 16px;
  color: grey;
  margin-bottom: 20px;
}

.order-confirm-header {
  text-align: center;
  background: #f9fbfc;
  padding: 50px 20px;
  margin-bottom: 40px;
}

@media (max-width: 767.98px) {
  .order-confirm-header {
    padding: 30px 20px;
  }
}

.order-confirm-header img {
  width: 250px;
}

@media (max-width: 767.98px) {
  .order-confirm-header img {
    width: 110px;
  }
}

.order-confirm h3 {
  color: #364152;
  font-weight: bold;
}

.order-confirm p {
  color: #495362;
  margin-bottom: 20px;
}

.order-confirm p strong {
  font-size: 22px !important;
  font-weight: initial;
}

.sepeartor-line {
  border-bottom: 2px solid #dee2e6 !important;
  margin: 40px 0;
}

.order-detail thead {
  background: transparent;
  color: #495362;
}

.shipping-info p {
  color: #495362;
}

.shipping-info p strong {
  font-size: 16px;
}

.order-confirm-footer {
  background: #f9fbfc;
  padding: 20px 0;
}

@media (max-width: 767.98px) {
  .order-confirm-footer {
    text-align: center;
  }
}

.order-confirm-footer p {
  display: inline-block;
}

.order-confirm-footer .copyright-text {
  color: #bec2cb;
  float: right;
}

@media (max-width: 767.98px) {
  .order-confirm-footer .copyright-text {
    float: none;
  }
}

.bill-info ul li,
.total-bill-info ul li {
  list-style: none;
  margin-bottom: 10px;
}

.bill-info ul li .list-detail,
.total-bill-info ul li .list-detail {
  display: inline-block;
  font-size: 18px;
  width: 49%;
}

.bill-info ul li .list-detail strong,
.total-bill-info ul li .list-detail strong {
  font-size: 16px;
}

.bill-info ul li .discount-value,
.total-bill-info ul li .discount-value {
  color: #29bfaf;
}

@media (max-width: 767.98px) {
  .product-order-detail img {
    margin-bottom: 15px;
  }
}

.product-order-detail p {
  margin-left: 10px;
}

@media (max-width: 767.98px) {
  .product-order-detail p {
    margin-left: 0;
  }
}

@media (max-width: 767.98px) {
  .item-price {
    margin-bottom: 20px;
    border-bottom: 1px solid #ccc;
    padding-bottom: 20px;
  }
}

@media (max-width: 767.98px) {
  .text-left-mob {
    text-align: left !important;
  }
}

.order-confirm-footer img {
  width: 140px;
  margin-right: 10px;
  margin-top: -6px;
}

@media (max-width: 767.98px) {
  .order-confirm-footer img {
    width: 100px;
    margin-bottom: 20px;
  }
}
.btn.btn-rounded-danger {
  color: #fff !important;
  background: #e5b052;
  background-image: linear-gradient(to right, #e5b052 0%, #e5b052 100%);
  background-repeat: repeat-x;
}

/*************** Reservation-section-end *******************/


</style>
@endsection
