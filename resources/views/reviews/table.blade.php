<div class="table-responsive">
    <table class="table" id="reviews-table">
        <thead>
            <tr>
                <th>@lang('models/reviews.fields.name')</th>
        <th>@lang('models/reviews.fields.message')</th>
        <th>@lang('models/reviews.fields.rating')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($reviews as $reviews)
            <tr>
                       <td>{{ $reviews->name }}</td>
            <td>{{ $reviews->message }}</td>
            <td>{{ $reviews->rating }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['reviews.destroy', $reviews->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('reviews.show', [$reviews->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('reviews.edit', [$reviews->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
