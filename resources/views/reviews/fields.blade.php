<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/reviews.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Message Field -->
<div class="form-group col-sm-6">
    {!! Form::label('message', __('models/reviews.fields.message').':') !!}
    {!! Form::text('message', null, ['class' => 'form-control']) !!}
</div>

<!-- Rating Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating', __('models/reviews.fields.rating').':') !!}
    {!! Form::text('rating', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('reviews.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
