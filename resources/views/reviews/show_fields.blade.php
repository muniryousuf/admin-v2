<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/reviews.fields.name').':') !!}
    <p>{{ $reviews->name }}</p>
</div>

<!-- Message Field -->
<div class="form-group">
    {!! Form::label('message', __('models/reviews.fields.message').':') !!}
    <p>{{ $reviews->message }}</p>
</div>

<!-- Rating Field -->
<div class="form-group">
    {!! Form::label('rating', __('models/reviews.fields.rating').':') !!}
    <p>{{ $reviews->rating }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/reviews.fields.created_at').':') !!}
    <p>{{ $reviews->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/reviews.fields.updated_at').':') !!}
    <p>{{ $reviews->updated_at }}</p>
</div>

