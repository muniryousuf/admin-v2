<?php

return array (
  'singular' => 'Category',
  'plural' => 'Categories',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'description' => 'Description',
    'image' => 'Image',
    'sort' => 'Sort',
    'status' => 'Status',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
