<?php

return array (
  'singular' => 'Gallery',
  'plural' => 'Galleries',
  'fields' => 
  array (
    'id' => 'Id',
    'image' => 'Image',
    'gallery_id' => 'Gallery Id',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'title' => 'Title',
  ),
);
