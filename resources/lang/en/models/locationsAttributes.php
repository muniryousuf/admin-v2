<?php

return array (
  'singular' => 'Locations Attributes',
  'plural' => 'Locations Attributes',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'details' => 'Details',
    'map' => 'Map',
    'id_location' => 'Location',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
