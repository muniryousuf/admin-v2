<?php

return array (
  'singular' => 'CustomerReservations',
  'plural' => 'CustomerReservations',
  'fields' => 
  array (
    'id' => 'Id',
    'firstname' => 'Firstname',
    'lastname' => 'Lastname',
    'phone' => 'Phone',
    'email' => 'Email',
    'booking_date' => 'Booking Date',
    'persons' => 'Persons',
    'status' => 'Status',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'booking_time' => 'Booking Time',
  ),
);
