<?php

return array (
  'singular' => 'HoldCarts',
  'plural' => 'HoldCarts',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'hold_data' => 'Hold Data',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
