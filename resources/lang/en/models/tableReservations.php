<?php

return array (
  'singular' => 'Table Management',
  'plural' => 'Table Management',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'is_reserve' => 'Is Reserve',
    'reservation_end_time' => 'Reservation End Time',
    'reservation_start_time' => 'Reservation Start Time',
    'id_category' => 'Id Category',
    'table_type' => 'Table Type',
    'number_of_person' => 'Number Of Person',
    'number_of_person_sitting' => 'Number Of Person Sitting',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
