<?php

return array (
  'singular' => 'Vouchers',
  'plural' => 'Vouchers',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'type' => 'Type',
    'expiry_date' => 'Expiry Date',
    'status' => 'Status',
    'coupon_limit' => 'Coupon Limit',
    'user_limit' => 'User Limit',
    'auto_coupon' => 'Auto Coupon',
    'for_first_order' => 'For First Order',
    'for_all_orders' => 'For All Orders',
    'with_delivery' => 'With Delivery',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'discount' => 'Discount',
    'is_limit_optional' => 'Limit Optional',
  ),
);
