<?php

return array (
  'singular' => 'Home Page',
  'plural' => 'Home Page',
  'fields' => 
  array (
    'id' => 'Id',
    'title' => 'Title',
    'content' => 'Content',
    'slug' => 'Slug',
    'meta_title' => 'Meta Title',
    'meta_desc' => 'Meta Desc',
    'index_follow' => 'Index Follow',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'id_gallery' => 'Gallery',
  ),
);
