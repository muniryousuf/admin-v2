<?php

return array (
  'singular' => 'MessageTemplating',
  'plural' => 'MessageTemplatings',
  'fields' => 
  array (
    'id' => 'Id',
    'template_slug' => 'Template Slug',
    'description' => 'Description',
    'status' => 'Status',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
