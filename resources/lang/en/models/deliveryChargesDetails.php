<?php

return array (
  'singular' => 'Delivery Charges',
  'plural' => 'Delivery Charges',
  'fields' => 
  array (
    'id' => 'Id',
    'delivery_id' => 'Delivery Id',
    'miles' => 'Miles',
    'postal_code' => 'Postal Code',
    'amount' => 'Amount',
    'fix_delivery_charges' => 'Fix Delivery Charges',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'delivery_types' => 'Delivery Type',
  ),
);
