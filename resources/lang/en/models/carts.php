<?php

return array (
  'singular' => 'Cart',
  'plural' => 'Carts',
  'fields' => 
  array (
    'id' => 'Id',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
