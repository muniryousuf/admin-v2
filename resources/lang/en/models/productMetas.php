<?php

return array (
  'singular' => 'Group',
  'plural' => 'Groups',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'label' => 'Label',
    'id_parent' => 'Id Parent',
    'id_attribute' => 'Id Attribute',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'image' => 'Image',
    'price' => 'Price',
    'is_required' => 'Required',
    'table_type' => 'Type',
    'limit' => 'Free Limit',
    'limit_' => 'limit',
    'multiple_quantity_allowed' => 'Mulltiple Quantity Allowed',
  ),
);
