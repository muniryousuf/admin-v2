<?php

return array (
  'singular' => 'Kiosk General Settings',
  'plural' => 'Kiosk General Settings',
  'fields' => 
  array (
    'id' => 'Id',
    'main_printer' => 'Main Printer',
    'kitchen_printer' => 'Kitchen Printer',
    'kicthen_copies' => 'Kicthen Copies',
    'shop_name_and_address' => 'Shop Name And Address',
    'reciept_end_text' => 'Reciept End Text',
    'menu_url' => 'Menu Url',
    'enable_multi_payment' => 'Enable Multi Payment',
    'extra_info' => 'Extra Info',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
