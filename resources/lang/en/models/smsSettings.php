<?php

return array (
  'singular' => 'Sms Setting',
  'plural' => 'Sms Settings',
  'fields' => 
  array (
    'id' => 'Id',
    'key' => 'Key',
    'secret' => 'Secret',
    'gateway_key' => 'Gateway Key',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
