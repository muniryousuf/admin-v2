<?php

return array (
  'singular' => 'TableCategories',
  'plural' => 'TableCategories',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'status' => 'Status',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
