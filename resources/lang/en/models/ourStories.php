<?php

return array (
  'singular' => 'Our Story',
  'plural' => 'Our Story',
  'fields' => 
  array (
    'id' => 'Id',
    'main_title' => 'Main Title',
    'all_title' => 'All Title',
    'description' => 'Description',
    'image' => 'Image',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
