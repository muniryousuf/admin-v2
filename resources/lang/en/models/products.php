<?php

return array (
  'singular' => 'Products',
  'plural' => 'Products',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'description' => 'Description',
    'food_allergy' => 'Food Allergy',
    'price' => 'Price',
    'id_category' => 'Category',
    'image' => 'Image',
    'status' => 'Status',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'id_meta' => 'Group',
    'offer' => 'Offer',
    'related_product_ids' => 'Related Products',
  ),
);
