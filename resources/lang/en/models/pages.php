<?php

return array (
  'singular' => 'Pages',
  'plural' => 'Pages',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'html' => 'Html',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'type' => 'Type',
    'page_type' => 'Page Type',
    'template' => 'Template',
    'background_type' => 'Background Type',
    'background_type_value' => 'Background Type Value',
  ),
);
