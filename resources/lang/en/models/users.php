<?php

return array (
  'singular' => 'Customer',
  'plural' => 'Customers',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'lname' => 'Lname',
    'email' => 'Email',
    'phone' => 'Phone',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
