<?php

return [
    'singular' => 'General Setting',
    'plural' => 'General Setting',
    'fields' => [

        'id'=> 'ID',
        'site_name'=> 'Site Name',
        'site_title'=> 'Site Title',
        'tag_line'=> 'Tag line',
        'copyright_text'=> 'CopyRight Text',
        'header_logo'=> 'Header Logo',
        'footer_logo'=> 'Footer Logo',
        'service_charges'=> 'Service Charges',
        'shop_status'=> 'Shop Status',
        'stripe_publishable_key'=> 'Stripe Publishable Key',
        'stripe_secret_key'=> 'Stripe Secret Key',
        'facebook'=> 'Facebook',
        'twitter'=> 'twitter',
        'pinterest'=> 'pinterest',
        'youtube'=> 'youtube',
        'instagram'=> 'instagram',
        'printer_ip_1'=> 'Printer IP',
        'min_collection_time'=> 'Min Collection Time (minutes)',
        'min_delivery_time'=> 'Minimum Delivery Time (minutes)',
        'address'=> 'Address',
        'total_allowed_person'=> 'Total Allowed Persons',
        'fav_icon'=> 'Favicon',
        'linkedinn'=> 'Linked In',
        'snapchat'=> 'Snapchat',
        'ticktok'=> 'Tik Tok',
        'currency'=> 'Currency',
        'map'=> 'Map',
        'auto_rotation'=> 'Slider auto rotation',
        'background_image'=> 'Background image',
        'minimum_amount_to_order'=> 'Minimum Amount For Order',
        'rider_will_deliver'=> 'Rider Will Deliver',
        'rider_dilver_message'=> 'Message For Rider Delivery Option.',

    ],




];
