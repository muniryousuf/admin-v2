<?php

return array (
  'singular' => 'Restaurant Timings Specific',
  'plural' => 'Restaurant Timings Specific',
  'fields' => 
  array (
    'id' => 'Id',
    'start_time' => 'Start Time',
    'end_time' => 'End Time',
    'shop_closed' => 'Shop Closed',
    'specific_date' => 'Specific Date',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
