<?php

return array (
  'singular' => 'Announcements',
  'plural' => 'Announcements',
  'fields' => 
  array (
    'id' => 'Id',
    'message' => 'Message',
    'title' => 'Title',
    'description' => 'Description',
    'amount' => 'Amount',
    'color' => 'Color',
    'status' => 'Status',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
