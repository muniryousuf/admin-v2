<?php

return array (
  'singular' => 'Locations',
  'plural' => 'Locations',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'details' => 'Details',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
