<?php

return array (
  'singular' => 'Orders',
  'plural' => 'Orders List',
  'fields' => 
  array (
    'id' => 'Id',
    'order_id' => 'Order Id',
    'Reference' => 'Reference',
    'total_amount' => 'Total Amount',
    'payment_method' => 'Payment Method',
    'delivery_address' => 'Delivery Address',
    'status' => 'Status',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'order_reference' => 'Order Reference #',
    'product_name' => 'Product Name',
    'product_price' => 'Product Price',
    'product_quantity' => 'Product Quantity',
    'order_products_detail' => 'Order Products Detail',
    'order_detail' => 'Order Detail',
    'change_order_status' => 'Change Order Status',
    'total_amount_with_delivery' => 'Total Amount With Delivery',
    'delivery_fees' => 'Delivery Fees',
    'order_type' => 'Order Type',
    'postal_code' => 'Postal Code',
    'table' => 'Table',
  ),
);
