<?php

return array (
  'singular' => 'Slider',
  'plural' => 'Sliders',
  'fields' => 
  array (
    'id' => 'Id',
    'image' => 'Image',
    'sort' => 'Sort',
    'url' => 'Url',
    'gallery_id' => 'Gallery Id',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
