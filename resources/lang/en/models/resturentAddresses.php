<?php

return array (
  'singular' => "What's your restaurant's address ?",
  'plural' => 'Name & Address',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'email' => 'Info Email',
    'website_url' => 'Website Url',
    'logo' => 'Logo',
    'phone_number' => 'Phone Number',
    'id_user' => 'Id User',
    'is_pickup' => 'Is Pickup',
    'is_delivery' => 'Is Delivery',
    'is_dine' => 'Is Dine',
    'admin_email' => 'Admin Email (For order receiving )',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'city' => 'City',
    'address' => 'Address',
    'zip_code' => 'Zip Code',
  ),
);
