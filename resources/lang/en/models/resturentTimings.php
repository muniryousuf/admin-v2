<?php

return array (
  'singular' => 'Restaurant Timings',
  'plural' => 'Restaurant Timings',
  'fields' => 
  array (
    'id' => 'Id',
    'day' => 'Day',
    'start_time' => 'Start Time',
    'end_time' => 'End Time',
    'shop_close' => 'Shop Close',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
