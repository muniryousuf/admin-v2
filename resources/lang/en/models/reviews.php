<?php

return array (
  'singular' => 'Reviews',
  'plural' => 'Reviews',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'message' => 'Message',
    'rating' => 'Rating',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
