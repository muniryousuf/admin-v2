<?php

return array (
  'singular' => 'PaymentMethods',
  'plural' => 'PaymentMethods',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'private_key' => 'Private Key',
    'public_key' => 'Public Key',
    'another_key' => 'Another Key',
    'active' => 'Active',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
