<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/forgot-password', function () {
  return view('auth.forgot-password');
})->middleware('guest')->name('password.request');
Route::get('/reset-password/{token}', function ($token) {
  return view('auth.reset-password', ['token' => $token]);
})->middleware('guest')->name('password.reset');
Auth::routes();
Route::get('/admin/login', [App\Http\Controllers\HomeController::class, 'adminLogin'])->name('admin.login');
Route::post('/admin/login/request', [App\Http\Controllers\HomeController::class, 'adminLoginRequest'])->name('admin.login.request');
Route::post('/admin/logout', [App\Http\Controllers\HomeController::class, 'adminLogout'])->name('admin.logut');
Route::middleware(['admin.auth'])->group( function () {
  Route::group(['prefix' => 'admin'], function(){

    Route::get('/home', [App\Http\Controllers\DashboardController::class, 'index'])->name('admin');
    Route::post('/admin-edit-profile', [App\Http\Controllers\HomeController::class, 'adminEditProfile'])->name('admin.edit.profile');
    Route::post('/admin-edit-password', [App\Http\Controllers\HomeController::class, 'adminEditPassword'])->name('admin.edit.password');
    Route::resource('generalSettings', App\Http\Controllers\generalSettingsController::class);
    Route::resource('resturentAddresses', App\Http\Controllers\ResturentAddressController::class);
    Route::get('resturentAddresses-add/{id?}', [App\Http\Controllers\ResturentAddressController::class,'addResturentAddress'])->name('resturentAddresses.add-address');

    Route::post('save-address-of_resturent', [App\Http\Controllers\ResturentAddressController::class,'saveResturentAddress'])->name('resturentAddresses.save-address');

    Route::resource('deliveryChargesDetails', App\Http\Controllers\deliveryChargesDetailsController::class);
    Route::resource('orders', App\Http\Controllers\ordersController::class);
    Route::resource('sliders', App\Http\Controllers\sliderController::class);
    Route::resource('ourStories', App\Http\Controllers\ourStoryController::class);
    Route::resource('galleries', App\Http\Controllers\GalleryController::class);
    Route::resource('cms', App\Http\Controllers\CmsController::class);
    Route::resource('smsSettings', App\Http\Controllers\SmsSettingController::class);
    Route::resource('messageTemplatings', App\Http\Controllers\MessageTemplatingController::class);
    Route::resource('tableReservations', App\Http\Controllers\TableReservationController::class);
    Route::get('page-builder', [App\Http\Controllers\PageBuilderController::class,'index'])->name('page-builder');
    Route::resource('categories', App\Http\Controllers\categoryController::class);
    Route::post('save.php', [App\Http\Controllers\PageBuilderController::class,'save']);
    Route::post('upload.php', [App\Http\Controllers\PageBuilderController::class,'upload']);
    Route::get('scan.php', [App\Http\Controllers\PageBuilderController::class,'scan']);
    Route::resource('pages', App\Http\Controllers\PagesController::class);
    Route::resource('products', App\Http\Controllers\ProductsController::class);
    Route::resource('productMetas', App\Http\Controllers\product_metaController::class);
    Route::post('add-attribute', [App\Http\Controllers\product_metaController::class,'addAttribute'])->name('add-attribute');
    Route::post('fetch-attributes', [App\Http\Controllers\product_metaController::class,'fetchAttributes'])->name('fetch-attributes');

    Route::get('add-child-product-meta/{id_meta}', [App\Http\Controllers\product_metaController::class,'addChildProductMeta'])->name('add-child-product-meta');
    Route::post('delete-product-meta-attribute/{id?}', [App\Http\Controllers\product_metaController::class,'deleteProductMetaAttribute'])->name('delete-product-meta-attribute');
    Route::post('update-attribute', [App\Http\Controllers\product_metaController::class,'updateAttributeForAdmin'])->name('update-attribute');
    Route::resource('resturentTimings', App\Http\Controllers\ResturentTimingsController::class);
    Route::resource('restaurantTimingsSpecifics', App\Http\Controllers\RestaurantTimingsSpecificController::class);
    Route::resource('paymentMethods', App\Http\Controllers\PaymentMethodsController::class);
    Route::resource('reviews', App\Http\Controllers\ReviewsController::class);
    Route::resource('users', App\Http\Controllers\UsersController::class);
    Route::get('users/orders/{id}', [App\Http\Controllers\UsersController::class,'showOrders'])->name('users.view.orders');
    Route::get('users/carts/{id}', [App\Http\Controllers\UsersController::class,'showCarts'])->name('users.view.carts');
    Route::resource('tableCategories', App\Http\Controllers\TableCategoriesController::class);
    Route::resource('customerReservations', App\Http\Controllers\CustomerReservationsController::class);

    Route::resource('locations', App\Http\Controllers\LocationsController::class);


    Route::resource('locationsAttributes', App\Http\Controllers\LocationsAttributesController::class);
    Route::get('reset-cache', [App\Http\Controllers\DashboardController::class,'resetCache'])->name('reset-cache');
  });
});


Route::get('my-account', [App\Http\Controllers\HomeController::class, 'myAccount'])->name('user.my-account');
Route::get('change-password', [App\Http\Controllers\HomeController::class, 'changePassword'])->name('user.change.password');
Route::post('user-details-update', [App\Http\Controllers\HomeController::class, 'userDetailsUpdate'])->name('user-details-update');
Route::post('user-registration', [App\Http\Controllers\HomeController::class, 'userRegister'])->name('user-registration');
Route::post('user-password-update', [App\Http\Controllers\HomeController::class, 'userPasswordUpdate'])->name('user.password.update');

Route::get('order-history', [App\Http\Controllers\HomeController::class, 'orderHistory'])->name('user.order.history');
Route::get('addresses', [App\Http\Controllers\HomeController::class, 'userAddresses'])->name('user.addresses');

Route::get('/', [App\Http\Controllers\HomeController::class, 'home'])->name('home-page');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'home'])->name('home-page');
Route::get('page/{name}', [App\Http\Controllers\PageBuilderController::class,'getPage'])->name('pages');

Route::get('order-online', [App\Http\Controllers\HomeController::class,'OrderOnline'])->name('order-online');
Route::get('payment-confirm', [App\Http\Controllers\HomeController::class,'paymentConfirm'])->name('payment-confirm');

Route::get('thank-you', [App\Http\Controllers\HomeController::class,'thankYou'])->name('thank-you');
Route::get('contact-us', [App\Http\Controllers\HomeController::class,'contactUs'])->name('contact-us');
Route::get('checkout/{reference?}', [App\Http\Controllers\HomeController::class,'checkout'])->name('checkout');
Route::post('place-order', [App\Http\Controllers\HomeController::class,'placeOrder'])->name('web.place-order');
Route::get('reviews/{ratings?}', [App\Http\Controllers\HomeController::class,'reviews'])->name('reviews');

Route::get('fetch-attributes', [App\Http\Controllers\product_metaController::class,'fetchAttributes']);


Route::resource('notifications', App\Http\Controllers\NotificationsController::class);
Route::post('stream-check-out', [App\Http\Controllers\PaymentMethodsController::class,'cardStreamCallback']);
Route::post('get-cart-id', [App\Http\Controllers\HomeController::class,'getCartId'])->name('get-cart-id');


Route::resource('vouchers', App\Http\Controllers\VouchersController::class);
Route::get('download/pdf/{reference}', [App\Http\Controllers\HomeController::class,'downloadPdf'])->name('downloadPdf');
Route::get('save/pdf/{reference}', [App\Http\Controllers\HomeController::class,'savePdf'])->name('savePdf');
Route::get('view/pdf/{reference}', [App\Http\Controllers\HomeController::class,'viewPdf'])->name('viewPdf');


Route::get('reservations',[App\Http\Controllers\HomeController::class,'reservations'])->name('reservations');
Route::get('locations',[App\Http\Controllers\HomeController::class,'locations'])->name('locations');
Route::get('locations/{id}',[App\Http\Controllers\HomeController::class,'locations'])->name('locations.attributes');











Route::resource('holdCarts', App\Http\Controllers\HoldCartsController::class);


Route::resource('caosGenralSettings', App\Http\Controllers\CaosGenralSettingsController::class);
