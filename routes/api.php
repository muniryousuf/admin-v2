<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//############################ Old Routes ##########################################\\
Route::middleware(['cors'])->group(function () {
Route::post('create-user', [App\Http\Controllers\API\UserController::class,'createNewUser']);
Route::put('update-user/{id}', [App\Http\Controllers\API\UserController::class,'updateUser']);
Route::put('delete-user/{id}', [App\Http\Controllers\API\UserController::class,'deleteUser']);
Route::get('get-all-users', [App\Http\Controllers\API\UserController::class,'getAll']);

Route::get('get-all-roles', [App\Http\Controllers\API\RolesController::class,'index']);

Route::post('reservation', [App\Http\Controllers\API\TableReservationController::class,'customerReservation']);
Route::get('restuarant_time', [App\Http\Controllers\API\TableReservationController::class,'getDayTime']);
Route::get('no_of_persons', [App\Http\Controllers\API\TableReservationController::class,'getNoOfPerson']);

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [App\Http\Controllers\Auth\ApiLoginController::class,'login']);
    Route::post('driver-login', [App\Http\Controllers\Auth\ApiLoginController::class,'driverLogin']);
});

/** Slider Routes */
Route::resource('slider', App\Http\Controllers\API\SliderController::class);

/** Gallery Routes */
Route::resource('gallery', App\Http\Controllers\API\GalleryController::class);

/** Delivery Check Postal Code Routes */
Route::post('check-postal', [App\Http\Controllers\API\DeliveryChargesController::class,'checkPostalCode'])->name('check-postal-code');

/** Categories Routes */
Route::apiResource('categories', CategoryController::class);

/** Products Routes */
Route::apiResource('products', ProductController::class);
Route::apiResource('v2/products', ProductController::class);

/** Choices Groups Routes */
Route::resource('choices-group', App\Http\Controllers\API\ChoicesGroupController::class);

/** Choices Routes */
Route::resource('choices', App\Http\Controllers\API\ChoicesController::class);

Route::post('add-group-to-product', [App\Http\Controllers\API\ProductController::class,'addGroupToProduct']);

Route::post('remove-group-to-product', [App\Http\Controllers\API\ProductController::class,'removeGroupFromProduct']);

/** Stories Routes */
Route::resource('our-story', App\Http\Controllers\API\OurStoryController::class);

/** Restaurant Information */
Route::get('restaurant_info', [App\Http\Controllers\API\RestaurantController::class,'getRestaurantInfo']);

Route::post('update-restaurant-info', [App\Http\Controllers\API\RestaurantController::class,'updateRestaurantInfo']);

/** Products Routes */
Route::resource('deals', App\Http\Controllers\API\DealController::class);

/** Place Order */
Route::post('placeOrder', [App\Http\Controllers\API\OrderController::class,'placeOrder']);

/** Stripe Order */
Route::post('stripe-order', [App\Http\Controllers\API\OrderController::class,'stripePayment']);

/** Stripe Order */
Route::post('card-stream-order', [App\Http\Controllers\API\OrderController::class,'cardStreamPayment']);

Route::post('get-total-sales', [App\Http\Controllers\API\OrderController::class,'getTotalSales']);

/** CMS Pages */
Route::get('cms/{slug}', [App\Http\Controllers\API\CmsController::class,'getCMSPage']);

/** Get All Orders */
Route::get('getAllOrders', [App\Http\Controllers\API\OrderController::class,'getAllOrders']);

/** Update Order Status */
Route::put('update-order/{id}', [App\Http\Controllers\API\OrderController::class,'updateOrderStatus']);

Route::put('update-pos-order/{id}', [App\Http\Controllers\API\OrderController::class,'updateOrder']);

/** Update General Settings */
Route::post('update-general-setting', [App\Http\Controllers\API\GeneralSettingController::class,'updateGeneralSettings']);

Route::get('downloadAllergy', [App\Http\Controllers\API\ProductController::class,'downloadAllergy']);

Route::get('downloadMenu', [App\Http\Controllers\API\ProductController::class,'downloadMenu']);

/** Get Offers*/
Route::apiResource('offer', OfferController::class);

Route::get('get-time-slots/{duration?}',[App\Http\Controllers\API\DeliveryChargesController::class,'getTimeSlots'])->name('get-time-slots');

Route::post('register', 'UserController@register');

Route::apiResource('tableReservation', TableReservationController::class);

Route::apiResource('tableCategories', TableCategoryController::class);

/** Printer Routes */
Route::apiResource('printers', PrintersController::class);

/** Get All Orders */
Route::get('getOrderDetails/{id}', [App\Http\Controllers\API\OrderController::class,'getOrderDetails']);

Route::get('getKitchenOrders', [App\Http\Controllers\API\OrderController::class,'getKitchenOrders']);

Route::apiResource('feedback', FeedbackController::class);

/** Reports APIs */

Route::post('get-orders',[App\Http\Controllers\API\ReportingController::class,'getOrders']);
Route::post('get-sale-by-category',[App\Http\Controllers\API\ReportingController::class,'getSalesByCategory']);
Route::post('get-sale-by-products',[App\Http\Controllers\API\ReportingController::class,'getSalesByProducts']);

Route::post('get-pdq-sales',[App\Http\Controllers\API\ReportingController::class,'getSalesByCardPDQ']);

Route::post('get-sale-by-type',[App\Http\Controllers\API\ReportingController::class,'getSalesByOrderType']);

Route::post('get-categorySales-by-type',[App\Http\Controllers\API\ReportingController::class,'getCategorySalesByOrderType']);

Route::post('update-product-status', [App\Http\Controllers\API\OrderController::class,'updateProductStatus']);

Route::post('add-table', [App\Http\Controllers\API\TableReservationController::class,'store']);

/** Driver Routes */

Route::get('drivers', [App\Http\Controllers\API\DriverController::class,'index']);

Route::post('assign-order-to-driver', [App\Http\Controllers\API\DriverController::class,'assignOrder']);

Route::post('unassign-order-to-driver', [App\Http\Controllers\API\DriverController::class,'unassignOrder']);

Route::post('collect-amount-from-driver', [App\Http\Controllers\API\DriverController::class,'collectAmount']);

Route::post('getDriverOrders', [App\Http\Controllers\API\DriverController::class,'getOrders']);

Route::post('getDriverOrderPayments', [App\Http\Controllers\API\DriverController::class,'getDriverOrders']);


Route::get('getXReport', [App\Http\Controllers\API\ReportingController::class,'getXReports']);

Route::get('updateXyReport', [App\Http\Controllers\API\ReportingController::class,'updateXYReport']);

Route::get('getTableTypes', [App\Http\Controllers\API\TableReservationController::class,'getTableTypes']);

Route::apiResource('restaurantTiming', RestaurantTimingController::class);
Route::get('restaurantTimingSpecific', [App\Http\Controllers\API\RestaurantTimingController::class,'getAllSpecificDateTime']);

Route::post('restaurantTimingSpecific', [App\Http\Controllers\API\RestaurantTimingController::class,'addSpecificDateTime']);




Route::post('project', [App\Http\Controllers\API\ProjectsController::class,'project']);

Route::post('user-discount', [App\Http\Controllers\UserController::class,'getFirstOrderDiscount']);

Route::get('getKioskPrint', [App\Http\Controllers\API\OrderController::class,'getKioskPrint']);

Route::post('updatePaymentStatus', [App\Http\Controllers\API\OrderController::class,'updatePaymentStatus']);

Route::get('getOrderDesktopPos', [App\Http\Controllers\API\OrderController::class,'getOrderDesktopPos']);

Route::post('search-pos-postal-code', [App\Http\Controllers\API\RestaurantController::class,'getPostalCode']);

//############################ Old Routes ##########################################\\






//products
Route::get('products-search/{id?}', [App\Http\Controllers\API\NewProductController::class,'getProductDetails'])->name('get-product-detail');


Route::get('products-search-v2/{id?}', [App\Http\Controllers\API\NewProductController::class,'getProductDetailsV2'])->name('get-product-detailv2');

Route::get('get-all-categories', [App\Http\Controllers\API\categoryAPIController::class,'getAllCategories'])->name('get-all-categories');
Route::get('get-all-categories-for-web', [App\Http\Controllers\API\categoryAPIController::class,'getAllCategoriesForWeb'])->name('get-all-categories-for-web');

Route::get('get-product-meta-herarcy', [App\Http\Controllers\API\NewProductController::class,'getProductMetaHerarcy'])->name('get-product-meta-herarcy');

Route::get('get-child-of-attribute/{id_attribute?}', [App\Http\Controllers\API\NewProductController::class,'getChildOfAttribute'])->name('get-child-of-attribute');


Route::get('get-cart/{id?}/{user_id?}', [App\Http\Controllers\API\CartAPIController::class,'getCart'])->name('get-cart');
Route::post('add-to-cart', [App\Http\Controllers\API\CartAPIController::class,'addToCart'])->name('add-to-cart');
Route::post('change-quantity', [App\Http\Controllers\API\CartAPIController::class,'changeQuantity'])->name('change-quantity');
Route::post('place-order', [App\Http\Controllers\API\CartAPIController::class,'placeOrder'])->name('place-order');
Route::post('change-serving-method', [App\Http\Controllers\API\CartAPIController::class,'changeServingMethod'])->name('change-serving-method');
Route::post('add-user-address', [App\Http\Controllers\API\CartAPIController::class,'addUserAddress'])->name('add-user-address');
Route::get('get-today-timing', [App\Http\Controllers\API\RestaurantTimingController::class,'getTodayTiming'])->name('get-today-timing');
Route::get('create-cart', [App\Http\Controllers\API\CartAPIController::class,'createCart'])->name('create-cart');
Route::get('get-customer-addresses', [App\Http\Controllers\API\CartAPIController::class,'getCustomerAddresses'])->name('get-customer-addresses');
Route::get('set-address-id', [App\Http\Controllers\API\CartAPIController::class,'setAddressId'])->name('set-address-id');

/** Stripe Payment Process */
Route::post('stripe-process', [App\Http\Controllers\PaymentMethodsController::class,'paymentAction'])->name('payment_process');

Route::post('stream-process', [App\Http\Controllers\PaymentMethodsController::class,'streamPaymentAction'])->name('stream_payment_process');


Route::apiResource('reviews', ReviewsAPIController::class);

Route::post('update-user-address', [App\Http\Controllers\API\CartAPIController::class,'updateUserAddress'])->name('update-user-address');

Route::get('meta-with-attribute/{id?}', [App\Http\Controllers\API\NewProductController::class,'metaWithAttribute'])->name('meta-with-attribute');


Route::post('apply-coupon-code', [App\Http\Controllers\API\CartAPIController::class,'applyCouponCode'])->name('apply-coupon-code');

Route::get('fetch-attributes', [App\Http\Controllers\product_metaController::class,'fetchAttributesV2'])->name('fetch-attributes');

Route::post('add-attribute', [App\Http\Controllers\product_metaController::class,'addAttribute'])->name('add-attribute');

Route::put('update-attribute/{id}', [App\Http\Controllers\product_metaController::class,'updateAttributeNew'])->name('update-attribute');

Route::put('update-attr/{id}', [App\Http\Controllers\product_metaController::class,'updateAttribute'])->name('update-attr');

 Route::delete('delete-product-meta-attribute/{id}', [App\Http\Controllers\product_metaController::class,'deleteProductMetaAttribute']);

Route::post('add-group', [App\Http\Controllers\product_metaController::class,'createGroup'])->name('store-group');


Route::put('update-group/{id}', [App\Http\Controllers\product_metaController::class,'updateGroup'])->name('update-group');

Route::post('add-product', [App\Http\Controllers\API\NewProductController::class,'addProduct'])->name('add-product');

Route::put('update-product/{id}', [App\Http\Controllers\API\NewProductController::class,'updateProduct'])->name('update-product');


Route::delete('delete-group/{id?}', [App\Http\Controllers\product_metaController::class,'deleteGroup'])->name('delete-group');

Route::delete('delete-product/{id?}', [App\Http\Controllers\API\NewProductController::class,'deleteProduct'])->name('delete-product');
Route::get('get-all-groups', [App\Http\Controllers\API\NewProductController::class,'getAllGroups'])->name('get-all-groups');

Route::post('send-email-to-admin', [App\Http\Controllers\API\CartAPIController::class,'sendEmailtoAdmin'])->name('send-email-to-admin');

Route::post('send-email-to-customer', [App\Http\Controllers\API\CartAPIController::class,'sendEmailtoCustomer'])->name('send-email-to-customer');
Route::post('send-message-to-customer', [App\Http\Controllers\API\CartAPIController::class,'sendMessagetoCustomer'])->name('send-message-to-customer');

Route::resource('table_categories', App\Http\Controllers\API\TableCategoriesAPIController::class);


Route::resource('locations', App\Http\Controllers\API\LocationsAPIController::class);


Route::resource('locations_attributes', App\Http\Controllers\API\LocationsAttributesAPIController::class);


Route::apiResource('hold-carts', HoldCartsAPIController::class);
Route::apiResource('generalSettings', GeneralSettingController::class);


Route::apiResource('caos_genral_settings', CaosGenralSettingsAPIController::class);
Route::post('evoCallback', [App\Http\Controllers\EvoPaymentController::class,'evoCallback'])->name('evoCallback');
});


Route::post('orderPostedInPos', [App\Http\Controllers\API\OrderController::class,'orderPostedInPos'])->name('orderPostedInPos');
Route::post('orderUpdateFromPos', [App\Http\Controllers\API\OrderController::class,'orderUpdateFromPos'])->name('orderUpdateFromPos');


################## reset password ##################
Route::post('password/forgot-password', [App\Http\Controllers\Auth\ApiLoginController::class,'forgotPassword']);

//reset password
Route::post('password/reset/{token}', [App\Http\Controllers\Auth\ApiLoginController::class,'passwordReset'])->name('reset-token-link-for-password-reset');

################ user orders ##############

Route::post('user-orders/{id}', [App\Http\Controllers\HomeController::class,'getOrdersOfUser']);

############### user address ##############

Route::post('user-addresses/{id}', [App\Http\Controllers\HomeController::class,'getAddressOfUser']);


################## offer products ##########

Route::get('get-offer-products', [App\Http\Controllers\API\ProductController::class,'getOfferProducts']);



################## related products ##########

Route::get('get-related-products/{id}', [App\Http\Controllers\API\ProductController::class,'getRelatedProducts']);

################## related products ##########
Route::post('productmetas-search',[App\Http\Controllers\product_metaController::class,'searchForGroups'])->name('productmetas.search');
Route::post('recreate_groups_attributes',[App\Http\Controllers\product_metaController::class,'recreateGroupsAttributes'])->name('recreate_groups_attributes');


################## Payment methods ##########
Route::get('app-payment-methods', [App\Http\Controllers\PaymentMethodsController::class,'GetPaymentMethodsForApp']);

