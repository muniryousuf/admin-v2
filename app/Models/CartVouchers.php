<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartVouchers extends Model
{
    use HasFactory;

    public $timestamps = false;


    public function voucher_detail(){
        return $this->belongsTo(Vouchers::class,'id_voucher','id');
    }
}
