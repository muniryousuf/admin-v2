<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Vouchers
 * @package App\Models
 * @version April 15, 2022, 8:21 pm UTC
 *
 * @property string $name
 * @property string $type
 * @property string $expiry_date
 * @property integer $status
 * @property integer $coupon_limit
 * @property integer $user_limit
 * @property integer $auto_coupon
 * @property integer $for_first_order
 * @property integer $for_all_orders
 * @property integer $with_delivery
 */
class Vouchers extends Model
{
    // use SoftDeletes;

    use HasFactory;

    public $table = 'vouchers';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'type',
        'expiry_date',
        'status',
        'coupon_limit',
        'user_limit',
        'auto_coupon',
        'for_first_order',
        'for_all_orders',
        'with_delivery',
        'discount'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'name' => 'string',
    //     'type' => 'string',
    //     'status' => 'integer',
    //     'coupon_limit' => 'integer',
    //     'user_limit' => 'integer',
    //     'auto_coupon' => 'integer',
    //     'for_first_order' => 'integer',
    //     'for_all_orders' => 'integer',
    //     'with_delivery' => 'integer'
    // ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'type' => 'required',
        // 'coupon_limit' => 'required',
        'user_limit' => 'required',
        'discount' => 'required'
    ];

    
}
