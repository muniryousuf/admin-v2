<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Locations
 * @package App\Models
 * @version May 13, 2022, 11:54 pm UTC
 *
 * @property string $name
 * @property string $details
 */
class Locations extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'locations';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'details'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'details' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    
}
