<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Cms
 * @package App\Models
 * @version February 7, 2022, 9:29 pm UTC
 *
 * @property string $title
 * @property string $content
 * @property string $slug
 * @property string $meta_title
 * @property string $meta_desc
 * @property string $index_follow
 */
class Cms extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'cms';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'content',
        'slug',
        'meta_title',
        'meta_desc',
        'index_follow',
        'image_1',
        'image_2',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'slug' => 'string',
        'meta_title' => 'string',
        'meta_desc' => 'string',
        'index_follow' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'content' => 'required',
        'slug' => 'required',
        'meta_title' => 'required',
        'meta_desc' => 'required',
        'index_follow' => 'required'
    ];

    
}
