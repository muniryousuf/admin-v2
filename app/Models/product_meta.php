<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class product_meta extends Model
{
    use SoftDeletes;
    use HasFactory;
    
    const TABLE_TYPES = ['radio','medium_circle','multiselect','yes/no','remove'];

    public $table = 'product_metas';
    

    protected $dates = ['deleted_at'];

    // protected $with = ['attributes'];



    public $fillable = [
        'name',
        'label',
        'id_parent',
        'id_attribute',
        'image',
        'price',
        'is_required',
        'table_type',
        'free_limit',
        'multiple_quantity_allowed',
        'limit',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        // 'image'=> 'required',
        'table_type'=> 'required',
        'label'=> 'required',
    ];


    public function childs(){
        return $this->hasMany(product_meta::class, 'id_parent', 'id')->with('childs');
    }

    public function attribute(){
        return $this->hasOne(ProductMetaAttributes::class, 'product_meta_id', 'id');
    }
    public function attributes(){
        return $this->hasMany(ProductMetaAttributes::class, 'product_meta_id', 'id');
    }


    
}
