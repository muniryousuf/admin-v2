<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class HoldCarts
 * @package App\Models
 * @version May 21, 2022, 8:30 am UTC
 *
 * @property string $name
 * @property string $hold_data
 */
class HoldCarts extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'hold_carts';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'hold_data'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'hold_data' => 'array'
    ];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'hold_data' => 'required'
    ];

    
}
