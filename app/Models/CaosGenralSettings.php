<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class CaosGenralSettings
 * @package App\Models
 * @version July 25, 2022, 11:50 am UTC
 *
 * @property string $main_printer
 * @property string $kitchen_printer
 * @property integer $kicthen_copies
 * @property string $shop_name_and_address
 * @property string $reciept_end_text
 * @property string $menu_url
 * @property boolean $enable_multi_payment
 * @property string $extra_info
 */
class CaosGenralSettings extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'caos_genral_settings';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'main_printer',
        'kitchen_printer',
        'kicthen_copies',
        'shop_name_and_address',
        'reciept_end_text',
        'menu_url',
        'enable_multi_payment',
        'extra_info'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'main_printer' => 'string',
        'kitchen_printer' => 'string',
        'kicthen_copies' => 'integer',
        'shop_name_and_address' => 'string',
        'reciept_end_text' => 'string',
        'menu_url' => 'string',
        'enable_multi_payment' => 'boolean',
        'extra_info' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'main_printer' => 'required',
        'kitchen_printer' => 'required',
        'kicthen_copies' => 'required',
        'shop_name_and_address' => 'required',
        'reciept_end_text' => 'required',
        'menu_url' => 'required'
    ];

    
}
