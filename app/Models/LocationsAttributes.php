<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class LocationsAttributes
 * @package App\Models
 * @version May 14, 2022, 12:00 am UTC
 *
 * @property string $name
 * @property string $details
 * @property string $map
 * @property integer $id_location
 */
class LocationsAttributes extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'locations_attributes';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'details',
        'map',
        'id_location'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'details' => 'string',
        'map' => 'string',
        'id_location' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'details' => 'required',
        'map' => 'required',
        'id_location' => 'required'
    ];

    
}
