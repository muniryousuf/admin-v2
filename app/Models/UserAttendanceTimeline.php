<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class UserAttendanceTimeline extends Model
{
    protected $table = 'user_attendance';
    protected $fillable = ['user_attendance_id','user_id', 'clock_in_time', 'clock_out_time'];
}
