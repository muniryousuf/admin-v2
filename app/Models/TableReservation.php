<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class TableReservation
 * @package App\Models
 * @version February 7, 2022, 9:53 pm UTC
 *
 * @property string $name
 * @property integer $is_reserve
 * @property string $reservation_end_time
 * @property string $reservation_start_time
 * @property integer $id_category
 * @property integer $table_type
 * @property integer $number_of_person
 * @property integer $number_of_person_sitting
 */
class TableReservation extends Model
{
    use SoftDeletes;

    use HasFactory;

    const TABLE_TYPES = [
        'small_circle',
        'medium_circle',
        'large_circle',
        'small_square',
        'medium_square',
        'large_square'
    ];


    public $table = 'table_reservation';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'is_reserve',
        'reservation_end_time',
        'reservation_start_time',
        'id_category',
        'table_type',
        'number_of_person',
        'number_of_person_sitting'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'name' => 'string',
    //     'is_reserve' => 'integer',
    //     'reservation_end_time' => 'string',
    //     'reservation_start_time' => 'string',
    //     'id_category' => 'integer',
    //     'table_type' => 'integer',
    //     'number_of_person' => 'integer',
    //     'number_of_person_sitting' => 'integer'
    // ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'reservation_end_time' => 'required',
        'reservation_start_time' => 'required',
        'table_type' => 'required',
        'number_of_person' => 'required',
        'number_of_person_sitting' => 'required'
    ];

    
}
