<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class CustomerReservations
 * @package App\Models
 * @version May 12, 2022, 10:56 pm UTC
 *
 * @property string $firstname
 * @property string $lastname
 * @property string $phone
 * @property string $email
 * @property string $booking_date
 * @property integer $persons
 * @property string $status
 */
class CustomerReservations extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'customer_reservation';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'firstname',
        'lastname',
        'phone',
        'email',
        'booking_date',
        'persons',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'firstname' => 'string',
        'lastname' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'booking_date' => 'string',
        'persons' => 'integer',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'firstname' => 'required',
        'lastname' => 'required',
        'phone' => 'required',
        'email' => 'required',
        'booking_date' => 'required',
        'persons' => 'required',
        'status' => 'required'
    ];

    
}
