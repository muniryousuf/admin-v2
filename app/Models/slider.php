<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class slider
 * @package App\Models
 * @version February 7, 2022, 8:44 pm UTC
 *
 * @property string $image
 * @property integer $sort
 * @property string $url
 * @property integer $gallery_id
 */
class slider extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'slider';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'image',
        'sort',
        'url',
        'gallery_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'image' => 'string',
        'sort' => 'integer',
        'url' => 'string',
        'gallery_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'image' => 'required',
        'sort' => 'required',
        'url' => 'required'
    ];

    
}
