<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Gallery
 * @package App\Models
 * @version February 7, 2022, 9:16 pm UTC
 *
 * @property string $image
 * @property integer $gallery_id
 */
class Gallery extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'gallery';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'image',
        'gallery_id',
        'title',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'image' => 'string',
        'gallery_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    
}
