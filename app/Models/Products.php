<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;



class Products extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'products';

    protected $appends = ['image_url'];


    protected $with = ['product_metas','product_metas_mobile','id_of_metas'];

    public $fillable = [
        'name',
        'description',
        'food_allergy',
        'price',
        'id_category',
        'image',
        'status',
        'id_meta',
        'offer',
        'related_product_ids',
    ];

    public static $rules = [
        'name' => 'required',
        'description' => 'required',
        // 'food_allergy' => 'required',
        'price' => 'required',
        'id_category' => 'required',
        // 'image' => 'required'
    ];

    // public function product_metas(){
    //     return $this->hasOne(product_meta::class,'id','id_meta');
    // }
    public function id_of_metas(){
        return $this->hasMany(ProductMetasBridge::class,'id_product','id');
    }
    public function product_metas(){
         return $this->hasManyThrough(
            product_meta::class,
            ProductMetasBridge::class,
            'id_product',
            'id',
            'id',
            'id_meta'
        ); //->orderBy('product_metas_bridge.sort','desc');
    }

    public function category(){
        return $this->hasOne(category::class,'id','id_category');
    }

    public function getImageUrlAttribute(){

        return "https://placeimg.com/640/480/any";

    }

     public function product_metas_mobile(){

        return $this->hasManyThrough(
            product_meta::class,
            ProductMetasBridge::class,
            'id_product',
            'id',
            'id',
            'id_meta'
        ); //->orderBy('product_metas_bridge.sort','desc');
    }
    protected $casts = [
        'related_product_ids' => 'array',
    ];

}
