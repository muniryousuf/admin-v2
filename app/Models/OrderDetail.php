<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class order_detail
 * @package App\Models
 * @version February 7, 2022, 5:27 pm UTC
 *
 * @property integer $order_id
 * @property string $Reference
 * @property integer $total_amount
 * @property string $payment_method
 * @property string $delivery_address
 * @property integer $status
 */
class OrderDetail extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'order_detail';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'order_id',
        'product_id',
        'product_name',
        'price',
        'quantity',
        'extras',
        'special_instructions',
        'product_status',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    
}
