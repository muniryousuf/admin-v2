<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Notifications
 * @package App\Models
 * @version April 14, 2022, 10:09 pm UTC
 *
 * @property string $message
 * @property string $color
 * @property integer $status
 */
class Notifications extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'notifications';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'message',
        'title',
        'description',
        'image',
        'color',
        'status',
        'amount'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'message' => 'string',
    //     'color' => 'string',
    //     'status' => 'integer'
    // ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'description' => 'required',
        'color' => 'required',
        'status' => 'required',
        'amount' => 'required'
    ];

    
}
