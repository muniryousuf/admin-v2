<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class SmsSetting
 * @package App\Models
 * @version February 7, 2022, 9:32 pm UTC
 *
 * @property string $key
 * @property string $secret
 * @property string $gateway_key
 */
class SmsSetting extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'sms_setting';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'key',
        'secret',
        'gateway_key'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'key' => 'required',
        'secret' => 'required',
        'gateway_key' => 'required'
    ];

    protected $primaryKey = 'customer_id';

    
}
