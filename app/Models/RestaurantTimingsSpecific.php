<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class RestaurantTimingsSpecific
 * @package App\Models
 * @version March 19, 2022, 4:40 pm UTC
 *
 * @property time $start_time
 * @property time $end_time
 * @property integer $shop_closed
 * @property string|\Carbon\Carbon $specific_date
 */
class RestaurantTimingsSpecific extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'restaurant_timing_specific';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'start_time',
        'end_time',
        'shop_closed',
        'specific_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'shop_closed' => 'integer',
        'specific_date' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'start_time' => 'required',
        'end_time' => 'required',
        'specific_date' => 'required'
    ];

    
}
