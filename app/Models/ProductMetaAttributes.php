<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductMetaAttributes extends Model
{
    use HasFactory;

    protected $fillable = ['id','product_meta_id','key','value','image','price','child_product_meta_id','preselected'];
    // protected $with = ['product_meta'];


    public function product_meta(){
        return $this->hasOne(product_meta::class, 'id', 'child_product_meta_id');
    }
    public function child_meta(){
        return $this->hasOne(product_meta::class, 'id', 'child_product_meta_id')->without('attributes');
    }



}
