<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class generalSettings extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'general_settings';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id',
        'site_name',
        'site_title',
        'tag_line',
        'copyright_text',
        'header_logo',
        'footer_logo',
        'shop_status',
        'facebook',
        'instagram',
        'youtube',
        'pinterest',
        'twitter',
        'stripe_publishable_key',
        'stripe_secret_key',
        'printer_ip_1',
        'min_collection_time',
        'min_delivery_time',
        'address',
        'total_allowed_person',
        'map',
        'ticktok',
        'snapchat',
        'linkedinn',
        'fav_icon',
        'currency',
        'background_image',
        'auto_rotation',
        'minimum_amount_to_order',
        'rider_will_deliver',
        'rider_dilver_message'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

}
