<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class orders
 * @package App\Models
 * @version February 7, 2022, 5:27 pm UTC
 *
 * @property integer $order_id
 * @property string $Reference
 * @property integer $total_amount
 * @property string $payment_method
 * @property string $delivery_address
 * @property integer $status
 */
class orders extends Model
{
    use SoftDeletes;

    use HasFactory;

    const ORDER_STATUS = [
                    'pending',
                    'accepted',
                    'declined',
                    'preparing',
                    'prepared',
                    'driver_assigned',
                    'picked_up',
                    'delivered',
                    'completed',
                    'refund'
                ];

    public $table = 'orders';
    

    protected $dates = ['deleted_at'];

    const SEARCH_ARRAY = [
        'id' => 'Order Id',
        'reference' => 'Reference',
        'total_amount_with_fee' => 'Total Amount',
        'payment' => 'Payment Method',
        'status' => 'Status',

    ];


    public $fillable = [
        'order_id',
        'Reference',
        'total_amount',
        'payment_method',
        'delivery_address',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public function order_details(){
        return $this->hasMany(OrderDetail::class,'order_id','id');
    }
    public function user_detail(){
        return $this->hasOne(User::class,'id','user_id');
    }
    public function table_detail(){
        
        return $this->hasOne(TableReservation::class,'id','table_id');
    }

    
}
