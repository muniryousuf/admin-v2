<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = "projects";

    protected $hidden = [
        'created_at',
        'updated_at'
    ];


    protected $fillable = [
        'company_name', 'installation_key','base_url'
    ];

}
