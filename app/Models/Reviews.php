<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Reviews
 * @package App\Models
 * @version March 31, 2022, 10:55 pm UTC
 *
 * @property string $name
 * @property string $message
 * @property string $rating
 */
class Reviews extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'reviews';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'message',
        'rating'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'message' => 'string',
        'rating' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'message' => 'required',
        'rating' => 'required'
    ];

    
}
