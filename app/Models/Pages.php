<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Pages
 * @package App\Models
 * @version February 13, 2022, 12:11 pm UTC
 *
 * @property string $name
 * @property string $html
 */
class Pages extends Model
{
    use SoftDeletes;

    use HasFactory;

    const PAGE_TYPES = ['static','dynamic','informative'];
    const BACKGROUND_TYPE = ['no_background','image','color'];
    const TYPES = ['header','footer-column-1','footer-column-2','footer-column-3','footer-column-4'];
    const TYPE_HEADER = ['header'];
    const TYPE_FOOTER = ['footer-column-1','footer-column-2','footer-column-3','footer-column-4'];
    const PAGE_TEMPLATES = ['about.html','contact.html','privacy.html','testimonial.html'];


    public $table = 'pages';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'type',
        'html',
        'page_type',
        'background_type',
        'background_type_value',
    ];

   

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'type' => 'required',
        'page_type' => 'required',
        // 'html' => 'required'
    ];

    
}
