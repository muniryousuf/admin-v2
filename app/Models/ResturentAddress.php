<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ResturentAddress
 * @package App\Models
 * @version February 6, 2022, 4:31 pm UTC
 *
 * @property string $name
 * @property string $email
 * @property string $website_url
 * @property string $logo
 * @property string $phone_number
 * @property integer $id_user
 * @property integer $is_pickup
 * @property integer $is_delivery
 * @property integer $is_dine
 * @property string $admin_email
 */
class ResturentAddress extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'restaurant';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'email',
        'website_url',
        'logo',
        'phone_number',
        'id_user',
        'is_pickup',
        'is_delivery',
        'is_dine',
        'admin_email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'name' => 'string',
    //     'email' => 'string',
    //     'website_url' => 'string',
    //     'logo' => 'string',
    //     'phone_number' => 'string',
    //     'id_user' => 'integer',
    //     'is_pickup' => 'integer',
    //     'is_delivery' => 'integer',
    //     'is_dine' => 'integer',
    //     'admin_email' => 'string'
    // ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        'website_url' => 'required',
        'phone_number' => 'required',
        // 'is_pickup' => 'required'
    ];

    
}
