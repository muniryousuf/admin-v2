<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Cart
 * @package App\Models
 * @version March 11, 2022, 1:08 pm UTC
 *
 */
class Cart extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'cart';
    

    protected $dates = ['deleted_at'];


    public function cart_products(){
        return $this->hasMany(CartProducts::class,'id_cart','id_cart');
    }
    public function cart_vouchers(){
        return $this->hasMany(CartVouchers::class,'id_cart','id_cart');
    }
    
}
