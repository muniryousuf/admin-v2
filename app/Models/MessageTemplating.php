<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class MessageTemplating
 * @package App\Models
 * @version February 7, 2022, 9:43 pm UTC
 *
 * @property string $template_slug
 * @property string $description
 * @property integer $status
 */
class MessageTemplating extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'message_templating';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'template_slug',
        'description',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'template_slug' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'template_slug' => 'required',
        'description' => 'required'
    ];

    
}
