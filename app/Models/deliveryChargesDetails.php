<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class deliveryChargesDetails
 * @package App\Models
 * @version February 6, 2022, 4:58 pm UTC
 *
 * @property bigint $delivery_id
 * @property bigint $delivery_id
 * @property number $miles
 * @property string $postal_code
 * @property number $amount
 * @property number $fix_delivery_charges
 */
class deliveryChargesDetails extends Model
{
    // use SoftDeletes;

    use HasFactory;


    const DELIVERY_TYPES = ['fix_delivery','postal_code','miles'];

    public $table = 'delivery_charges_details';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'delivery_id',
        'delivery_id',
        'miles',
        'postal_code',
        'amount',
        'fix_delivery_charges'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
  

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'postal_code' => 'required',
        'amount' => 'required'
    ];

    public function delivery_type(){
        return $this->hasOne(DeliveryCharges::class,'id','delivery_id');
    }

    
}
