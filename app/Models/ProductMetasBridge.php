<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class ProductMetasBridge extends Model
{
    use HasFactory;
    protected $dates = ['deleted_at'];
    public $table = 'product_metas_bridge';

}
