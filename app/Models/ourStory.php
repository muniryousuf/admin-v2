<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ourStory
 * @package App\Models
 * @version February 7, 2022, 9:02 pm UTC
 *
 * @property string $main_title
 * @property string $all_title
 * @property string $description
 * @property string $image
 */
class ourStory extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'our_story';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'main_title',
        'all_title',
        'description',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'main_title' => 'string',
        'all_title' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    
}
