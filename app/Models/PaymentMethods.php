<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class PaymentMethods
 * @package App\Models
 * @version March 19, 2022, 4:51 pm UTC
 *
 * @property string $name
 * @property string $private_key
 * @property string $public_key
 * @property string $another_key
 * @property intiger $active
 */
class PaymentMethods extends Model
{
    use SoftDeletes;

    use HasFactory;


    const PAYMENT_METHODS = ['stripe','card_stream','cod'];

    public $table = 'payment_methods';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'private_key',
        'public_key',
        'another_key',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'private_key' => 'string',
        'public_key' => 'string',
        'another_key' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'private_key' => 'required',
        'public_key' => 'required',
        'active' => 'required'
    ];

    
}
