<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
    protected $table = 'payment_type';
    protected $fillable = ['order_id', 'payment_type', 'amount'];

}
