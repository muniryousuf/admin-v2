<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Cart
 * @package App\Models
 * @version March 11, 2022, 1:08 pm UTC
 *
 */
class CartProducts extends Model
{
    

    use HasFactory;

    public $table = 'cart_products';

    protected $with = ['product'];



    public function product(){
        return $this->hasOne(Products::class,'id','id_product')->without('product_metas','product_metas_mobile','id_of_metas');
    }
    
}
