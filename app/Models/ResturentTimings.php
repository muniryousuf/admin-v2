<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ResturentTimings
 * @package App\Models
 * @version March 19, 2022, 4:10 pm UTC
 *
 * @property time $day
 * @property time $start_time
 * @property time $end_time
 * @property integer $shop_close
 */
class ResturentTimings extends Model
{
    use SoftDeletes;

    use HasFactory;

    const DAYS = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']; 

    public $table = 'restaurant_timing';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'day',
        'start_time',
        'end_time',
        'shop_close'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'shop_close' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'day' => 'required',
        'start_time' => 'required',
        'end_time' => 'required'
    ];

    
}
