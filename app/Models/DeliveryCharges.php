<?php

namespace App\Models;

use Eloquent as Model;



class DeliveryCharges extends Model
{
    

    protected $table = 'delivery_charges';

    protected $dates = ['deleted_at'];



    public $fillable = [
        'delivery_types'
    ];

    
}
