<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\orders;

class OrderPlace extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */


    public $order;
    public $settings;

    public function __construct($reference)
    {
        $this->order = orders::where('reference',$reference)->first();
        $this->settings = getSettings();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->view('mails.order-place');
            // ->attach(asset('storage/pdf/'.$this->order->reference.'.pdf'));;
    }
}
