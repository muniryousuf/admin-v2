<?php

namespace App\Repositories;

use App\Models\Cms;
use App\Repositories\BaseRepository;

/**
 * Class CmsRepository
 * @package App\Repositories
 * @version February 7, 2022, 9:29 pm UTC
*/

class CmsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'content',
        'slug',
        'meta_title',
        'meta_desc',
        'index_follow'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cms::class;
    }
}
