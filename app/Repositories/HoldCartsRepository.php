<?php

namespace App\Repositories;

use App\Models\HoldCarts;
use App\Repositories\BaseRepository;

/**
 * Class HoldCartsRepository
 * @package App\Repositories
 * @version May 21, 2022, 8:30 am UTC
*/

class HoldCartsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'hold_data'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HoldCarts::class;
    }
}
