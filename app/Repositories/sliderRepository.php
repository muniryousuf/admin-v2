<?php

namespace App\Repositories;

use App\Models\slider;
use App\Repositories\BaseRepository;

/**
 * Class sliderRepository
 * @package App\Repositories
 * @version February 7, 2022, 8:44 pm UTC
*/

class sliderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'image',
        'sort',
        'url',
        'gallery_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return slider::class;
    }
}
