<?php

namespace App\Repositories;

use App\Models\generalSettings;
use App\Repositories\BaseRepository;

/**
 * Class generalSettingsRepository
 * @package App\Repositories
 * @version February 6, 2022, 12:51 pm UTC
*/

class generalSettingsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'site_name',
        'site_title',
        'tag_line',
        'copyright_text',
        'header_logo',
        'footer_logo',
        'service_charges',
        'vat',
        'currencySign',
        'shop_status',
        'printer_ip_1'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return generalSettings::class;
    }
}
