<?php

namespace App\Repositories;

use App\Models\Cart;
use App\Models\CartProducts;
use App\Models\OrderDetail;
use App\Models\orders;
use App\Models\product_meta;
use App\Models\Vouchers;
use App\Models\CartVouchers;
use App\Models\TableReservation;
use App\Models\ResturentAddress;
use App\Data\Models\UserAddress;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Cookie;
use App\Http\Controllers\Api\OrderController;
use Auth;
use Carbon\Carbon;
use App\Data\Models\UserDevices;
use Edujugon\PushNotification\PushNotification;
use PDF;
use Illuminate\Support\Facades\Storage;
use Validator;


/**
 * Class CartRepository
 * @package App\Repositories
 * @version March 11, 2022, 1:08 pm UTC
*/

class CartRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cart::class;
    }
    public function createCart($data){
        $cart = new Cart();
        $cart->id_cart = rand(111111,999999);
        $cart->payment = 'cash';
        $cart->order_type = $data['selection'] ?? 'collection';
        $cart->delivery_fees = $data['delivery_fees'] ?? 0;
        $cart->save();
        return $cart;

    }
    public function addToCart($data){
        $cart = new Cart();
        if($data['id_cart'] == 0 || $data['id_cart'] == null){
            $data['id_cart'] = rand(111111,999999);
            $cart->id_cart = $data['id_cart'];
            $cart->payment = 'cash';
            $cart->order_type = $data['order_type'] ?? 'collection';
            $cart->save();
        }
        $meta_selection = $data['selection_with_price'] ?? '';

        $product = json_decode($data['product_data'],true);
        if($product){

            $already_in_cart_product = CartProducts::where('id_cart',$data['id_cart'])->where('id_product',$product['id'])->where('selection',$data['selection'] ?? '')->first();
            if($already_in_cart_product){
                $already_in_cart_product->quantity += 1;
                $already_in_cart_product->save();
            }else{
                $cart_products = new CartProducts();
                $cart_products->id_cart = $data['id_cart'];
                $cart_products->id_product = $product['id'];
                $cart_products->quantity = 1;
                $cart_products->price = $data['price'];
                $cart_products->selection = $data['selection'] ?? '';
                $cart_products->selection_meta = $meta_selection ?? '';
                $cart_products->save();
            }

        }
        return $cart;
    }
    public function checkVoucherUserLimit($voucher,$user_id){
        // checking the times perticular user has use this cart
        $orders = orders::join('cart_vouchers', 'cart_vouchers.id_cart', '=', 'orders.id_cart')
                        ->where('orders.user_id',$user_id)
                        ->where('cart_vouchers.id_voucher',$voucher->id)
                        ->get();
        return $orders->count();
    }
    public function checkVoucherLimit($voucher){
        // checking the times perticular user has use this cart
        $orders = orders::join('cart_vouchers', 'cart_vouchers.id_cart', '=', 'orders.id_cart')
                        ->where('cart_vouchers.id_voucher',$voucher->id)
                        ->get();
        return $orders->count();
    }
    public function checkConditionsForVoucher($cart,$voucher){
        $times_coupon_used_by_user = $this->checkVoucherUserLimit($voucher,$cart->user_id);
        $times_coupon_used = $this->checkVoucherLimit($voucher);

        if(@$cart->cart_vouchers->count() > 0){
            return null;
        }
        if($times_coupon_used_by_user >= $voucher->user_limit){
            return null;
        }

        if($times_coupon_used >= $voucher->coupon_limit){
            return null;
        }

        return $voucher;


    }
    public function checkIfHasAutoCoupon($cart){
        foreach($cart->cart_vouchers as $voucher){
            if($voucher?->voucher_detail?->auto_coupon == 1){
                return $voucher;
            }
        }
        return false;
    }
    public function applyAutoCoupon($cart){
        $has_auto_coupon = $this->checkIfHasAutoCoupon($cart);
        if($has_auto_coupon){
            $selected_voucher = Vouchers::where('expiry_date','>', Carbon::now()
                ->format('Y-m-d H:i:s'))
                ->where('status',1)
                ->where('auto_coupon',1)
                ->find($has_auto_coupon->id_voucher);
            if(!$selected_voucher){
                CartVouchers::where('id_voucher',$has_auto_coupon->id_voucher)->delete();
            }
            return;
        }
        $auto_coupon = Vouchers::where('expiry_date','>', Carbon::now()
                ->format('Y-m-d H:i:s'))
                ->where('status',1)
                ->where('auto_coupon',1)
                ->first();
        if($auto_coupon){
            $auto_coupon = $this->checkConditionsForVoucher($cart,$auto_coupon);
            if($auto_coupon){
                $cart_voucher = new CartVouchers();
                $cart_voucher->id_cart = $cart->id_cart;
                $cart_voucher->id_voucher = $auto_coupon->id;
                $cart_voucher->save();
            }

        }
    }
    public function applyCouponCode($params){
        $cart = $this->getCart($params['id_cart']);
        $voucher = Vouchers::where('name',$params['code'])->first();
        if(!$voucher){
            return ['error'=>'Voucher not found'];
        }
        $voucher = $this->checkConditionsForVoucher($cart,$voucher);
        if(!$voucher){
            return ['error'=>'You can not use this voucher'];
        }
        if($voucher){
            $cart_voucher = new CartVouchers();
            $cart_voucher->id_cart = $cart->id_cart;
            $cart_voucher->id_voucher = $voucher->id;
            $cart_voucher->save();
            return ['success'=>'Voucher Applied'];
        }

    }
    public function getCart($id_cart,$user_id = 0){
        $cart = Cart::where('id_cart',$id_cart)->with(['cart_products','cart_vouchers'])->first();
        $total_amount = 0;
        $total_discount = 0;
        if($cart->cart_products){

            foreach($cart->cart_products as $cart_product){
                if(!$cart_product->product){
                    $cart_product->delete();
                }else{
                    $total_amount += $cart_product->price * $cart_product->quantity;
                }
            }

        }
        if($cart->user_id == null && ($user_id > 0 && $user_id  != null && $user_id != '') ){
            $cart->user_id = $user_id;
            $cart->save();
        }
        if(@$cart->cart_products->count() > 0){
            $this->applyAutoCoupon($cart);
            // discount calculation work
            $cart = Cart::where('id_cart',$id_cart)->with('cart_products','cart_vouchers')->first();
            foreach($cart->cart_vouchers as $voucher){
                $detail = $voucher?->voucher_detail;
                if($detail?->type == 'amount'){
                    $total_discount += $detail?->discount;
                }else{
                    $total_discount += $total_amount * ($detail?->discount/100);
                }
            }
        }
        $cart->total_discount = $total_discount;
        $cart->total_amount = $total_amount;
        return $cart;

    }

    public function changeQuantity($data){
        $selected_product = CartProducts::find($data['ref_id']);
        if($data['type'] == 'add'){
            $selected_product->quantity += 1;
            $selected_product->save();
        }else{
            if($selected_product->quantity - 1 == 0){
                $selected_product->delete();
            }else{
                $selected_product->quantity -= 1;
                $selected_product->save();
            }
        }
        return true;
    }
    public function placeOrder($data){
        // dd($data);
        $total_price = 0;
        foreach($data['cart_products'] as $cart_product){
            $total_price += $cart_product['price'] * $cart_product['quantity'];
        }
        // dd($total_price);
        $delivery_fees = $data['delivery_fees'] ?? 0;
        $total_dsicount = $data['total_discount'] ?? 0;
        $order = new orders();
        $order->reference = uniqid();
        $order->id_cart = $data['id_cart'] ?? 0;
        $order->user_id = Auth::check() ? Auth::id() : 0;
        $order->total_amount_with_fee = $total_price + $delivery_fees - $total_dsicount;
        $order->delivery_fees = $delivery_fees;
        $order->payment = $data['payment'] ?? 'cash';
        $order->delivery_address = $data['delivery_address'] ?? 'no';
        $order->status = 'pending';
        $order->transaction_id = 'pending';
        $order->payment_status = 'unpaid';
        $order->table_id = $data['table_id'] ?? 0;
        $order->order_type = $data['order_type'] ?? '';
        $order->discounted_amount = $data['total_discount'] ?? 0;
        $order->is_pos = 0;
        $order->xy_report = 0;
        $order->cashier_id = 0;
        $order->number_of_person_sitting = 0;
        $order->customer_selected_time = $data['selected_time'] ?? '';
        $order->postal_code = $data['postal_code'] ?? '0';
        $order->special_instructions = $data['special_instructions'] ?? '0';
        $order->save();

        foreach($data['cart_products'] as $cart_product){
            $selection = $this->getSelectionJson($cart_product['selection_meta']);
            $order_detail = new OrderDetail();
            $order_detail->order_id = $order->id;
            $order_detail->product_id = $cart_product['id_product'] ?? '0';
            $order_detail->product_name = $cart_product['product']['name'] ?? 'N/A';
            $order_detail->price = $cart_product['price'] ?? 0;
            $order_detail->quantity = $cart_product['quantity'] ?? 0;
            $order_detail->extras = $selection;
            $order_detail->selection_meta = $cart_product['selection_meta'] ?? 'N/A';
            $order_detail->special_instructions = '';
            $order_detail->product_status = 'ToBePrepared';
            $order_detail->save();
        }

        if($data['table_id'] > 0){
            $table_rservation = TableReservation::find($data['table_id']);
            $table_rservation->is_reserve = 1;
            $table_rservation->save();
        }

        // $this->savePdf($order->reference);
        $this->sendNotification($order);
        if(Auth::check() && Auth::user()->email != ''){
            \Mail::to(Auth::user()->email)->send(new \App\Mail\OrderPlace($order->reference));
        }
        // sending email to admin
        $address = ResturentAddress::first();
        if($address){
            \Mail::to($address->email)->send(new \App\Mail\OrderPlace($order->reference));
        }

        return $order;

    }
    public function savePdf($reference){
        $order = orders::where('reference',$reference)->first();
        $settings = getSettings();
        $user_detail = $order->user_detail;
        // return view('mails.recipt',['order'=>$order,'settings'=>$settings,'user_detail'=>$user_detail]);
        $pdf = PDF::loadView('mails.recipt',['order'=>$order,'settings'=>$settings]);
        Storage::put("public/pdf/$reference.pdf", $pdf->output());
    }
    public function sendNotification($data)
    {

        $devices = UserDevices::get()->pluck('device_token')->toArray();

        $push = new PushNotification('fcm');

        $push->setMessage([
            'data' => [
                'title' => 'This is the title',
                'body' => 'New order has been placed to your restaurant',
                'sound' => 'default',
                'order_id' => $data->id,
                'total_amount'=> $data->total_amount_with_fee,
                'reference'=>$data->reference,
                'order_type'=> $data->order_type,
                'payment'=>$data->payment,
                'is_pos' => $data->is_pos
            ]
        ])->setDevicesToken($devices)->send();

        $response = $push->getFeedback();
        \Log::info(json_encode($response));
    }
     public function getSelectionJson($selection_json){
        $_josn = json_decode($selection_json);
        if(is_array($_josn)){
            $return = [];
            foreach($_josn as $selection){
                $group_name = product_meta::find($selection->product_meta_id);
                if($group_name != null){
                    $return[] = [
                        "group_name"=>$group_name->name,
                        "choice"=>$selection->key,
                        "price"=>$selection->price,
                    ];
                }

            }
            return json_encode($return);
        }else{
            return '';
        }
    }
    public function changeServingMethod($data){
        $cart = Cart::where('id_cart',$data['id_cart'])->first();
        $cart->order_type = $data['method'] ?? 'collection';
        $cart->delivery_fees = $data['amount'] ?? 0;
        $cart->save();
        return $cart;
    }
    public function addUserAddress($data){

        $validator = Validator::make($data, [
            'address' => 'required',
            'street' => 'required',
            'town' => 'required',
            'postal_code' => 'required',
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
       $user_address = new UserAddress();
       $user_address->address = $data['address'];
       $user_address->street = $data['street'];
       $user_address->town = $data['town'];
       $user_address->postal_code = $data['postal_code'];
       $user_address->user_id = $data['user_id'];
       $user_address->active = 1;
       $user_address->save();
       if(isset($data['id_cart'])){
        $cart = Cart::where('id_cart',$data['id_cart'])->first();
        $cart->delivery_address = $data['address'] . ',' .  $data['street'] . ',' . $data['town'] . ',' .$data['postal_code'];
        $cart->save();
       }

       $user_address->place_order = true;
       return $user_address;
    }
    public function updateUserAddress($data){



        $validator = Validator::make($data, [
            'id_address' => 'required',
            'address' => 'required',
            'street' => 'required',
            'town' => 'required',
            'postal_code' => 'required',
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }


        if(isset($data['id_address'])){
            $user_address = UserAddress::find($data['id_address']);

        }else{
            $user_address = new UserAddress();
        }
       $user_address->address = $data['address'];
       $user_address->street = $data['street'];
       $user_address->town = $data['town'];
       $user_address->postal_code = $data['postal_code'];
       $user_address->user_id = $data['user_id'];
       $user_address->active = 1;
       $user_address->save();
       return $user_address;
    }
    public function getCustomerAddresses($data){
        return UserAddress::where('user_id',$data['user_id'])->get();
    }
    public function setAddressId($data){
       $user_address = UserAddress::find($data['id_address']);
       if(isset($data['postal_code'])){
        $user_address->postal_code = $data['postal_code'];
        $user_address->save();
       }
       $cart = Cart::where('id_cart',$data['id_cart'])->first();
       $cart->delivery_address = $user_address->address . ',' .  $user_address->street . ',' . $user_address->town . ',' .$user_address->postal_code;
       $cart->save();
       $user_address->place_order = true;
       return $user_address;
    }
}
