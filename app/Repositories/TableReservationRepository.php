<?php

namespace App\Repositories;

use App\Models\TableReservation;
use App\Repositories\BaseRepository;

/**
 * Class TableReservationRepository
 * @package App\Repositories
 * @version February 7, 2022, 9:53 pm UTC
*/

class TableReservationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'is_reserve',
        'reservation_end_time',
        'reservation_start_time',
        'id_category',
        'table_type',
        'number_of_person',
        'number_of_person_sitting'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TableReservation::class;
    }
}
