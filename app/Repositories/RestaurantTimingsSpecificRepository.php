<?php

namespace App\Repositories;

use App\Models\RestaurantTimingsSpecific;
use App\Repositories\BaseRepository;

/**
 * Class RestaurantTimingsSpecificRepository
 * @package App\Repositories
 * @version March 19, 2022, 4:40 pm UTC
*/

class RestaurantTimingsSpecificRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'start_time',
        'end_time',
        'shop_closed',
        'specific_date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RestaurantTimingsSpecific::class;
    }
}
