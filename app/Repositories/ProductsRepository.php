<?php

namespace App\Repositories;

use App\Models\Products;
use App\Repositories\BaseRepository;
use DB;
use App\Models\ProductMetasBridge;
use Illuminate\Support\Facades\Cache;
// use Illuminate\Support\Arr;

/**
 * Class ProductsRepository
 * @package App\Repositories
 * @version February 23, 2022, 7:51 pm UTC
*/

class ProductsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'food_allergy',
        'price',
        'id_category',
        'image',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Products::class;
    }
    public function getAllIdsOfMetas($mainProductMetas,$all_ids){

        $newIds = DB::table('product_meta_attributes')
                    ->whereIn('product_meta_id',$mainProductMetas)
                    ->groupBy('child_product_meta_id')
                    ->pluck('child_product_meta_id')
                    ->toArray();

        $all_ids = array_merge($all_ids,$newIds);
         if($newIds){
            $this->getAllIdsOfMetas($newIds,$all_ids);
         }
        return $all_ids;

    }
    public function getproductDetails($id){
        $cached_data = Cache::get('product_'.$id);
        if($cached_data){

            return $cached_data;

        }

        $product = DB::table('products')->where('id',$id)->first();
        $productMeta = array();
        $bridgeData = DB::table('product_metas_bridge')->where('id_product',$id)->get();
        $bridgeAttribute = $bridgeData->pluck('id_meta')->toArray();
        $mainProductMetas = DB::table('product_meta_attributes')
                                ->whereIn('product_meta_id',$bridgeAttribute)
                                ->groupBy('child_product_meta_id')
                                ->pluck('child_product_meta_id')
                                ->toArray();
        $more_ids = $this->getAllIdsOfMetas($mainProductMetas,[]);

        $mainProductMetas = array_merge($more_ids,$mainProductMetas);




        $allMetas = array_merge($bridgeAttribute,$mainProductMetas);
        $bridgeAttribute = DB::table('product_metas_bridge')->where('id_product',$id)->pluck('id_meta')->toArray();
        $allMetas = DB::table('product_metas')->select('id','name','label','image','price','id_parent','table_type','is_required','free_limit','multiple_quantity_allowed')->whereIn('id',$allMetas)->get()->toArray();
        $metaID = array();
        foreach ($allMetas as $allMeta){
            $metaID[] = $allMeta->id;
        }

        $allAttributes = DB::table('product_meta_attributes')->select('id','product_meta_id','key','value','image','price','child_product_meta_id','preselected')
        ->OrderBy('preselected','DESC')
        ->whereIn('product_meta_id',$metaID)->get()->toArray();

        $productMetasData = [];


        $allMatasData = collect($allMetas);
        $allAttributesData = collect($allAttributes);




        foreach( $bridgeData  as $meta){
             $thisMeta = $allMatasData->where('id',$meta->id_meta)->first();
             $thisMeta->sort = $meta->sort;
             $productMetasData[] = $this->assignMetaDatas($thisMeta,true,$allMetas,$allAttributes);
        }

        // $product->product_metas = $productMetasData;
        $product->product_metas_mobile = $productMetasData;
        Cache::forever('product_'.$id,$product);
        return $product;
    }

    // function fix_object( $object ) {
    //     $dump = serialize( $object );
    //   //  print_r($dump); echo "<hr>";
    //     $dump = str_replace('s:6:"series";r:1;','s:6:"series";s:6:"series";',$dump);
    //    // print_r($dump); echo "<hr>";
    //     $dump = preg_replace( '/^O:\d+:"[^"]++"/', 'O:8:"stdClass"', $dump );
    //     $dump = preg_replace_callback( '/:\d+:"\0.*?\0([^"]+)"/', function($matches){
    //                 return ":" . strlen( $matches[1] ) . ":\"" . $matches[1] . "\"";}, $dump );
    //     return unserialize( $dump );
    // }

    function serach($object,$field,$id){
        $return = [];
        foreach($object as $fields){
            if($fields->{$field} == $id){
                $return[] = $fields;
            }
        }
        return $return;
    }
    public function assignMetaDatas($meta,$is_attribute_index = true,$all_metas,$all_attributes){
        $this_meta = [];
        if($is_attribute_index){

            // $meta->attributes = DB::table('product_meta_attributes')->where('product_meta_id',$meta->id)->get();
            $meta->attributes = $this->serach($all_attributes,'product_meta_id', $meta->id);
            foreach($meta->attributes  as $attribute){
                $attribute->product_meta = null;
                $this_meta_data = null;
                if($attribute->child_product_meta_id > 0){
                    // $this_meta_data = $all_metas->where('id',$attribute->child_product_meta_id)->first();
                    $this_meta_data = $this->serach($all_metas,'id', $attribute->child_product_meta_id)[0] ?? null;
                    if($this_meta_data){
                        // $this_meta_data->attributes = $all_attributes->where('product_meta_id',$attribute->child_product_meta_id)->all();

                        $this_meta_data->attributes = $this->serach($all_attributes,'product_meta_id', $attribute->child_product_meta_id);

                        foreach($this_meta_data->attributes as $this_attribute){
                            // $this_attribute->product_meta = $all_metas->where('id',$this_attribute->product_meta_id)->first();
                            $this_attribute->product_meta = $this->serach($all_metas,'id', $this_attribute->product_meta_id)[0] ?? null;
                            $this_meta_data = $this->assignMetaDatas($this_attribute->product_meta,true,$all_metas,$all_attributes);

                        }

                    }

                    $attribute->product_meta = $this_meta_data;
                }
            }
            $this_meta = $meta;
        }
        return $this_meta;
    }


    public function metaWithAttribute($id){
        $product_meta = [];
        if($id != 0){
            $meta = DB::table('product_metas')->where('id',$id)->first();
            $meta->attributes = DB::table('product_meta_attributes')->where('product_meta_id',$id)->get();
            $product_meta =  $meta;
        }
        return $product_meta;
    }

    public function findFirstLevelGroups($product_metas,$bridge_data){
        $all_metas = [];

        foreach($product_metas as $key => $meta){
            $meta->sort = $bridge_data->where('id_meta',$meta->id)->first()['sort'] ?? 0;
            $meta->attributes = DB::table('product_meta_attributes')->where('product_meta_id',$meta->id)->get();
        }
        $sorted_data = $product_metas->sortBy('sort');

        foreach($sorted_data as $key => $meta){
            $all_metas[] =  $meta;
        }
        return $all_metas;
    }

    public function getproductDetailsV2($id){

        $product = DB::table('products')->find($id);
        $metas_bridge = ProductMetasBridge::where('id_product',$id)->get();
        $meta_ids =  $metas_bridge->pluck('id_meta')->toArray();
        $product_metas = DB::table('product_metas')->whereIn('id',$meta_ids)->get();
        $product_metas = $this->findFirstLevelGroups($product_metas,$metas_bridge);
        if($product){
            $product->product_metas = $product_metas;
        }

        return $product;
    }
}
