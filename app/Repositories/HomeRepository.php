<?php

namespace App\Repositories;

use App\Repositories\generalSettingsRepository;
use App\Models\Pages;
use App\Models\Reviews;
use App\Models\Restaurant;
use App\Models\Notifications;
use App\Models\product_meta;
use App\Models\TableReservation;

class HomeRepository {

    public function getGeneralSettings(){

        $is_pickup = 0;
        $is_delivery = 0;
        $is_dine = 0;
        $restaurant = Restaurant::where('deleted_at',null)->first();
        if($restaurant){
            $is_pickup = $restaurant->is_pickup;
            $is_delivery = $restaurant->is_delivery;
            $is_dine = $restaurant->is_dine;
        }

        $general_settings = app(generalSettingsRepository::class)->model()::first();
        $general_settings['is_pickup'] = $is_pickup;
        $general_settings['is_delivery'] = $is_delivery;
        $general_settings['is_dine'] = $is_dine;
        $general_settings['email'] = $restaurant->email;;
        $general_settings['phone_number'] = $restaurant->phone_number;;

        return $general_settings;
    }
    public function getHeader(){
        return Pages::whereIn('type',Pages::TYPE_HEADER)->get();
    }
    public function getFooter(){
        $footer_array = [];
        $footer_data =  Pages::whereIn('type',Pages::TYPE_FOOTER)->get();
        foreach($footer_data as $footer){
            foreach(Pages::TYPE_FOOTER as $key => $value){
                if($value == $footer->type){
                    $footer_array[$value][] = $footer->toArray();
                }
            }
        }
        return $footer_array;
    }
    public function getTotalReviews(){
        $reviews =  Reviews::get();
        $total_reviews = 0;
        $total_ratings = 0;
        foreach($reviews as $review){
            $total_ratings += $review->rating;
            $total_reviews++;
        }
        return ['average_rating' => $total_ratings/5 , 'total_reviews' => $total_reviews];

    }
    public function getGroupData($id){
        return product_meta::find($id)->toArray();
    }
    public function getNotification(){
        $notification = Notifications::where('status',1)->first();
        if($notification != null){
            $notification = Notifications::where('status',1)->first()->toArray();
        }else{
            $notification = array();
        }
        return $notification;
    }
    public function getTableDetail($id){
       return TableReservation::find($id);
    }

}
