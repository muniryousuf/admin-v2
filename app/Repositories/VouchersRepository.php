<?php

namespace App\Repositories;

use App\Models\Vouchers;
use App\Repositories\BaseRepository;

/**
 * Class VouchersRepository
 * @package App\Repositories
 * @version April 15, 2022, 8:21 pm UTC
*/

class VouchersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'type',
        'expiry_date',
        'status',
        'coupon_limit',
        'user_limit',
        'auto_coupon',
        'for_first_order',
        'for_all_orders',
        'with_delivery'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Vouchers::class;
    }
}
