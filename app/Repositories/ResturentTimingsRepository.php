<?php

namespace App\Repositories;

use App\Models\ResturentTimings;
use App\Repositories\BaseRepository;

/**
 * Class ResturentTimingsRepository
 * @package App\Repositories
 * @version March 19, 2022, 4:10 pm UTC
*/

class ResturentTimingsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'day',
        'start_time',
        'end_time',
        'shop_close'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ResturentTimings::class;
    }
}
