<?php

namespace App\Repositories;

use App\Models\SmsSetting;
use App\Repositories\BaseRepository;

/**
 * Class SmsSettingRepository
 * @package App\Repositories
 * @version February 7, 2022, 9:32 pm UTC
*/

class SmsSettingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'key',
        'secret',
        'gateway_key'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SmsSetting::class;
    }
}
