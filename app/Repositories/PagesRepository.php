<?php

namespace App\Repositories;

use App\Models\Pages;
use App\Repositories\BaseRepository;

/**
 * Class PagesRepository
 * @package App\Repositories
 * @version February 13, 2022, 12:11 pm UTC
*/

class PagesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'html'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pages::class;
    }
}
