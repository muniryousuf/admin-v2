<?php

namespace App\Repositories;

use App\Models\CustomerReservations;
use App\Repositories\BaseRepository;

/**
 * Class CustomerReservationsRepository
 * @package App\Repositories
 * @version May 12, 2022, 10:56 pm UTC
*/

class CustomerReservationsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'firstname',
        'lastname',
        'phone',
        'email',
        'booking_date',
        'persons',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomerReservations::class;
    }
}
