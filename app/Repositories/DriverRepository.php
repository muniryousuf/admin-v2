<?php

namespace App\Data\Repositories;

use App\Data\Models\AssignOrderToDriver;
use App\Data\Models\Category;
use App\Data\Models\Deal;
use App\Data\Models\DealProducts;
use App\Data\Models\Orders;
use App\Data\Models\Products;
use App\User;
use function App\Helpers\paginator;

class DriverRepository
{
    /**
     * @param bool $pagination
     * @param int $perPage
     * @param array $input
     * @return mixed
     */
    public function findByAll($pagination = false,$perPage = 10, $input = [])
    {
       $drivers = User::with(['driverOrders'])->whereHas('user_roles', function ($query) {
                return $query->where('role_id', '=', 3);
            })->get();

       foreach ($drivers as $key => $driver) {

           $drivers[$key]['total_unpaid'] = 0;

           foreach ($driver->driverOrders as $order) {

               if (isset($order->order)) {
                   if($order->order['payment_status'] == "unpaid") {
                       $drivers[$key]['total_unpaid'] += $order->order->total_amount_with_fee - $order->order->discounted_amount;
                   }
               }

           }
       }

       return $drivers;
    }

    public function assignOrderToDriver($data){

        foreach ($data['order_id'] as $order) {
            AssignOrderToDriver::create([
                'driver_id' => $data['driver_id'],
                'order_id' => $order
            ]);
        }

        return $data;
    }

    public function unassignOrderFromDriver($data){

        foreach ($data['order_id'] as $order) {
            AssignOrderToDriver::where(['driver_id' => $data['driver_id'], 'order_id' => $order])->delete();
        }

        return $data;
    }

    public function collectAmountFromDriver($data){

        foreach ($data['order_id'] as $order) {
            Orders::where(['id' => $order])->update(["payment" => "cash", "payment_status" => "paid"]);
        }

        return $data;
    }

    public function getDriverOrders($pagination = false,$perPage = 10, $input = [])
    {
        $data = array();

        $model = Orders::where('order_type', 'delivery')->whereHas('driver', function($q) use ($input){
            $q->where('driver_id', $input['driver_id']);
        })->orderBy('id', 'desc');

        if ($pagination) {
            $model = $model->paginate($perPage);
            $data['data'] = $model->items();
            $data = paginator($data, $model);
        } else {
            $data['data'] = $model->get();
        }

        $data['total_orders'] = count($data['data']);
        $data['total_amount'] = 0;
        $data['total_unpaid'] = 0;
        $data['driver_return'] = array();

        foreach ($data['data']  as $key => $value){

            $data['total_amount'] += $value['total_amount_with_fee'] - $value['discounted_amount'];

            if($value['payment_status'] == "unpaid") {
                $data['total_unpaid'] += $value['total_amount_with_fee'] - $value['discounted_amount'];
            }

            $data['data'][$key]['final_amount'] = $value['total_amount_with_fee'] - $value['discounted_amount'];

            $user = User::find($value['user_id']);
            if($user){
                $data['data'][$key]['phone_number'] = $user->phone_number;
            }else {
                $data['data'][$key]['phone_number'] = 78601;
            }

            $data['driver_return'][$key] = array("order_id" => $value->id, "customer_address" => $user->address, "total_amount" => $data['data'][$key]['final_amount'], 'phone_number' => $data['data'][$key]['phone_number'], 'status' => $value->payment_status);
        }

        return $data;
    }
}
