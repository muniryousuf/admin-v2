<?php

namespace App\Repositories;

use App\Models\PaymentMethods;
use App\Repositories\BaseRepository;

/**
 * Class PaymentMethodsRepository
 * @package App\Repositories
 * @version March 19, 2022, 4:51 pm UTC
*/

class PaymentMethodsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'private_key',
        'public_key',
        'another_key',
        'active'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PaymentMethods::class;
    }
}
