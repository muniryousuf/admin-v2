<?php

namespace App\Repositories;

use App\Models\TableCategories;
use App\Repositories\BaseRepository;

/**
 * Class TableCategoriesRepository
 * @package App\Repositories
 * @version May 11, 2022, 7:53 pm UTC
*/

class TableCategoriesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TableCategories::class;
    }
}
