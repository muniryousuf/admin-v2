<?php

namespace App\Repositories;

use App\Models\LocationsAttributes;
use App\Repositories\BaseRepository;

/**
 * Class LocationsAttributesRepository
 * @package App\Repositories
 * @version May 14, 2022, 12:00 am UTC
*/

class LocationsAttributesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'details',
        'map',
        'id_location'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LocationsAttributes::class;
    }
}
