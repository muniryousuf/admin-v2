<?php

namespace App\Repositories;

use App\Models\Locations;
use App\Repositories\BaseRepository;

/**
 * Class LocationsRepository
 * @package App\Repositories
 * @version May 13, 2022, 11:54 pm UTC
*/

class LocationsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'details'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Locations::class;
    }
}
