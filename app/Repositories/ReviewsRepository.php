<?php

namespace App\Repositories;

use App\Models\Reviews;
use App\Repositories\BaseRepository;

/**
 * Class ReviewsRepository
 * @package App\Repositories
 * @version March 31, 2022, 10:55 pm UTC
*/

class ReviewsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'message',
        'rating'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Reviews::class;
    }
}
