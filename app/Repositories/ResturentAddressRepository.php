<?php

namespace App\Repositories;

use App\Models\ResturentAddress;
use App\Repositories\BaseRepository;

/**
 * Class ResturentAddressRepository
 * @package App\Repositories
 * @version February 6, 2022, 4:31 pm UTC
*/

class ResturentAddressRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'website_url',
        'logo',
        'phone_number',
        'id_user',
        'is_pickup',
        'is_delivery',
        'is_dine',
        'admin_email'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ResturentAddress::class;
    }
}
