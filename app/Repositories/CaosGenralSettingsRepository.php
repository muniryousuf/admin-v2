<?php

namespace App\Repositories;

use App\Models\CaosGenralSettings;
use App\Repositories\BaseRepository;

/**
 * Class CaosGenralSettingsRepository
 * @package App\Repositories
 * @version July 25, 2022, 11:50 am UTC
*/

class CaosGenralSettingsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'main_printer',
        'kitchen_printer',
        'kicthen_copies',
        'shop_name_and_address',
        'reciept_end_text',
        'menu_url',
        'enable_multi_payment',
        'extra_info'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CaosGenralSettings::class;
    }
}
