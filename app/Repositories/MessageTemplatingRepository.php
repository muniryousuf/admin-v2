<?php

namespace App\Repositories;

use App\Models\MessageTemplating;
use App\Repositories\BaseRepository;

/**
 * Class MessageTemplatingRepository
 * @package App\Repositories
 * @version February 7, 2022, 9:43 pm UTC
*/

class MessageTemplatingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'template_slug',
        'description',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MessageTemplating::class;
    }
}
