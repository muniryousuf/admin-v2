<?php

namespace App\Repositories;

use App\Models\ourStory;
use App\Repositories\BaseRepository;

/**
 * Class ourStoryRepository
 * @package App\Repositories
 * @version February 7, 2022, 9:02 pm UTC
*/

class ourStoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'main_title',
        'all_title',
        'description',
        'image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ourStory::class;
    }
}
