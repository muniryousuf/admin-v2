<?php

namespace App\Repositories;

use App\Models\product_meta;
use App\Models\ProductMetaAttributes;
use App\Repositories\BaseRepository;

/**
 * Class product_metaRepository
 * @package App\Repositories
 * @version February 26, 2022, 5:31 pm UTC
*/

class product_metaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'value',
        'id_parent',
        'id_attribute'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return product_meta::class;
    }

    public function addAttribute($attribute_data){
        $attribute_data['price'] = $attribute_data['price'] == null ? 0 : $attribute_data['price'];
        $product_meta_attributes = new ProductMetaAttributes();
        foreach($attribute_data as $key => $value){
            $product_meta_attributes->{$key} = $value;
        }
        $product_meta_attributes->save();
        return ProductMetaAttributes::where('product_meta_id',$attribute_data['product_meta_id'])->get();
    }
    public function fetchAttributes($attribute_data){

        return ProductMetaAttributes::without('product_meta')
                ->with('child_meta')
                ->where('product_meta_id',$attribute_data['id_attribute'])
                ->get();
    }
    public function fetchAttributesV2($attribute_data){

        return ProductMetaAttributes::without('product_meta')
                ->with('child_meta')
                ->where('product_meta_id',$attribute_data['id_group'])
                ->get();
    }
}
