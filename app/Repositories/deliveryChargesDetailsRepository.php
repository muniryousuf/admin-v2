<?php

namespace App\Repositories;

use App\Models\deliveryChargesDetails;
use App\Repositories\BaseRepository;

/**
 * Class deliveryChargesDetailsRepository
 * @package App\Repositories
 * @version February 6, 2022, 4:58 pm UTC
*/

class deliveryChargesDetailsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'postal_code',
        'amount',
        'fix_delivery_charges'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return deliveryChargesDetails::class;
    }
}
