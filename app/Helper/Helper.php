<?php

use App\Repositories\HomeRepository;

function generateGroupsTree($childs,$second_child = false)
{
  $tree = '';
    if($childs){
      foreach($childs as $child){
        if($second_child){
          $tree .= '<ul class="children">';
        }else{
          $tree .= '<ul class="tree-listing">';
        }
        $route= route("productMetas.edit", [$child->id]);
        $tree .='
            <li>
            <span class="rootTree"> <a href= "'.$route.'"> '.$child->name.'</a></span>
              '.generateGroupsTree($child->childs,true).'
            </li>';
        $tree .= '</ul>';
      }
    }
    return $tree;
}
function generateGroupsTreeNew($metas,$second_child = false)
{

  $tree = '';

    if($metas){
      foreach($metas->attributes as $meta){
        if($second_child){
          $tree .= '<ul >';
          $meta_name = $metas->name;
        }else{
          $tree .= '<ul ">';
          $meta_name = $meta->key;
        }
        $route= route("productMetas.edit", [$metas->id]);
        $tree .='
            <li>
              <span class="rootTree"> 
                <a href= "'.$route.'"> '.$meta_name.'</a>
              </span>
              '.generateGroupsTreeNew($meta->product_meta,true).'
            </li>';

        $tree .= '</ul>';
        // dump($tree);
      }
    }
    return $tree;
}
function getSettings(){
  $settings = app(HomeRepository::class)->getGeneralSettings();


  if($settings){
    return  $settings->toArray();
  }
  return [];
}
function getHeader(){
  $header = app(HomeRepository::class)->getHeader();
  if($header){
    return  $header->toArray();
  }
  return [];
}
function getFooter(){
  return app(HomeRepository::class)->getFooter();
}
function getTotalReviews(){
  return app(HomeRepository::class)->getTotalReviews();
}
function GetGroupName($id){
  return app(HomeRepository::class)->getGroupData($id);
}
function getNotification(){
 return app(HomeRepository::class)->getNotification();
}
function getTableDetail($id){
 return app(HomeRepository::class)->getTableDetail($id);
}
