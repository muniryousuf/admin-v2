<?php

namespace App\Data\Repositories;

use App\Data\Models\OrderDetail;
use App\Data\Models\Orders;
use App\Data\Models\PaymentType;
use App\Data\Models\RestaurantTiming;
use App\Data\Models\TableReservation;
use App\Data\Models\UserAddress;
use App\Mail\OrderPlace;
use App\Data\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Stripe\Order;
use function App\Helpers\paginator;

class OrderRepository
{
    protected $model;

    public function __construct(Orders $model) {
        $this->model = $model;
    }

    /**
     * @param bool $pagination
     * @param int $perPage
     * @param array $input
     * @return array|mixed
     */
    public function findByAll($pagination = false,$perPage = 10, $input = [])
    {
        $data = array();
        $model = $this->model->orderBy('id', 'desc');

        if(isset($input['date_from']) && isset($input['date_to'])) {

            $from = date($input['date_from']);
            $to = date($input['date_to']);

            $model =  $model->whereDate('created_at', '>=', $from. " 00:00:00")
                ->whereDate('created_at', '<=', $to. " 23:59:59");;
        }

        if(isset($input['driver_id'])) {
            $model->where('order_type', 'delivery')->whereHas('driver', function($q) use ($input){
                $q->where('driver_id', $input['driver_id']);
            });
        }

        if(isset($input['order_type'])) {
            $model->where('order_type', $input['order_type']);
        }


        if ($pagination) {
            $model = $model->paginate($perPage);
            $data['data'] = $model->items();
            $data = paginator($data, $model);
        } else {
            $data['data'] = $model->get();
        }

        $data['total_orders'] = count($data['data']);
        $data['total_amount'] = 0;
        $data['total_unpaid'] = 0;

        foreach ($data['data']  as $key => $value){

            $data['total_amount'] += $value['total_amount_with_fee'] - $value['discounted_amount'];

            if($value['payment_status'] == "unpaid") {
                $data['total_unpaid'] += $value['total_amount_with_fee'] - $value['discounted_amount'];
            }

            $data['data'][$key]['final_amount'] = $value['total_amount_with_fee'] - $value['discounted_amount'];

            $user = User::find($value['user_id']);
            if($user){
                $data['data'][$key]['phone_number'] = $user->phone_number;
            }else {
                $data['data'][$key]['phone_number'] = 78601;
            }
        }

        return $data;
    }

    /**
     * @param $id
     * @return array|null
     */
    public function findById($id)
    {
        $data = array();

        $query = $this->model->where('id',$id)->first();
        if ($query != NULL) {
            $data = $query;
        } else {
            $data = null;
        }



        return $data;
    }

    /**
     * @param $request
     * @param $id
     * @return mixed
     */
    public function updateRecord($request, $id)
    {
        $data = $this->model->findOrFail($id);
        $data->fill($request)->save();
        $phone_number = User::where('id',$data->user_id)->pluck('phone')->first();
        $data->phone_number = !empty($phone_number)?$phone_number:"Number Not Found";
        if(isset($data->table_id) && $data->table_id > 0){
            $table_name = TableReservation::where('id',$data->table_id)->select('name')->first();
            $data->table_name = empty($table_name)? 'N/A':$table_name->name;
        }
        return $data;
    }

    public function placeOrder($data) {

        $data['reference'] = random_int(1000, 9999);
        $password = "yousuf+1";
        $discount = isset($data['discounted_amount']) ? $data['discounted_amount'] : 0;
        if($data['order_type'] == 'collection'){
            $phone_number = $data['phone_number'];
            $user_name = $data['name'];
            $userData = User::create([
                'name'=>$user_name,
                'email'=> $user_name.'-'.rand(),
                'password'=>bcrypt($password),
                'phone_number'=>$phone_number,
                'phone'=>$phone_number
                ]
            );
            UserAddress::create(['user_id'=>$userData->id,'address'=>isset($data['user_data']['address'])?$data['user_data']['address']:'','street'=>isset($data['user_data']['street']) ? $data['user_data']['street']:'','town'=>isset($data['user_data']['town']) ? $data['user_data']['town']:'','postal_code'=>isset($data['user_data']['postal_code']) ? $data['user_data']['postal_code']:'','active'=>1]);
        }else{
            if(isset($data['user_data']))
            {
                $userData = User::where('email',$data['user_data']['email'])->first();
                if(!$userData){
                    $userData = User::create(['name'=>$data['user_data']['name'],'email'=>$data['user_data']['email'],'password'=>bcrypt($password),'phone_number'=>$data['user_data']['number']]);
                    UserAddress::create(['user_id'=>$userData->id,'address'=>isset($data['user_data']['address'])?$data['user_data']['address']:'','street'=>isset($data['user_data']['street']) ? $data['user_data']['street']:'','town'=>isset($data['user_data']['town']) ? $data['user_data']['town']:'','postal_code'=>isset($data['user_data']['postal_code']) ? $data['user_data']['postal_code']:'','active'=>1]);
                }
            }
        }

        $isPos =   isset($data['is_pos']) ? $data['is_pos'] : 0;
        $kiosk = isset($data['is_kiosk_print']) && $data['is_kiosk_print'] ==1 ? 'kiosk': null;
        $data['device_type'] = $kiosk;
        $placed = $this->model->create(
            ["user_id" => isset($userData->id) ? $userData->id : '1',
                "reference" => $data['reference'],
                "total_amount_with_fee" => $data['total_amount_with_fee'],
                "delivery_fees" => $data['delivery_fees'],
                "payment" => $data['payment'],
                "payment_status" => $data['payment_status'],
                "order_type" => $data['order_type'],
                "delivery_address" => $data['delivery_address'],
                "status" => "pending",
                'discounted_amount' => $discount,
                'is_pos' => $isPos,
                'is_kiosk_print'=>isset($data['is_kiosk_print'])? $data['is_kiosk_print']:0,
                'device_type' =>  $data['device_type'],
                'number_of_person_sitting' => isset($data['number_of_person_sitting']) ? $data['number_of_person_sitting'] : 0,
                'table_id'=> isset($data['table_id']) ? $data['table_id'] : 0
            ]);

        $order_id = $placed['id'];

        //for Payment Type
        if(isset($data['payment_object'])) {
            foreach ($data['payment_object'] as $key => $val){
                PaymentType::create(array('order_id'=>$order_id,'payment_type'=>$val['payment_type'],'amount'=> $val['amount']));
            }
        }

        if($isPos == 0){
            PaymentType::create(array('order_id'=>$order_id,'payment_type'=>$data['payment'],'amount'=> $data['total_amount_with_fee']));
        }

        if($placed) {

            if(isset($data['table_id']) && $data['table_id'] != null) {

                if($data['payment'] == "unpaid") {
                    TableReservation::where('id', $data['table_id'])->update(['is_reserve' => 1, 'table_json' => $data['table_json'], 'number_of_person_sitting'=>isset($data['number_of_person_sitting']) ? $data['number_of_person_sitting'] : 0]);
                } else {
                    TableReservation::where('id', $data['table_id'])->update(['is_reserve' => 0, 'table_json' => null, 'number_of_person_sitting'=>isset($data['number_of_person_sitting']) ? $data['number_of_person_sitting'] : 0]);
                }


                if ($data['payment'] == "unpaid") {
                    TableReservation::where('id', $data['table_id'])->update(['is_reserve' => 1, 'table_json' => $data['table_json'], 'number_of_person_sitting' => isset($data['number_of_person_sitting']) ? $data['number_of_person_sitting'] : 0]);
                } else {
                    TableReservation::where('id', $data['table_id'])->update(['is_reserve' => 0, 'table_json' => null, 'number_of_person_sitting'=>isset($data['number_of_person_sitting']) ? $data['number_of_person_sitting'] : 0]);
                }

                $table_name = TableReservation::where('id',$data['table_id'])->select('name')->first();
                $placed['table_name'] = empty($table_name)? 'N/A':$table_name->name;
                $placed['table_id'] = $data['table_id'];
            }

            foreach ($data['order_details'] as $detail) {

                if(isset($detail['extras'])) {
                    $detail['extras'] = json_encode($detail['extras']);
                }

                $detail['order_id'] = $placed['id'];
                $detail['special_instructions'] = $detail['special_instructions'] != '' ? $detail['special_instructions'] : 'no'; 
                OrderDetail::create($detail);
            }

            $placed['phone_number'] =$userData->phone_number;
        }
         $placed['order_id'] = $placed['id'];
        return $placed;
    }

    public function updateOrder($request, $id)
    {

        $data = $this->model->findOrFail($id);
        $data->fill($request)->save();

        OrderDetail::where('id', $id)->delete();

        if(isset($request['table_id']) && $request['table_id'] != null) {

            if ($request['payment'] != "unpaid") {
                TableReservation::where('id', $request['table_id'])->update(['is_reserve' => 0]);
            }

        }

        foreach ($request['order_details'] as $detail) {

            if(isset($detail['extras'])) {
                $detail['extras'] = json_encode($detail['extras']);
            }

            $detail['order_id'] = $id;
            OrderDetail::create($detail);
        }

        $result = $this->model->findOrFail($id);


        return $result;
    }

    public function getTotalSales($data) {

        $totalSale = $this->model->where('status', 'delivered')->sum('total_amount_with_fee');
        return $totalSale;
    }

    public function getKitchenOrders($pagination = false,$perPage = 10, $input = [])
    {
        $return = array();

        $getCurrentDay = Carbon::today()->format('l');
        $getCurrentDate = Carbon::today()->format('Y-m-d');

        $start_time = $getCurrentDate." 00:00:00";
        $end_time = $getCurrentDate." 23:59:59";

        $availableDay = RestaurantTiming::where('day', $getCurrentDay)->first();

        if (isset($availableDay)) {
            $start_time = $getCurrentDate. " ".$availableDay['start_time'];
            $end_time = $getCurrentDate. " ".$availableDay['end_time'];
        }

        $data = $this->model
            ->with(['details' => function ($query) {
                $query->without('product');
            }])
            ->orderBy('id', 'desc')
            ->whereBetween('created_at', [$start_time, $end_time])
            ->get()->toArray();

        $return['orders'] = $data;
        $return['accepted_orders'] = $this->model->where('status', 'accepted')->whereBetween('created_at', [$start_time, $end_time])->count();
        $return['preparing_orders'] = $this->model->where('status', 'Preparing')->whereBetween('created_at', [$start_time, $end_time])->count();
        $return['completed_orders'] = $this->model->where('status', 'completed')->whereBetween('created_at', [$start_time, $end_time])->count();
        $return['new_orders'] = $this->model->where('status', 'panding')->whereBetween('created_at', [$start_time, $end_time])->count();

        return $return;
    }

    public function updateProductStatus($data){

//        $data['order_id'] = 1;
//        $data['product_id'] =4;

        $productData = OrderDetail::where(['order_id' => $data['order_id'],'product_id' => $data['product_id']])->first();
        if(isset($productData->product_status)){
            $existingStatus = $productData->product_status;
            $newStatus = '';

            switch ($existingStatus) {
                case "ToBePrepared":
                    $newStatus = 'Preparing';
                    break;
                case "Preparing":
                    $newStatus = 'Prepared';
                    break;
                case "Prepared":
                    $newStatus = 'ToBePrepared';
                    break;
            }




            OrderDetail::where(['order_id' => $data['order_id'],'product_id' => $data['product_id']])
                ->update(['product_status' => $newStatus]);


            $orderDetail = OrderDetail::where('order_id',$data['order_id'])->get()->toArray();

            $totalProducts = count($orderDetail);
            $preparedCount = 0;

            foreach ($orderDetail as $value){
                if($value['product_status'] == 'Prepared'){
                    $preparedCount++;
                }
            }

            if($preparedCount == $totalProducts){
                Orders::where('id',$data['order_id'])->update(['status' => 'Prepared']);
            }

            if($preparedCount < $totalProducts){
                Orders::where('id',$data['order_id'])->update(['status' => 'Preparing']);
            }

            return  $orderDetail;
        }


    }

    public function getOrderDesktopPos()
    {
        $data = $this->model->where('posted_to_desktop_pos',0)->orderBy('id', 'desc')->limit(2)->get()->toArray();
        $order = array();
        foreach ($data as $d) {
            $details = array();
            foreach ($d['details'] as $key => $value) {
                $extra = $value['extras'];
                if (!empty($value['extras'])) {
                    $extra = json_decode($value['extras']);
                }
                $details[] = array(
                    'id' => $value['id'],
                    'order_id' => $value['order_id'],
                    'product_id' => $value['product_id'],
                    'product_name' => $value['product_name'],
                    'price' => $value['price'],
                    'quantity' => $value['quantity'],
                    'extras' => $extra,
                    'special_instructions' => $value['special_instructions'],
                    'product_status' => $value['product_status'],
                    'created_at' => $value['created_at'],
                    'updated_at' => $value['updated_at'],
                );
            }

            $order[] = array(
                'id' => $d['id'],
                'reference' => $d['reference'],
                'user_id' => $d['user_id'],
                'total_amount_with_fee' => $d['total_amount_with_fee'],
                'delivery_fees' => $d['delivery_fees'],
                'payment' => $d['payment'],
                'delivery_address' => $d['delivery_address'],
                'status' => $d['status'],
                'payment_status' => $d['payment_status'],
                'created_at' => $d['created_at'],
                'updated_at' => $d['updated_at'],
                'order_type' => $d['order_type'],
                'discounted_amount' => $d['discounted_amount'],
                'number_of_person_sitting' => $d['number_of_person_sitting'],
                'table_id' => $d['table_id'],
               // 'is_kiosk_order' => $d['is_kiosk_order'],
                'is_kiosk_print' => $d['is_kiosk_print'],
                'xy_report' => $d['xy_report'],
                'details' => $details,
                'user_detail' => $d['user_detail'],
                'driver' => $d['driver'],
                'payment_type' => $d['payment_type'],
            );
        }
        return $order;
    }


}
