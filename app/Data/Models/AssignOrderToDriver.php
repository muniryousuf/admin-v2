<?php

namespace App\Data\Models;

// use App\User;
use Illuminate\Database\Eloquent\Model;

class AssignOrderToDriver extends Model
{
    protected $table = 'assign_orders_to_driver';

    protected $with = ['driverDetails'];

    protected $fillable = ['driver_id', 'order_id'];

    public function driverDetails()
    {
        return $this->hasOne(User::class,'id','driver_id');
    }

    public function order()
    {
        return $this->hasOne(Orders::class,'id','order_id');
    }

}
