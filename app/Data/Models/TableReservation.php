<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TableReservation extends Model
{
    protected $table = 'table_reservation';

    protected $fillable = ['name','is_reserve', 'reservation_start_time','reservation_end_time','created_at','updated_at','id_category','number_of_person','table_type','number_of_person_sitting', 'table_json'];

    public static function getEnumValues($table, $column) {
        $type = DB::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '{$column}'"))[0]->Type ;
        preg_match('/^enum((.*))$/', $type, $matches);
        $enum = array();
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim( $value, "'" );
            $enum = array_add($enum, $v, $v);
        }
        return $enum;
    }

}
