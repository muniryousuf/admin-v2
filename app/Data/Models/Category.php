<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = ['name', 'description', 'status'];
    use SoftDeletes;

    public function products()
    {
        return $this->hasMany(Products::class,'id_category','id');
    }
}
