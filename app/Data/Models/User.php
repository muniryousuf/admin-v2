<?php

namespace App\Data\Models;

use App\Data\Models\AssignOrderToDriver;
use App\Data\Models\Roles;
use App\Data\Models\UserRole;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
#use Laravel\Cashier\Billable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone_number','phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $with = ['user_roles'];

    public function roles()
    {
        return $this->hasOne(UserRole::class, 'user_id');
    }

    public function user_roles()
    {
        return $this->belongsToMany(Roles::class,'user_roles','user_id','role_id');
    }

    public function driverOrders()
    {
        return $this->hasMany(AssignOrderToDriver::class,'driver_id','id')->with('order')->without('driverDetails');
    }

}
