<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class CardStream extends Model
{
    protected $table = 'card_stream';

    public $timestamps = false;

    protected $fillable = ['request_data', 'threeDSr'];

}
