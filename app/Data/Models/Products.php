<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Products extends Model
{
    protected $table = 'products';

    protected $hidden = ['pivot'];
    use SoftDeletes;


    protected $appends = ['image_url'];

    protected $fillable = ['name', 'price', 'description', 'image', 'status', 'id_category'];

    public function getImageUrlAttribute()
    {
        return "https://placeimg.com/640/480/any";

    }


}
