<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TableCategory extends Model
{
    protected $table = 'table_category';

    use SoftDeletes;

    protected $fillable = ['name', 'status'];

    protected $with = ['tables'];

    public function tables()
    {
        return $this->hasMany(TableReservation::class,'id_category','id');
    }
}
