<?php

namespace App\Data\Models;

// use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Orders extends Model
{
    const PAYMENT_COMPLETED = 1;
    const PAYMENT_PENDING = 0;

    protected $table = 'orders';

    protected $with = ['details','user_detail', 'driver','payment_type'];

    protected $fillable = ['reference', 'user_id', 'total_amount_with_fee', 'delivery_fees', 'payment', 'delivery_address', 'order_type', 'payment_status', 'status','discounted_amount', 'is_pos','xy_report','cashier_id','table_id','number_of_person_sitting','is_kiosk_print','is_kiosk_order','device_type'];

    public function details()
    {
        return $this->hasMany(OrderDetail::class,'order_id','id');
    }
    public function user_detail(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function driver(){
        return $this->hasOne(AssignOrderToDriver::class,'order_id','id');
    }

    public function payment_type()
    {
        return $this->hasMany(PaymentType::class,'order_id','id');
    }

}
