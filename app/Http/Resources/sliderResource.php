<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class sliderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'image' => $this->image,
            'sort' => $this->sort,
            'url' => $this->url,
            'gallery_id' => $this->gallery_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
