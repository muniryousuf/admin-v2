<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CaosGenralSettingsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'main_printer' => $this->main_printer,
            'kitchen_printer' => $this->kitchen_printer,
            'kicthen_copies' => $this->kicthen_copies,
            'shop_name_and_address' => $this->shop_name_and_address,
            'reciept_end_text' => $this->reciept_end_text,
            'menu_url' => $this->menu_url,
            'enable_multi_payment' => $this->enable_multi_payment,
            'extra_info' => $this->extra_info,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
