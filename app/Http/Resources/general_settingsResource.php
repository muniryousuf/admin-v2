<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class general_settingsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'id' => $this->id,
            'site_name' => $this->site_name,
            'site_title' => $this->site_title,
            'tag_line' => $this->tag_line,
            'copyright_text' => $this->copyright_text,
            'header_logo' => $this->header_logo,
            'footer_logo' => $this->footer_logo,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'service_charges' => $this->service_charges,
            'vat' => $this->vat,
            'currencySign' => $this->currencySign,
            'shop_status' => $this->shop_status,
            'printer_ip_1' => $this->printer_ip_1,
            'printer_ip_2' => $this->printer_ip_2,
            'printer_ip_3' => $this->printer_ip_3,
            'printer_ip_4' => $this->printer_ip_4,
            'printer_ip_5' => $this->printer_ip_5,
            'printer_ip' => $this->printer_ip,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
