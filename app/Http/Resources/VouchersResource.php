<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VouchersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'expiry_date' => $this->expiry_date,
            'status' => $this->status,
            'coupon_limit' => $this->coupon_limit,
            'user_limit' => $this->user_limit,
            'auto_coupon' => $this->auto_coupon,
            'for_first_order' => $this->for_first_order,
            'for_all_orders' => $this->for_all_orders,
            'with_delivery' => $this->with_delivery,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
