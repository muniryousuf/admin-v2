<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TableReservationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'is_reserve' => $this->is_reserve,
            'reservation_end_time' => $this->reservation_end_time,
            'reservation_start_time' => $this->reservation_start_time,
            'id_category' => $this->id_category,
            'table_type' => $this->table_type,
            'number_of_person' => $this->number_of_person,
            'number_of_person_sitting' => $this->number_of_person_sitting,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
