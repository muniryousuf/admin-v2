<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ordersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_id' => $this->order_id,
            'Reference' => $this->Reference,
            'total_amount' => $this->total_amount,
            'payment_method' => $this->payment_method,
            'delivery_address' => $this->delivery_address,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
