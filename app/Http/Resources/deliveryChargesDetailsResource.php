<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class deliveryChargesDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'delivery_id' => $this->delivery_id,
            'delivery_id' => $this->delivery_id,
            'miles' => $this->miles,
            'postal_code' => $this->postal_code,
            'amount' => $this->amount,
            'fix_delivery_charges' => $this->fix_delivery_charges,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
