<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RestaurantTimingsSpecificResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'shop_closed' => $this->shop_closed,
            'specific_date' => $this->specific_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
