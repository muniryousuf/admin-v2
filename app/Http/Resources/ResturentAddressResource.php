<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ResturentAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'website_url' => $this->website_url,
            'logo' => $this->logo,
            'phone_number' => $this->phone_number,
            'id_user' => $this->id_user,
            'is_pickup' => $this->is_pickup,
            'is_delivery' => $this->is_delivery,
            'is_dine' => $this->is_dine,
            'admin_email' => $this->admin_email,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
