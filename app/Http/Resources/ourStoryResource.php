<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ourStoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'main_title' => $this->main_title,
            'all_title' => $this->all_title,
            'description' => $this->description,
            'image' => $this->image,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
