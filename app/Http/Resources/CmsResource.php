<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CmsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'content' => $this->content,
            'slug' => $this->slug,
            'meta_title' => $this->meta_title,
            'meta_desc' => $this->meta_desc,
            'index_follow' => $this->index_follow,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
