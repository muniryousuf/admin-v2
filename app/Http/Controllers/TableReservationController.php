<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTableReservationRequest;
use App\Http\Requests\UpdateTableReservationRequest;
use App\Repositories\TableReservationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\TableCategories;

class TableReservationController extends AppBaseController
{
    /** @var  TableReservationRepository */
    private $tableReservationRepository;

    public function __construct(TableReservationRepository $tableReservationRepo)
    {
        $this->tableReservationRepository = $tableReservationRepo;
    }

    /**
     * Display a listing of the TableReservation.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tableReservations = $this->tableReservationRepository->paginate(10);

        return view('table_reservations.index')
            ->with('tableReservations', $tableReservations);
    }

    /**
     * Show the form for creating a new TableReservation.
     *
     * @return Response
     */
    public function create()
    {
        $tableCategories = TableCategories::where('status',1)->get();
        return view('table_reservations.create')->with('tableCategories',$tableCategories);
    }

    /**
     * Store a newly created TableReservation in storage.
     *
     * @param CreateTableReservationRequest $request
     *
     * @return Response
     */
    public function store(CreateTableReservationRequest $request)
    {
        $input = $request->all();
        
        $tableReservation = $this->tableReservationRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/tableReservations.singular')]));

        return redirect(route('tableReservations.index'));
    }

    /**
     * Display the specified TableReservation.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tableReservation = $this->tableReservationRepository->find($id);

        if (empty($tableReservation)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tableReservations.singular')]));

            return redirect(route('tableReservations.index'));
        }

        return view('table_reservations.show')->with('tableReservation', $tableReservation);
    }

    /**
     * Show the form for editing the specified TableReservation.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tableReservation = $this->tableReservationRepository->find($id);


        $tableCategories = TableCategories::where('status',1)->get();

        if (empty($tableReservation)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tableReservations.singular')]));

            return redirect(route('tableReservations.index'));
        }

        return view('table_reservations.edit')->with('tableReservation', $tableReservation)->with('tableCategories',$tableCategories);
    }

    /**
     * Update the specified TableReservation in storage.
     *
     * @param int $id
     * @param UpdateTableReservationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTableReservationRequest $request)
    {
        $tableReservation = $this->tableReservationRepository->find($id);

        if (empty($tableReservation)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tableReservations.singular')]));

            return redirect(route('tableReservations.index'));
        }

        $tableReservation = $this->tableReservationRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/tableReservations.singular')]));

        return redirect(route('tableReservations.index'));
    }

    /**
     * Remove the specified TableReservation from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tableReservation = $this->tableReservationRepository->find($id);

        if (empty($tableReservation)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tableReservations.singular')]));

            return redirect(route('tableReservations.index'));
        }

        $this->tableReservationRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/tableReservations.singular')]));

        return redirect(route('tableReservations.index'));
    }
}
