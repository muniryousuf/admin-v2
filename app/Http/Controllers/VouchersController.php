<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVouchersRequest;
use App\Http\Requests\UpdateVouchersRequest;
use App\Repositories\VouchersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class VouchersController extends AppBaseController
{
    /** @var  VouchersRepository */
    private $vouchersRepository;

    public function __construct(VouchersRepository $vouchersRepo)
    {
        $this->vouchersRepository = $vouchersRepo;
    }

    /**
     * Display a listing of the Vouchers.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $vouchers = $this->vouchersRepository->all();

        return view('vouchers.index')
            ->with('vouchers', $vouchers);
    }

    /**
     * Show the form for creating a new Vouchers.
     *
     * @return Response
     */
    public function create()
    {
        return view('vouchers.create');
    }

    /**
     * Store a newly created Vouchers in storage.
     *
     * @param CreateVouchersRequest $request
     *
     * @return Response
     */
    public function store(CreateVouchersRequest $request)
    {
        $input = $request->all();
        if($input['coupon_limit'] == null){
            $input['coupon_limit'] = 999;
        }
        $vouchers = $this->vouchersRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/vouchers.singular')]));

        return redirect(route('vouchers.index'));
    }

    /**
     * Display the specified Vouchers.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $vouchers = $this->vouchersRepository->find($id);

        if (empty($vouchers)) {
            Flash::error(__('messages.not_found', ['model' => __('models/vouchers.singular')]));

            return redirect(route('vouchers.index'));
        }

        return view('vouchers.show')->with('vouchers', $vouchers);
    }

    /**
     * Show the form for editing the specified Vouchers.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $vouchers = $this->vouchersRepository->find($id);

        if (empty($vouchers)) {
            Flash::error(__('messages.not_found', ['model' => __('models/vouchers.singular')]));

            return redirect(route('vouchers.index'));
        }

        return view('vouchers.edit')->with('vouchers', $vouchers);
    }

    /**
     * Update the specified Vouchers in storage.
     *
     * @param int $id
     * @param UpdateVouchersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVouchersRequest $request)
    {
        $vouchers = $this->vouchersRepository->find($id);

        if (empty($vouchers)) {
            Flash::error(__('messages.not_found', ['model' => __('models/vouchers.singular')]));

            return redirect(route('vouchers.index'));
        }
        $inputs = $request->all();

        if($inputs['coupon_limit'] == null){
            $inputs['coupon_limit'] = 999;
        }
        $vouchers = $this->vouchersRepository->update($inputs, $id);

        Flash::success(__('messages.updated', ['model' => __('models/vouchers.singular')]));

        return redirect(route('vouchers.index'));
    }

    /**
     * Remove the specified Vouchers from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $vouchers = $this->vouchersRepository->find($id);

        if (empty($vouchers)) {
            Flash::error(__('messages.not_found', ['model' => __('models/vouchers.singular')]));

            return redirect(route('vouchers.index'));
        }

        $this->vouchersRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/vouchers.singular')]));

        return redirect(route('vouchers.index'));
    }
}
