<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductsRequest;
use App\Http\Requests\UpdateProductsRequest;
use App\Repositories\ProductsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\product_meta;
use App\Models\category;
use App\Models\Products;
use App\Models\ProductMetasBridge;

class ProductsController extends AppBaseController
{
    /** @var  ProductsRepository */
    private $productsRepository;

    public function __construct(ProductsRepository $productsRepo)
    {
        $this->productsRepository = $productsRepo;
    }

    /**
     * Display a listing of the Products.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        session()->forget('product-previous-url');
        $search = '';
        $products = new Products();
        $categories = category::all();
        if(isset($request->search)){
            $search = $request->search;
            $products = $products->where('name','like','%'.$search.'%');
        }
        if(isset($request->ids)){
            $exploded_ids  = explode('-',$request->ids);
            $products = $products->whereIn('id_category',$exploded_ids);

        }
        $products = $products->paginate(5)->appends(request()->query());
        return view('products.index')->with('products', $products)->with('categories',$categories)->with('search',$search);
    }

    /**
     * Show the form for creating a new Products.
     *
     * @return Response
     */
    public function create()
    {
        if(session()->get('product-previous-url') == null){
            session()->put('product-previous-url',url()->previous());
        }
        $meats = product_meta::get();
        $categories = category::get();
        $products_data = Products::without(['product_metas','product_metas_mobile','id_of_metas'])->get();
        return view('products.create',['meats'=>$meats,'categories'=> $categories,'products_data'=>$products_data]);
    }

    /**
     * Store a newly created Products in storage.
     *
     * @param CreateProductsRequest $request
     *
     * @return Response
     */
    public function store(CreateProductsRequest $request)
    {
        $input = $request->all();
        if(isset($request->image)){
            $image = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'), $image);
            $input['image'] = $image;
        }
        $products = $this->productsRepository->create($input);
        if(isset($input['id_metas'])){
            foreach ($input['id_metas'] as $key => $id_meta) {
             $product_metas_bridge = new ProductMetasBridge();
             $product_metas_bridge->id_product = $products->id;
             $product_metas_bridge->sort = $key;
             $product_metas_bridge->id_meta = $id_meta;
             $product_metas_bridge->save();
            }

        }
        Flash::success(__('messages.saved', ['model' => __('models/products.singular')]));
        $previous_url = session()->get('product-previous-url');
        return redirect($previous_url);
    }

    /**
     * Display the specified Products.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $products = $this->productsRepository->find($id);

        if (empty($products)) {
            Flash::error(__('messages.not_found', ['model' => __('models/products.singular')]));

            return redirect(route('products.index'));
        }

        return view('products.show')->with('products', $products);
    }

    /**
     * Show the form for editing the specified Products.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if(session()->get('product-previous-url') == null){
            session()->put('product-previous-url',url()->previous());
        }
        $products = $this->productsRepository->find($id);

        if (empty($products)) {
            Flash::error(__('messages.not_found', ['model' => __('models/products.singular')]));

            return redirect(route('products.index'));
        }
         $meats = product_meta::get();
         $products_data = Products::where('id','!=',$id)->without(['product_metas','product_metas_mobile','id_of_metas'])->get();
        $categories = category::get();
        return view('products.edit')->with('products', $products)->with('meats',$meats)->with('categories',$categories)->with('products_data',$products_data);
    }

    /**
     * Update the specified Products in storage.
     *
     * @param int $id
     * @param UpdateProductsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductsRequest $request)
    {
        $products = $this->productsRepository->find($id);

        if (empty($products)) {
            Flash::error(__('messages.not_found', ['model' => __('models/products.singular')]));

            return redirect(route('products.index'));
        }


        $input = $request->all();
        if(isset($request->image)){
            $image = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'), $image);
            $input['image'] = $image;

        }
        ProductMetasBridge::where('id_product',$id)->delete();
        if(isset($input['id_metas'])){
            foreach ($input['id_metas'] as $key => $id_meta) {
             $product_metas_bridge = new ProductMetasBridge();
             $product_metas_bridge->id_product = $products->id;
             $product_metas_bridge->sort = $key;
             $product_metas_bridge->id_meta = $id_meta;
             $product_metas_bridge->save();
            }

        }
        $products = $this->productsRepository->update($input, $id);
        \Artisan::call('cache:clear');

        Flash::success(__('messages.updated', ['model' => __('models/products.singular')]));

        $previous_url = session()->get('product-previous-url');
        return redirect($previous_url);
    }

    /**
     * Remove the specified Products from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $products = $this->productsRepository->find($id);

        if (empty($products)) {
            Flash::error(__('messages.not_found', ['model' => __('models/products.singular')]));

            return redirect(route('products.index'));
        }

        $this->productsRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/products.singular')]));

        return redirect()->back();
    }
}
