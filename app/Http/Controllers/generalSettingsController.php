<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreategeneralSettingsRequest;
use App\Http\Requests\UpdategeneralSettingsRequest;
use App\Repositories\generalSettingsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class generalSettingsController extends AppBaseController
{
    /** @var  generalSettingsRepository */
    private $generalSettingsRepository;

    public function __construct(generalSettingsRepository $generalSettingsRepo)
    {
        $this->generalSettingsRepository = $generalSettingsRepo;
    }

    /**
     * Display a listing of the general_settings.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $generalSettings = $this->generalSettingsRepository->all();

        return view('general_settings.index')
            ->with('generalSettings', $generalSettings);
    }

    /**
     * Show the form for creating a new general_settings.
     *
     * @return Response
     */
    public function create()
    {
        return view('general_settings.create');
    }

    /**
     * Store a newly created general_settings in storage.
     *
     * @param CreategeneralSettingsRequest $request
     *
     * @return Response
     */
    public function store(CreategeneralSettingsRequest $request)
    {
        $input = $request->all();
        $input['shop_status']  = isset($input['shop_status']) && $input['shop_status'] == 'on' ? 1 : 0;

        //uplaoding work header logo
        $imagefavIcon = time().'.'.$request->fav_icon->extension();
        $request->fav_icon->move(public_path('images'), $imagefavIcon);
        //uplaoding work header logo

        $input['fav_icon'] = $imagefavIcon;




        //uplaoding work header logo
        $imageNameHeader = time().'.'.$request->header_logo->extension();
        $request->header_logo->move(public_path('images'), $imageNameHeader);
        //uplaoding work header logo

        $input['header_logo'] = $imageNameHeader;


        //uplaoding work footer logo
        $imageNameFooter = time().'.'.$request->footer_logo->extension();
        $request->footer_logo->move(public_path('images'), $imageNameFooter);
        //uplaoding work footer logo

        $input['footer_logo'] = $imageNameFooter;



        if(isset($request->background_image)){
            //uplaoding work footer logo
            $backgroundImage = time().'.'.$request->background_image->extension();
            $request->background_image->move(public_path('images'), $backgroundImage);
            //uplaoding work footer logo

            $input['background_image'] = $backgroundImage;
        }

        $generalSettings = $this->generalSettingsRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/generalSettings.singular')]));

        return redirect(route('generalSettings.index'));
    }

    /**
     * Display the specified general_settings.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $generalSettings = $this->generalSettingsRepository->find($id);

        if (empty($generalSettings)) {
            Flash::error(__('messages.not_found', ['model' => __('models/generalSettings.singular')]));

            return redirect(route('generalSettings.index'));
        }

        return view('general_settings.show')->with('generalSettings', $generalSettings);
    }

    /**
     * Show the form for editing the specified general_settings.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $generalSettings = $this->generalSettingsRepository->find($id);

        if (empty($generalSettings)) {
            Flash::error(__('messages.not_found', ['model' => __('models/generalSettings.singular')]));

            return redirect(route('generalSettings.index'));
        }

        return view('general_settings.edit')->with('generalSettings', $generalSettings);
    }

    /**
     * Update the specified general_settings in storage.
     *
     * @param int $id
     * @param UpdategeneralSettingsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdategeneralSettingsRequest $request)
    {
        $generalSettings = $this->generalSettingsRepository->find($id);
        $inputs = $request->all();
        if (empty($generalSettings)) {
            Flash::error(__('messages.not_found', ['model' => __('models/generalSettings.singular')]));

            return redirect(route('generalSettings.index'));
        }
        $inputs['shop_status']  = (isset($inputs['shop_status']) && $inputs['shop_status'] == 'on') ? 1 : 0;
        if(isset($inputs['fav_icon'])){
            //uplaoding work header logo
            $imagefavIcon = time().'.'.$request->fav_icon->extension();
            $request->fav_icon->move(public_path('images'), $imagefavIcon);
            //uplaoding work header logo

            $inputs['fav_icon'] = $imagefavIcon;
        }
        if(isset($inputs['header_logo'])){
            //uplaoding work header logo
            $imageNameHeader = time().'.'.$request->header_logo->extension();
            $request->header_logo->move(public_path('images'), $imageNameHeader);
            //uplaoding work header logo

            $inputs['header_logo'] = $imageNameHeader;
        }

        if(isset($inputs['footer_logo'])){
            //uplaoding work footer logo
            $imageNameFooter = time().'.'.$request->footer_logo->extension();
            $request->footer_logo->move(public_path('images'), $imageNameFooter);
            //uplaoding work footer logo

            $inputs['footer_logo'] = $imageNameFooter;
        }

        if(isset($request->background_image)){
            //uplaoding work footer logo
            $backgroundImage = time().'.'.$request->background_image->extension();
            $request->background_image->move(public_path('images'), $backgroundImage);
            //uplaoding work footer logo

            $inputs['background_image'] = $backgroundImage;
        }
        if(isset($inputs['rider_will_deliver'])){
             $inputs['rider_will_deliver'] = $request->rider_will_deliver == 'on' ? 1 : 0;
        }else{
             $inputs['rider_will_deliver'] = 0;
        }
        // if(isset($inputs['auto_rotation'])){
        //      $inputs['auto_rotation'] = $request->auto_rotation == 'on' ? 1 : 0;
        // }else{
        //      $inputs['auto_rotation'] = 0;
        // }

        $generalSettings = $this->generalSettingsRepository->update($inputs, $id);

        Flash::success(__('messages.updated', ['model' => __('models/generalSettings.singular')]));

        return redirect(route('generalSettings.index'));
    }

    /**
     * Remove the specified general_settings from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $generalSettings = $this->generalSettingsRepository->find($id);

        if (empty($generalSettings)) {
            Flash::error(__('messages.not_found', ['model' => __('models/generalSettings.singular')]));

            return redirect(route('generalSettings.index'));
        }

        $this->generalSettingsRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/generalSettings.singular')]));

        return redirect(route('generalSettings.index'));
    }
}
