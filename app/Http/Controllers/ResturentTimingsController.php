<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateResturentTimingsRequest;
use App\Http\Requests\UpdateResturentTimingsRequest;
use App\Repositories\ResturentTimingsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ResturentTimingsController extends AppBaseController
{
    /** @var  ResturentTimingsRepository */
    private $resturentTimingsRepository;

    public function __construct(ResturentTimingsRepository $resturentTimingsRepo)
    {
        $this->resturentTimingsRepository = $resturentTimingsRepo;
    }

    /**
     * Display a listing of the ResturentTimings.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $resturentTimings = $this->resturentTimingsRepository->all();

        return view('resturent_timings.index')
            ->with('resturentTimings', $resturentTimings);
    }

    /**
     * Show the form for creating a new ResturentTimings.
     *
     * @return Response
     */
    public function create()
    {
        return view('resturent_timings.create');
    }

    /**
     * Store a newly created ResturentTimings in storage.
     *
     * @param CreateResturentTimingsRequest $request
     *
     * @return Response
     */
    public function store(CreateResturentTimingsRequest $request)
    {
        $input = $request->all();

         
        if(isset($input['shop_close']) && $input['shop_close'] == 1){
            $input['shop_close'] = 0;
        }else{
            $input['shop_close'] = 1;
        }


        $resturentTimings = $this->resturentTimingsRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/resturentTimings.singular')]));

        return redirect(route('resturentTimings.index'));
    }

    /**
     * Display the specified ResturentTimings.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $resturentTimings = $this->resturentTimingsRepository->find($id);

        if (empty($resturentTimings)) {
            Flash::error(__('messages.not_found', ['model' => __('models/resturentTimings.singular')]));

            return redirect(route('resturentTimings.index'));
        }

        return view('resturent_timings.show')->with('resturentTimings', $resturentTimings);
    }

    /**
     * Show the form for editing the specified ResturentTimings.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $resturentTimings = $this->resturentTimingsRepository->find($id);
        
        if (empty($resturentTimings)) {
            Flash::error(__('messages.not_found', ['model' => __('models/resturentTimings.singular')]));

            return redirect(route('resturentTimings.index'));
        }

        return view('resturent_timings.edit')->with('resturentTimings', $resturentTimings);
    }

    /**
     * Update the specified ResturentTimings in storage.
     *
     * @param int $id
     * @param UpdateResturentTimingsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResturentTimingsRequest $request)
    {
        $resturentTimings = $this->resturentTimingsRepository->find($id);

        if (empty($resturentTimings)) {
            Flash::error(__('messages.not_found', ['model' => __('models/resturentTimings.singular')]));

            return redirect(route('resturentTimings.index'));
        }
        $inputs = $request->all();
        if(isset($inputs['shop_close']) && $inputs['shop_close'] == 1){
            $inputs['shop_close'] = 0;
        }else{
            $inputs['shop_close'] = 1;
        }
        $resturentTimings = $this->resturentTimingsRepository->update($inputs, $id);

        Flash::success(__('messages.updated', ['model' => __('models/resturentTimings.singular')]));

        return redirect(route('resturentTimings.index'));
    }

    /**
     * Remove the specified ResturentTimings from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $resturentTimings = $this->resturentTimingsRepository->find($id);

        if (empty($resturentTimings)) {
            Flash::error(__('messages.not_found', ['model' => __('models/resturentTimings.singular')]));

            return redirect(route('resturentTimings.index'));
        }

        $this->resturentTimingsRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/resturentTimings.singular')]));

        return redirect(route('resturentTimings.index'));
    }
}
