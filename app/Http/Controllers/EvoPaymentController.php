<?php

namespace App\Http\Controllers;

use App\Models\OrderDetail;
use App\Repositories\CmsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;

class EvoPaymentController extends AppBaseController
{
    public function evoCallback( Request $request){

        $detail['extras'] = json_encode($request->all());
        $detail['order_id'] = 22222222222222;
        $detail['product_id'] =1;
        $detail['product_name'] ='evoPayment';
        $detail['price'] =1;
        $detail['quatity'] =1;
        \App\Data\Models\OrderDetail::create($detail);
        return $request;
    }
}
