<?php

namespace App\Http\Controllers;

use App\Models\PaymentMethods;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use App\Repositories\sliderRepository;
use App\Repositories\CmsRepository;
use App\Repositories\GalleryRepository;
use App\Repositories\CartRepository;
use App\Models\Admins;
use App\Models\User;
use App\Models\ResturentTimings;
use App\Models\category;
use App\Models\ResturentAddress;
use App\Models\orders;
use Auth;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Hash;
use App\Data\Models\UserAddress;
use App\Models\Reviews;
use PDF;
use Illuminate\Support\Facades\Storage;
use App\Models\Locations;
use App\Models\LocationsAttributes;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function home(){
        $sliders = app(sliderRepository::class)->all();
        $about_us = app(CmsRepository::class)->model()::where('slug','about-us')->first();
        $gallery = app(GalleryRepository::class)->all();
        return view('welcome',compact('sliders','about_us','gallery'));
    }

    public function OrderOnline(){
        return view('order-online');
    }
    public function thankYou(){
        return view('thank-you');
    }

    public function adminLogin(){
        return view('auth.admin-login');
    }

    public function adminLoginRequest(Request $request){
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            return redirect()->intended('/admin/home');
        }else{
            return back()->withErrors(['msg' => 'invalid credentials'])->withInput($request->only('email', 'remember'));
        }
    }
    public function adminLogout(){
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }
    public function adminEditProfile(Request $request){
        $admin = Admins::find(Auth::guard('admin')->id());

        $params = $request->all();
        if(isset($request->photo)){
            $photo = time().'.'.$request->photo->extension();
            $request->photo->move(public_path('images'), $photo);
            $admin->image = $photo;
        }

        $admin->name = $params['name'];
        $admin->save();
        return response()->json(['success'=>true], Response::HTTP_OK);
    }
    public function adminEditPassword(Request $request){


        $this->validate($request, [
            'password_current' => 'required',
            'password' => 'required',
        ]);
       $hashedPassword = Auth::user()->password;

        if (\Hash::check($request->password_current , $hashedPassword )) {

            $admin = Admins::find(Auth::guard('admin')->id());
            $admin->password = Hash::make($request->password);
            $admin->save();

        }else{
            session()->flash('message','old password doesnt matched ');
        }
        return redirect()->back();

    }

    public function myAccount(){
        return view('my-account-area');
    }
    public function changePassword(){
        return view('change-password-area');
    }

    public function userDetailsUpdate(Request $request){
        $params = $request->all();
        $user = User::find(Auth::id());
        $user->name = $params['first_name'];
        $user->lname = $params['last_name'];
        $user->phone = $params['phone'];
        $user->save();
        return back();
    }

    public function userRegister(Request $request){
        $validated = $request->validate([
            'email' => 'required|email|unique:users,email'
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->phone_number = $request->phone;


        if(!isset($request->guest)){
            $user->lname = $request->lname;
            $user->password = Hash::make($request->password);
        }

        $user->save();
        Auth::login($user);
        return response()->json(['success'=>true], Response::HTTP_OK);
    }

    public function userPasswordUpdate(Request $request){
        $validated = $request->validate([
            'password'              => 'required | confirmed ',
            'password_confirmation' => 'required ',
        ]);
        $user = User::find(Auth::id());
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect()->route('user.my-account');

    }
    public function contactUs(){
        $settings = getSettings();
        $restaurant_timings  = ResturentTimings::get();
        $categories  = category::get();
        $restaurant  = ResturentAddress::first();
        $contact_us = app(CmsRepository::class)->model()::where('slug','contact-us')->first();
        return view('contact-us',[
            'settings'=>$settings,
            'restaurant_timings'=>$restaurant_timings,
            'categories'=>$categories,
            'restaurant' => $restaurant,
            'contact_us' => $contact_us,
        ]);
    }
    public function getCurrentStatusValue($status){
        $current_value = 0;
        switch ($status) {
            case 'accepted':
                $current_value = 1;
                break;
            case 'preparing':
                $current_value = 2;
                break;
            case 'prepared':
                $current_value = 3;
                break;
            case 'completed':
                $current_value = 4;
                break;
            case 'pending':
                $current_value = 0;
                break;
        }
        return $current_value;
    }
    public function checkout($reference){


        $order = orders::with(['order_details','table_detail'])->where('reference',$reference)->first();


        $current_status = $this->getCurrentStatusValue($order->status);
        return view('checkout', ['order'=>$order,'current_status'=>$current_status]);
    }
    public function orderHistory(){
        $orders = orders::where('user_id',Auth::id())->paginate(3);
        return view('order-history',['orders'=>$orders]);
    }
    public function placeOrder(Request $request)
    {
        $order = app(CartRepository::class)->placeOrder($request->all());
        return response()->json(['data'=>$order], Response::HTTP_OK);
    }
    Public function userAddresses(){
        $user_address = UserAddress::where('user_id',Auth::id())->paginate(3);
        return view('user-addresses',['user_address'=>$user_address]);
    }
    public function reviews($ratings=5){
        $reviews = Reviews::where('rating',$ratings)->get();
        $reviews_count_array = [];
        for ($i=1; $i <= 5; $i++) {
            $reviews_count_array[$i] = Reviews::where('rating',$i)->count();
        }

        return view('reviews',['reviews'=>$reviews,'ratings'=>$ratings,'reviews_count_array' =>$reviews_count_array   ]);
    }

    public function paymentConfirm(){

        $paymentMethods = PaymentMethods::where('active', 1)->get();
        $settings = getSettings();
        if($settings['rider_will_deliver'] == 1){
            $paymentMethods = [];
            $class = new \stdClass();
            $class->id=1;
            $class->name='Card';
            $paymentMethods[0] = $class;
        }

        $months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

        $currentYear = Carbon::now()->format('Y');

        $expirationYears = [];
        for ($i=$currentYear; $i < $currentYear+9; $i++) {
            $expirationYears[] = $i;
        }

        return view('payment-confirm', ['paymentMethods' => $paymentMethods, 'months' => $months, 'expiration_years' => $expirationYears,'settings'=>$settings]);
    }
    public function getCartId(Request $request){
        return orders::where('reference',$request->ref)->first();
    }

    public function downloadPdf($reference){
        $order = orders::where('reference',$reference)->first();
        $settings = getSettings();

        // return view('mails.order-recipt',['order'=>$order,'settings'=>$settings]);
        $pdf = PDF::loadView('mails.recipt',['order'=>$order,'settings'=>$settings]);
        return $pdf->download('order.pdf');
    }
    public function viewPdf($reference){

        $filename = $reference.'.pdf';

         $order = orders::where('reference',$reference)->first();
        $settings = getSettings();

        // return view('mails.order-recipt',['order'=>$order,'settings'=>$settings]);
        return view('mails.recipt',['order'=>$order,'settings'=>$settings]);

        $path = asset('storage/pdf/'.$reference.'.pdf');
        return \Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"'
        ]);
    }


    public function reservations(){
        return view('reservations');
    }
    public function locations($id = 0){
        if($id > 0){
            $locations = LocationsAttributes::where('id_location',$id)->get();
        }else{
            $locations = Locations::all();
        }
        return view('locations')->with('locations',$locations);
    }


    public function getOrdersOfUser($id){
        $orders = orders::where('user_id',$id)->get();
        return ['message'=>'orders are fetched' , 'data'=>$orders];
    }

    public function getAddressOfUser($id){
        $orders = UserAddress::where('user_id',$id)->get();
        return ['message'=>'addresses are fetched' , 'data'=>$orders];
    }



}
