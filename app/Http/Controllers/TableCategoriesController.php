<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTableCategoriesRequest;
use App\Http\Requests\UpdateTableCategoriesRequest;
use App\Repositories\TableCategoriesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class TableCategoriesController extends AppBaseController
{
    /** @var  TableCategoriesRepository */
    private $tableCategoriesRepository;

    public function __construct(TableCategoriesRepository $tableCategoriesRepo)
    {
        $this->tableCategoriesRepository = $tableCategoriesRepo;
    }

    /**
     * Display a listing of the TableCategories.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tableCategories = $this->tableCategoriesRepository->all();

        return view('table_categories.index')
            ->with('tableCategories', $tableCategories);
    }

    /**
     * Show the form for creating a new TableCategories.
     *
     * @return Response
     */
    public function create()
    {
        return view('table_categories.create');
    }

    /**
     * Store a newly created TableCategories in storage.
     *
     * @param CreateTableCategoriesRequest $request
     *
     * @return Response
     */
    public function store(CreateTableCategoriesRequest $request)
    {
        $input = $request->all();

        $tableCategories = $this->tableCategoriesRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/tableCategories.singular')]));

        return redirect(route('tableCategories.index'));
    }

    /**
     * Display the specified TableCategories.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tableCategories = $this->tableCategoriesRepository->find($id);

        if (empty($tableCategories)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tableCategories.singular')]));

            return redirect(route('tableCategories.index'));
        }

        return view('table_categories.show')->with('tableCategories', $tableCategories);
    }

    /**
     * Show the form for editing the specified TableCategories.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tableCategories = $this->tableCategoriesRepository->find($id);

        if (empty($tableCategories)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tableCategories.singular')]));

            return redirect(route('tableCategories.index'));
        }

        return view('table_categories.edit')->with('tableCategories', $tableCategories);
    }

    /**
     * Update the specified TableCategories in storage.
     *
     * @param int $id
     * @param UpdateTableCategoriesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTableCategoriesRequest $request)
    {
        $tableCategories = $this->tableCategoriesRepository->find($id);

        if (empty($tableCategories)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tableCategories.singular')]));

            return redirect(route('tableCategories.index'));
        }

        $tableCategories = $this->tableCategoriesRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/tableCategories.singular')]));

        return redirect(route('tableCategories.index'));
    }

    /**
     * Remove the specified TableCategories from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tableCategories = $this->tableCategoriesRepository->find($id);

        if (empty($tableCategories)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tableCategories.singular')]));

            return redirect(route('tableCategories.index'));
        }

        $this->tableCategoriesRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/tableCategories.singular')]));

        return redirect(route('tableCategories.index'));
    }
}
