<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLocationsAttributesRequest;
use App\Http\Requests\UpdateLocationsAttributesRequest;
use App\Repositories\LocationsAttributesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Locations;
use Flash;
use Response;

class LocationsAttributesController extends AppBaseController
{
    /** @var  LocationsAttributesRepository */
    private $locationsAttributesRepository;

    public function __construct(LocationsAttributesRepository $locationsAttributesRepo)
    {
        $this->locationsAttributesRepository = $locationsAttributesRepo;
    }

    /**
     * Display a listing of the LocationsAttributes.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $locationsAttributes = $this->locationsAttributesRepository->all();

        return view('locations_attributes.index')
            ->with('locationsAttributes', $locationsAttributes);
    }

    /**
     * Show the form for creating a new LocationsAttributes.
     *
     * @return Response
     */
    public function create()
    {
        $locations = Locations::all();
        return view('locations_attributes.create')->with('locations',$locations);
    }

    /**
     * Store a newly created LocationsAttributes in storage.
     *
     * @param CreateLocationsAttributesRequest $request
     *
     * @return Response
     */
    public function store(CreateLocationsAttributesRequest $request)
    {
        $input = $request->all();

        $locationsAttributes = $this->locationsAttributesRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/locationsAttributes.singular')]));

        return redirect(route('locationsAttributes.index'));
    }

    /**
     * Display the specified LocationsAttributes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $locationsAttributes = $this->locationsAttributesRepository->find($id);

        if (empty($locationsAttributes)) {
            Flash::error(__('messages.not_found', ['model' => __('models/locationsAttributes.singular')]));

            return redirect(route('locationsAttributes.index'));
        }

        return view('locations_attributes.show')->with('locationsAttributes', $locationsAttributes);
    }

    /**
     * Show the form for editing the specified LocationsAttributes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $locationsAttributes = $this->locationsAttributesRepository->find($id);
        $locations = Locations::all();

        if (empty($locationsAttributes)) {
            Flash::error(__('messages.not_found', ['model' => __('models/locationsAttributes.singular')]));

            return redirect(route('locationsAttributes.index'));
        }

        return view('locations_attributes.edit')->with('locationsAttributes', $locationsAttributes)->with('locations',$locations);
    }

    /**
     * Update the specified LocationsAttributes in storage.
     *
     * @param int $id
     * @param UpdateLocationsAttributesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLocationsAttributesRequest $request)
    {
        $locationsAttributes = $this->locationsAttributesRepository->find($id);

        if (empty($locationsAttributes)) {
            Flash::error(__('messages.not_found', ['model' => __('models/locationsAttributes.singular')]));

            return redirect(route('locationsAttributes.index'));
        }

        $locationsAttributes = $this->locationsAttributesRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/locationsAttributes.singular')]));

        return redirect(route('locationsAttributes.index'));
    }

    /**
     * Remove the specified LocationsAttributes from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $locationsAttributes = $this->locationsAttributesRepository->find($id);

        if (empty($locationsAttributes)) {
            Flash::error(__('messages.not_found', ['model' => __('models/locationsAttributes.singular')]));

            return redirect(route('locationsAttributes.index'));
        }

        $this->locationsAttributesRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/locationsAttributes.singular')]));

        return redirect(route('locationsAttributes.index'));
    }
}
