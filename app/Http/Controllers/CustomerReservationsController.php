<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerReservationsRequest;
use App\Http\Requests\UpdateCustomerReservationsRequest;
use App\Repositories\CustomerReservationsRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\CustomerReservations;
use Illuminate\Http\Request;
use Flash;
use Response;

class CustomerReservationsController extends AppBaseController
{
    /** @var  CustomerReservationsRepository */
    private $customerReservationsRepository;

    public function __construct(CustomerReservationsRepository $customerReservationsRepo)
    {
        $this->customerReservationsRepository = $customerReservationsRepo;
    }

    /**
     * Display a listing of the CustomerReservations.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $customerReservations =CustomerReservations::orderBy('id','desc')->get();

        return view('customer_reservations.index')
            ->with('customerReservations', $customerReservations);
    }

    /**
     * Show the form for creating a new CustomerReservations.
     *
     * @return Response
     */
    public function create()
    {
        return view('customer_reservations.create');
    }

    /**
     * Store a newly created CustomerReservations in storage.
     *
     * @param CreateCustomerReservationsRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerReservationsRequest $request)
    {
        $input = $request->all();

        $customerReservations = $this->customerReservationsRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/customerReservations.singular')]));

        return redirect(route('customerReservations.index'));
    }

    /**
     * Display the specified CustomerReservations.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customerReservations = $this->customerReservationsRepository->find($id);

        if (empty($customerReservations)) {
            Flash::error(__('messages.not_found', ['model' => __('models/customerReservations.singular')]));

            return redirect(route('customerReservations.index'));
        }

        return view('customer_reservations.show')->with('customerReservations', $customerReservations);
    }

    /**
     * Show the form for editing the specified CustomerReservations.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customerReservations = $this->customerReservationsRepository->find($id);

        if (empty($customerReservations)) {
            Flash::error(__('messages.not_found', ['model' => __('models/customerReservations.singular')]));

            return redirect(route('customerReservations.index'));
        }

        return view('customer_reservations.edit')->with('customerReservations', $customerReservations);
    }

    /**
     * Update the specified CustomerReservations in storage.
     *
     * @param int $id
     * @param UpdateCustomerReservationsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerReservationsRequest $request)
    {
        $customerReservations = $this->customerReservationsRepository->find($id);

        if (empty($customerReservations)) {
            Flash::error(__('messages.not_found', ['model' => __('models/customerReservations.singular')]));

            return redirect(route('customerReservations.index'));
        }

        $customerReservations = $this->customerReservationsRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/customerReservations.singular')]));

        return redirect(route('customerReservations.index'));
    }

    /**
     * Remove the specified CustomerReservations from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customerReservations = $this->customerReservationsRepository->find($id);

        if (empty($customerReservations)) {
            Flash::error(__('messages.not_found', ['model' => __('models/customerReservations.singular')]));

            return redirect(route('customerReservations.index'));
        }

        $this->customerReservationsRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/customerReservations.singular')]));

        return redirect(route('customerReservations.index'));
    }
}
