<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateourStoryRequest;
use App\Http\Requests\UpdateourStoryRequest;
use App\Repositories\ourStoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ourStoryController extends AppBaseController
{
    /** @var  ourStoryRepository */
    private $ourStoryRepository;

    public function __construct(ourStoryRepository $ourStoryRepo)
    {
        $this->ourStoryRepository = $ourStoryRepo;
    }

    /**
     * Display a listing of the ourStory.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $ourStories = $this->ourStoryRepository->all();

        return view('our_stories.index')
            ->with('ourStories', $ourStories);
    }

    /**
     * Show the form for creating a new ourStory.
     *
     * @return Response
     */
    public function create()
    {
        return view('our_stories.create');
    }

    /**
     * Store a newly created ourStory in storage.
     *
     * @param CreateourStoryRequest $request
     *
     * @return Response
     */
    public function store(CreateourStoryRequest $request)
    {
        $input = $request->all();

        $image = time().'.'.$request->image->extension();  
        $request->image->move(public_path('images'), $image);
        $input['image'] = $image;


        $ourStory = $this->ourStoryRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/ourStories.singular')]));

        return redirect(route('ourStories.index'));
    }

    /**
     * Display the specified ourStory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ourStory = $this->ourStoryRepository->find($id);

        if (empty($ourStory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/ourStories.singular')]));

            return redirect(route('ourStories.index'));
        }

        return view('our_stories.show')->with('ourStory', $ourStory);
    }

    /**
     * Show the form for editing the specified ourStory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ourStory = $this->ourStoryRepository->find($id);

        if (empty($ourStory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/ourStories.singular')]));

            return redirect(route('ourStories.index'));
        }

        return view('our_stories.edit')->with('ourStory', $ourStory);
    }

    /**
     * Update the specified ourStory in storage.
     *
     * @param int $id
     * @param UpdateourStoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateourStoryRequest $request)
    {
        $ourStory = $this->ourStoryRepository->find($id);

        if (empty($ourStory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/ourStories.singular')]));

            return redirect(route('ourStories.index'));
        }

        $inputs = $request->all();
        if(isset($request->image)){
            $image = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images'), $image);
            $inputs['image'] = $image;
        }


        $ourStory = $this->ourStoryRepository->update($inputs, $id);

        Flash::success(__('messages.updated', ['model' => __('models/ourStories.singular')]));

        return redirect(route('ourStories.index'));
    }

    /**
     * Remove the specified ourStory from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ourStory = $this->ourStoryRepository->find($id);

        if (empty($ourStory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/ourStories.singular')]));

            return redirect(route('ourStories.index'));
        }

        $this->ourStoryRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/ourStories.singular')]));

        return redirect(route('ourStories.index'));
    }
}
