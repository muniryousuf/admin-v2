<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatedeliveryChargesDetailsRequest;
use App\Http\Requests\UpdatedeliveryChargesDetailsRequest;
use App\Repositories\deliveryChargesDetailsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\DeliveryCharges;

class deliveryChargesDetailsController extends AppBaseController
{
    /** @var  deliveryChargesDetailsRepository */
    private $deliveryChargesDetailsRepository;

    public function __construct(deliveryChargesDetailsRepository $deliveryChargesDetailsRepo)
    {
        $this->deliveryChargesDetailsRepository = $deliveryChargesDetailsRepo;
    }

    /**
     * Display a listing of the deliveryChargesDetails.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryChargesDetails = $this->deliveryChargesDetailsRepository->paginate(10);

        return view('delivery_charges_details.index')
            ->with('deliveryChargesDetails', $deliveryChargesDetails);
    }

    /**
     * Show the form for creating a new deliveryChargesDetails.
     *
     * @return Response
     */
    public function create()
    {
        return view('delivery_charges_details.create');
    }

    /**
     * Store a newly created deliveryChargesDetails in storage.
     *
     * @param CreatedeliveryChargesDetailsRequest $request
     *
     * @return Response
     */
    public function store(CreatedeliveryChargesDetailsRequest $request)
    {
        $input = $request->all();

        $DeliveryCharges = DeliveryCharges::create($input);
        $input['delivery_id'] = $DeliveryCharges->id;
        $input['miles'] = $input['miles'] > 0 ? $input['miles'] : 0;
        $input['fix_delivery_charges'] = $input['fix_delivery_charges'] > 0 ? $input['fix_delivery_charges'] : 0;
        $deliveryChargesDetails = $this->deliveryChargesDetailsRepository->create($input);
        
        

        Flash::success(__('messages.saved', ['model' => __('models/deliveryChargesDetails.singular')]));

        return redirect(route('deliveryChargesDetails.index'));
    }

    /**
     * Display the specified deliveryChargesDetails.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $deliveryChargesDetails = $this->deliveryChargesDetailsRepository->find($id);

        if (empty($deliveryChargesDetails)) {
            Flash::error(__('messages.not_found', ['model' => __('models/deliveryChargesDetails.singular')]));

            return redirect(route('deliveryChargesDetails.index'));
        }

        return view('delivery_charges_details.show')->with('deliveryChargesDetails', $deliveryChargesDetails);
    }

    /**
     * Show the form for editing the specified deliveryChargesDetails.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $deliveryChargesDetails = $this->deliveryChargesDetailsRepository->find($id);

        if (empty($deliveryChargesDetails)) {
            Flash::error(__('messages.not_found', ['model' => __('models/deliveryChargesDetails.singular')]));

            return redirect(route('deliveryChargesDetails.index'));
        }

        return view('delivery_charges_details.edit')->with('deliveryChargesDetails', $deliveryChargesDetails);
    }

    /**
     * Update the specified deliveryChargesDetails in storage.
     *
     * @param int $id
     * @param UpdatedeliveryChargesDetailsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatedeliveryChargesDetailsRequest $request)
    {
        $deliveryChargesDetails = $this->deliveryChargesDetailsRepository->find($id);

        if (empty($deliveryChargesDetails)) {
            Flash::error(__('messages.not_found', ['model' => __('models/deliveryChargesDetails.singular')]));

            return redirect(route('deliveryChargesDetails.index'));
        }
        $DeliveryCharges = DeliveryCharges::find($id);
        if($DeliveryCharges){
            $DeliveryCharges->delivery_types = $request->delivery_types;
            $DeliveryCharges->save();
        }
        $inputs = $request->all();
        $inputs['delivery_id'] = $id;
        $inputs['miles'] = $inputs['miles'] > 0 ? $inputs['miles'] : 0;
        $inputs['fix_delivery_charges'] = $inputs['fix_delivery_charges'] > 0 ? $inputs['fix_delivery_charges'] : 0;




        $deliveryChargesDetails = $this->deliveryChargesDetailsRepository->update($inputs, $id);

        Flash::success(__('messages.updated', ['model' => __('models/deliveryChargesDetails.singular')]));

        return redirect(route('deliveryChargesDetails.index'));
    }

    /**
     * Remove the specified deliveryChargesDetails from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deliveryChargesDetails = $this->deliveryChargesDetailsRepository->find($id);

        if (empty($deliveryChargesDetails)) {
            Flash::error(__('messages.not_found', ['model' => __('models/deliveryChargesDetails.singular')]));

            return redirect(route('deliveryChargesDetails.index'));
        }

        $this->deliveryChargesDetailsRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/deliveryChargesDetails.singular')]));

        return redirect(route('deliveryChargesDetails.index'));
    }
}
