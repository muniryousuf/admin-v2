<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateordersRequest;
use App\Http\Requests\UpdateordersRequest;
use App\Repositories\ordersRepository;
use App\Models\orders;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ordersController extends AppBaseController
{
    /** @var  ordersRepository */
    private $ordersRepository;

    public function __construct(ordersRepository $ordersRepo)
    {
        $this->ordersRepository = $ordersRepo;
    }

    /**
     * Display a listing of the orders.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orders = orders::orderBy('id','Desc');
        if(isset($request->searchBy) && isset($request->keyword)){
            $orders = $orders->where($request->searchBy,$request->keyword);  
        }
        // dd($orders->toSql());
        $orders = $orders->paginate(10);
        
        return view('orders.index')->with('orders', $orders);
    }

    /**
     * Show the form for creating a new orders.
     *
     * @return Response
     */
    public function create()
    {
        return view('orders.create');
    }

    /**
     * Store a newly created orders in storage.
     *
     * @param CreateordersRequest $request
     *
     * @return Response
     */
    public function store(CreateordersRequest $request)
    {
        $input = $request->all();

        $orders = $this->ordersRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/orders.singular')]));

        return redirect(route('orders.index'));
    }

    /**
     * Display the specified orders.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orders = $this->ordersRepository->find($id);

        if (empty($orders)) {
            Flash::error(__('messages.not_found', ['model' => __('models/orders.singular')]));

            return redirect(route('orders.index'));
        }

        return view('orders.show')->with('orders', $orders);
    }

    /**
     * Show the form for editing the specified orders.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $order = $this->ordersRepository->findOrderWithDetails($id);

        if (empty($order)) {
            Flash::error(__('messages.not_found', ['model' => __('models/orders.singular')]));

            return redirect(route('orders.index'));
        }

        return view('orders.edit')->with('order', $order);
    }

    /**
     * Update the specified orders in storage.
     *
     * @param int $id
     * @param UpdateordersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateordersRequest $request)
    {
        $orders = $this->ordersRepository->find($id);

        if (empty($orders)) {
            Flash::error(__('messages.not_found', ['model' => __('models/orders.singular')]));

            return redirect(route('orders.index'));
        }
        $orders = $this->ordersRepository->update($request->all(), $id);
        
        \Mail::to($orders->user_detail->email ?? '')->send(new \App\Mail\OrderPlace($orders->reference));

        Flash::success(__('messages.updated', ['model' => __('models/orders.singular')]));

        return redirect(route('orders.index'));
    }

    /**
     * Remove the specified orders from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orders = $this->ordersRepository->find($id);

        if (empty($orders)) {
            Flash::error(__('messages.not_found', ['model' => __('models/orders.singular')]));

            return redirect(route('orders.index'));
        }

        $this->ordersRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/orders.singular')]));

        return redirect(route('orders.index'));
    }
}
