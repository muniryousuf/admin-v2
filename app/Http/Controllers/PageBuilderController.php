<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Pages;
use File;



class PageBuilderController extends AppBaseController
{


    private function readFiles($pages){
        
        $path = public_path('assets/builder/demo');

        $dir = scandir($path);
            foreach($pages as $page){
                $exist = false;
                $name = $page['name'].'.html';
                foreach($dir as $file){
                    if($file != '.' && $file != '..' && $file != null ){
                        if($name == $file){
                            $exist = true;  
                        }
                    }   
                }
                if(!$exist){
                    $f = fopen($path.'/'.$name, 'wb');
                    fwrite($f, $page['html']);
                    fclose($f);
                }
            }
    }

    public function index(){
        $pages = Pages::all()->toArray();
        $this->readFiles($pages);

        $pages_array = [];
        foreach ($pages as $key => $value) {
            $pages_array[$key]['name'] = $value['name'];
            $pages_array[$key]['title'] = $value['name'];
            $pages_array[$key]['url'] = asset('assets/builder/demo/'.$value['name'].'.html');
            $pages_array[$key]['file'] = asset('assets/builder/demo/'.$value['name'].'.html');
            $pages_array[$key]['assets'] = [asset('assets/builder/demo/'.$value['name'].'.js')];
        }
        return view('page_builder.index',['pages'=>json_encode($pages_array)]);
    }

    public function save(Request $request){
        $request_data = $request->all();
        $file_name = explode('/',$request_data['file']);
        $page_name =   str_replace('.html', '', end($file_name));
        $html = $request_data['html'];
        // $without_header = explode('<!-----header----->',$html)[1];
        // $without_footer = explode('<!-----footer----->',$html)[0];
        Pages::updateOrCreate(
            ['name' =>$page_name ],
            ['html'=> $html , 'name' => $page_name ]
        );
    }

    public function getPage($name){
        $page = Pages::where('name',$name)->first();
        return view('pages',compact('page'));
    }
    public function sanitizeFileName($file){
        $file = preg_replace('@\?.*$@' , '', preg_replace('@\.{2,}@' , '', preg_replace('@[^\/\\a-zA-Z0-9\-\._]@', '', $file)));
        return $file;
    }

    public function upload(Request $request){
        $input = $request->all();
        $image = time().'.'.$request->file->extension();  
        $request->file->move(public_path('images'), $image);

        echo $image;
    }
    public function scan(){
        $scandir = public_path('images');
        // Run the recursive function
        // This function scans the files folder recursively, and builds a large array

        $scan = function ($dir) use ($scandir, &$scan) {
            $files = [];

            // Is there actually such a folder/file?

            if (file_exists($dir)) {
                foreach (scandir($dir) as $f) {
                    if (! $f || $f[0] == '.') {
                        continue; // Ignore hidden files
                    }

                    if (is_dir($dir . '/' . $f)) {
                        // The path is a folder

                        $files[] = [
                            'name'  => $f,
                            'type'  => 'folder',
                            'path'  => str_replace($scandir, '', $dir) . '/' . $f,
                            'items' => $scan($dir . '/' . $f), // Recursively get the contents of the folder
                        ];
                    } else {
                        // It is a file

                        $files[] = [
                            'name' => $f,
                            'type' => 'file',
                            'path' => str_replace($scandir, '', $dir) . '/' . $f,
                            'size' => filesize($dir . '/' . $f), // Gets the size of this file
                        ];
                    }
                }
            }

            return $files;
        };

        $response = $scan($scandir);

        // Output the directory listing as JSON

        header('Content-type: application/json');

        echo json_encode([
            'name'  => '',
            'type'  => 'folder',
            'path'  => '',
            'items' => $response,
        ]);
    }

}
