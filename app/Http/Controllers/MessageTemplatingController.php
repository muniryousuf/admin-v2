<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMessageTemplatingRequest;
use App\Http\Requests\UpdateMessageTemplatingRequest;
use App\Repositories\MessageTemplatingRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class MessageTemplatingController extends AppBaseController
{
    /** @var  MessageTemplatingRepository */
    private $messageTemplatingRepository;

    public function __construct(MessageTemplatingRepository $messageTemplatingRepo)
    {
        $this->messageTemplatingRepository = $messageTemplatingRepo;
    }

    /**
     * Display a listing of the MessageTemplating.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $messageTemplatings = $this->messageTemplatingRepository->paginate(10);

        return view('message_templatings.index')
            ->with('messageTemplatings', $messageTemplatings);
    }

    /**
     * Show the form for creating a new MessageTemplating.
     *
     * @return Response
     */
    public function create()
    {
        return view('message_templatings.create');
    }

    /**
     * Store a newly created MessageTemplating in storage.
     *
     * @param CreateMessageTemplatingRequest $request
     *
     * @return Response
     */
    public function store(CreateMessageTemplatingRequest $request)
    {
        $input = $request->all();

        $messageTemplating = $this->messageTemplatingRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/messageTemplatings.singular')]));

        return redirect(route('messageTemplatings.index'));
    }

    /**
     * Display the specified MessageTemplating.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $messageTemplating = $this->messageTemplatingRepository->find($id);

        if (empty($messageTemplating)) {
            Flash::error(__('messages.not_found', ['model' => __('models/messageTemplatings.singular')]));

            return redirect(route('messageTemplatings.index'));
        }

        return view('message_templatings.show')->with('messageTemplating', $messageTemplating);
    }

    /**
     * Show the form for editing the specified MessageTemplating.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $messageTemplating = $this->messageTemplatingRepository->find($id);

        if (empty($messageTemplating)) {
            Flash::error(__('messages.not_found', ['model' => __('models/messageTemplatings.singular')]));

            return redirect(route('messageTemplatings.index'));
        }

        return view('message_templatings.edit')->with('messageTemplating', $messageTemplating);
    }

    /**
     * Update the specified MessageTemplating in storage.
     *
     * @param int $id
     * @param UpdateMessageTemplatingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMessageTemplatingRequest $request)
    {
        $messageTemplating = $this->messageTemplatingRepository->find($id);

        if (empty($messageTemplating)) {
            Flash::error(__('messages.not_found', ['model' => __('models/messageTemplatings.singular')]));

            return redirect(route('messageTemplatings.index'));
        }

        $messageTemplating = $this->messageTemplatingRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/messageTemplatings.singular')]));

        return redirect(route('messageTemplatings.index'));
    }

    /**
     * Remove the specified MessageTemplating from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $messageTemplating = $this->messageTemplatingRepository->find($id);

        if (empty($messageTemplating)) {
            Flash::error(__('messages.not_found', ['model' => __('models/messageTemplatings.singular')]));

            return redirect(route('messageTemplatings.index'));
        }

        $this->messageTemplatingRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/messageTemplatings.singular')]));

        return redirect(route('messageTemplatings.index'));
    }
}
