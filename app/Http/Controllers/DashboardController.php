<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\orders;
use App\Models\OrderDetail;
use App\Models\Products;
use App\Models\category;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    public function index(){
        $months = [];
        for ($m=1; $m<=12; $m++) {
            $months[] = date('F', mktime(0,0,0,$m, 1, date('Y')));
         }
        $orders_break_down = [];
        $start_of_year = Carbon::now()->startOfYear();
        $end_of_year = Carbon::now()->endOfYear();
        $top_products = [];
        $top_categories = [];
        $orders = orders::
                select(DB::raw("date_format(orders.created_at,'%M') AS month, count(*) as total_orders"))
                ->whereBetween('orders.created_at',[$start_of_year,$end_of_year])
                ->groupBy( DB::raw("month(orders.created_at)") )
                ->get();


        foreach($months as $key => $month){
            $this_month_data = $orders->where('month', $month)->first();
            $this_month_count = 0;
            
            if($this_month_data){
                $start = new Carbon('first day of'.$month);
                $end = new Carbon('last day of'.$month);
                $orders_of_this_month = orders::whereBetween('created_at',[$start,$end])->get();
                foreach($orders_of_this_month as $this_month_order){
                    $order_detail_of_this_order = OrderDetail::where('order_id',$this_month_order->id)->get();
                    foreach($order_detail_of_this_order as $order_detail){

                        if(!isset($top_products[$month][$order_detail->product_id])){
                            $product = Products::without('product_metas')->find($order_detail->product_id);
                            $top_products[$month][$order_detail->product_id] = 
                                [
                                    'name' => $product->name ?? '',
                                    'count' => 1,
                                ];
                            if(!isset($top_categories[$month][$product->id_category ?? ''])){
                                $category = category::find($product->id_category ?? '');
                                $top_categories[$month][$product->id_category ?? ''] = $category->name ?? '';
                            }
                        }else{
                            $top_products[$month][$order_detail->product_id]['count'] = 
                            $top_products[$month][$order_detail->product_id]['count']+1;
                        }
                    }
                }
                $this_month_count = $this_month_data->total_orders ?? 0;
            }
            $orders_break_down[$key] = $this_month_count;
        }



        return view('home')
                ->with('months',json_encode($months,JSON_NUMERIC_CHECK))
                ->with('orders',json_encode($orders_break_down,JSON_NUMERIC_CHECK))
                ->with('top_products',$top_products)
                ->with('top_categories',$top_categories);
    }
    public function resetCache(){
        \Artisan::call('cache:clear');
        return redirect()->back()->with('success','Cache Cleard');
    }
}
