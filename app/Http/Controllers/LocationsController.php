<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLocationsRequest;
use App\Http\Requests\UpdateLocationsRequest;
use App\Repositories\LocationsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class LocationsController extends AppBaseController
{
    /** @var  LocationsRepository */
    private $locationsRepository;

    public function __construct(LocationsRepository $locationsRepo)
    {
        $this->locationsRepository = $locationsRepo;
    }

    /**
     * Display a listing of the Locations.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $locations = $this->locationsRepository->all();

        return view('locations.index')
            ->with('locations', $locations);
    }

    /**
     * Show the form for creating a new Locations.
     *
     * @return Response
     */
    public function create()
    {
        return view('locations.create');
    }

    /**
     * Store a newly created Locations in storage.
     *
     * @param CreateLocationsRequest $request
     *
     * @return Response
     */
    public function store(CreateLocationsRequest $request)
    {
        $input = $request->all();

        $locations = $this->locationsRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/locations.singular')]));

        return redirect(route('locations.index'));
    }

    /**
     * Display the specified Locations.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $locations = $this->locationsRepository->find($id);

        if (empty($locations)) {
            Flash::error(__('messages.not_found', ['model' => __('models/locations.singular')]));

            return redirect(route('locations.index'));
        }

        return view('locations.show')->with('locations', $locations);
    }

    /**
     * Show the form for editing the specified Locations.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $locations = $this->locationsRepository->find($id);

        if (empty($locations)) {
            Flash::error(__('messages.not_found', ['model' => __('models/locations.singular')]));

            return redirect(route('locations.index'));
        }

        return view('locations.edit')->with('locations', $locations);
    }

    /**
     * Update the specified Locations in storage.
     *
     * @param int $id
     * @param UpdateLocationsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLocationsRequest $request)
    {
        $locations = $this->locationsRepository->find($id);

        if (empty($locations)) {
            Flash::error(__('messages.not_found', ['model' => __('models/locations.singular')]));

            return redirect(route('locations.index'));
        }

        $locations = $this->locationsRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/locations.singular')]));

        return redirect(route('locations.index'));
    }

    /**
     * Remove the specified Locations from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $locations = $this->locationsRepository->find($id);

        if (empty($locations)) {
            Flash::error(__('messages.not_found', ['model' => __('models/locations.singular')]));

            return redirect(route('locations.index'));
        }

        $this->locationsRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/locations.singular')]));

        return redirect(route('locations.index'));
    }
}
