<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateHoldCartsRequest;
use App\Http\Requests\UpdateHoldCartsRequest;
use App\Repositories\HoldCartsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class HoldCartsController extends AppBaseController
{
    /** @var  HoldCartsRepository */
    private $holdCartsRepository;

    public function __construct(HoldCartsRepository $holdCartsRepo)
    {
        $this->holdCartsRepository = $holdCartsRepo;
    }

    /**
     * Display a listing of the HoldCarts.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $holdCarts = $this->holdCartsRepository->all();

        return view('hold_carts.index')
            ->with('holdCarts', $holdCarts);
    }

    /**
     * Show the form for creating a new HoldCarts.
     *
     * @return Response
     */
    public function create()
    {
        return view('hold_carts.create');
    }

    /**
     * Store a newly created HoldCarts in storage.
     *
     * @param CreateHoldCartsRequest $request
     *
     * @return Response
     */
    public function store(CreateHoldCartsRequest $request)
    {
        $input = $request->all();

        $holdCarts = $this->holdCartsRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/holdCarts.singular')]));

        return redirect(route('holdCarts.index'));
    }

    /**
     * Display the specified HoldCarts.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $holdCarts = $this->holdCartsRepository->find($id);

        if (empty($holdCarts)) {
            Flash::error(__('messages.not_found', ['model' => __('models/holdCarts.singular')]));

            return redirect(route('holdCarts.index'));
        }

        return view('hold_carts.show')->with('holdCarts', $holdCarts);
    }

    /**
     * Show the form for editing the specified HoldCarts.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $holdCarts = $this->holdCartsRepository->find($id);

        if (empty($holdCarts)) {
            Flash::error(__('messages.not_found', ['model' => __('models/holdCarts.singular')]));

            return redirect(route('holdCarts.index'));
        }

        return view('hold_carts.edit')->with('holdCarts', $holdCarts);
    }

    /**
     * Update the specified HoldCarts in storage.
     *
     * @param int $id
     * @param UpdateHoldCartsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHoldCartsRequest $request)
    {
        $holdCarts = $this->holdCartsRepository->find($id);

        if (empty($holdCarts)) {
            Flash::error(__('messages.not_found', ['model' => __('models/holdCarts.singular')]));

            return redirect(route('holdCarts.index'));
        }

        $holdCarts = $this->holdCartsRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/holdCarts.singular')]));

        return redirect(route('holdCarts.index'));
    }

    /**
     * Remove the specified HoldCarts from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $holdCarts = $this->holdCartsRepository->find($id);

        if (empty($holdCarts)) {
            Flash::error(__('messages.not_found', ['model' => __('models/holdCarts.singular')]));

            return redirect(route('holdCarts.index'));
        }

        $this->holdCartsRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/holdCarts.singular')]));

        return redirect(route('holdCarts.index'));
    }
}
