<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSmsSettingRequest;
use App\Http\Requests\UpdateSmsSettingRequest;
use App\Repositories\SmsSettingRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SmsSettingController extends AppBaseController
{
    /** @var  SmsSettingRepository */
    private $smsSettingRepository;

    public function __construct(SmsSettingRepository $smsSettingRepo)
    {
        $this->smsSettingRepository = $smsSettingRepo;
    }

    /**
     * Display a listing of the SmsSetting.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $smsSettings = $this->smsSettingRepository->paginate(10);

        return view('sms_settings.index')
            ->with('smsSettings', $smsSettings);
    }

    /**
     * Show the form for creating a new SmsSetting.
     *
     * @return Response
     */
    public function create()
    {
        return view('sms_settings.create');
    }

    /**
     * Store a newly created SmsSetting in storage.
     *
     * @param CreateSmsSettingRequest $request
     *
     * @return Response
     */
    public function store(CreateSmsSettingRequest $request)
    {
        $input = $request->all();
        $smsSetting = $this->smsSettingRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/smsSettings.singular')]));

        return redirect(route('smsSettings.index'));
    }

    /**
     * Display the specified SmsSetting.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $smsSetting = $this->smsSettingRepository->find($id);

        if (empty($smsSetting)) {
            Flash::error(__('messages.not_found', ['model' => __('models/smsSettings.singular')]));

            return redirect(route('smsSettings.index'));
        }

        return view('sms_settings.show')->with('smsSetting', $smsSetting);
    }

    /**
     * Show the form for editing the specified SmsSetting.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $smsSetting = $this->smsSettingRepository->find($id);

        if (empty($smsSetting)) {
            Flash::error(__('messages.not_found', ['model' => __('models/smsSettings.singular')]));

            return redirect(route('smsSettings.index'));
        }

        return view('sms_settings.edit')->with('smsSetting', $smsSetting);
    }

    /**
     * Update the specified SmsSetting in storage.
     *
     * @param int $id
     * @param UpdateSmsSettingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSmsSettingRequest $request)
    {
        $smsSetting = $this->smsSettingRepository->find($id);

        if (empty($smsSetting)) {
            Flash::error(__('messages.not_found', ['model' => __('models/smsSettings.singular')]));

            return redirect(route('smsSettings.index'));
        }

        $smsSetting = $this->smsSettingRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/smsSettings.singular')]));

        return redirect(route('smsSettings.index'));
    }

    /**
     * Remove the specified SmsSetting from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $smsSetting = $this->smsSettingRepository->find($id);

        if (empty($smsSetting)) {
            Flash::error(__('messages.not_found', ['model' => __('models/smsSettings.singular')]));

            return redirect(route('smsSettings.index'));
        }

        $this->smsSettingRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/smsSettings.singular')]));

        return redirect(route('smsSettings.index'));
    }
}
