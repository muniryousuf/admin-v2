<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatesliderRequest;
use App\Http\Requests\UpdatesliderRequest;
use App\Repositories\sliderRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class sliderController extends AppBaseController
{
    /** @var  sliderRepository */
    private $sliderRepository;

    public function __construct(sliderRepository $sliderRepo)
    {
        $this->sliderRepository = $sliderRepo;
    }

    /**
     * Display a listing of the slider.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $sliders = $this->sliderRepository->all();

        return view('sliders.index')
            ->with('sliders', $sliders);
    }

    /**
     * Show the form for creating a new slider.
     *
     * @return Response
     */
    public function create()
    {
        return view('sliders.create');
    }

    /**
     * Store a newly created slider in storage.
     *
     * @param CreatesliderRequest $request
     *
     * @return Response
     */
    public function store(CreatesliderRequest $request)
    {
        $input = $request->all();
        $image = time().'.'.$request->image->extension();  
        $request->image->move(public_path('images'), $image);
        $input['image'] = $image;
        $input['gallery_id'] = $input['gallery_id'] > 0 ? $input['gallery_id'] : 0;
        $slider = $this->sliderRepository->create($input);
        Flash::success(__('messages.saved', ['model' => __('models/sliders.singular')]));
        return redirect(route('sliders.index'));
    }

    /**
     * Display the specified slider.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $slider = $this->sliderRepository->find($id);

        if (empty($slider)) {
            Flash::error(__('messages.not_found', ['model' => __('models/sliders.singular')]));

            return redirect(route('sliders.index'));
        }

        return view('sliders.show')->with('slider', $slider);
    }

    /**
     * Show the form for editing the specified slider.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $slider = $this->sliderRepository->find($id);

        if (empty($slider)) {
            Flash::error(__('messages.not_found', ['model' => __('models/sliders.singular')]));

            return redirect(route('sliders.index'));
        }

        return view('sliders.edit')->with('slider', $slider);
    }

    /**
     * Update the specified slider in storage.
     *
     * @param int $id
     * @param UpdatesliderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatesliderRequest $request)
    {
        $slider = $this->sliderRepository->find($id);

        if (empty($slider)) {
            Flash::error(__('messages.not_found', ['model' => __('models/sliders.singular')]));

            return redirect(route('sliders.index'));
        }
        $inputs = $request->all();
        if(isset($request->image)){
            $image = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images'), $image);
            $inputs['image'] = $image;
        }

        $slider = $this->sliderRepository->update($inputs, $id);

        Flash::success(__('messages.updated', ['model' => __('models/sliders.singular')]));

        return redirect(route('sliders.index'));
    }

    /**
     * Remove the specified slider from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $slider = $this->sliderRepository->find($id);

        if (empty($slider)) {
            Flash::error(__('messages.not_found', ['model' => __('models/sliders.singular')]));

            return redirect(route('sliders.index'));
        }

        $this->sliderRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/sliders.singular')]));

        return redirect(route('sliders.index'));
    }
}
