<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateResturentAddressRequest;
use App\Http\Requests\UpdateResturentAddressRequest;
use App\Repositories\ResturentAddressRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Data\Models\RestaurantAddress;
use App\Models\generalSettings;

class ResturentAddressController extends AppBaseController
{
    /** @var  ResturentAddressRepository */
    private $resturentAddressRepository;

    public function __construct(ResturentAddressRepository $resturentAddressRepo)
    {
        $this->resturentAddressRepository = $resturentAddressRepo;
    }

    /**
     * Display a listing of the ResturentAddress.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $resturentAddresses = $this->resturentAddressRepository->paginate(10);

        return view('resturent_addresses.index')
            ->with('resturentAddresses', $resturentAddresses);
    }

    /**
     * Show the form for creating a new ResturentAddress.
     *
     * @return Response
     */
    public function create()
    {
        return view('resturent_addresses.create');
    }

    /**
     * Store a newly created ResturentAddress in storage.
     *
     * @param CreateResturentAddressRequest $request
     *
     * @return Response
     */
    public function store(CreateResturentAddressRequest $request)
    {
        $input = $request->all();

        $image = time().'.'.$request->logo->extension();  
        $request->logo->move(public_path('images'), $image);
        $input['logo'] = $image;
        

        $resturentAddress = $this->resturentAddressRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/resturentAddresses.singular')]));

        return redirect(route('resturentAddresses.index'));
    }

    /**
     * Display the specified ResturentAddress.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $resturentAddress = $this->resturentAddressRepository->find($id);

        if (empty($resturentAddress)) {
            Flash::error(__('messages.not_found', ['model' => __('models/resturentAddresses.singular')]));

            return redirect(route('resturentAddresses.index'));
        }

        return view('resturent_addresses.show')->with('resturentAddress', $resturentAddress);
    }

    /**
     * Show the form for editing the specified ResturentAddress.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $resturentAddress = $this->resturentAddressRepository->find($id);

        if (empty($resturentAddress)) {
            Flash::error(__('messages.not_found', ['model' => __('models/resturentAddresses.singular')]));

            return redirect(route('resturentAddresses.index'));
        }
        
        return view('resturent_addresses.edit')->with('resturentAddress', $resturentAddress);
    }

    /**
     * Update the specified ResturentAddress in storage.
     *
     * @param int $id
     * @param UpdateResturentAddressRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResturentAddressRequest $request)
    {
        $resturentAddress = $this->resturentAddressRepository->find($id);

        if (empty($resturentAddress)) {
            Flash::error(__('messages.not_found', ['model' => __('models/resturentAddresses.singular')]));

            return redirect(route('resturentAddresses.index'));
        }
        $inputs = $request->all();
        if(isset($request->logo)){
            $image = time().'.'.$request->logo->extension();  
            $request->logo->move(public_path('images'), $image);
            $inputs['logo'] = $image;
        }


        $resturentAddress = $this->resturentAddressRepository->update($inputs, $id);

        Flash::success(__('messages.updated', ['model' => __('models/resturentAddresses.singular')]));

        return redirect(route('resturentAddresses.index'));
    }

    /**
     * Remove the specified ResturentAddress from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $resturentAddress = $this->resturentAddressRepository->find($id);

        if (empty($resturentAddress)) {
            Flash::error(__('messages.not_found', ['model' => __('models/resturentAddresses.singular')]));

            return redirect(route('resturentAddresses.index'));
        }

        $this->resturentAddressRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/resturentAddresses.singular')]));

        return redirect(route('resturentAddresses.index'));
    }
    
    public function addResturentAddress($id){
        $restaurant_address = RestaurantAddress::where('id_restaurant',$id)->first();
        return view('resturent_addresses.add-address')->with('restaurant_address', $restaurant_address)->with('id_restaurant',$id);
    }

    public function saveResturentAddress(Request $request){
        $params = $request->all();

        if($params['id'] > 0){
            $restaurant_address = RestaurantAddress::where('id_restaurant',$params['id_restaurant'])->first();
        }else{
            $restaurant_address = new RestaurantAddress();
        }
        $restaurant_address->city = $params['city'];
        $restaurant_address->address = $params['address'];
        $restaurant_address->zip_code = $params['zip_code'];
        $restaurant_address->id_country = 1;
        $restaurant_address->id_restaurant = $params['id_restaurant'];
        $restaurant_address->save();
        $general_settings = generalSettings::first();
        $general_settings->address = $params['address'];
        $general_settings->save();

        return redirect(route('resturentAddresses.add-address',['id'=> $params['id_restaurant']]));
    }
}
