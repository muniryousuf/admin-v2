<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePagesRequest;
use App\Http\Requests\UpdatePagesRequest;
use App\Repositories\PagesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PagesController extends AppBaseController
{
    /** @var  PagesRepository */
    private $pagesRepository;

    public function __construct(PagesRepository $pagesRepo)
    {
        $this->pagesRepository = $pagesRepo;
    }

    /**
     * Display a listing of the Pages.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pages = $this->pagesRepository->all();

        return view('pages.index')
            ->with('pages', $pages);
    }

    /**
     * Show the form for creating a new Pages.
     *
     * @return Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created Pages in storage.
     *
     * @param CreatePagesRequest $request
     *
     * @return Response
     */
    public function store(CreatePagesRequest $request)
    {
        $input = $request->all();
        if($input['page_type'] == 'static'){
            $file_path = public_path('assets/builder/demo-bk/Basic/'.$input['page_template_name']);
            $f = fopen($file_path, 'r');
            $content = fread($f,filesize($file_path));
            fclose($f);
            $input['html'] = $content;
        }
        $input['name'] = str_replace(' ', '-', $input['name']);

        if ($request->file('background_type_value')!=null){
            $image = time().'.'.$request->background_type_value->extension();  
            $request->background_type_value->move(public_path('images'), $image);
            $input['background_type_value'] = $image;
        }




        $pages = $this->pagesRepository->create($input);
        Flash::success(__('messages.saved', ['model' => __('models/pages.singular')]));
        return redirect(route('pages.index'));
    }

    /**
     * Display the specified Pages.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pages = $this->pagesRepository->find($id);

        if (empty($pages)) {
            Flash::error(__('messages.not_found', ['model' => __('models/pages.singular')]));

            return redirect(route('pages.index'));
        }

        return view('pages.show')->with('pages', $pages);
    }

    /**
     * Show the form for editing the specified Pages.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pages = $this->pagesRepository->find($id);

        if (empty($pages)) {
            Flash::error(__('messages.not_found', ['model' => __('models/pages.singular')]));

            return redirect(route('pages.index'));
        }

        return view('pages.edit')->with('pages', $pages);
    }

    /**
     * Update the specified Pages in storage.
     *
     * @param int $id
     * @param UpdatePagesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePagesRequest $request)
    {
        $pages = $this->pagesRepository->find($id);

        if (empty($pages)) {
            Flash::error(__('messages.not_found', ['model' => __('models/pages.singular')]));

            return redirect(route('pages.index'));
        }
        $input = $request->all();
        // dd($request->all());
        if ($request->file('background_type_value')!=null){
            $image = time().'.'.$request->background_type_value->extension();  
            $request->background_type_value->move(public_path('images'), $image);
            $input['background_type_value'] = $image;
        }


        $pages = $this->pagesRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/pages.singular')]));

        return redirect(route('pages.index'));
    }

    /**
     * Remove the specified Pages from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pages = $this->pagesRepository->find($id);

        if (empty($pages)) {
            Flash::error(__('messages.not_found', ['model' => __('models/pages.singular')]));

            return redirect(route('pages.index'));
        }

        $this->pagesRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/pages.singular')]));

        return redirect(route('pages.index'));
    }
}
