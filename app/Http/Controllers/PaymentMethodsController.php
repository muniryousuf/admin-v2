<?php

namespace App\Http\Controllers;

use App\Data\Models\CardStream;
use App\Data\Models\User;
use App\Models\PaymentMethods;;
use App\External\Gateway;
use App\Http\Requests\CreatePaymentMethodsRequest;
use App\Http\Requests\UpdatePaymentMethodsRequest;
use App\Repositories\PaymentMethodsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;
use Laravel\Cashier\Exceptions\IncompletePayment;

class PaymentMethodsController extends AppBaseController
{
    /** @var  PaymentMethodsRepository */
    private $paymentMethodsRepository;

    public function __construct(PaymentMethodsRepository $paymentMethodsRepo)
    {
        $this->paymentMethodsRepository = $paymentMethodsRepo;
    }

    /**
     * Display a listing of the PaymentMethods.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $paymentMethods = $this->paymentMethodsRepository->all();

        return view('payment_methods.index')
            ->with('paymentMethods', $paymentMethods);
    }

    /**
     * Show the form for creating a new PaymentMethods.
     *
     * @return Response
     */
    public function create()
    {
        return view('payment_methods.create');
    }

    /**
     * Store a newly created PaymentMethods in storage.
     *
     * @param CreatePaymentMethodsRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentMethodsRequest $request)
    {
        $input = $request->all();

        $paymentMethods = $this->paymentMethodsRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/paymentMethods.singular')]));

        return redirect(route('paymentMethods.index'));
    }

    /**
     * Display the specified PaymentMethods.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $paymentMethods = $this->paymentMethodsRepository->find($id);

        if (empty($paymentMethods)) {
            Flash::error(__('messages.not_found', ['model' => __('models/paymentMethods.singular')]));

            return redirect(route('paymentMethods.index'));
        }

        return view('payment_methods.show')->with('paymentMethods', $paymentMethods);
    }

    /**
     * Show the form for editing the specified PaymentMethods.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $paymentMethods = $this->paymentMethodsRepository->find($id);

        if (empty($paymentMethods)) {
            Flash::error(__('messages.not_found', ['model' => __('models/paymentMethods.singular')]));

            return redirect(route('paymentMethods.index'));
        }

        return view('payment_methods.edit')->with('paymentMethods', $paymentMethods);
    }

    /**
     * Update the specified PaymentMethods in storage.
     *
     * @param int $id
     * @param UpdatePaymentMethodsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentMethodsRequest $request)
    {
        $paymentMethods = $this->paymentMethodsRepository->find($id);

        if (empty($paymentMethods)) {
            Flash::error(__('messages.not_found', ['model' => __('models/paymentMethods.singular')]));

            return redirect(route('paymentMethods.index'));
        }

        $paymentMethods = $this->paymentMethodsRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/paymentMethods.singular')]));

        return redirect(route('paymentMethods.index'));
    }

    /**
     * Remove the specified PaymentMethods from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $paymentMethods = $this->paymentMethodsRepository->find($id);

        if (empty($paymentMethods)) {
            Flash::error(__('messages.not_found', ['model' => __('models/paymentMethods.singular')]));

            return redirect(route('paymentMethods.index'));
        }

        $this->paymentMethodsRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/paymentMethods.singular')]));

        return redirect(route('paymentMethods.index'));
    }

    public function paymentAction(Request $request) {

        $requestData = $request->all();

        $userId = Auth::check() ? Auth::id() : 1;

        $user = User::where('id', $userId)->first();

        try {
            $stripeCharge = $user->charge($requestData['total_amount']*100, $requestData['pmethod']);

            $output = ['data' => $stripeCharge, 'message' => "your order has been placed successfully "];
            return response()->json($output, 200);

        } catch (IncompletePayment $exception) {

            $code = 401;
            $output = ['error' => ['code' => 402, 'payment_data' => $exception->payment]];
            return response()->json($output, $code);
        }
    }

    public function streamPaymentAction(Request $request) {

        try {

            $requestData = $request->all();

            $userId = Auth::check() ? Auth::id() : 1;

            $user = User::where('id', $userId)->first();

            $req = array(

                'merchantID' => 100001,
                'action' => 'SALE',
                'type' => 1,
                'currencyCode' => 826,
                'countryCode' => 826,
                'cardNumber' => '4012001037141112',
                'cardExpiryMonth' => $requestData['card_month'],
                'cardExpiryYear' => substr($requestData['card_year'], -2),
                'cardCVV' => '083', //$requestData['card_cvv'], //'083',
                'amount' => 3000, //$requestData['total_amount']*100,
                'customerName' => $user->name,
                'customerEmail' => $user->email,
                'customerAddress' => "----",
                'customerPostCode' => "TE15 5ST", //'TE15 5ST',
                'orderRef' => "Aisha Cafe Demo Orders",
                // The following fields are only mandatory for 3DS v2 direct integration
                'remoteAddress' => $_SERVER['REMOTE_ADDR'],
                'threeDSRedirectURL' => 'http://142.93.42.68/stream-check-out&acs=1'
            );

            $return = Gateway::directRequest($req);

            if ($return['responseCode'] === 65802) {
                $code = 402;
                $output = ['error' => ['code' => 402, 'payment_data' => $return]];
                return response()->json($output, $codeF);
            } else {

              /*  $code = 401;
                $output = ['error' => ['code' => 200, 'message' => $return['responseMessage']]];
                return response()->json($output, $code);*/

                $output = ['data' => [], 'message' => "Thank you for your payment."];
                return response()->json($output, 200);

            }
        } catch (\Exception $exception) {
            $code = 500;
            $output = ['error' => ['code' => 500, 'message' => $exception->getMessage()]];
            return response()->json($output, $code);
        }
    }

    public function cardStreamCallback(Request $request)
    {
        $requestData = $request->all();

        $cardData = CardStream::where('threeDSr', $requestData['MD'])->first();

        if($cardData) {

            $dd = json_decode($cardData->request_data);

            $req = array(
                'merchantID' => $dd->merchantID,
                'action' => 'SALE',
                'type' => 1,
                'currencyCode' => 826,
                'countryCode' => 826,
                'amount' => $dd->amount,
                'cardNumber' => $dd->cardNumber,
                'cardExpiryMonth' => $dd->cardExpiryMonth,
                'cardExpiryYear' => $dd->cardExpiryYear,
                'cardCVV' => $dd->cardCVV,
                'customerName' => $dd->customerName,
                'customerEmail' => $dd->customerEmail,
                'customerAddress' => $dd->customerAddress,
                'customerPostCode' => $dd->customerPostCode,
                'orderRef' => $dd->orderRef,
                'threeDSRef' => $requestData['MD'],
                'threeDSResponse' => $requestData,
            );

            $return = Gateway::directRequest($req);

            if($return['responseCode'] === 65802) {
                $code = 401;
                $output = ['error' => ['code' => 402, 'payment_data' => $return]];
                return response()->json($output, $code);
            }
        } else {

            $output = ['error' => ['code' => 500, 'message' => "Something went wrong"]];
            return response()->json($output, 401);
        }
    }
    public function GetPaymentMethodsForApp(){
        $output = ['data' => PaymentMethods::select([  'name',
        'private_key',
        'public_key',
        'another_key',
        'active'])->get(), 'message' => "success"];
        return response()->json($output, 200);
    }
}
