<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCartAPIRequest;
use App\Http\Requests\API\UpdateCartAPIRequest;
use App\Models\Cart;
use App\Models\ResturentAddress;
use App\Repositories\CartRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\CartResource;
use App\Data\Models\Users;
use Response;

/**
 * Class CartController
 * @package App\Http\Controllers\API
 */

class CartAPIController extends AppBaseController
{
    /** @var  CartRepository */
    private $cartRepository;

    public function __construct(CartRepository $cartRepo)
    {
        $this->cartRepository = $cartRepo;
    }

   
    public function getCart($id,$user_id = 0)
    {
        /** @var Cart $cart */
        $cart = $this->cartRepository->getCart($id,$user_id);

        if (empty($cart)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/carts.singular')])
            );
        }

        return $this->sendResponse(
            $cart,
            __('messages.retrieved', ['model' => __('models/carts.singular')])
        );
    }
    public function addToCart(Request $request)
    {
        /** @var Cart $cart */
        $cart = $this->cartRepository->addToCart($request->all());

        if (empty($cart)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/carts.singular')])
            );
        }

        return $this->sendResponse(
            $cart,
            __('messages.retrieved', ['model' => __('models/carts.singular')])
        );
    }
    public function applyCouponCode(Request $request)
    {
        /** @var Cart $cart */
        $cart = $this->cartRepository->applyCouponCode($request->all());

        if (empty($cart)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/carts.singular')])
            );
        }

        return $this->sendResponse(
            $cart,
            __('messages.retrieved', ['model' => __('models/carts.singular')])
        );
    }

    public function changeQuantity(Request $request)
    {
        /** @var Cart $cart */
        $cart = $this->cartRepository->changeQuantity($request->all());

        if (empty($cart)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/carts.singular')])
            );
        }

        return $this->sendResponse(
            $cart,
            __('messages.retrieved', ['model' => __('models/carts.singular')])
        );
    }
    public function placeOrder(Request $request)
    {
        /** @var Cart $cart */
        $cart = $this->cartRepository->placeOrder($request->all());

        if (empty($cart)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/carts.singular')])
            );
        }

        return $this->sendResponse(
            $cart,
            __('messages.retrieved', ['model' => __('models/carts.singular')])
        );
    }
    public function changeServingMethod(Request $request)
    {
        /** @var Cart $cart */
        $cart = $this->cartRepository->changeServingMethod($request->all());

        if (empty($cart)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/carts.singular')])
            );
        }

        return $this->sendResponse(
            $cart,
            __('messages.retrieved', ['model' => __('models/carts.singular')])
        );
    }
    public function addUserAddress(Request $request)
    {
        /** @var Cart $cart */
        $address = $this->cartRepository->addUserAddress($request->all());

        if (empty($address)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/carts.singular')])
            );
        }

        return $this->sendResponse(
            $address,
            __('messages.retrieved', ['model' => __('models/carts.singular')])
        );
    } 
    public function updateUserAddress(Request $request)
    {
        /** @var Cart $cart */
        $address = $this->cartRepository->updateUserAddress($request->all());

        if (empty($address)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/carts.singular')])
            );
        }

        return $this->sendResponse(
            $address,
            __('messages.retrieved', ['model' => __('models/carts.singular')])
        );
    }
    public function createCart(Request $request)
    {
        
        /** @var Cart $cart */
        $cart = $this->cartRepository->createCart($request->all());

        if (empty($cart)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/carts.singular')])
            );
        }

        return $this->sendResponse(
            $cart,
            __('messages.retrieved', ['model' => __('models/carts.singular')])
        );
    }
    public function getCustomerAddresses(Request $request)
    {
        
        /** @var Cart $cart */
        $addresses = $this->cartRepository->getCustomerAddresses($request->all());

        if (empty($addresses)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/carts.singular')])
            );
        }

        return $this->sendResponse(
            $addresses,
            __('messages.retrieved', ['model' => __('models/carts.singular')])
        );
    }
    public function setAddressId(Request $request)
    {
        
        /** @var Cart $cart */
        $address = $this->cartRepository->setAddressId($request->all());

        if (empty($address)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/carts.singular')])
            );
        }

        return $this->sendResponse(
            $address,
            __('messages.retrieved', ['model' => __('models/carts.singular')])
        );
    }
    public function sendEmailtoAdmin(Request $request)
    {
        
        $address = ResturentAddress::first();
        if($address){
            \Mail::to($address->email)->send(new \App\Mail\OrderPlace($request->reference));   
        }

        if (empty($address)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/carts.singular')])
            );
        }

        return $this->sendResponse(
            $address,
            __('messages.retrieved', ['model' => __('models/carts.singular')])
        );
    }
    public function sendEmailtoCustomer(Request $request)
    {
        
        $user = Users::find($request->user_id);
        if($user){
            \Mail::to($user->email)->send(new \App\Mail\OrderPlace($request->reference));   
        }

        if (empty($user)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/carts.singular')])
            );
        }

        return $this->sendResponse(
            $user,
            __('messages.retrieved', ['model' => __('models/carts.singular')])
        );
    }
    public function sendMessagetoCustomer(Request $request)
    {
        
        $user = Users::find($request->user_id);
        if($user){
            \SMSGlobal\Credentials::set('11b2e9d3a812f58625d7db0681fda11c', '9fbe4f32d9dad777c559ac4d0befcb75');
            $sms = new \SMSGlobal\Resource\Sms();
            try {
                $response = $sms->sendToOne($user->phone, 'Thanks for your order');

                return $this->sendResponse(
                    $response['messages'],
                    __('messages.retrieved', ['model' => __('models/carts.singular')])
                );

            } catch (\Exception $e) {
                return $this->sendError($e->getMessage());
            }
            
        }
    }

    
}
