<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCaosGenralSettingsAPIRequest;
use App\Http\Requests\API\UpdateCaosGenralSettingsAPIRequest;
use App\Models\CaosGenralSettings;
use App\Repositories\CaosGenralSettingsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\CaosGenralSettingsResource;
use Response;

/**
 * Class CaosGenralSettingsController
 * @package App\Http\Controllers\API
 */

class CaosGenralSettingsAPIController extends AppBaseController
{
    /** @var  CaosGenralSettingsRepository */
    private $caosGenralSettingsRepository;

    public function __construct(CaosGenralSettingsRepository $caosGenralSettingsRepo)
    {
        $this->caosGenralSettingsRepository = $caosGenralSettingsRepo;
    }

    /**
     * Display a listing of the CaosGenralSettings.
     * GET|HEAD /caosGenralSettings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $settings = getSettings();
        $caosGenralSettings = $this->caosGenralSettingsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        $data = [];
        if(isset($caosGenralSettings[0])){
            $data = $caosGenralSettings[0] ?? [];
            $data->header_logo = $settings['header_logo'];
            $data->footer_logo = $settings['footer_logo'];
            $data->map = $settings['map'];
        }
        return $this->sendResponse(
            $data,
            __('messages.retrieved', ['model' => __('models/caosGenralSettings.plural')])
        );
    }

    /**
     * Store a newly created CaosGenralSettings in storage.
     * POST /caosGenralSettings
     *
     * @param CreateCaosGenralSettingsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCaosGenralSettingsAPIRequest $request)
    {
        $input = $request->all();

        $caosGenralSettings = $this->caosGenralSettingsRepository->create($input);

        return $this->sendResponse(
            new CaosGenralSettingsResource($caosGenralSettings),
            __('messages.saved', ['model' => __('models/caosGenralSettings.singular')])
        );
    }

    /**
     * Display the specified CaosGenralSettings.
     * GET|HEAD /caosGenralSettings/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CaosGenralSettings $caosGenralSettings */
        $caosGenralSettings = $this->caosGenralSettingsRepository->find($id);

        if (empty($caosGenralSettings)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/caosGenralSettings.singular')])
            );
        }

        return $this->sendResponse(
            new CaosGenralSettingsResource($caosGenralSettings),
            __('messages.retrieved', ['model' => __('models/caosGenralSettings.singular')])
        );
    }

    /**
     * Update the specified CaosGenralSettings in storage.
     * PUT/PATCH /caosGenralSettings/{id}
     *
     * @param int $id
     * @param UpdateCaosGenralSettingsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $input = $request->all();

        /** @var CaosGenralSettings $caosGenralSettings */
        $caosGenralSettings = $this->caosGenralSettingsRepository->find($id);

        if (empty($caosGenralSettings)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/caosGenralSettings.singular')])
            );
        }

        $caosGenralSettings = $this->caosGenralSettingsRepository->update($input, $id);

        return $this->sendResponse(
            new CaosGenralSettingsResource($caosGenralSettings),
            __('messages.updated', ['model' => __('models/caosGenralSettings.singular')])
        );
    }

    /**
     * Remove the specified CaosGenralSettings from storage.
     * DELETE /caosGenralSettings/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CaosGenralSettings $caosGenralSettings */
        $caosGenralSettings = $this->caosGenralSettingsRepository->find($id);

        if (empty($caosGenralSettings)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/caosGenralSettings.singular')])
            );
        }

        $caosGenralSettings->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/caosGenralSettings.singular')])
        );
    }
}
