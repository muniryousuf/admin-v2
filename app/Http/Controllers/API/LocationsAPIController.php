<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLocationsAPIRequest;
use App\Http\Requests\API\UpdateLocationsAPIRequest;
use App\Models\Locations;
use App\Repositories\LocationsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\LocationsResource;
use Response;

/**
 * Class LocationsController
 * @package App\Http\Controllers\API
 */

class LocationsAPIController extends AppBaseController
{
    /** @var  LocationsRepository */
    private $locationsRepository;

    public function __construct(LocationsRepository $locationsRepo)
    {
        $this->locationsRepository = $locationsRepo;
    }

    /**
     * Display a listing of the Locations.
     * GET|HEAD /locations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $locations = $this->locationsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            LocationsResource::collection($locations),
            __('messages.retrieved', ['model' => __('models/locations.plural')])
        );
    }

    /**
     * Store a newly created Locations in storage.
     * POST /locations
     *
     * @param CreateLocationsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLocationsAPIRequest $request)
    {
        $input = $request->all();

        $locations = $this->locationsRepository->create($input);

        return $this->sendResponse(
            new LocationsResource($locations),
            __('messages.saved', ['model' => __('models/locations.singular')])
        );
    }

    /**
     * Display the specified Locations.
     * GET|HEAD /locations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Locations $locations */
        $locations = $this->locationsRepository->find($id);

        if (empty($locations)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/locations.singular')])
            );
        }

        return $this->sendResponse(
            new LocationsResource($locations),
            __('messages.retrieved', ['model' => __('models/locations.singular')])
        );
    }

    /**
     * Update the specified Locations in storage.
     * PUT/PATCH /locations/{id}
     *
     * @param int $id
     * @param UpdateLocationsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLocationsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Locations $locations */
        $locations = $this->locationsRepository->find($id);

        if (empty($locations)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/locations.singular')])
            );
        }

        $locations = $this->locationsRepository->update($input, $id);

        return $this->sendResponse(
            new LocationsResource($locations),
            __('messages.updated', ['model' => __('models/locations.singular')])
        );
    }

    /**
     * Remove the specified Locations from storage.
     * DELETE /locations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Locations $locations */
        $locations = $this->locationsRepository->find($id);

        if (empty($locations)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/locations.singular')])
            );
        }

        $locations->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/locations.singular')])
        );
    }
}
