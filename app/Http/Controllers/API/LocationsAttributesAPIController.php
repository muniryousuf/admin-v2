<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLocationsAttributesAPIRequest;
use App\Http\Requests\API\UpdateLocationsAttributesAPIRequest;
use App\Models\LocationsAttributes;
use App\Repositories\LocationsAttributesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\LocationsAttributesResource;
use Response;

/**
 * Class LocationsAttributesController
 * @package App\Http\Controllers\API
 */

class LocationsAttributesAPIController extends AppBaseController
{
    /** @var  LocationsAttributesRepository */
    private $locationsAttributesRepository;

    public function __construct(LocationsAttributesRepository $locationsAttributesRepo)
    {
        $this->locationsAttributesRepository = $locationsAttributesRepo;
    }

    /**
     * Display a listing of the LocationsAttributes.
     * GET|HEAD /locationsAttributes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $locationsAttributes = $this->locationsAttributesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            LocationsAttributesResource::collection($locationsAttributes),
            __('messages.retrieved', ['model' => __('models/locationsAttributes.plural')])
        );
    }

    /**
     * Store a newly created LocationsAttributes in storage.
     * POST /locationsAttributes
     *
     * @param CreateLocationsAttributesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLocationsAttributesAPIRequest $request)
    {
        $input = $request->all();

        $locationsAttributes = $this->locationsAttributesRepository->create($input);

        return $this->sendResponse(
            new LocationsAttributesResource($locationsAttributes),
            __('messages.saved', ['model' => __('models/locationsAttributes.singular')])
        );
    }

    /**
     * Display the specified LocationsAttributes.
     * GET|HEAD /locationsAttributes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var LocationsAttributes $locationsAttributes */
        $locationsAttributes = $this->locationsAttributesRepository->find($id);

        if (empty($locationsAttributes)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/locationsAttributes.singular')])
            );
        }

        return $this->sendResponse(
            new LocationsAttributesResource($locationsAttributes),
            __('messages.retrieved', ['model' => __('models/locationsAttributes.singular')])
        );
    }

    /**
     * Update the specified LocationsAttributes in storage.
     * PUT/PATCH /locationsAttributes/{id}
     *
     * @param int $id
     * @param UpdateLocationsAttributesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLocationsAttributesAPIRequest $request)
    {
        $input = $request->all();

        /** @var LocationsAttributes $locationsAttributes */
        $locationsAttributes = $this->locationsAttributesRepository->find($id);

        if (empty($locationsAttributes)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/locationsAttributes.singular')])
            );
        }

        $locationsAttributes = $this->locationsAttributesRepository->update($input, $id);

        return $this->sendResponse(
            new LocationsAttributesResource($locationsAttributes),
            __('messages.updated', ['model' => __('models/locationsAttributes.singular')])
        );
    }

    /**
     * Remove the specified LocationsAttributes from storage.
     * DELETE /locationsAttributes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var LocationsAttributes $locationsAttributes */
        $locationsAttributes = $this->locationsAttributesRepository->find($id);

        if (empty($locationsAttributes)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/locationsAttributes.singular')])
            );
        }

        $locationsAttributes->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/locationsAttributes.singular')])
        );
    }
}
