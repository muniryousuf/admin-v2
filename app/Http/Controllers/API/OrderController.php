<?php

namespace App\Http\Controllers\Api;

use App\Data\Models\CardStream;
use App\Data\Models\Orders;
use App\Data\Models\Restaurant;
use App\Data\Models\RestaurantAddress;
use App\Data\Models\UserDevices;
use App\Data\Repositories\OrderRepository;
use App\External\Gateway;
use App\Http\Controllers\Controller;
use App\PayPal;
use App\Models\User;


use Edujugon\PushNotification\PushNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Laravel\Cashier\Exceptions\IncompletePayment;
use Laravel\Cashier\Exceptions\PaymentActionRequired;
use Omnipay\Common\CreditCard;
use Omnipay\Omnipay;
use PhpParser\Node\Expr\PreInc;
use Validator;
use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Cartalyst\Stripe\Exception\CardErrorException;
use Cartalyst\Stripe\Exception\ServerErrorException;

class OrderController extends Controller
{
    protected $_repository;
    const PER_PAGE = 10;

    public function __construct(OrderRepository $repository)
    {
        $this->_repository = $repository;
    }

    public function getAllOrders(Request $request)
    {
        $requestData = $request->all();
        $pagination = !empty($requestData['pagination']) ? $requestData['pagination'] : false;
        $per_page = self::PER_PAGE;

        $data = $this->_repository->findByAll($pagination, $per_page, $requestData);

        $output = [
            'data' => $data['data'],
            'pagination' => !empty($data['pagination']) ? $data['pagination'] : false,
            'message' => "Orders Retrieved Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }

    public function updateOrderStatus(Request $request, $id)
    {

        $requestData = $request->all();
        $requestData['id'] = $id;

        $validator = Validator::make($requestData, [
            'id' => 'required|exists:orders,id',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        $data = $this->_repository->updateRecord($requestData, $id);

        $output = ['data' => $data, 'message' => "your order status has been updated successfully"];
        return response()->json($output, Response::HTTP_OK);
    }

    public function placeOrder(Request $request)
    {
        $requestData = $request->all();
        if(isset($requestData['is_stripe']) && $requestData['is_stripe'] == true){
            // Stripe object creation.
            $stripe = Stripe::make(config('services.stripe.secret'));
            // customer creation.
            $customer = $stripe->customers()->create([
                'email' => $requestData['user_data']['email'] ?? 'N/A',
                'phone' => $requestData['user_data']['number'] ?? 'N/A',
                'name' => $requestData['user_data']['name'] ?? 'N/A',
            ]);
            // card token creation
            try{
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number'    => $requestData['card_no'],
                        'exp_month' => $requestData['ccExpiryMonth'],
                        'cvc'       => $requestData['cvvNumber'],
                        'exp_year'  => $requestData['ccExpiryYear'],
                    ],
                ]);
            } catch (Exception $e) {
                // Get the status code
                $code = $e->getCode();
            
                // Get the error message returned by Stripe
                $message = $e->getMessage();
            
                // Get the error type returned by Stripe
                $type = $e->getErrorType();
                return response()->json(['message'=> $message,'code'=>$code,'type'=>$type], $code);
            }



            // creating card 

            try{
                $card = $stripe->cards()->create( $customer['id'], $token['id']);
            } catch (Exception $e) {
                // Get the status code
                $code = $e->getCode();
            
                // Get the error message returned by Stripe
                $message = $e->getMessage();
            
                // Get the error type returned by Stripe
                $type = $e->getErrorType();
                return response()->json(['message'=> $message,'code'=>$code,'type'=>$type], $code);
            }
       
        }
        
        $validator = Validator::make($requestData, [
            'user_id' => 'required',
            'total_amount_with_fee' => 'required',
            'delivery_fees' => 'required',
            'payment' => 'required',
            'payment_status' => 'required',
            'delivery_address' => 'required',
            'order_details' => 'required|array',
            'order_details.*.price' => 'required|numeric',
            'order_details.*.product_id' => 'required|numeric',
            'order_details.*.product_name' => 'required',
            'order_details.*.quantity' => 'required'
        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        $data = $this->_repository->placeOrder($requestData);
        if(isset($requestData['is_stripe']) && $requestData['is_stripe'] == true){
            try{
                $charge = $stripe->charges()->create([
                    'customer' => $customer['id'],
                    'currency' => 'gbp',
                    'amount'   => $requestData['total_amount_with_fee'],
                    'source'   =>  $card['id'],
                ]);
                $order_data = Orders::find($data['id']);
                $order_data->charge_id = $charge['id'];
                $order_data->payment_status = 'paid';
                $order_data->save();
            } catch (Exception $e) {
                // Get the status code
                $code = $e->getCode();
            
                // Get the error message returned by Stripe
                $message = $e->getMessage();
            
                // Get the error type returned by Stripe
                $type = $e->getErrorType();
                return response()->json(['message'=> $message,'code'=>$code,'type'=>$type], $code);
            }
        }
        
        if ($data) {

            $requestData['user_id'] = $data['user_id'];
            $requestData['user_data'] = User::find($data['user_id']);
            $requestData['phone_number'] = $data['phone_number'];
            $requestData['total_amount'] = $requestData['total_amount_with_fee'];

           $this->sendNotification($data);
        }

        $requestData['order_id'] = $data['order_id'];
        $output = ['data' => $requestData, 'message' => "your order has been placed successfully "];
        return response()->json($output, Response::HTTP_OK);
    }

    public function updateOrder(Request $request, $id)
    {
        $requestData = $request->all();
        $requestData['payment_status'] = 'unpaid';
        if(!isset($requestData['payment_status'] )){
            $requestData['payment_status'] = 'unpaid';
        }

        $validator = Validator::make($requestData, [
            'total_amount_with_fee' => 'required',
            'payment' => 'required',
            'payment_status' => 'required',
            'delivery_address' => 'required',
            'order_details' => 'required|array',
            'order_details.*.price' => 'required|numeric',
            'order_details.*.product_id' => 'required|numeric',
            'order_details.*.product_name' => 'required',
            'order_details.*.quantity' => 'required'
        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        $data = $this->_repository->updateOrder($requestData, $id);

        if ($data) {

            $requestData['user_id'] = $data['user_id'];
            $requestData['phone_number'] = $data['phone_number'];
            $requestData['total_amount'] = $requestData['total_amount_with_fee'];

            $this->sendNotification($data);
        }


        $output = ['data' => $data, 'message' => "your order has been updated successfully "];
        return response()->json($output, Response::HTTP_OK);
    }

    public function stripePayment(Request $request)
    {
        $requestData = $request->all();

        $validator = Validator::make($requestData, [
            'user_id' => 'required',
            'total_amount_with_fee' => 'required',
            'delivery_fees' => 'required',
            'payment' => 'required',
            'payment_status' => 'required',
            'delivery_address' => 'required',
            'order_details' => 'required|array',
            'order_details.*.price' => 'required|numeric',
            'order_details.*.product_id' => 'required|numeric',
            'order_details.*.product_name' => 'required',
            'order_details.*.quantity' => 'required'
        ]);

        $user = User::firstOrCreate(
            [
                "email" => $requestData['user_data']['email']
            ],
            [
                "password" => Hash::make(Str::random(12)),
                "name" =>  $requestData['user_data']['name'],
                "phone_number" => $requestData['user_data']['number']
            ]
        );

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        try {

            $chargedAmount = $requestData['total_amount_with_fee'] - $requestData['discounted_amount'];

            $payment = $user->charge(
                $chargedAmount*100,
                $requestData['payment_method_id']
            );

            $payment = $payment->asStripePaymentIntent();

            $data = $this->_repository->placeOrder($requestData);

        } catch (IncompletePayment $e) {

            if ($e instanceof PaymentActionRequired) {

                $code = 401;
                $output = ['error' => ['code' => 402, 'payment_data' => $e->payment]];
                return response()->json($output, $code);

            } else {

                $code = 401;
                $output = ['error' => ['code' => 401, 'message' => $e->getMessage()]];
                return response()->json($output, $code);
            }

        }

        if ($data) {

            $requestData['user_id'] = $data['user_id'];
            $requestData['phone_number'] = $data['phone_number'];
            $requestData['total_amount'] = $requestData['total_amount_with_fee'];

            $this->sendNotification($data);
        }

        $output = ['data' => $requestData, 'message' => "your order has been placed successfully "];
        return response()->json($output, Response::HTTP_OK);
    }

    public function cardStreamPayment(Request $request) {

        $requestData = $request->all();

        $validator = Validator::make($requestData, [
            'user_id' => 'required',
            'total_amount_with_fee' => 'required',
            'delivery_fees' => 'required',
            'payment' => 'required',
            'delivery_address' => 'required',
            'order_details' => 'required|array',
            'order_details.*.price' => 'required|numeric',
            'order_details.*.product_id' => 'required|numeric',
            'order_details.*.product_name' => 'required',
            'order_details.*.quantity' => 'required'
        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        $req = array(
            //'merchantID' => 100856,
            'merchantID' => 133016,
            'action' => 'SALE',
            'type' => 1,
            'currencyCode' => 826,
            'countryCode' => 826,
            'amount' => $requestData['total_amount_with_fee']*100,
            'cardNumber' => $requestData['user_data']['cardNumber'], //'4012001037141112',
            'cardExpiryMonth' => $requestData['user_data']['monthSelection'],
            'cardExpiryYear' => substr($requestData['user_data']['yearSelection'], -2),
            'cardCVV' => $requestData['user_data']['cvv'], //'083',
            'customerName' => $requestData['user_data']['name'],
            'customerEmail' => $requestData['user_data']['email'],
            'customerAddress' => $requestData['user_data']['address'],
            'customerPostCode' => $requestData['user_data']['postal_code'], //'TE15 5ST',
            'orderRef' => "Aisha Cafe Demo Orders",
            // The following fields are only mandatory for 3DS v2 direct integration
            'remoteAddress' => $_SERVER['REMOTE_ADDR'],
            'threeDSRedirectURL' => 'http://papag-live.test/stream-check-out'
        );

        $return = Gateway::directRequest($req);

        if($return['responseCode'] === 65802) {
            $code = 401;
            $output = ['error' => ['code' => 402, 'payment_data' => $return]];
            return response()->json($output, $code);
        }

    }

    /** Send Push Notification */
    public function sendNotification($data)
    {
        $devices = UserDevices::get()->pluck('device_token')->toArray();

        $push = new PushNotification('fcm');

        $push->setMessage([
            'data' => [
                'title' => 'This is the title',
                'body' => 'New order has been placed to your restaurant',
                'sound' => 'default',
                'order_id' => $data->id,
                'total_amount'=> $data->total_amount_with_fee,
                'reference'=>$data->reference,
                'order_type'=> $data->order_type,
                'payment'=>$data->payment,
                'is_pos' => $data->is_pos,
                'device_type' => $data->device_type
            ]
        ])->setDevicesToken($devices)->send();

        $response = $push->getFeedback();
    }

    public function stripeCharge($data)
    {
        //$stripe = Stripe::make(env('STRIPE_SECRET'));

        $stripe = Stripe::make(env('STRIPE_KEY'), '2020-08-27');

        try {
            $token = $stripe->tokens()->create([
                'card' => [
                    'number' => $data['card_no'],
                    'exp_month' => $data['ccExpiryMonth'],
                    'exp_year' => $data['ccExpiryYear'],
                    'cvc' => $data['cvvNumber'],
                ],
            ]);

            if (!isset($token['id'])) {
                return redirect()->route('addmoney.paymentstripe');
            }
            $charge = $stripe->charges()->create([
                'card' => $token['id'],
                'currency' => 'GBP',
                'amount' => $data['total_amount_with_fee'],
                'description' => 'wallet',
            ]);

            if ($charge['status'] == 'succeeded') {
                return  ["status" => $charge['status'], "data" => $charge];
                //           return redirect()->route('addmoney.paymentstripe');
            } else {
                \Session::put('error', 'Money not add in wallet!!');

                $code = "401";
                $output = ['code' => $code, 'message' => "Money not add in wallet!!"];
                return $output;


//                return redirect()->route('addmoney.paymentstripe');
            }
        } catch (Exception $e) {
            \Session::put('error', $e->getMessage());
//            return redirect()->route('addmoney.paymentstripe');

            $code = 401;
            $output = ['status' => $code, 'message' => $e->getMessage()];
            return $output;


        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
            \Session::put('error', $e->getMessage());

            $code = 401;
            $output = ['status' => $code, 'message' => $e->getMessage()];
            return $output;

            //return redirect()->route('addmoney.paywithstripe');
        } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {

            $code = 401;
            $output = ['status' => $code, 'message' => $e->getMessage()];
            return $output;

            \Session::put('error', $e->getMessage());
//            return redirect()->route('addmoney.paymentstripe');
        }

    }

    public function paypalPayment($requestData)
    {
        $paypal = new PayPal;

        $card = new CreditCard(array(
            'firstName' => $requestData['user_data']['name'],
            'lastName' =>  $requestData['user_data']['name'],
            'number'                => $requestData['user_data']['card_no'],
            'expiryMonth'           => $requestData['user_data']['expiration_month'],
            'expiryYear'            => $requestData['user_data']['expiration_year'],
            'cvv'                   => $requestData['user_data']['cvc'],
            /* 'billingAddress1'       => '1 Scrubby Creek Road',
             'billingCountry'        => 'AU',
             'billingCity'           => 'Scrubby Creek',
             'billingPostcode'       => '4999',
             'billingState'          => 'QLD',*/
        ));

        $response = $paypal->purchase([
            'amount' => $paypal->formatAmount($requestData['total_amount_with_fee']),
            'currency' => 'USD',
            'card'     => $card,
            'description'   => 'This is a test purchase transaction.',
        ]);

        return $response;
    }

    public function getTotalSales(Request $request)
    {
        $requestData = $request->all();

        $validator =  Validator::make($requestData, [
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        $data = $this->_repository->getTotalSales($requestData);

        $output = ['data' => ['total_sales' => $data, 'most_sale_item' => "Margherita Pizza", 'tota_orders' => 15], 'message' => "your order has been placed successfully "];
        return response()->json($output, Response::HTTP_OK);
    }

    public function getOrderDetails($id){
        $data = $this->_repository->findById($id);
        $output = [
            'data' =>$data,
            'pagination' => !empty($data['pagination']) ? $data['pagination'] : false,
            'message' => "Orders Retrieved Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }

    public function getKitchenOrders(Request $request)
    {
        $requestData = $request->all();
        $pagination = !empty($requestData['pagination']) ? $requestData['pagination'] : false;
        $per_page = self::PER_PAGE;

        $data = $this->_repository->getKitchenOrders($pagination, $per_page, $requestData);

        $output = [
            'data' => $data,
            'pagination' => !empty($data['pagination']) ? $data['pagination'] : false,
            'message' => "Orders Retrieved Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }

    public function cardStreamCallback(Request $request)
    {
        $requestData = $request->all();

        $cardData = CardStream::where('threeDSr', $requestData['MD'])->first();

        if($cardData) {

            $dd = json_decode($cardData->request_data);

            $req = array(
                'merchantID' => $dd->merchantID,
                'action' => 'SALE',
                'type' => 1,
                'currencyCode' => 826,
                'countryCode' => 826,
                'amount' => $dd->amount,
                'cardNumber' => $dd->cardNumber,
                'cardExpiryMonth' => $dd->cardExpiryMonth,
                'cardExpiryYear' => $dd->cardExpiryYear,
                'cardCVV' => $dd->cardCVV,
                'customerName' => $dd->customerName,
                'customerEmail' => $dd->customerEmail,
                'customerAddress' => $dd->customerAddress,
                'customerPostCode' => $dd->customerPostCode,
                'orderRef' => $dd->orderRef,
                'threeDSRef' => $requestData['MD'],
                'threeDSResponse' => $requestData,
            );

            $return = Gateway::directRequest($req);

            if($return['responseCode'] === 65802) {
                $code = 401;
                $output = ['error' => ['code' => 402, 'payment_data' => $return]];
                return response()->json($output, $code);
            }
        } else {

            $output = ['error' => ['code' => 401, 'message' => "Something went wrong"]];
            return response()->json($output, 401);
        }
    }

    public function updateProductStatus(Request $request)
    {
        $requestData = $request->all();

        $validator = Validator::make($requestData, [
            'order_id' => 'required',
            'product_id' => 'required',

        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }
        $data = $this->_repository->updateProductStatus($requestData);
        $output = ['data' => $data, 'message' => "your order has been placed successfully "];
        return response()->json($output, Response::HTTP_OK);
    }

    public function getKioskPrint(){
        $orders = Orders::where(['is_kiosk_print'=>1])->orderBy('id','Desc')->first();
        if(isset($orders->id)) {
            $restaurant = Restaurant::first();
            $restaurantAddress = RestaurantAddress::first();
            $orderData = array();
            $orderData['restaurant_name'] = $restaurant->name;
            $orderData['restaurant_phone_number'] = $restaurant->phone_number;
            $orderData['restaurant_city'] = $restaurantAddress->city;
            $orderData['restaurant_zip_code'] = $restaurantAddress->zip_code;
            $orderData['restaurant_address'] = $restaurantAddress->address;
            $orderData['order_id'] = $orders->id;
            $orderData['total_amount'] = $orders->total_amount_with_fee;
            $orderData['order_type'] = $orders->order_type;
            $orderData['payment_status'] = $orders->payment_status;
            $orderProducts = array();
            foreach ($orders->details as $value) {
                $extra = json_decode($value->extras);
                $orderProducts[] = array(
                    'product_name' => $value->product_name,
                    'price' => $value->price,
                    'quantity' => $value->quantity,
                    'extras' => $extra,
                );
            }
            $orderData['detail'] = $orderProducts;
            $orders = Orders::where(['id'=>$orders->id])->update(['is_kiosk_print'=>0]);
            return response()->json($orderData, Response::HTTP_OK);
        } else {
            $output = ['error' => ['code' => 401, 'message' => "No Order Found"]];
            return response()->json($output, 401);
        }
    }

    public function updatePaymentStatus(Request $request){
        $requestData = $request->all();
        $validator = Validator::make($requestData, [
            'order_id' => 'required',
            'payment_status' => 'required',

        ]);
        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }
        $orderData = Orders::where(['id' => $requestData['order_id']])->update(['payment_status'=>$requestData['payment_status']]);

        return response()->json($orderData, Response::HTTP_OK);
    }


    public function getOrderDesktopPos(){
        $data = $this->_repository->getOrderDesktopPos();
        $output = ['data' => $data, 'message' => "Retrieved"];
        return response()->json($output, Response::HTTP_OK);
    }

    public function orderPostedInPos(Request $request){

        $requestData = $request->all();
        $validator = Validator::make($requestData, [
            'order_ids' => 'required',

        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        $orderIds = explode(',',$requestData['order_ids']);
        $orderData = Orders::whereIn('id',$orderIds)->update(['posted_to_desktop_pos'=>1]);
        $output = ['data' => $orderData, 'message' => "Retrieved"];
        return response()->json($output, Response::HTTP_OK);
    }

    public function orderUpdateFromPos(Request $request){

        $requestData = $request->all();

        $validator = Validator::make($requestData, [
            'order_ids' => 'required',
            'status'  => 'required',
        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }
        $orderIds = explode(',',$requestData['order_ids']);
        $orderData = Orders::whereIn('id',$orderIds)->update(['posted_to_desktop_pos'=>$requestData['status']]);
        $output = ['data' => $orderData, 'message' => "Retrieved"];
        return response()->json($output, Response::HTTP_OK);
    }






}
