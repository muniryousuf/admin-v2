<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHoldCartsAPIRequest;
use App\Http\Requests\API\UpdateHoldCartsAPIRequest;
use App\Models\HoldCarts;
use App\Repositories\HoldCartsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\HoldCartsResource;
use Response;

/**
 * Class HoldCartsController
 * @package App\Http\Controllers\API
 */

class HoldCartsAPIController extends AppBaseController
{
    /** @var  HoldCartsRepository */
    private $holdCartsRepository;

    public function __construct(HoldCartsRepository $holdCartsRepo)
    {
        $this->holdCartsRepository = $holdCartsRepo;
    }

    /**
     * Display a listing of the HoldCarts.
     * GET|HEAD /holdCarts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $holdCarts = $this->holdCartsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            HoldCartsResource::collection($holdCarts),
            __('messages.retrieved', ['model' => __('models/holdCarts.plural')])
        );
    }

    /**
     * Store a newly created HoldCarts in storage.
     * POST /holdCarts
     *
     * @param CreateHoldCartsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHoldCartsAPIRequest $request)
    {
        $input = $request->all();
        
        $holdCarts = $this->holdCartsRepository->create($input);

        return $this->sendResponse(
            new HoldCartsResource($holdCarts),
            __('messages.saved', ['model' => __('models/holdCarts.singular')])
        );
    }

    /**
     * Display the specified HoldCarts.
     * GET|HEAD /holdCarts/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var HoldCarts $holdCarts */
        $holdCarts = $this->holdCartsRepository->find($id);

        if (empty($holdCarts)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/holdCarts.singular')])
            );
        }

        return $this->sendResponse(
            new HoldCartsResource($holdCarts),
            __('messages.retrieved', ['model' => __('models/holdCarts.singular')])
        );
    }

    /**
     * Update the specified HoldCarts in storage.
     * PUT/PATCH /holdCarts/{id}
     *
     * @param int $id
     * @param UpdateHoldCartsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHoldCartsAPIRequest $request)
    {
        $input = $request->all();
        

        /** @var HoldCarts $holdCarts */
        $holdCarts = $this->holdCartsRepository->find($id);

        if (empty($holdCarts)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/holdCarts.singular')])
            );
        }

        $holdCarts = $this->holdCartsRepository->update($input, $id);

        return $this->sendResponse(
            new HoldCartsResource($holdCarts),
            __('messages.updated', ['model' => __('models/holdCarts.singular')])
        );
    }

    /**
     * Remove the specified HoldCarts from storage.
     * DELETE /holdCarts/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var HoldCarts $holdCarts */
        $holdCarts = $this->holdCartsRepository->find($id);

        if (empty($holdCarts)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/holdCarts.singular')])
            );
        }

        $holdCarts->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/holdCarts.singular')])
        );
    }
}
