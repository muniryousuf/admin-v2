<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateReviewsAPIRequest;
use App\Http\Requests\API\UpdateReviewsAPIRequest;
use App\Models\Reviews;
use App\Repositories\ReviewsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\ReviewsResource;
use Response;

/**
 * Class ReviewsController
 * @package App\Http\Controllers\API
 */

class ReviewsAPIController extends AppBaseController
{
    /** @var  ReviewsRepository */
    private $reviewsRepository;

    public function __construct(ReviewsRepository $reviewsRepo)
    {
        $this->reviewsRepository = $reviewsRepo;
    }

    /**
     * Display a listing of the Reviews.
     * GET|HEAD /reviews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $reviews = $this->reviewsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            ReviewsResource::collection($reviews),
            __('messages.retrieved', ['model' => __('models/reviews.plural')])
        );
    }

    /**
     * Store a newly created Reviews in storage.
     * POST /reviews
     *
     * @param CreateReviewsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateReviewsAPIRequest $request)
    {
        $input = $request->all();

        $reviews = $this->reviewsRepository->create($input);

        return $this->sendResponse(
            new ReviewsResource($reviews),
            __('messages.saved', ['model' => __('models/reviews.singular')])
        );
    }

    /**
     * Display the specified Reviews.
     * GET|HEAD /reviews/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Reviews $reviews */
        $reviews = $this->reviewsRepository->find($id);

        if (empty($reviews)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/reviews.singular')])
            );
        }

        return $this->sendResponse(
            new ReviewsResource($reviews),
            __('messages.retrieved', ['model' => __('models/reviews.singular')])
        );
    }

    /**
     * Update the specified Reviews in storage.
     * PUT/PATCH /reviews/{id}
     *
     * @param int $id
     * @param UpdateReviewsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReviewsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Reviews $reviews */
        $reviews = $this->reviewsRepository->find($id);

        if (empty($reviews)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/reviews.singular')])
            );
        }

        $reviews = $this->reviewsRepository->update($input, $id);

        return $this->sendResponse(
            new ReviewsResource($reviews),
            __('messages.updated', ['model' => __('models/reviews.singular')])
        );
    }

    /**
     * Remove the specified Reviews from storage.
     * DELETE /reviews/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Reviews $reviews */
        $reviews = $this->reviewsRepository->find($id);

        if (empty($reviews)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/reviews.singular')])
            );
        }

        $reviews->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/reviews.singular')])
        );
    }
}
