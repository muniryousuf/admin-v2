<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateNotificationsAPIRequest;
use App\Http\Requests\API\UpdateNotificationsAPIRequest;
use App\Models\Notifications;
use App\Repositories\NotificationsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\NotificationsResource;
use Response;

/**
 * Class NotificationsController
 * @package App\Http\Controllers\API
 */

class NotificationsAPIController extends AppBaseController
{
    /** @var  NotificationsRepository */
    private $notificationsRepository;

    public function __construct(NotificationsRepository $notificationsRepo)
    {
        $this->notificationsRepository = $notificationsRepo;
    }

    /**
     * Display a listing of the Notifications.
     * GET|HEAD /notifications
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $notifications = $this->notificationsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            NotificationsResource::collection($notifications),
            __('messages.retrieved', ['model' => __('models/notifications.plural')])
        );
    }

    /**
     * Store a newly created Notifications in storage.
     * POST /notifications
     *
     * @param CreateNotificationsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateNotificationsAPIRequest $request)
    {
        $input = $request->all();

        $notifications = $this->notificationsRepository->create($input);

        return $this->sendResponse(
            new NotificationsResource($notifications),
            __('messages.saved', ['model' => __('models/notifications.singular')])
        );
    }

    /**
     * Display the specified Notifications.
     * GET|HEAD /notifications/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Notifications $notifications */
        $notifications = $this->notificationsRepository->find($id);

        if (empty($notifications)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/notifications.singular')])
            );
        }

        return $this->sendResponse(
            new NotificationsResource($notifications),
            __('messages.retrieved', ['model' => __('models/notifications.singular')])
        );
    }

    /**
     * Update the specified Notifications in storage.
     * PUT/PATCH /notifications/{id}
     *
     * @param int $id
     * @param UpdateNotificationsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNotificationsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Notifications $notifications */
        $notifications = $this->notificationsRepository->find($id);

        if (empty($notifications)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/notifications.singular')])
            );
        }

        $notifications = $this->notificationsRepository->update($input, $id);

        return $this->sendResponse(
            new NotificationsResource($notifications),
            __('messages.updated', ['model' => __('models/notifications.singular')])
        );
    }

    /**
     * Remove the specified Notifications from storage.
     * DELETE /notifications/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Notifications $notifications */
        $notifications = $this->notificationsRepository->find($id);

        if (empty($notifications)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/notifications.singular')])
            );
        }

        $notifications->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/notifications.singular')])
        );
    }
}
