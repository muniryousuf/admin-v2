<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatecategoryAPIRequest;
use App\Http\Requests\API\UpdatecategoryAPIRequest;
use App\Models\category;
use App\Repositories\categoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\categoryResource;
use Response;
use App\Repositories\ProductsRepository;
use App\Models\Products;

/**
 * Class categoryController
 * @package App\Http\Controllers\API
 */

class categoryAPIController extends AppBaseController
{
    /** @var  categoryRepository */
    private $categoryRepository;

    public function __construct(categoryRepository $categoryRepo)
    {
        $this->categoryRepository = $categoryRepo;
    }

    public function getAllCategories()
    {
        $response_array['categories'] = [];
        $response_array['products'] = [];
        $response_array['category_wise_products'] = [];
        $products = [];
        $categories = category::where('status',1)->OrderBy('sort','ASC')->get();
        foreach($categories as $category){
            $products = Products::where('id_category',$category->id)->without(['product_metas','product_metas_mobile'])->with('category')->where('status',1)->get()->toArray();
            foreach($products as $key => $product){
                $products[$key]['sort'] = $category->sort;
            }
            $response_array['products'] = array_merge($products, $response_array['products']);
        }
        $response_array['categories'] = $categories;

        $collection = collect($response_array['products']);

        // group by id_category


        $response_array['category_wise_products'] = $collection->sortBy(['sort','asc'])->groupBy('id_category');
        if (empty($categories)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/categories.singular')])
            );
        }
        unset($response_array['products']);
        return $this->sendResponse(
            $response_array,
            __('messages.retrieved', ['model' => __('models/categories.singular')])
        );
    }
    public function getAllCategoriesForWeb()
    {
        $response_array['categories'] = [];
        $response_array['products'] = [];
        $response_array['category_wise_products'] = [];
        $products = [];
        $categories = category::where('status',1)->orderByRaw('sort IS NULL, sort asc')->get();
        foreach($categories as $category){
            $products = Products::where('id_category',$category->id)->without(['product_metas','product_metas_mobile'])->with('category')->where('status',1)->get()->toArray();
            foreach($products as $key => $product){
                $products[$key]['sort'] = $category->sort == null ? 0 : $category->sort;
            }
            $response_array['products'] = array_merge($products, $response_array['products']);
        }
        $response_array['categories'] = $categories;
        array_multisort(array_column($response_array['products'], 'sort'), SORT_ASC, $response_array['products']);


        $category_wise_products = [];
        foreach($categories as $cat){
            foreach($response_array['products'] as $product){
                if($product['id_category'] == $cat->id){
                    $category_wise_products[$cat->id.'-cat'][] = $product;
                }
            }
        }
        // dd($category_wise_products);
        $response_array['category_wise_products'] = $category_wise_products;

        unset($response_array['products']);
        if (empty($categories)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/categories.singular')])
            );
        }

        return $this->sendResponse(
            $response_array,
            __('messages.retrieved', ['model' => __('models/categories.singular')])
        );
    }
}
