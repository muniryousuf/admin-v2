<?php

namespace App\Http\Controllers\Api;

use App\Data\Models\RestaurantTiming;
use App\Data\Models\RestaurantTimingSpecific;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Validator;
use Carbon\Carbon;

class RestaurantTimingController extends Controller
{
    public function store(Request $request)
    {
        $requestData = $request->all();
        $validator = Validator::make($requestData, [
            "data" => 'required|array',
            "day.*" => 'required',
            'start_time.*' => 'required',
            'end_time.*' => 'required',
            'shop_close.*' => 'required',
        ]);


        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()]];
            return response()->json($output, $code);
        }
        RestaurantTiming::truncate();

        foreach ($requestData['data'] as $data) {

            /*if ($data['shop_close']) {
                $data['shop_close'] = 0;
            } else {
                $data['shop_close'] = 1;
            }*/

            RestaurantTiming::create($data);
        }
        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [RestaurantTiming::all()]
        ]);

    }

    public function index(Request $request)
    {
        $output = [
            'data' => RestaurantTiming::all(),
            'message' => "Restaurant Timing Retrieved Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }


    public function addSpecificDateTime(Request $request)
    {
        $requestData = $request->all();

        $validator = Validator::make($requestData, [
            "data" => 'required|array',
            'start_time.*' => 'required',
            'end_time.*' => 'required',
            'shop_close.*' => 'required',
            'date.*' => 'required'
        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()]];
            return response()->json($output, $code);
        }

        foreach ($requestData['data'] as $data) {
            if ($data['shop_close']) {
                $data['shop_closed'] = 0;
            } else {
                $data['shop_closed'] = 1;
            }
            RestaurantTimingSpecific::create($data);
        }
        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [RestaurantTimingSpecific::all()]
        ]);
    }

    public function getAllSpecificDateTime()
    {
        $output = [
            'data' => RestaurantTimingSpecific::all(),
            'message' => "Restaurant Timing Retrieved Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }

    public function destroy($id)
    {
        RestaurantTimingSpecific::find($id)->delete();

        return response()->json([
            'status' => true,
            'deleted' => true,
            'data' => []
        ]);
    }

    public function getTodayTiming()
    {
        $shop_closed = true;
        $today_timing = RestaurantTiming::where('day','like', date('l') )->first();
        $general_settings = getSettings();
        $shop_status = $general_settings['shop_status'] ?? 0;

        if($today_timing && $today_timing->shop_close == 1 && $shop_status == 1){
           if (Carbon::now()->between($today_timing->start_time, $today_timing->end_time)) {
                $shop_closed = false;
            }
        }
        $output = [
            'data' => ['shop_closed'=>$shop_closed],
            'message' => "Restaurant Timing Retrieved Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }
    

}
