<?php

namespace App\Http\Controllers\API;

use App\Models\Products;
use App\Models\ProductMetasBridge;
use App\Repositories\ProductsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\ProductMetaAttributes;
use App\Models\product_meta;

/**
 * Class ProductController
 * @package App\Http\Controllers\API
 */

class NewProductController extends AppBaseController
{
    /** @var  ProductsRepository */
    private $productsRepository;

    public function __construct(ProductsRepository $productsRepo)
    {
        $this->productsRepository = $productsRepo;
    }

    public function getproductDetails($id){
      $product = $this->productsRepository->getproductDetails($id);

        if (empty($product)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/products.singular')])
            );
        }

        return $this->sendResponse(
            $product,
            __('messages.retrieved', ['model' => __('models/products.singular')])
        );
    }
    public function getproductDetailsV2($id){
      $product = $this->productsRepository->getproductDetailsV2($id);

        if (empty($product)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/products.singular')])
            );
        }

        return $this->sendResponse(
            $product,
            __('messages.retrieved', ['model' => __('models/products.singular')])
        );
    }
    public function metaWithAttribute($id){
      $product = $this->productsRepository->metaWithAttribute($id);

        if (empty($product)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/products.singular')])
            );
        }

        return $this->sendResponse(
            $product,
            __('messages.retrieved', ['model' => __('models/products.singular')])
        );
    }
    public function getMetaHtml($product_meta,$meta_html = ''){

        $meta_html .= ' 
          <div id="missed-item" name="'.$product_meta['name'].'" class="sub-cat mt-3">
              <h4>'.$product_meta['name'].'</h4>
              <ul class="selectionlist radio-list">
              ';

        $attributes_data = $product_meta['attributes'];
        foreach( $attributes_data as $j => $attributes){
          $meta_html .= '
                <li><label><input type="radio" value="'.$attributes['value'].'">
                    '.$attributes['key'].'
                    <span class="checkmark"></span></label> <span style="float: right;"> '.$attributes['price'].'</span>
                </li>
            ';
        }
        $meta_html .= '</ul></div>';
        return $meta_html;
    }

    public function getAllMetas($product_meta,$arr){
      if(count($product_meta['product_meta']['attributes']) > 0){
        array_push($arr, $product_meta['product_meta']);
        foreach( $product_meta['product_meta']['attributes'] as $attributes){
            if($attributes['product_meta'] != null){
                $arr = $this->getAllMetas($attributes['product_meta'],$arr);
            }else{
              array_push($arr, $attributes);
            }
        }
      }
        return $arr;
    }

    public function getProductMetaHerarcy(Request $request){

      $product_meta = $request->all();
      return $this->getMetaHtml($product_meta);

    }
    public function getChildOfAttribute($id){
      return ProductMetaAttributes::find($id);
    }


    public function addProduct(Request $request){
     $product =  Products::create($request->all());
     $input = $request->all();
     if(isset($input['id_metas'])){
          foreach ($input['id_metas'] as $key => $id_meta) {
           $product_metas_bridge = new ProductMetasBridge();
           $product_metas_bridge->id_product = $product->id;
           $product_metas_bridge->sort = $key;
           $product_metas_bridge->id_meta = $id_meta;
           $product_metas_bridge->save();
          }

        }
    return $product;
    }

    public function updateProduct(Request $request, $id)
    {

        $input = $request->all();
        $product = Products::find($id)->update($input);
        
        if (isset($input['id_metas'])) {
            ProductMetasBridge::where('id_product', $id)->delete();
            foreach ($input['id_metas'] as $key => $id_meta) {
                $product_metas_bridge = new ProductMetasBridge();
                $product_metas_bridge->id_product = $input['id_product'];
                $product_metas_bridge->sort = $key;
                $product_metas_bridge->id_meta = $id_meta;
                $product_metas_bridge->save();
            }

        }
        \Artisan::call('cache:clear');
        return Products::find($id);
    }



    public function getAllGroups(){
      $product_meta = product_meta::without('attributes')->get();

        if (empty($product_meta)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/products.singular')])
            );
        }

        return $this->sendResponse(
            $product_meta,
            __('messages.retrieved', ['model' => __('models/products.singular')])
        );
    }
    public function deleteProduct($id){
      Products::find($id)?->delete();
       return $this->sendResponse(
            [],
            __('messages.retrieved', ['model' => __('models/products.singular')])
        );
    }




}
