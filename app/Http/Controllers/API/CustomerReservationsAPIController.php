<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerReservationsAPIRequest;
use App\Http\Requests\API\UpdateCustomerReservationsAPIRequest;
use App\Models\CustomerReservations;
use App\Repositories\CustomerReservationsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\CustomerReservationsResource;
use Response;

/**
 * Class CustomerReservationsController
 * @package App\Http\Controllers\API
 */

class CustomerReservationsAPIController extends AppBaseController
{
    /** @var  CustomerReservationsRepository */
    private $customerReservationsRepository;

    public function __construct(CustomerReservationsRepository $customerReservationsRepo)
    {
        $this->customerReservationsRepository = $customerReservationsRepo;
    }

    /**
     * Display a listing of the CustomerReservations.
     * GET|HEAD /customerReservations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $customerReservations = $this->customerReservationsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            CustomerReservationsResource::collection($customerReservations),
            __('messages.retrieved', ['model' => __('models/customerReservations.plural')])
        );
    }

    /**
     * Store a newly created CustomerReservations in storage.
     * POST /customerReservations
     *
     * @param CreateCustomerReservationsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerReservationsAPIRequest $request)
    {
        $input = $request->all();

        $customerReservations = $this->customerReservationsRepository->create($input);

        return $this->sendResponse(
            new CustomerReservationsResource($customerReservations),
            __('messages.saved', ['model' => __('models/customerReservations.singular')])
        );
    }

    /**
     * Display the specified CustomerReservations.
     * GET|HEAD /customerReservations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CustomerReservations $customerReservations */
        $customerReservations = $this->customerReservationsRepository->find($id);

        if (empty($customerReservations)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/customerReservations.singular')])
            );
        }

        return $this->sendResponse(
            new CustomerReservationsResource($customerReservations),
            __('messages.retrieved', ['model' => __('models/customerReservations.singular')])
        );
    }

    /**
     * Update the specified CustomerReservations in storage.
     * PUT/PATCH /customerReservations/{id}
     *
     * @param int $id
     * @param UpdateCustomerReservationsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerReservationsAPIRequest $request)
    {
        $input = $request->all();

        /** @var CustomerReservations $customerReservations */
        $customerReservations = $this->customerReservationsRepository->find($id);

        if (empty($customerReservations)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/customerReservations.singular')])
            );
        }

        $customerReservations = $this->customerReservationsRepository->update($input, $id);

        return $this->sendResponse(
            new CustomerReservationsResource($customerReservations),
            __('messages.updated', ['model' => __('models/customerReservations.singular')])
        );
    }

    /**
     * Remove the specified CustomerReservations from storage.
     * DELETE /customerReservations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CustomerReservations $customerReservations */
        $customerReservations = $this->customerReservationsRepository->find($id);

        if (empty($customerReservations)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/customerReservations.singular')])
            );
        }

        $customerReservations->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/customerReservations.singular')])
        );
    }
}
