<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTableCategoriesAPIRequest;
use App\Http\Requests\API\UpdateTableCategoriesAPIRequest;
use App\Models\TableCategories;
use App\Repositories\TableCategoriesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\TableCategoriesResource;
use Response;

/**
 * Class TableCategoriesController
 * @package App\Http\Controllers\API
 */

class TableCategoriesAPIController extends AppBaseController
{
    /** @var  TableCategoriesRepository */
    private $tableCategoriesRepository;

    public function __construct(TableCategoriesRepository $tableCategoriesRepo)
    {
        $this->tableCategoriesRepository = $tableCategoriesRepo;
    }

    /**
     * Display a listing of the TableCategories.
     * GET|HEAD /tableCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tableCategories = $this->tableCategoriesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            TableCategoriesResource::collection($tableCategories),
            __('messages.retrieved', ['model' => __('models/tableCategories.plural')])
        );
    }

    /**
     * Store a newly created TableCategories in storage.
     * POST /tableCategories
     *
     * @param CreateTableCategoriesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTableCategoriesAPIRequest $request)
    {
        $input = $request->all();

        $tableCategories = $this->tableCategoriesRepository->create($input);

        return $this->sendResponse(
            new TableCategoriesResource($tableCategories),
            __('messages.saved', ['model' => __('models/tableCategories.singular')])
        );
    }

    /**
     * Display the specified TableCategories.
     * GET|HEAD /tableCategories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TableCategories $tableCategories */
        $tableCategories = $this->tableCategoriesRepository->find($id);

        if (empty($tableCategories)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tableCategories.singular')])
            );
        }

        return $this->sendResponse(
            new TableCategoriesResource($tableCategories),
            __('messages.retrieved', ['model' => __('models/tableCategories.singular')])
        );
    }

    /**
     * Update the specified TableCategories in storage.
     * PUT/PATCH /tableCategories/{id}
     *
     * @param int $id
     * @param UpdateTableCategoriesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTableCategoriesAPIRequest $request)
    {
        $input = $request->all();

        /** @var TableCategories $tableCategories */
        $tableCategories = $this->tableCategoriesRepository->find($id);

        if (empty($tableCategories)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tableCategories.singular')])
            );
        }

        $tableCategories = $this->tableCategoriesRepository->update($input, $id);

        return $this->sendResponse(
            new TableCategoriesResource($tableCategories),
            __('messages.updated', ['model' => __('models/tableCategories.singular')])
        );
    }

    /**
     * Remove the specified TableCategories from storage.
     * DELETE /tableCategories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TableCategories $tableCategories */
        $tableCategories = $this->tableCategoriesRepository->find($id);

        if (empty($tableCategories)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tableCategories.singular')])
            );
        }

        $tableCategories->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/tableCategories.singular')])
        );
    }
}
