<?php

namespace App\Http\Controllers\Api;

use App\Data\Repositories\RestaurantRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class RestaurantController extends Controller
{
    protected $_repository;

    public function __construct(RestaurantRepository $repository)
    {
        $this->_repository = $repository;
    }

    public function getRestaurantInfo(Request $request)
    {
        $data = $this->_repository->findByAll();
        $output = [
            'data' => $data,
            'message' => "Restaurant Information Retrieved Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }

    public function updateRestaurantInfo(Request $request)
    {
        $requestData = $request->all();

        $data = $this->_repository->updateRecord($requestData);

        $output = ['data' => $data, 'message' => "your restaurant has been updated successfully"];
        return response()->json($output, Response::HTTP_OK);
    }

    public function getPostalCode(Request $request){
        $requestData = $request->all();
        $validator =  Validator::make($requestData, [
            'postal_code' => 'required',
        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        $results = DB::table('pos_postal_code')
            ->limit(50)
            ->where('postal_code', 'LIKE', '%' . $requestData['postal_code'] . '%')
            ->get();

        $output = ['data' => $results, 'message' => "Success"];
        return response()->json($output, Response::HTTP_OK);

    }

}
