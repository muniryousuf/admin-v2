<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVouchersAPIRequest;
use App\Http\Requests\API\UpdateVouchersAPIRequest;
use App\Models\Vouchers;
use App\Repositories\VouchersRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\VouchersResource;
use Response;

/**
 * Class VouchersController
 * @package App\Http\Controllers\API
 */

class VouchersAPIController extends AppBaseController
{
    /** @var  VouchersRepository */
    private $vouchersRepository;

    public function __construct(VouchersRepository $vouchersRepo)
    {
        $this->vouchersRepository = $vouchersRepo;
    }

    /**
     * Display a listing of the Vouchers.
     * GET|HEAD /vouchers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $vouchers = $this->vouchersRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            VouchersResource::collection($vouchers),
            __('messages.retrieved', ['model' => __('models/vouchers.plural')])
        );
    }

    /**
     * Store a newly created Vouchers in storage.
     * POST /vouchers
     *
     * @param CreateVouchersAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVouchersAPIRequest $request)
    {
        $input = $request->all();

        $vouchers = $this->vouchersRepository->create($input);

        return $this->sendResponse(
            new VouchersResource($vouchers),
            __('messages.saved', ['model' => __('models/vouchers.singular')])
        );
    }

    /**
     * Display the specified Vouchers.
     * GET|HEAD /vouchers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Vouchers $vouchers */
        $vouchers = $this->vouchersRepository->find($id);

        if (empty($vouchers)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/vouchers.singular')])
            );
        }

        return $this->sendResponse(
            new VouchersResource($vouchers),
            __('messages.retrieved', ['model' => __('models/vouchers.singular')])
        );
    }

    /**
     * Update the specified Vouchers in storage.
     * PUT/PATCH /vouchers/{id}
     *
     * @param int $id
     * @param UpdateVouchersAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVouchersAPIRequest $request)
    {
        $input = $request->all();

        /** @var Vouchers $vouchers */
        $vouchers = $this->vouchersRepository->find($id);

        if (empty($vouchers)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/vouchers.singular')])
            );
        }

        $vouchers = $this->vouchersRepository->update($input, $id);

        return $this->sendResponse(
            new VouchersResource($vouchers),
            __('messages.updated', ['model' => __('models/vouchers.singular')])
        );
    }

    /**
     * Remove the specified Vouchers from storage.
     * DELETE /vouchers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Vouchers $vouchers */
        $vouchers = $this->vouchersRepository->find($id);

        if (empty($vouchers)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/vouchers.singular')])
            );
        }

        $vouchers->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/vouchers.singular')])
        );
    }
}
