<?php

namespace App\Http\Controllers\Api;

use App\Data\Repositories\DriverRepository;
use App\Data\Repositories\OrderRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Symfony\Component\HttpFoundation\Response;

class DriverController  extends Controller
{
    protected $_repository;
    const PER_PAGE = 10;

    public function __construct(DriverRepository $repository, OrderRepository $orderRepository)
    {
        $this->_repository = $repository;
        $this->_orderRepository = $orderRepository;
    }

    public function index(Request $request)
    {
        $requestData = $request->all();
        $pagination = !empty($requestData['pagination']) ? $requestData['pagination'] : false;
        $per_page = self::PER_PAGE;
        $data = $this->_repository->findByAll($pagination,$per_page,$requestData);
        $output = [
            'data' => $data,
            'pagination' => !empty($data['pagination']) ? $data['pagination'] : false,
            'message' => "Assigned Orders To Driver",
        ];
        return response()->json($output, Response::HTTP_OK);
    }

    public function assignOrder(Request $request)
    {
        $requestData = $request->all();

        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
            'order_id' => 'required|array'
        ]);

        if ($validator->fails()) {
            $output = ['error' => ['code' => 401, 'message' => $validator->errors()->first()]];
            return response()->json($output, 401);
        }

        $data = $this->_repository->assignOrderToDriver($requestData);
        $output = [
            'data' => $data,
            'pagination' => !empty($data['pagination']) ? $data['pagination'] : false,
            'message' => "Drivers Retrieved Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }

    public function unassignOrder(Request $request)
    {
        $requestData = $request->all();

        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
            'order_id' => 'required|array'
        ]);

        if ($validator->fails()) {
            $output = ['error' => ['code' => 401, 'message' => $validator->errors()->first()]];
            return response()->json($output, 401);
        }

        $data = $this->_repository->unassignOrderFromDriver($requestData);
        $output = [
            'data' => $data,
            'pagination' => !empty($data['pagination']) ? $data['pagination'] : false,
            'message' => "Unassigned Orders From Driver Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }

    public function collectAmount(Request $request)
    {
        $requestData = $request->all();

        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
            'order_id' => 'required|array'
        ]);

        if ($validator->fails()) {
            $output = ['error' => ['code' => 401, 'message' => $validator->errors()->first()]];
            return response()->json($output, 401);
        }

        $data = $this->_repository->collectAmountFromDriver($requestData);
        $output = [
            'data' => $data,
            'pagination' => !empty($data['pagination']) ? $data['pagination'] : false,
            'message' => "Amount collected From Driver Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }

    public function getOrders(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
        ]);

        if ($validator->fails()) {
            $output = ['error' => ['code' => 401, 'message' => $validator->errors()->first()]];
            return response()->json($output, 401);
        }

        $requestData = $request->all();

        $pagination = !empty($requestData['pagination']) ? $requestData['pagination'] : false;
        $per_page = self::PER_PAGE;
        $data = $this->_orderRepository->findByAll($pagination,$per_page,$requestData);
        $output = [
            'data' => $data['data'],
            'total_orders' => $data['total_orders'],
            'total_amount' => round($data['total_amount'], 2),
            'total_unpaid' => round($data['total_unpaid'], 2),
            'pagination' => !empty($data['pagination']) ? $data['pagination'] : false,
            'message' => "Drivers Retrieved Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }

    public function getDriverOrders(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
        ]);

        if ($validator->fails()) {
            $output = ['error' => ['code' => 401, 'message' => $validator->errors()->first()]];
            return response()->json($output, 401);
        }

        $requestData = $request->all();

        $pagination = !empty($requestData['pagination']) ? $requestData['pagination'] : false;
        $per_page = self::PER_PAGE;
        $data = $this->_repository->getDriverOrders($pagination,$per_page,$requestData);
        $output = [
            'data' => $data['driver_return'],
            'total_orders' => $data['total_orders'],
            'total_amount' => round($data['total_amount'], 2),
            'total_unpaid' => round($data['total_unpaid'], 2),
            'pagination' => !empty($data['pagination']) ? $data['pagination'] : false,
            'message' => "Drivers Retrieved Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }

}
