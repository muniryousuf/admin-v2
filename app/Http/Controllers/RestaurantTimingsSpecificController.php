<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRestaurantTimingsSpecificRequest;
use App\Http\Requests\UpdateRestaurantTimingsSpecificRequest;
use App\Repositories\RestaurantTimingsSpecificRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class RestaurantTimingsSpecificController extends AppBaseController
{
    /** @var  RestaurantTimingsSpecificRepository */
    private $restaurantTimingsSpecificRepository;

    public function __construct(RestaurantTimingsSpecificRepository $restaurantTimingsSpecificRepo)
    {
        $this->restaurantTimingsSpecificRepository = $restaurantTimingsSpecificRepo;
    }

    /**
     * Display a listing of the RestaurantTimingsSpecific.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurantTimingsSpecifics = $this->restaurantTimingsSpecificRepository->all();

        return view('restaurant_timings_specifics.index')
            ->with('restaurantTimingsSpecifics', $restaurantTimingsSpecifics);
    }

    /**
     * Show the form for creating a new RestaurantTimingsSpecific.
     *
     * @return Response
     */
    public function create()
    {
        return view('restaurant_timings_specifics.create');
    }

    /**
     * Store a newly created RestaurantTimingsSpecific in storage.
     *
     * @param CreateRestaurantTimingsSpecificRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantTimingsSpecificRequest $request)
    {
        $input = $request->all();

        $restaurantTimingsSpecific = $this->restaurantTimingsSpecificRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/restaurantTimingsSpecifics.singular')]));

        return redirect(route('restaurantTimingsSpecifics.index'));
    }

    /**
     * Display the specified RestaurantTimingsSpecific.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $restaurantTimingsSpecific = $this->restaurantTimingsSpecificRepository->find($id);

        if (empty($restaurantTimingsSpecific)) {
            Flash::error(__('messages.not_found', ['model' => __('models/restaurantTimingsSpecifics.singular')]));

            return redirect(route('restaurantTimingsSpecifics.index'));
        }

        return view('restaurant_timings_specifics.show')->with('restaurantTimingsSpecific', $restaurantTimingsSpecific);
    }

    /**
     * Show the form for editing the specified RestaurantTimingsSpecific.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $restaurantTimingsSpecific = $this->restaurantTimingsSpecificRepository->find($id);

        if (empty($restaurantTimingsSpecific)) {
            Flash::error(__('messages.not_found', ['model' => __('models/restaurantTimingsSpecifics.singular')]));

            return redirect(route('restaurantTimingsSpecifics.index'));
        }

        return view('restaurant_timings_specifics.edit')->with('restaurantTimingsSpecific', $restaurantTimingsSpecific);
    }

    /**
     * Update the specified RestaurantTimingsSpecific in storage.
     *
     * @param int $id
     * @param UpdateRestaurantTimingsSpecificRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantTimingsSpecificRequest $request)
    {
        $restaurantTimingsSpecific = $this->restaurantTimingsSpecificRepository->find($id);

        if (empty($restaurantTimingsSpecific)) {
            Flash::error(__('messages.not_found', ['model' => __('models/restaurantTimingsSpecifics.singular')]));

            return redirect(route('restaurantTimingsSpecifics.index'));
        }

        $restaurantTimingsSpecific = $this->restaurantTimingsSpecificRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/restaurantTimingsSpecifics.singular')]));

        return redirect(route('restaurantTimingsSpecifics.index'));
    }

    /**
     * Remove the specified RestaurantTimingsSpecific from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $restaurantTimingsSpecific = $this->restaurantTimingsSpecificRepository->find($id);

        if (empty($restaurantTimingsSpecific)) {
            Flash::error(__('messages.not_found', ['model' => __('models/restaurantTimingsSpecifics.singular')]));

            return redirect(route('restaurantTimingsSpecifics.index'));
        }

        $this->restaurantTimingsSpecificRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/restaurantTimingsSpecifics.singular')]));

        return redirect(route('restaurantTimingsSpecifics.index'));
    }
}
