<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createproduct_metaRequest;
use App\Http\Requests\Updateproduct_metaRequest;
use App\Repositories\product_metaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\ProductMetaAttributes;
use App\Models\product_meta;

class product_metaController extends AppBaseController
{
    /** @var  product_metaRepository */
    private $productMetaRepository;

    public function __construct(product_metaRepository $productMetaRepo)
    {
        $this->productMetaRepository = $productMetaRepo;
    }

    /**
     * Display a listing of the product_meta.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $search = '';
        $productMetas = $this->productMetaRepository->model()::where('id_parent',0);
        if(isset($request->search)){
            $search = $request->search;
            $productMetas = $productMetas->where('name','like','%'.$search.'%');
        }
        $productMetas = $productMetas->paginate(5)->appends(request()->query());
        return view('product_metas.index')->with('productMetas', $productMetas)->with('search',$search);
    }


    public function searchForGroups(Request $request)
    {
        $new_attributes = [];
        if(isset($request->search)){
            $product_meta = product_meta::find($request->search);
            $product_meta_new = product_meta::create($product_meta->replicate()->toArray());
            if($product_meta){
                $attributes = $product_meta->attributes;
                foreach($attributes as $attribute){
                  $new_attribute =  ProductMetaAttributes::create($attribute->replicate()->fill(['product_meta_id'=>$product_meta_new->id])->toArray());
                //   $new_attributes[] = $new_attribute->id;
                }
            }
            return route('productMetas.edit', [$product_meta_new->id]);
        }


    }

    /**
     * Show the form for creating a new product_meta.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        return view('product_metas.create');
    }
    public function addChildProductMeta($id)
    {
        return view('product_metas.create',['id'=>$id]);
    }

    /**
     * Store a newly created product_meta in storage.
     *
     * @param Createproduct_metaRequest $request
     *
     * @return Response
     */
    public function store(Createproduct_metaRequest $request)
    {
        $input = $request->all();
        if(isset($request->image)){
            $image = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'), $image);
            $input['image'] = $image;
        }
        $productMeta = $this->productMetaRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/productMetas.singular')]));
        return redirect(route("productMetas.edit", [$productMeta->id]));
    }
    public function createGroup(Createproduct_metaRequest $request)
    {
        $input = $request->all();
        if(isset($request->image)){
            $image = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'), $image);
            $input['image'] = $image;
        }
        $productMeta = $this->productMetaRepository->create($input);
        return $productMeta;
    }

    /**
     * Display the specified product_meta.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $productMeta = $this->productMetaRepository->find($id);

        if (empty($productMeta)) {
            Flash::error(__('messages.not_found', ['model' => __('models/productMetas.singular')]));

            return redirect(route('productMetas.index'));
        }

        return view('product_metas.show')->with('productMeta', $productMeta);
    }

    /**
     * Show the form for editing the specified product_meta.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $productMeta = $this->productMetaRepository->model()::with('childs')->find($id);
        $metas = product_meta::where('id','!=',$id)->get();
        if (empty($productMeta)) {
            Flash::error(__('messages.not_found', ['model' => __('models/productMetas.singular')]));

            return redirect(route('productMetas.index'));
        }

        return view('product_metas.edit')->with('productMeta', $productMeta)->with('metas',$metas);
    }

    /**
     * Update the specified product_meta in storage.
     *
     * @param int $id
     * @param Updateproduct_metaRequest $request
     *
     * @return Response
     */
    public function update($id, Updateproduct_metaRequest $request)
    {
        $productMeta = $this->productMetaRepository->find($id);

        if (empty($productMeta)) {
            Flash::error(__('messages.not_found', ['model' => __('models/productMetas.singular')]));

            return redirect(route('productMetas.index'));
        }
        $input = $request->all();
        $update_params = [
            'name' => $request->name ?? 'N/A',
            'label' => $request->label ?? 'N/A',
            'table_type' => $request->table_type ?? 'N/A',
            'is_required' => $request->is_required ?? '0',
            'free_limit' => $request->free_limit ?? '0',
            'limit' => $request->limit ?? null,
            'multiple_quantity_allowed' => $request->multiple_quantity_allowed ?? '0',
        ];

        $productMeta = $this->productMetaRepository->update($update_params, $id);
        \Artisan::call('cache:clear');
        Flash::success(__('messages.updated', ['model' => __('models/productMetas.singular')]));

        return redirect()->back();
    }
    public function updateGroup(Request $request,$id)
    {
        $productMeta = $this->productMetaRepository->find($id);
        if (empty($productMeta)) {
             return $this->sendError(
                __('messages.not_found', ['model' => __('models/products.singular')])
            );
        }
        $input = $request->all();
        product_meta::find($id)->update($input);
        \Artisan::call('cache:clear');
        return product_meta::find($id);
    }

    /**
     * Remove the specified product_meta from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $productMeta = $this->productMetaRepository->find($id);

        if (empty($productMeta)) {
            Flash::error(__('messages.not_found', ['model' => __('models/productMetas.singular')]));

            return redirect(route('productMetas.index'));
        }

        $this->productMetaRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/productMetas.singular')]));

       return redirect()->back();
    }

    public function addAttribute(Request $request){
        $fields = $request->all();
        unset($fields['_token']);
        if(isset($request->image)){
            $image = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'), $image);
            $fields['image'] = $image;
        }
        return $this->productMetaRepository->addAttribute($fields);
    }
    public function fetchAttributes(Request $request){
        $fields = $request->all();
        return $this->productMetaRepository->fetchAttributes($fields);
    }
    public function fetchAttributesV2(Request $request){
        $fields = $request->all();
        $attributes =  $this->productMetaRepository->fetchAttributesV2($fields);
        if (empty($attributes)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/products.singular')])
            );
        }

        return $this->sendResponse(
            $attributes,
            __('messages.retrieved', ['model' => __('models/products.singular')])
        );
    }

    public function deleteProductMetaAttribute(Request $request, $id){
        ProductMetaAttributes::find($id)->delete();
        return $this->sendResponse(
            [],
            __('messages.deleted', ['model' => __('models/products.singular')])
        );
    }
    public function updateAttributeNew(Request $request, $id)
    {
        $product_meta_attribute = ProductMetaAttributes::find($id);
        if (isset($request->child_meta_id)) {
            $product_meta_attribute->child_product_meta_id = $request->child_meta_id;

        } else {
            $product_meta_attribute->preselected = $request->selected_val ? 1 : 0;
        }
        $product_meta_attribute->save();
        return $product_meta_attribute;
    }
    public function updateAttributeForAdmin(Request $request)
    {

        $product_meta_attribute = ProductMetaAttributes::find($request->id_attribute);
        if (isset($request->child_meta_id)) {
            $product_meta_attribute->child_product_meta_id = $request->child_meta_id;
        } else {

            $product_meta_attribute->preselected = $request->selected_val == "true"? 1 : 0;
        }
        if(isset($request->key_value) && $request->key_value != ''){
            $product_meta_attribute->key = $request->key_value;
            $product_meta_attribute->value = $request->key_value;
        }
        $product_meta_attribute->save();
        return $product_meta_attribute;
    }


    public function updateAttribute(Request $request, $id)
    {

        $product_meta_attribute = ProductMetaAttributes::find($id);
        if($product_meta_attribute){
            $product_meta_attribute->price = $request->price;
            $product_meta_attribute->value = $request->value;
            $product_meta_attribute->key = $request->key;
            // $product_meta_attribute->product_meta_id = $request->product_meta_id;
            $product_meta_attribute->preselected = $request->preselected;
            $product_meta_attribute->save();
            $product_meta_attribute = ProductMetaAttributes::find($id);
        }

        return $product_meta_attribute;
    }




    public function deleteGroup($id){
      product_meta::find($id)->delete();
      return $this->sendResponse(
            [],
            __('messages.deleted', ['model' => __('models/products.singular')])
        );
    }

    public function recreateGroupsAttributes(Request $request){
        foreach($request->new_data as $attr){
            $attr = json_decode($attr,true);
            unset($attr['child_meta']);
            $ProductMetaAttributes = new ProductMetaAttributes();
            foreach($attr as $key => $value){
                if($key  != 'id'){
                    $ProductMetaAttributes->{$key} = $value;
                }
            }
            $ProductMetaAttributes->save();
            ProductMetaAttributes::find($attr['id'])->delete();

        }
    }

}
