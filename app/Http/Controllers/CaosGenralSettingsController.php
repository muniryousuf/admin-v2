<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCaosGenralSettingsRequest;
use App\Http\Requests\UpdateCaosGenralSettingsRequest;
use App\Repositories\CaosGenralSettingsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CaosGenralSettingsController extends AppBaseController
{
    /** @var  CaosGenralSettingsRepository */
    private $caosGenralSettingsRepository;

    public function __construct(CaosGenralSettingsRepository $caosGenralSettingsRepo)
    {
        $this->caosGenralSettingsRepository = $caosGenralSettingsRepo;
    }

    /**
     * Display a listing of the CaosGenralSettings.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $caosGenralSettings = $this->caosGenralSettingsRepository->all();

        return view('caos_genral_settings.index')
            ->with('caosGenralSettings', $caosGenralSettings);
    }

    /**
     * Show the form for creating a new CaosGenralSettings.
     *
     * @return Response
     */
    public function create()
    {
        return view('caos_genral_settings.create');
    }

    /**
     * Store a newly created CaosGenralSettings in storage.
     *
     * @param CreateCaosGenralSettingsRequest $request
     *
     * @return Response
     */
    public function store(CreateCaosGenralSettingsRequest $request)
    {
        $input = $request->all();

        $caosGenralSettings = $this->caosGenralSettingsRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/caosGenralSettings.singular')]));

        return redirect(route('caosGenralSettings.index'));
    }

    /**
     * Display the specified CaosGenralSettings.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $caosGenralSettings = $this->caosGenralSettingsRepository->find($id);

        if (empty($caosGenralSettings)) {
            Flash::error(__('messages.not_found', ['model' => __('models/caosGenralSettings.singular')]));

            return redirect(route('caosGenralSettings.index'));
        }

        return view('caos_genral_settings.show')->with('caosGenralSettings', $caosGenralSettings);
    }

    /**
     * Show the form for editing the specified CaosGenralSettings.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $caosGenralSettings = $this->caosGenralSettingsRepository->find($id);

        if (empty($caosGenralSettings)) {
            Flash::error(__('messages.not_found', ['model' => __('models/caosGenralSettings.singular')]));

            return redirect(route('caosGenralSettings.index'));
        }

        return view('caos_genral_settings.edit')->with('caosGenralSettings', $caosGenralSettings);
    }

    /**
     * Update the specified CaosGenralSettings in storage.
     *
     * @param int $id
     * @param UpdateCaosGenralSettingsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCaosGenralSettingsRequest $request)
    {
        $caosGenralSettings = $this->caosGenralSettingsRepository->find($id);

        if (empty($caosGenralSettings)) {
            Flash::error(__('messages.not_found', ['model' => __('models/caosGenralSettings.singular')]));

            return redirect(route('caosGenralSettings.index'));
        }

        $caosGenralSettings = $this->caosGenralSettingsRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/caosGenralSettings.singular')]));

        return redirect(route('caosGenralSettings.index'));
    }

    /**
     * Remove the specified CaosGenralSettings from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $caosGenralSettings = $this->caosGenralSettingsRepository->find($id);

        if (empty($caosGenralSettings)) {
            Flash::error(__('messages.not_found', ['model' => __('models/caosGenralSettings.singular')]));

            return redirect(route('caosGenralSettings.index'));
        }

        $this->caosGenralSettingsRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/caosGenralSettings.singular')]));

        return redirect(route('caosGenralSettings.index'));
    }
}
