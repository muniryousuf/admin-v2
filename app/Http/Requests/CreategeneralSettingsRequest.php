<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\generalSettings;

class CreategeneralSettingsRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'site_name' => 'required',
            'site_title' => 'required',
            'tag_line' => 'required',
            'copyright_text' => 'required',
            'header_logo' => 'required',
            'fav_icon' => 'required',
            'footer_logo' => 'required',
            'site_name'  => 'required',
            'site_title'  => 'required',
            'tag_line'  => 'required',
            'copyright_text'  => 'required',
            // 'facebook'  => 'required',
            // 'instagram'  => 'required',
            // 'youtube'  => 'required',
            // 'pinterest'  => 'required',
            // 'twitter'  => 'required',
            // 'stripe_publishable_key'  => 'required',
            // 'stripe_secret_key'  => 'required',
            
        ];
    }
}
