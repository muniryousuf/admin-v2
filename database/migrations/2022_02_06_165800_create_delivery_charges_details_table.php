<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryChargesDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_charges_details', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('delivery_id');
            $table->double('miles');
            $table->string('postal_code', 255);
            $table->double('amount');
            $table->double('fix_delivery_charges');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('delivery_charges_details');
    }
}
