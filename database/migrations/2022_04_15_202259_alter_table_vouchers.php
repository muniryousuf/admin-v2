<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableVouchers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('vouchers', function (Blueprint $table) {
            $table->integer('user_limit');
            $table->integer('auto_coupon');
            $table->integer('for_first_order');
            $table->integer('for_all_orders');
            $table->integer('with_delivery');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
