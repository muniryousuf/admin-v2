<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->id('id');
            $table->integer('id_cart');
            $table->integer('user_id')->nullable();
            $table->string('delivery_fees',255)->nullable();
            $table->enum('payment', ['cash', 'creditcard','unpaid'])->nullable();
            $table->string('delivery_address',255)->nullable();
            $table->enum('order_type',[
                'collection',
                'delivery',
                'eatin',
                'drive_through',
                'takeaway'
            ])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cart');
    }
}
