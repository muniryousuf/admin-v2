<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableProductMetaAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_meta_attributes', function (Blueprint $table) {
            $table->id();
            $table->integer('product_meta_id');
            $table->string('key', 255)->nullable();
            $table->string('value', 255)->nullable();
            $table->string('image', 255)->nullable();
            $table->string('price', 255)->nullable();
            $table->integer('child_product_meta_id')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_meta_attributes');
    }
}
