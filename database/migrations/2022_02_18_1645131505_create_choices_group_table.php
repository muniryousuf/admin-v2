<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChoicesGroupTable extends Migration
{
    public function up()
    {
        Schema::create('choices_group', function (Blueprint $table) {

		$table->id('id');
		$table->string('name');
		$table->string('type');
		$table->timestamps();
        $table->softDeletes();
		$table->string('display_type')->default('Single');

        });
    }

    public function down()
    {
        Schema::dropIfExists('choices_group');
    }
}