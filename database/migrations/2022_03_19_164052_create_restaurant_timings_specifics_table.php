<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantTimingsSpecificsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_timing_specific', function (Blueprint $table) {
            $table->id('id');
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->integer('shop_closed')->default(0);
            $table->datetime('specific_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('restaurant_timing_specific');
    }
}
