<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCategoryTable extends Migration
{
    public function up()
    {
        Schema::create('table_category', function (Blueprint $table) {

		$table->id();
		$table->string('name')->nullable();
		$table->integer('status')->default(1);
		$table->timestamps();

        });
    }

    public function down()
    {
        Schema::dropIfExists('table_category');
    }
}