<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeTableOrderDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail', function (Blueprint $table) {
            $table->id('id');
            $table->integer('order_id');
            $table->integer('product_id');
            $table->string('product_name',255);
            $table->float('price');
            $table->integer('quantity');
            $table->longText('extras');
            $table->string('special_instructions',255);
            $table->enum('product_status', ['ToBePrepared', 'Preparing','Prepared']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_detail');
    }
}
