<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentTypeTable extends Migration
{
    public function up()
    {
        Schema::create('payment_type', function (Blueprint $table) {

    		$table->id('id');
    		$table->string('order_id',11);
    		$table->enum('payment_type',['cash','creditcard']);
    		$table->float('amount');
    		$table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('payment_type');
    }
}