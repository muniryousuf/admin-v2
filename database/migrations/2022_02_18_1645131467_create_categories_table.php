<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {

		$table->id('id');
		$table->string('name');
		$table->string('description');
		$table->string('image')->nullable()->default(NULL);
		$table->integer('status')->default('1');
		$table->integer('sort')->nullable()->default(NULL);
        $table->timestamps();
        $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('categories');
    }
}