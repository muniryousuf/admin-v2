<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantAddressTable extends Migration
{
    public function up()
    {
        Schema::create('restaurant_address', function (Blueprint $table) {

		$table->id();
		$table->integer('id_restaurant')->nullable();
		$table->integer('id_country')->nullable();
		$table->string('city')->nullable();
		$table->string('zip_code')->nullable();
		$table->string('address')->nullable();
		$table->timestamps();
		

        });
    }

    public function down()
    {
        Schema::dropIfExists('restaurant_address');
    }
}