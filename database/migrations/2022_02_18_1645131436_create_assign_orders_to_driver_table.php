<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignOrdersToDriverTable extends Migration
{
    public function up()
    {
        Schema::create('assign_orders_to_driver', function (Blueprint $table) {

    		$table->id('id');
    		$table->integer('driver_id');
    		$table->integer('order_id');
    		$table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('assign_orders_to_driver');
    }
}