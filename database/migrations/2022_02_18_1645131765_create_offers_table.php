<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {

    		$table->id('id');
    		$table->integer('percentage');
    		$table->integer('orders_over');
    		$table->integer('status')->default('0');
    		$table->timestamp('valid_from')->useCurrent();
    		$table->timestamp('valid_to')->nullable();
    		$table->integer('used_once')->default('1');
    		$table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('offers');
    }
}