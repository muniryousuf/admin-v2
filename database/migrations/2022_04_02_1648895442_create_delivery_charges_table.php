<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryChargesTable extends Migration
{
    public function up()
    {
        Schema::create('delivery_charges', function (Blueprint $table) {

            $table->id();
            $table->string('delivery_types')->nullable();
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::dropIfExists('delivery_charges');
    }
}