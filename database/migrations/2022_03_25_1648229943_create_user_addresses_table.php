<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAddressesTable extends Migration
{
    public function up()
    {
        Schema::create('user_addresses', function (Blueprint $table) {

			$table->id();
			$table->integer('user_id');
			$table->string('address')->nullable();
			$table->string('street')->nullable();
			$table->string('town')->nullable();
			$table->string('postal_code')->nullable();
			$table->string('land_mark')->nullable();
			$table->integer('active');
			$table->timestamps();
			
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_addresses');
    }
}