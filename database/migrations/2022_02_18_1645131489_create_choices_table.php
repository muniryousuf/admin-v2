<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChoicesTable extends Migration
{
    public function up()
    {
        Schema::create('choices', function (Blueprint $table) {

		$table->id('id');
		$table->string('name');
		$table->string('price');
		$table->integer('preselect')->default('0');
		$table->integer('id_group');
		$table->timestamps();
        $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('choices');
    }
}