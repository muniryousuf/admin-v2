<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralSettingsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_settings', function (Blueprint $table) {
            $table->id('id');
            $table->string('site_name', 255)->nullable();
            $table->string('site_title', 255)->nullable();
            $table->longtext('tag_line')->nullable();
            $table->longtext('copyright_text')->nullable();
            $table->string('header_logo', 255)->nullable();
            $table->string('footer_logo', 255)->nullable();
            $table->double('service_charges')->nullable();
            $table->double('vat')->nullable();
            $table->string('currencySign', 255)->nullable();
            $table->integer('shop_status')->nullable();
            $table->string('printer_ip_1', 255)->nullable();
            $table->string('printer_ip_2', 255)->nullable();
            $table->string('printer_ip_3', 255)->nullable();
            $table->string('printer_ip_4', 255)->nullable();
            $table->string('printer_ip_5', 255)->nullable();
            $table->string('printer_ip', 255)->nullable();
            $table->string('stripe_publishable_key', 255)->nullable();
            $table->string('stripe_secret_key', 255)->nullable();
            $table->string('facebook', 255)->nullable();
            $table->string('twitter', 255)->nullable();
            $table->string('instagram', 255)->nullable();
            $table->string('pinterest', 255)->nullable();
            $table->string('youtube', 255)->nullable();
            $table->integer('min_collection_time')->nullable();
            $table->integer('min_delivery_time')->nullable();
            $table->integer('total_allowed_person')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('general_settings');
    }
}
