<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableReservationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_reservation', function (Blueprint $table) {
            $table->id('id');
            $table->string('name', 255);
            $table->integer('is_reserve')->default(0);
            $table->string('reservation_end_time', 255)->nullable();
            $table->string('reservation_start_time', 255)->nullable();
            $table->integer('id_category')->nullable();
            $table->enum('table_type',[
                'small_circle',
                'medium_circle',
                'large_circle',
                'small_square',
                'medium_square',
                'large_square'
            ]);
            $table->integer('number_of_person')->nullable();
            $table->integer('number_of_person_sitting')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('table_reservation');
    }
}
