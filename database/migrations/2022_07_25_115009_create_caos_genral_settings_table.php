<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaosGenralSettingsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caos_genral_settings', function (Blueprint $table) {
            $table->id('id');
            $table->text('main_printer')->nullable();
            $table->text('kitchen_printer')->nullable();
            $table->integer('kicthen_copies')->nullable();
            $table->text('shop_name_and_address')->nullable();
            $table->text('reciept_end_text')->nullable();
            $table->text('menu_url')->nullable();
            $table->boolean('enable_multi_payment')->nullable();
            $table->text('extra_info')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('caos_genral_settings');
    }
}
