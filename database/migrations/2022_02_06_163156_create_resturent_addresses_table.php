<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResturentAddressesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant', function (Blueprint $table) {
            $table->id('id');
            $table->string('name', 255);
            $table->string('email', 255)->nullable();
            $table->string('website_url', 255);
            $table->string('logo', 255)->nullable();
            $table->string('phone_number', 255);
            $table->integer('id_user')->nullable();
            $table->integer('is_pickup')->default(0);
            $table->integer('is_delivery')->default(0);
            $table->integer('is_dine')->default(0);
            $table->string('admin_email', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('restaurant');
    }
}
