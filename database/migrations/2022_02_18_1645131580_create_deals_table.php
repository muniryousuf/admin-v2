<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealsTable extends Migration
{
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
    		$table->bigInteger('id');
    		$table->string('name');
    		$table->string('image')->nullable()->default(NULL);
    		$table->string('price')->nullable()->default(NULL);
    		$table->integer('status');
    		$table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('deals');
    }
}