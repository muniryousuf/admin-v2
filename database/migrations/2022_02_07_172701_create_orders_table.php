<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id('id');
            $table->string('reference', 255);
            $table->integer('user_id');
            $table->float('total_amount_with_fee');
            $table->string('delivery_fees',255);
            $table->enum('payment', ['cash', 'creditcard','unpaid']);
            $table->string('delivery_address',255);
            $table->enum('status', [
                'pending',
                'accepted',
                'declined',
                'preparing',
                'prepared',
                'driver_assigned',
                'picked_up',
                'delivered',
                'completed',
                'refund'
            ]);
            $table->string('transaction_id',255);
            $table->enum('payment_status', ['paid', 'unpaid']);
            $table->integer('table_id');
            $table->enum('order_type',[
                'collection',
                'delivery',
                'eatin',
                'drive_through',
                'takeaway'
            ]);
            $table->float('discounted_amount');
            $table->integer('is_pos');
            $table->integer('xy_report');
            $table->integer('cashier_id');
            $table->integer('number_of_person_sitting');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
