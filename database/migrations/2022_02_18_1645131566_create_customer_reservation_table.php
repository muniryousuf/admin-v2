<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerReservationTable extends Migration
{
    public function up()
    {
        Schema::create('customer_reservation', function (Blueprint $table) {

		$table->id('id');
		$table->string('firstname',50);
		$table->string('lastname',50);
		$table->string('phone',50);
		$table->string('email',50);
		$table->datetime('booking_date');
		$table->bigInteger('persons')->default('0');
		$table->time('booking_time')->default('00:00:00');
		$table->bigInteger('special_occasion')->default('0');
		$table->bigInteger('restaurant_newsletter')->nullable()->default(NULL);
		$table->bigInteger('opentable_newsletter')->nullable()->default(NULL);
		$table->bigInteger('reservations_reminder')->nullable()->default(NULL);
		$table->enum('status',['Requested','Accepted','Declined'])->default('Requested');
		$table->timestamps();
        $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('customer_reservation');
    }
}