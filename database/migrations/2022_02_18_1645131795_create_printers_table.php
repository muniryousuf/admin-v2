<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrintersTable extends Migration
{
    public function up()
    {
        Schema::create('printers', function (Blueprint $table) {

    		$table->id('id');
    		$table->string('name');
    		$table->string('ip');
    		$table->integer('is_default')->default('0');
    		$table->integer('status')->default('1');
    		$table->integer('print_count');
    		$table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('printers');
    }
}