<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {

    		$table->id('id');
    		$table->string('file');
    		$table->string('name')->nullable()->default(NULL);
    		$table->string('mime')->nullable()->default(NULL);
    		$table->bigInteger('size')->unsigned()->nullable()->default(NULL);
    		$table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('files');
    }
}