<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     *
     */
    public function up()
    {
        Schema::create('cms', function (Blueprint $table) {
            $table->id('id');
            $table->string('title', 255);
            $table->longtext('content');
            $table->string('slug', 255);
            $table->string('meta_title', 255);
            $table->string('meta_desc', 255);
            $table->string('index_follow', 255);
            $table->string('image_1',255)->nullable();
            $table->string('image_2',255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cms');
    }
}
