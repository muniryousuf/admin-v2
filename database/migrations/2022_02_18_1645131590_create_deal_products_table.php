<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealProductsTable extends Migration
{
    public function up()
    {
        Schema::create('deal_products', function (Blueprint $table) {

    		$table->id('id');
    		$table->integer('id_deal');
    		$table->integer('quantity');
    		$table->string('item_id')->nullable()->default(NULL);
    		$table->string('is_category')->nullable()->default(NULL);
    		$table->string('is_options')->nullable()->default(NULL);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('deal_products');
    }
}