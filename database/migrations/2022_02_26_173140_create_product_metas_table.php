<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductMetasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_metas', function (Blueprint $table) {
            $table->id('id');
            $table->string('name', 255);
            $table->string('label', 255)->nullable();
            $table->string('image', 255)->nullable();
            $table->string('price', 255)->nullable();
            $table->integer('id_parent')->default(0);
            $table->enum('table_type',[
                'radio',
                'medium_circle',
                'multiselect',
                'yes/no',
                'remove',
            ])->nullable();
            $table->tinyInteger('is_required')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_metas');
    }
}
