<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbackTable extends Migration
{
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {

    		$table->id('id');
    		$table->string('name',250)->nullable()->default(NULL);
    		$table->string('email',250)->nullable()->default(NULL);
    		$table->string('rating',250)->nullable()->default(NULL);
    		$table->string('experience')->nullable()->default(NULL);
    		$table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}