<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {

		$table->id('id');
		$table->bigInteger('file_id')->unsigned()->nullable()->default(NULL);
		$table->string('name')->nullable()->default(NULL);
		$table->string('city')->nullable()->default(NULL);
		$table->string('company')->nullable()->default(NULL);
		$table->integer('progress')->unsigned()->nullable()->default(NULL);
		$table->timestamps();
        $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('clients');
    }
}