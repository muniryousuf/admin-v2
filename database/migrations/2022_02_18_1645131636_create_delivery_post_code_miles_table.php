<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryPostCodeMilesTable extends Migration
{
    public function up()
    {
        Schema::create('delivery_post_code_miles', function (Blueprint $table) {

    		$table->id('id');
    		$table->string('post_code');
    		$table->string('miles');
    		$table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('delivery_post_code_miles');
    }
}