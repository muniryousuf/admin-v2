<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {

		$table->id();
		$table->string('name');
		$table->string('type');
		$table->integer('discount');
		$table->timestamp('expiry_date')->useCurrent();
		$table->integer('status')->default('0');
		$table->integer('coupon_limit')->default('0');
        $table->timestamps();
		
		

        });
    }

    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}