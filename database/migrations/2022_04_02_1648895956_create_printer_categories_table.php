<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrinterCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('printer_categories', function (Blueprint $table) {

		$table->integer('id_printer')->nullable();
		$table->integer('id_category')->nullable();

        });
    }

    public function down()
    {
        Schema::dropIfExists('printer_categories');
    }
}