<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use DB;


class WholeDbDump extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path('DB/database.sql');
        $sql = file_get_contents($path);
        \Artisan::call('config:clear');
        dump('configeration cleared');
        \Artisan::call('migrate:refresh');
        dump('migration refreshed');
        DB::unprepared($sql);
        dump('Done ;-)');

    }
}
