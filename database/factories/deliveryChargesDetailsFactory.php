<?php

namespace Database\Factories;

use App\Models\deliveryChargesDetails;
use Illuminate\Database\Eloquent\Factories\Factory;

class deliveryChargesDetailsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = deliveryChargesDetails::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'delivery_id' => $this->faker->word,
        'delivery_id' => $this->faker->word,
        'miles' => $this->faker->word,
        'postal_code' => $this->faker->word,
        'amount' => $this->faker->word,
        'fix_delivery_charges' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
