<?php

namespace Database\Factories;

use App\Models\TableReservation;
use Illuminate\Database\Eloquent\Factories\Factory;

class TableReservationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TableReservation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'is_reserve' => $this->faker->randomDigitNotNull,
        'reservation_end_time' => $this->faker->word,
        'reservation_start_time' => $this->faker->word,
        'id_category' => $this->faker->randomDigitNotNull,
        'table_type' => $this->faker->randomDigitNotNull,
        'number_of_person' => $this->faker->randomDigitNotNull,
        'number_of_person_sitting' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
