<?php

namespace Database\Factories;

use App\Models\general_settings;
use Illuminate\Database\Eloquent\Factories\Factory;

class general_settingsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = general_settings::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->randomDigitNotNull,
        'site_name' => $this->faker->word,
        'site_title' => $this->faker->word,
        'tag_line' => $this->faker->word,
        'copyright_text' => $this->faker->word,
        'header_logo' => $this->faker->word,
        'footer_logo' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'service_charges' => $this->faker->word,
        'vat' => $this->faker->word,
        'currencySign' => $this->faker->word,
        'shop_status' => $this->faker->randomDigitNotNull,
        'printer_ip_1' => $this->faker->word,
        'printer_ip_2' => $this->faker->word,
        'printer_ip_3' => $this->faker->word,
        'printer_ip_4' => $this->faker->word,
        'printer_ip_5' => $this->faker->word,
        'printer_ip' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
