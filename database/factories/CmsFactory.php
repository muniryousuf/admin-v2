<?php

namespace Database\Factories;

use App\Models\Cms;
use Illuminate\Database\Eloquent\Factories\Factory;

class CmsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cms::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word,
        'content' => $this->faker->word,
        'slug' => $this->faker->word,
        'meta_title' => $this->faker->word,
        'meta_desc' => $this->faker->word,
        'index_follow' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
