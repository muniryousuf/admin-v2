<?php

namespace Database\Factories;

use App\Models\Vouchers;
use Illuminate\Database\Eloquent\Factories\Factory;

class VouchersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Vouchers::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'type' => $this->faker->word,
        'expiry_date' => $this->faker->date('Y-m-d H:i:s'),
        'status' => $this->faker->randomDigitNotNull,
        'coupon_limit' => $this->faker->randomDigitNotNull,
        'user_limit' => $this->faker->randomDigitNotNull,
        'auto_coupon' => $this->faker->randomDigitNotNull,
        'for_first_order' => $this->faker->randomDigitNotNull,
        'for_all_orders' => $this->faker->randomDigitNotNull,
        'with_delivery' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
