<?php

namespace Database\Factories;

use App\Models\CaosGenralSettings;
use Illuminate\Database\Eloquent\Factories\Factory;

class CaosGenralSettingsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CaosGenralSettings::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'main_printer' => $this->faker->text,
        'kitchen_printer' => $this->faker->text,
        'kicthen_copies' => $this->faker->randomDigitNotNull,
        'shop_name_and_address' => $this->faker->text,
        'reciept_end_text' => $this->faker->text,
        'menu_url' => $this->faker->text,
        'enable_multi_payment' => $this->faker->word,
        'extra_info' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
