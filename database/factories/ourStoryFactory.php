<?php

namespace Database\Factories;

use App\Models\ourStory;
use Illuminate\Database\Eloquent\Factories\Factory;

class ourStoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ourStory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'main_title' => $this->faker->word,
        'all_title' => $this->faker->word,
        'description' => $this->faker->word,
        'image' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
