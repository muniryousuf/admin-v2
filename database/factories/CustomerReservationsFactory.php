<?php

namespace Database\Factories;

use App\Models\CustomerReservations;
use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerReservationsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CustomerReservations::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'firstname' => $this->faker->text,
        'lastname' => $this->faker->text,
        'phone' => $this->faker->text,
        'email' => $this->faker->text,
        'booking_date' => $this->faker->text,
        'persons' => $this->faker->randomDigitNotNull,
        'status' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
