<?php

namespace Database\Factories;

use App\Models\product_meta;
use Illuminate\Database\Eloquent\Factories\Factory;

class product_metaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = product_meta::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'value' => $this->faker->word,
        'id_parent' => $this->faker->randomDigitNotNull,
        'id_attribute' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
