<?php

namespace Database\Factories;

use App\Models\PaymentMethods;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentMethodsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PaymentMethods::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'private_key' => $this->faker->word,
        'public_key' => $this->faker->word,
        'another_key' => $this->faker->word,
        'active' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
