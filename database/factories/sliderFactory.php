<?php

namespace Database\Factories;

use App\Models\slider;
use Illuminate\Database\Eloquent\Factories\Factory;

class sliderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = slider::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'image' => $this->faker->word,
        'sort' => $this->faker->randomDigitNotNull,
        'url' => $this->faker->word,
        'gallery_id' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
