<?php

namespace Database\Factories;

use App\Models\ResturentAddress;
use Illuminate\Database\Eloquent\Factories\Factory;

class ResturentAddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ResturentAddress::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'email' => $this->faker->word,
        'website_url' => $this->faker->word,
        'logo' => $this->faker->word,
        'phone_number' => $this->faker->word,
        'id_user' => $this->faker->randomDigitNotNull,
        'is_pickup' => $this->faker->randomDigitNotNull,
        'is_delivery' => $this->faker->randomDigitNotNull,
        'is_dine' => $this->faker->randomDigitNotNull,
        'admin_email' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
