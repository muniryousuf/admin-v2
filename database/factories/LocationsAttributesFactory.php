<?php

namespace Database\Factories;

use App\Models\LocationsAttributes;
use Illuminate\Database\Eloquent\Factories\Factory;

class LocationsAttributesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LocationsAttributes::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'details' => $this->faker->text,
        'map' => $this->faker->word,
        'id_location' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
