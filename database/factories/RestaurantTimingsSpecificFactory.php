<?php

namespace Database\Factories;

use App\Models\RestaurantTimingsSpecific;
use Illuminate\Database\Eloquent\Factories\Factory;

class RestaurantTimingsSpecificFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RestaurantTimingsSpecific::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'start_time' => $this->faker->word,
        'end_time' => $this->faker->word,
        'shop_closed' => $this->faker->randomDigitNotNull,
        'specific_date' => $this->faker->date('Y-m-d H:i:s'),
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
